﻿# You can place the script of your game in this file.

# Declare images below this line, using the image statement.
# eg. image eileen happy = "eileen_happy.png"

#Backgrounds
image Wall = "images/Backgrounds/1A-Wall.jpg"
image WallDamaged = "images/Backgrounds/1B-WallDamaged.jpg"
image WallDestroyed = "images/Backgrounds/1C-WallDestroyed.jpg"
image TentDay = "images/Backgrounds/2A-TentDay.jpg"
image TentNight = "images/Backgrounds/2B-TentNight.jpg"
image ValeriusRoom = "images/Backgrounds/3A-ValeriusRoomDay.jpg"
image ValeriusRoomNight = "images/Backgrounds/3B-ValeriusRoomNight.jpg"
image HighHall = im.Scale("images/Backgrounds/4-HighHall.jpg", 1920, 1116)
image HallDay = im.Scale("images/Backgrounds/5A-HallDay.jpg", 1920, 1116)
image HallNight = "images/Backgrounds/5B-HallNight.jpg"
image HallBloody = "images/Backgrounds/5C-HallwayBloody.jpg"
image ClodiaRoomDay = "images/Backgrounds/6A-ClodiaRoomDay.jpg"
image ClodiaRoomNight = im.Scale("images/Backgrounds/6B-ClodiaRoomNight.jpg", 1920, 1116)
image CampDay = "images/Backgrounds/7A-CampDay.jpg"
image CampNight = "images/Backgrounds/7B-CampNight.jpg"
image Sewer = "images/Backgrounds/8-Sewer.jpg"
image StreetsDay = "images/Backgrounds/9A-StreetsDay.jpg"
image StreetsNight = "images/Backgrounds/9B-StreetsNight.jpg"
image TempleDay = "images/Backgrounds/10A-TempleDay.jpg"
image TempleEvening = "images/Backgrounds/10B-TempleEvening.jpg"
image PitofNightmares = "images/Backgrounds/11-PitofNightmares.jpg"
image Dungeons = "images/Backgrounds/12-Dungeon.jpg"
image Forest = "images/Backgrounds/13-Forest.jpg"
image Ship = "images/Backgrounds/14-Ship.jpg"
image Test ="images/Backgrounds/2KWallpaperTest.jpg"
image Ballistae = im.Scale("images/Backgrounds/Altered/Ballistae.jpg", 1920, 2575)

#CGs
image CG1A = im.Scale("images/CGs/CG1/cg1A.png", 1920, 1238)
image CG1B = im.Scale("images/CGs/CG1/cg1B.png", 2022, 1080)
image CG1C = im.Scale("images/CGs/CG1/cg1C.png", 1920, 780)
image CG1D = "images/CGs/CG1/cg1D.png"
image CG2A = im.Scale("images/CGs/CG2/cg2A.png", 1920, 1238)
image CG2B = "images/CGs/CG2/cg2B.png"
image CG2C = "images/CGs/CG2/cg2C.png"
image CG3A = im.Scale("images/CGs/CG3/cg3A.png", 1920, 1235)
image CG3B = im.Scale("images/CGs/CG3/cg3B.png", 1920, 2501)
image CG3C = im.Scale("images/CGs/CG3/cg3C.png", 1920, 2690)
image CG3D = "images/CGs/CG3/cg3D.png"
image CG3E = im.Scale("images/CGs/CG3/cg3E.png", 1920, 1851)
image CGX = im.Scale("images/CGs/CGX/cgx.png", 1920, 1238)
image CGT = im.Scale("images/CGs/CGT/CGT.png", 1920, 1235)
image CGTFlip = im.Scale("images/CGs/CGT/CGTFlip.png", 1920, 1235)
image CGTTest = im.Scale("images/CGs/CGT/CGTTest.png", 1920, 2985)
image CG4 = im.Scale("images/CGs/CG4/cg4.png", 1920, 1238)
image CG4f = im.Scale("images/CGs/CG4/cg4f.png", 1920, 2089)
image CG4B = im.Scale("images/CGs/CG4/cg4B.png", 1920, 1238)
image CG4C = im.Scale("images/CGs/CG4/cg4C.png", 1920, 1238)
image CG4D = im.Scale("images/CGs/CG4/cg4D.png", 1920, 1238)
image CG4E = im.Scale("images/CGs/CG4/cg4E.png", 1920, 1238)
image CG4Ea = im.Scale("images/CGs/CG4/CG4Ea.png", 1920, 2089)
image CG5 = im.Scale("images/CGs//CG5/CG5.png", 1920, 1235)
image CG6 = im.Scale("images/CGs/CG6/cg6.png", 1920, 1238)
image CG6b = im.Scale("images/CGs/CG6/cg6b.png", 1920, 1238)
image CG6c = im.Scale("images/CGs/CG6/cg6c.png", 1920, 1238)
image CG7 = im.Scale("images/CGs/CG7/cg7.png", 1920, 1237)
image CG8A = im.Scale("images/CGs/CG8/cg8a.png", 1920, 1235)
image CG8B = im.Scale("images/CGs/CG8/cg8b.png", 1920, 1235)
image CGX = im.Scale("images/CGs/CGX/cgx.png", 1920, 1238)

#Valerius
image val ac-annoyed = im.Scale("images/Valerius/val-ac-annoyed.png", 1481, 960)
image val ac-fact = im.Scale("images/Valerius/val-ac-fact.png", 1481, 960)
image val ac-grin = im.Scale("images/Valerius/val-ac-grin.png", 1481, 960)
image val ac-pleased = im.Scale("images/Valerius/val-ac-pleased.png", 1481, 960)
image val br-scanning = im.Scale("images/Valerius/val-br-scanning.png", 1481, 960)
image val br-shouting = im.Scale("images/Valerius/val-br-shouting.png", 1481, 960)
image val br-grin = im.Scale("images/Valerius/val-br-grin.png", 1481, 960)
image val br2-scanning = im.Scale("images/Valerius/val-br2-scanning.png", 1481, 960)
image val br2-shouting = im.Scale("images/Valerius/val-br2-shouting.png", 1481, 960)
image val br2-grin = im.Scale("images/Valerius/val-br2-grin.png", 1481, 960)
image val cmd-ordering = im.Scale("images/Valerius/val-cmd-ordering.png", 1481, 960)
image val cmd-pleasant = im.Scale("images/Valerius/val-cmd-pleasant.png", 1481, 960)
image val cmd-urgent = im.Scale("images/Valerius/val-cmd-urgent.png", 1481, 960)
image val njrd-angry = im.Scale("images/Valerius/val-njrd-angry.png", 1481, 960)
image val njrd-shocked = im.Scale("images/Valerius/val-nrjd-shocked.png", 1481, 960)
image val njrd-wincing = im.Scale("images/Valerius/val-njrd-wincing.png", 1481, 960)
image val rea-grin = im.Scale("images/Valerius/val-rea-grin.png", 1481, 960)
image val rea-scanning = im.Scale("images/Valerius/val-rea-scanning.png", 1481, 960)
image val rea-shouting = im.Scale("images/Valerius/val-rea-shouting.png", 1481, 960)
image val rea2-grin = im.Scale("images/Valerius/val-rea2-grin.png", 1481, 960)
image val rea2-scanning = im.Scale("images/Valerius/val-rea2-scanning.png", 1481, 960)
image val rea2-shouting = im.Scale("images/Valerius/val-rea2-shouting.png", 1481, 960)
image val rel-emotional = im.Scale("images/Valerius/val-rel-emotional.png", 1481, 960)
image val rel-grin = im.Scale("images/Valerius/val-rel-grin.png", 1481, 960)
image val rel-normal = im.Scale("images/Valerius/val-rel-normal.png", 1481, 960)
image val rel-serious = im.Scale("images/Valerius/val-rel-serious.png", 1481, 960)
image val rel-pleased = im.Scale("images/Valerius/val-rel-pleased.png", 1481, 960)

#Flipped Valerius
image val ac-annoyed-flip = im.Scale("images/Valerius/flipped/val-ac-annoyed.png", 1481, 960)
image val ac-fact-flip = im.Scale("images/Valerius/flipped/val-ac-fact.png", 1481, 960)
image val ac-grin-flip = im.Scale("images/Valerius/flipped/val-ac-grin.png", 1481, 960)
image val ac-pleased-flip = im.Scale("images/Valerius/flipped/val-ac-pleased.png", 1481, 960)
image val br-scanning-flip = im.Scale("images/Valerius/flipped/val-br-scanning.png", 1481, 960)
image val br-shouting-flip = im.Scale("images/Valerius/flipped/val-br-shouting.png", 1481, 960)
image val br-grin-flip = im.Scale("images/Valerius/flipped/val-br-grin.png", 1481, 960)
image val br2-scanning-flip = im.Scale("images/Valerius/flipped/val-br2-scanning.png", 1481, 960)
image val br2-shouting-flip = im.Scale("images/Valerius/flipped/val-br2-shouting.png", 1481, 960)
image val br2-grin-flip = im.Scale("images/Valerius/flipped/val-br2-grin.png", 1481, 960)
image val cmd-ordering-flip = im.Scale("images/Valerius/flipped/val-cmd-ordering.png", 1481, 960)
image val cmd-pleasant-flip = im.Scale("images/Valerius/flipped/val-cmd-pleasant.png", 1481, 960)
image val cmd-urgent-flip = im.Scale("images/Valerius/flipped/val-cmd-urgent.png", 1481, 960)
image val njrd-angry-flip = im.Scale("images/Valerius/flipped/val-njrd-angry.png", 1481, 960)
image val njrd-shocked-flip = im.Scale("images/Valerius/flipped/val-nrjd-shocked.png", 1481, 960)
image val njrd-wincing-flip = im.Scale("images/Valerius/flipped/val-njrd-wincing.png", 1481, 960)
image val rea-grin-flip = im.Scale("images/Valerius/flipped/val-rea-grin.png", 1481, 960)
image val rea-scanning-flip = im.Scale("images/Valerius/flipped/val-rea-scanning.png", 1481, 960)
image val rea-shouting-flip = im.Scale("images/Valerius/flipped/val-rea-shouting.png", 1481, 960)
image val rea2-grin-flip = im.Scale("images/Valerius/flipped/val-rea2-grin.png", 1481, 960)
image val rea2-scanning-flip = im.Scale("images/Valerius/flipped/val-rea2-scanning.png", 1481, 960)
image val rea2-shouting-flip = im.Scale("images/Valerius/flipped/val-rea2-shouting.png", 1481, 960)
image val rel-emotional-flip = im.Scale("images/Valerius/flipped/val-rel-emotional.png", 1481, 960)
image val rel-grin-flip = im.Scale("images/Valerius/flipped/val-rel-grin.png", 1481, 960)
image val rel-normal-flip = im.Scale("images/Valerius/flipped/val-rel-normal.png", 1481, 960)
image val rel-serious-flip = im.Scale("images/Valerius/flipped/val-rel-serious.png", 1481, 960)
image val rel-pleased-flip = im.Scale("images/Valerius/flipped/val-rel-pleased.png", 1481, 960)

#Clodia
image clo cha1-joy = im.Scale("images/Clodia/clo-cha1-joy.png", 820, 860)
image clo cha1-sass = im.Scale("images/Clodia/clo-cha1-sass.png", 820, 860)
image clo cha1-sexy = im.Scale("images/Clodia/clo-cha1-sexy.png", 820, 860)
image clo cha2-joy = im.Scale("images/Clodia/clo-cha2-joy.png", 820, 860)
image clo cha2-sass = im.Scale("images/Clodia/clo-cha2-sass.png", 820, 860)
image clo cha2-sexy = im.Scale("images/Clodia/clo-cha2-sexy.png", 820, 860)
image clo cha3-joy = im.Scale("images/Clodia/clo-cha3-joy.png", 820, 860)
image clo cha3-sass = im.Scale("images/Clodia/clo-cha3-sass.png", 820, 860)
image clo cha3-sexy = im.Scale("images/Clodia/clo-cha3-sexy.png", 820, 860)
image clo con-angered = im.Scale("images/Clodia/clo-con-angered.png", 820, 860)
image clo con-charming = im.Scale("images/Clodia/clo-con-charming.png", 820, 860)
image clo con-surprised = im.Scale("images/Clodia/clo-con-surprised.png", 820, 860)
image clo defen-crying = im.Scale("images/Clodia/clo-def-crying.png", 820, 860)
image clo defen-declaring = im.Scale("images/Clodia/clo-def-declaring.png", 820, 860)
image clo defen-wincing1 = im.Scale("images/Clodia/clo-def-wincing1.png", 820, 860)
image clo defen-wincing2 = im.Scale("images/Clodia/clo-def-wincing2.png", 820, 860)
image clo reg-declaring = im.Scale("images/Clodia/clo-reg-declaring.png", 820, 860)
image clo reg-emotional = im.Scale("images/Clodia/clo-reg-emotional.png", 820, 860)
image clo reg-stoic = im.Scale("images/Clodia/clo-reg-stoic.png", 820, 860)
image clo rel-angered = im.Scale("images/Clodia/clo-rel-angered.png", 820, 860)
image clo rel-charming = im.Scale("images/Clodia/clo-rel-charming.png", 820, 860)
image clo rel-neutral = im.Scale("images/Clodia/clo-rel-neutral.png", 820, 860)
image clo rel-surprised = im.Scale("images/Clodia/clo-rel-surprised.png", 820, 860)
image clo snk-relief = im.Scale("images/Clodia/clo-snk-relief.png", 820, 860)
image clo snk-serious = im.Scale("images/Clodia/clo-snk-serious.png", 820, 860)
image clo snk-worry = im.Scale("images/Clodia/clo-snk-worry.png", 820, 860)

#Flipped Clodia
image clo cha1-joy-flip = im.Scale("images/Clodia/flipped/clo-cha1-joy.png", 820, 860)
image clo cha1-sass-flip = im.Scale("images/Clodia/flipped/clo-cha1-sass.png", 820, 860)
image clo cha1-sexy-flip = im.Scale("images/Clodia/flipped/clo-cha1-sexy.png", 820, 860)
image clo cha2-joy-flip = im.Scale("images/Clodia/flipped/clo-cha2-joy.png", 820, 860)
image clo cha2-sass-flip = im.Scale("images/Clodia/flipped/clo-cha2-sass.png", 820, 860)
image clo cha2-sexy-flip = im.Scale("images/Clodia/flipped/clo-cha2-sexy.png", 820, 860)
image clo cha3-joy-flip = im.Scale("images/Clodia/flipped/clo-cha3-joy.png", 820, 860)
image clo cha3-sass-flip = im.Scale("images/Clodia/flipped/clo-cha3-sass.png", 820, 860)
image clo cha3-sexy-flip = im.Scale("images/Clodia/flipped/clo-cha3-sexy.png", 820, 860)
image clo con-angered-flip = im.Scale("images/Clodia/flipped/clo-con-angered.png", 820, 860)
image clo con-charming-flip = im.Scale("images/Clodia/flipped/clo-con-charming.png", 820, 860)
image clo con-surprised-flip = im.Scale("images/Clodia/flipped/clo-con-surprised.png", 820, 860)
image clo defen-crying-flip = im.Scale("images/Clodia/flipped/clo-def-crying.png", 820, 860)
image clo defen-declaring-flip = im.Scale("images/Clodia/flipped/clo-def-declaring.png", 820, 860)
image clo defen-wincing1-flip = im.Scale("images/Clodia/flipped/clo-def-wincing1.png", 820, 860)
image clo defen-wincing2-flip = im.Scale("images/Clodia/flipped/clo-def-wincing2.png", 820, 860)
image clo reg-declaring-flip = im.Scale("images/Clodia/flipped/clo-reg-declaring.png", 820, 860)
image clo reg-emotional-flip = im.Scale("images/Clodia/flipped/clo-reg-emotional.png", 820, 860)
image clo reg-stoic-flip = im.Scale("images/Clodia/flipped/clo-reg-stoic.png", 820, 860)
image clo rel-angered-flip = im.Scale("images/Clodia/flipped/clo-rel-angered.png", 820, 860)
image clo rel-charming-flip = im.Scale("images/Clodia/flipped/clo-rel-charming.png", 820, 860)
image clo rel-neutral-flip = im.Scale("images/Clodia/flipped/clo-rel-neutral.png", 820, 860)
image clo rel-surprised-flip = im.Scale("images/Clodia/flipped/clo-rel-surprised.png", 820, 860)
image clo snk-relief-flip = im.Scale("images/Clodia/flipped/clo-snk-relief.png", 820, 860)
image clo snk-serious-flip = im.Scale("images/Clodia/flipped/clo-snk-serious.png", 820, 860)
image clo snk-worry-flip = im.Scale("images/Clodia/flipped/clo-snk-worry.png", 820, 860)

#Alt Clodia
image clo2 cha1-joy = im.Scale("images/Clodia/Alt/alt-clo-cha1-joy.png", 820, 860)
image clo2 cha1-sass = im.Scale("images/Clodia/Alt/alt-clo-cha1-sass.png", 820, 860)
image clo2 cha1-sexy = im.Scale("images/Clodia/Alt/alt-clo-cha1-sexy.png", 820, 860)
image clo2 cha2-joy = im.Scale("images/Clodia/Alt/alt-clo-cha2-joy.png", 820, 860)
image clo2 cha2-sass = im.Scale("images/Clodia/Alt/alt-clo-cha2-sass.png", 820, 860)
image clo2 cha2-sexy = im.Scale("images/Clodia/Alt/alt-clo-cha2-sexy.png", 820, 860)
image clo2 cha3-joy = im.Scale("images/Clodia/Alt/alt-clo-cha3-joy.png", 820, 860)
image clo2 cha3-sass = im.Scale("images/Clodia/Alt/alt-clo-cha3-sass.png", 820, 860)
image clo2 cha3-sexy = im.Scale("images/Clodia/Alt/alt-clo-cha3-sexy.png", 820, 860)
image clo2 con-angered = im.Scale("images/Clodia/Alt/alt-clo-con-angered.png", 820, 860)
image clo2 con-charming = im.Scale("images/Clodia/Alt/alt-clo-con-charming.png", 820, 860)
image clo2 con-surprised = im.Scale("images/Clodia/Alt/alt-clo-con-surprised.png", 820, 860)
image clo2 defen-crying = im.Scale("images/Clodia/Alt/alt-clo-def-crying.png", 820, 860)
image clo2 defen-declaring = im.Scale("images/Clodia/Alt/alt-clo-def-declaring.png", 820, 860)
image clo2 defen-wincing1 = im.Scale("images/Clodia/Alt/alt-clo-def-wincing1.png", 820, 860)
image clo2 defen-wincing2 = im.Scale("images/Clodia/Alt/alt-clo-def-wincing2.png", 820, 860)
image clo2 reg-declaring = im.Scale("images/Clodia/Alt/alt-clo-reg-declaring.png", 820, 860)
image clo2 reg-emotional = im.Scale("images/Clodia/Alt/alt-clo-reg-emotional.png", 820, 860)
image clo2 reg-stoic = im.Scale("images/Clodia/Alt/alt-clo-reg-stoic.png", 820, 860)
image clo2 rel-angered = im.Scale("images/Clodia/Alt/alt-clo-rel-angered.png", 820, 860)
image clo2 rel-charming = im.Scale("images/Clodia/Alt/alt-clo-rel-charming.png", 820, 860)
image clo2 rel-neutral = im.Scale("images/Clodia/Alt/alt-clo-rel-neutral.png", 820, 860)
image clo2 rel-surprised = im.Scale("images/Clodia/Alt/alt-clo-rel-surprised.png", 820, 860)
image clo2 snk-relief = im.Scale("images/Clodia/Alt/alt-clo-snk-relief.png", 820, 860)
image clo2 snk-serious = im.Scale("images/Clodia/Alt/alt-clo-snk-serious.png", 820, 860)
image clo2 snk-worry = im.Scale("images/Clodia/Alt/alt-clo-snk-worry.png", 820, 860)

#Alt Clodia Flipped
image clo2 cha1-joy-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-cha1-joy.png", 820, 860)
image clo2 cha1-sass-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-cha1-sass.png", 820, 860)
image clo2 cha1-sexy-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-cha1-sexy.png", 820, 860)
image clo2 cha2-joy-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-cha2-joy.png", 820, 860)
image clo2 cha2-sass-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-cha2-sass.png", 820, 860)
image clo2 cha2-sexy-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-cha2-sexy.png", 820, 860)
image clo2 cha3-joy-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-cha3-joy.png", 820, 860)
image clo2 cha3-sass-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-cha3-sass.png", 820, 860)
image clo2 cha3-sexy-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-cha3-sexy.png", 820, 860)
image clo2 con-angered-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-con-angered.png", 820, 860)
image clo2 con-charming-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-con-charming.png", 820, 860)
image clo2 con-surprised-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-con-surprised.png", 820, 860)
image clo2 defen-crying-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-def-crying.png", 820, 860)
image clo2 defen-declaring-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-def-declaring.png", 820, 860)
image clo2 defen-wincing1-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-def-wincing1.png", 820, 860)
image clo2 defen-wincing2-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-def-wincing2.png", 820, 860)
image clo2 reg-declaring-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-reg-declaring.png", 820, 860)
image clo2 reg-emotional-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-reg-emotional.png", 820, 860)
image clo2 reg-stoic-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-reg-stoic.png", 820, 860)
image clo2 rel-angered-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-rel-angered.png", 820, 860)
image clo2 rel-charming-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-rel-charming.png", 820, 860)
image clo2 rel-neutral-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-rel-neutral.png", 820, 860)
image clo2 rel-surprised-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-rel-surprised.png", 820, 860)
image clo2 snk-relief-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-snk-relief.png", 820, 860)
image clo2 snk-serious-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-snk-serious.png", 820, 860)
image clo2 snk-worry-flip = im.Scale("images/Clodia/Alt/flipped/alt-clo-snk-worry.png", 820, 860)

#Sergius
image ser neutral-serious = im.Scale("images/Sergius/ser-neutral-serious.png", 1657, 1100)
image ser neutral-grin = im.Scale("images/Sergius/ser-neutral-grin.png", 1657, 1100)
image ser neutral-annoyed = im.Scale("images/Sergius/ser-neutral-annoyed.png", 1657, 1100)
image ser ac-annoyed = im.Scale("images/Sergius/ser-ac-annoyed.png", 1657, 1100)
image ser ac-grin = im.Scale("images/Sergius/ser-ac-grin.png", 1657, 1100)
image ser ac-insulting = im.Scale("images/Sergius/ser-ac-insulting.png", 1657, 1100)
image ser com1-barking = im.Scale("images/Sergius/ser-com1-barking.png", 1657, 1100)
image ser com1-boasting = im.Scale("images/Sergius/ser-com1-boasting.png", 1657, 1100)
image ser com1-rage = im.Scale("images/Sergius/ser-com1-rage.png", 1657, 1100)
image ser com2-barking = im.Scale("images/Sergius/ser-com2-barking.png", 1657, 1100)
image ser com2-boasting = im.Scale("images/Sergius/ser-com2-boasting.png", 1657, 1100)
image ser com2-rage = im.Scale("images/Sergius/ser-com2-rage.png", 1657, 1100)

#Flipped Sergius
image ser neutral-serious-flip = im.Scale("images/Sergius/flipped/ser-neutral-serious.png", 1657, 1100)
image ser neutral-grin-flip = im.Scale("images/Sergius/flipped/ser-neutral-grin.png", 1657, 1100)
image ser neutral-annoyed-flip = im.Scale("images/Sergius/flipped/ser-neutral-annoyed.png", 1657, 1100)
image ser ac-annoyed-flip = im.Scale("images/Sergius/flipped/ser-ac-annoyed.png", 1657, 1100)
image ser ac-grin-flip = im.Scale("images/Sergius/flipped/ser-ac-grin.png", 1657, 1100)
image ser ac-insulting-flip = im.Scale("images/Sergius/flipped/ser-ac-insulting.png", 1657, 1100)
image ser com1-barking-flip = im.Scale("images/Sergius/flipped/ser-com1-barking.png", 1657, 1100)
image ser com1-boasting-flip = im.Scale("images/Sergius/flipped/ser-com1-boasting.png", 1657, 1100)
image ser com1-rage-flip = im.Scale("images/Sergius/flipped/ser-com1-rage.png", 1657, 1100)
image ser com2-barking-flip = im.Scale("images/Sergius/flipped/ser-com2-barking.png", 1657, 1100)
image ser com2-boasting-flip = im.Scale("images/Sergius/flipped/ser-com2-boasting.png", 1657, 1100)
image ser com2-rage-flip = im.Scale("images/Sergius/flipped/ser-com2-rage.png", 1657, 1100)

#Sejanus
image sej neutral-normal = im.Scale("images/Sejanus/sej-neutral-normal.png", 1242, 1060)
image sej neutral-angry = im.Scale("images/Sejanus/sej-neutral-angry.png", 1242, 1060)
image sej neutral-serious = im.Scale("images/Sejanus/sej-neutral-serious.png", 1242, 1060)
image sej neutral-smile = im.Scale("images/Sejanus/sej-neutral-smile.png", 1242, 1060)
image sej diplo-order = im.Scale("images/Sejanus/sej-diplo-order.png", 1242, 1060)
image sej diplo-threat = im.Scale("images/Sejanus/sej-diplo-threat.png", 1242, 1060)
image sej diplo-per = im.Scale("images/Sejanus/sej-diplo-per.png", 1242, 1060)
image sej rel-bored = im.Scale("images/Sejanus/sej-rel-bored.png", 1242, 1060)
image sej rel-glance = im.Scale("images/Sejanus/sej-rel-glance.png", 1242, 1060)
image sej rel-laugh = im.Scale("images/Sejanus/sej-rel-laugh.png", 1242, 1060)
image sej sin-grin = im.Scale("images/Sejanus/sej-sin-grin.png", 1242, 1060)
image sej sin-snarl = im.Scale("images/Sejanus/sej-sin-snarl.png", 1242, 1060)
image sej sin-surprise = im.Scale("images/Sejanus/sej-sin-surprise.png", 1242, 1060)
image sej br-rage = im.Scale("images/Sejanus/sej-br-rage.png", 1242, 1060)
image sej br-focused = im.Scale("images/Sejanus/sej-br-focused.png", 1242, 1060)
image sej br-victor = im.Scale("images/Sejanus/sej-br-victor.png", 1242, 1060)

#Flipped Sejanus
image sej neutral-normal-flip = im.Scale("images/Sejanus/flipped/sej-neutral-normal.png", 1242, 1060)
image sej neutral-angry-flip = im.Scale("images/Sejanus/flipped/sej-neutral-angry.png", 1242, 1060)
image sej neutral-serious-flip = im.Scale("images/Sejanus/flipped/sej-neutral-serious.png", 1242, 1060)
image sej neutral-smile-flip = im.Scale("images/Sejanus/flipped/sej-neutral-smile.png", 1242, 1060)
image sej diplo-order-flip = im.Scale("images/Sejanus/flipped/sej-diplo-order.png", 1242, 1060)
image sej diplo-threat-flip = im.Scale("images/Sejanus/flipped/sej-diplo-threat.png", 1242, 1060)
image sej diplo-per-flip = im.Scale("images/Sejanus/flipped/sej-diplo-per.png", 1242, 1060)
image sej rel-bored-flip = im.Scale("images/Sejanus/flipped/sej-rel-bored.png", 1242, 1060)
image sej rel-glance-flip = im.Scale("images/Sejanus/flipped/sej-rel-glance.png", 1242, 1060)
image sej rel-laugh-flip = im.Scale("images/Sejanus/flipped/sej-rel-laugh.png", 1242, 1060)
image sej sin-grin-flip = im.Scale("images/Sejanus/flipped/sej-sin-grin.png", 1242, 1060)
image sej sin-snarl-flip = im.Scale("images/Sejanus/flipped/sej-sin-snarl.png", 1242, 1060)
image sej sin-surprise-flip = im.Scale("images/Sejanus/flipped/sej-sin-surprise.png", 1242, 1060)
image sej br-rage = im.Scale("images/Sejanus/flipped/sej-br-rage.png", 1242, 1060)
image sej br-focused = im.Scale("images/Sejanus/flipped/sej-br-focused.png", 1242, 1060)
image sej br-victor = im.Scale("images/Sejanus/flipped/sej-br-victor.png", 1242, 1060)

#Portia
image por con-gossip = im.Scale("images/Portia/por-con-gossip.png", 849, 1010)
image por con-laugh = im.Scale("images/Portia/por-con-laugh.png", 849, 1010)
image por con-sigh = im.Scale("images/Portia/por-con-sigh.png", 849, 1010)
image por neu-distaste = im.Scale("images/Portia/por-neu-distaste.png", 849, 1010)
image por neu-neu = im.Scale("images/Portia/por-neu-neu.png", 849, 1010)
image por neu-smile = im.Scale("images/Portia/por-neu-smile.png", 849, 1010)
image por neu-worried = im.Scale("images/Portia/por-neu-worried.png", 849, 1010)
image por plan-concern = im.Scale("images/Portia/por-plan-concern.png", 849, 1010)
image por plan-explain = im.Scale("images/Portia/por-plan-explain.png", 849, 1010)
image por plan-insist = im.Scale("images/Portia/por-plan-insist.png", 849, 1010)

#Flipped Portia
image por con-gossip-flip = im.Scale("images/Portia/flipped/por-con-gossip.png", 849, 1010)
image por con-laugh-flip = im.Scale("images/Portia/flipped/por-con-laugh.png", 849, 1010)
image por con-sigh-flip = im.Scale("images/Portia/flipped/por-con-sigh.png", 849, 1010)
image por neu-distaste-flip = im.Scale("images/Portia/flipped/por-neu-distaste.png", 849, 1010)
image por neu-neu-flip = im.Scale("images/Portia/flipped/por-neu-neu.png", 849, 1010)
image por neu-smile-flip = im.Scale("images/Portia/flipped/por-neu-smile.png", 849, 1010)
image por neu-worried-flip = im.Scale("images/Portia/flipped/por-neu-worried.png", 849, 1010)
image por plan-concern-flip = im.Scale("images/Portia/flipped/por-plan-concern.png", 849, 1010)
image por plan-explain-flip = im.Scale("images/Portia/flipped/por-plan-explain.png", 849, 1010)
image por plan-insist-flip = im.Scale("images/Portia/flipped/por-plan-insist.png", 849, 1010) 

#Nasrin
image nas act-attacking = im.Scale("images/Nasrin/Default/nas-act-attacking.png", 930, 970)
image nas act-stealthy = im.Scale("images/Nasrin/Default/nas-act-stealthy.png", 930, 970)
image nas act-wincing = im.Scale("images/Nasrin/Default/nas-act-wincing.png", 930, 970)
image nas arm-blank = im.Scale("images/Nasrin/Default/nas-arm-blank.png", 930, 970)
image nas arm-disappointed = im.Scale("images/Nasrin/Default/nas-arm-disappointed.png", 930, 970)
image nas arm-frustrated = im.Scale("images/Nasrin/Default/nas-arm-frustrated.png", 930, 970)
image nas rel-annoyed = im.Scale("images/Nasrin/Default/nas-rel-annoyed.png", 930, 970)
image nas rel-blank = im.Scale("images/Nasrin/Default/nas-rel-blank.png", 930, 970)
image nas rel-defaul = im.Scale("images/Nasrin/Default/nas-rel-default.png", 930, 970)
image nas rel-grin = im.Scale("images/Nasrin/Default/nas-rel-grin.png", 930, 970)
image nas sed-flirtatious = im.Scale("images/Nasrin/Default/nas-sed-flirtatious.png", 930, 970)
image nas sed-laughing = im.Scale("images/Nasrin/Default/nas-sed-laughing.png", 930, 970)
image nas sed-sly = im.Scale("images/Nasrin/Default/nas-sed-sly.png", 930, 970)

#Flipped Nasrin
image nas act-attacking-flip = im.Scale("images/Nasrin/Default/flipped/nas-act-attacking.png", 930, 970)
image nas act-stealthy-flip = im.Scale("images/Nasrin/Default/flipped/nas-act-stealthy.png", 930, 970)
image nas act-wincing-flip = im.Scale("images/Nasrin/Default/flipped/nas-act-wincing.png", 930, 970)
image nas arm-blank-flip = im.Scale("images/Nasrin/Default/flipped/nas-arm-blank.png", 930, 970)
image nas arm-disappointed-flip = im.Scale("images/Nasrin/Default/flipped/nas-arm-disappointed.png", 930, 970)
image nas arm-frustrated-flip = im.Scale("images/Nasrin/Default/flipped/nas-arm-frustrated.png", 930, 970)
image nas rel-annoyed-flip = im.Scale("images/Nasrin/Default/flipped/nas-rel-annoyed.png", 930, 970)
image nas rel-blank-flip = im.Scale("images/Nasrin/Default/flipped/nas-rel-blank.png", 930, 970)
image nas rel-defaul-flip = im.Scale("images/Nasrin/Default/flipped/nas-rel-default.png", 930, 970)
image nas rel-grin-flip = im.Scale("images/Nasrin/Default/flipped/nas-rel-grin.png", 930, 970)
image nas sed-flirtatious-flip = im.Scale("images/Nasrin/Default/flipped/nas-sed-flirtatious.png", 930, 970)
image nas sed-laughing-flip = im.Scale("images/Nasrin/Default/flipped/nas-sed-laughing.png", 930, 970)
image nas sed-sly-flip = im.Scale("images/Nasrin/Default/flipped/nas-sed-sly.png", 930, 970)

#Cloaked Nasrin
image nas2 act-attacking = im.Scale("images/Nasrin/Cloaked/nas2-act-attacking.png", 930, 970)
image nas2 act-stealthy = im.Scale("images/Nasrin/Cloaked/nas2-act-stealthy.png", 930, 970)
image nas2 act-wincing = im.Scale("images/Nasrin/Cloaked/nas2-act-wincing.png", 930, 970)
image nas2 arm-blank = im.Scale("images/Nasrin/Cloaked/nas2-arm-blank.png", 930, 970)
image nas2 arm-disappointed = im.Scale("images/Nasrin/Cloaked/nas2-arm-disappointed.png", 930, 970)
image nas2 arm-frustrated = im.Scale("images/Nasrin/Cloaked/nas2-arm-frustrated.png", 930, 970)
image nas2 rel-annoyed = im.Scale ("images/Nasrin/Cloaked/nas2-rel-annoyed.png", 930, 970)
image nas2 rel-blank = im.Scale("images/Nasrin/Cloaked/nas2-rel-blank.png", 930, 970)
image nas2 rel-defaul = im.Scale("images/Nasrin/Cloaked/nas2-rel-default.png", 930, 970)
image nas2 rel-grin = im.Scale("images/Nasrin/Cloaked/nas2-rel-grin.png", 930, 970)
image nas2 sed-flirtatious = im.Scale("images/Nasrin/Cloaked/nas2-sed-flirtatious.png", 930, 970)
image nas2 sed-laughing = im.Scale("images/Nasrin/Cloaked/nas2-sed-laughing.png", 930, 970)
image nas2 sed-sly = im.Scale("images/Nasrin/Cloaked/nas2-sed-sly.png", 930, 970)

#Flipped Cloaked Nasrin
image nas2 act-attacking-flip = im.Scale("images/Nasrin/Cloaked/flipped/nas2-act-attacking.png", 930, 970)
image nas2 act-stealthy-flip = im.Scale("images/Nasrin/Cloaked/flipped/nas2-act-stealthy.png", 930, 970)
image nas2 act-wincing-flip = im.Scale("images/Nasrin/Cloaked/flipped/nas2-act-wincing.png", 930, 970)
image nas2 arm-blank-flip = im.Scale("images/Nasrin/Cloaked/flipped/nas2-arm-blank.png", 930, 970)
image nas2 arm-disappointed-flip = im.Scale("images/Nasrin/Cloaked/flipped/nas2-arm-disappointed.png", 930, 970)
image nas2 arm-frustrated-flip = im.Scale("images/Nasrin/Cloaked/flipped/nas2-arm-frustrated.png", 930, 970)
image nas2 rel-annoyed-flip = im.Scale("images/Nasrin/Cloaked/flipped/nas2-rel-annoyed.png", 930, 970)
image nas2 rel-blank-flip = im.Scale("images/Nasrin/Cloaked/flipped/nas2-rel-blank.png", 930, 970)
image nas2 rel-defaul-flip = im.Scale("images/Nasrin/Cloaked/flipped/nas2-rel-default.png", 930, 970)
image nas2 rel-grin-flip = im.Scale("images/Nasrin/Cloaked/flipped/nas2-rel-grin.png", 930, 970)
image nas2 sed-flirtatious-flip = im.Scale("images/Nasrin/Cloaked/flipped/nas2-sed-flirtatious.png", 930, 970)
image nas2 sed-laughing-flip = im.Scale("images/Nasrin/Cloaked/flipped/nas2-sed-laughing.png", 930, 970)
image nas2 sed-sly-flip = im.Scale("images/Nasrin/Cloaked/flipped/nas2-sed-sly.png", 930, 970)

#Avalaran
image av neu-neu = im.Scale("images/Avalaran/av-neutral-neutral.png", 1105, 1000)
image av ac-confuse = im.Scale("images/Avalaran/av-ac-confuse.png", 1105, 1000)
image av ac-laugh = im.Scale("images/Avalaran/av-ac-laugh.png", 1105, 1000) 
image av ac-orders = im.Scale("images/Avalaran/av-ac-orders.png", 1105, 1000)
image av dis-annoyed = im.Scale("images/Avalaran/av-dis-annoyed.png", 1105, 1000)
image av dis-smug = im.Scale("images/Avalaran/av-dis-smug.png", 1105, 1000)
image av dis-bored = im.Scale("images/Avalaran/av-dis-bored.png", 1105, 1000)

#Flipped Avalaran
image av neu-neu-flip = im.Scale("images/Avalaran/flipped/av-neutral-neutral.png", 1105, 1000)
image av ac-confuse-flip = im.Scale("images/Avalaran/flipped/av-ac-confuse.png", 1105, 1000)
image av ac-laugh-flip = im.Scale("images/Avalaran/flipped/av-ac-laugh.png", 1105, 1000) 
image av ac-orders-flip = im.Scale("images/Avalaran/flipped/av-ac-orders.png", 1105, 1000)
image av dis-annoyed-flip = im.Scale("images/Avalaran/flipped/av-dis-annoyed.png", 1105, 1000)
image av dis-smug-flip = im.Scale("images/Avalaran/flipped/av-dis-smug.png", 1105, 1000)
image av dis-bored-flip = im.Scale("images/Avalaran/flipped/av-dis-bored.png", 1105, 1000)

#Soldier
image sol neu-neu = im.Scale("images/Soldiers/Soldier/sol-neu-neu.png", 1956, 1250)
image sol neu-shout = im.Scale("images/Soldiers/Soldier/sol-neu-shout.png", 1956, 1250)
image sol neu-confuse = im.Scale("images/Soldiers/Soldier/sol-neu-confuse.png", 1956, 1250)
image sol br1-sp-neu = im.Scale("images/Soldiers/Soldier/sol-br1-sp-neu.png", 1956, 1250)
image sol br1-sp-scare = im.Scale("images/Soldiers/Soldier/sol-br1-sp-scare.png", 1956, 1250)
image sol br1-sp-shout = im.Scale("images/Soldiers/Soldier/sol-br1-sp-shout.png", 1956, 1250)
image sol br1-sp-wince = im.Scale("images/Soldiers/Soldier/sol-br1-sp-wince.png", 1956, 1250)
image sol br1-sw-neu = im.Scale("images/Soldiers/Soldier/sol-br1-sw-neu.png", 1956, 1250)
image sol br1-sw-scare = im.Scale("images/Soldiers/Soldier/sol-br1-sw-scare.png", 1956, 1250)
image sol br1-sw-shout = im.Scale("images/Soldiers/Soldier/sol-br1-sw-shout.png", 1956, 1250)
image sol br1-sw-wince = im.Scale("images/Soldiers/Soldier/sol-br1-sw-wince.png", 1956, 1250)
image sol br2-neu = im.Scale("images/Soldiers/Soldier/sol-br2-neu.png", 1956, 1250)
image sol br2-scare = im.Scale("images/Soldiers/Soldier/sol-br2-scare.png", 1956, 1250)
image sol br2-shout = im.Scale("images/Soldiers/Soldier/sol-br2-shout.png", 1956, 1250)
image sol br2-wince = im.Scale("images/Soldiers/Soldier/sol-br2-wince.png", 1956, 1250)

#Flipped Soldier
image sol neu-neu-flip = im.Scale("images/Soldiers/Soldier/flipped/sol-neu-neu.png", 1956, 1250)
image sol neu-shout-flip = im.Scale("images/Soldiers/Soldier/flipped/sol-neu-shout.png", 1956, 1250)
image sol neu-confuse-flip = im.Scale("images/Soldiers/Soldier/flipped/sol-neu-confuse.png", 1956, 1250)
image sol br1-sp-neu-flip = im.Scale("images/Soldiers/Soldier/flipped/sol-br1-sp-neu.png", 1956, 1250)
image sol br1-sp-scare-flip = im.Scale("images/Soldiers/Soldier/flipped/sol-br1-sp-scare.png", 1956, 1250)
image sol br1-sp-shout-flip = im.Scale("images/Soldiers/Soldier/flipped/sol-br1-sp-shout.png", 1956, 1250)
image sol br1-sp-wince-flip = im.Scale("images/Soldiers/Soldier/flipped/sol-br1-sp-wince.png", 1956, 1250)
image sol br1-sw-neu-flip = im.Scale("images/Soldiers/Soldier/flipped/sol-br1-sw-neu.png", 1956, 1250)
image sol br1-sw-scare-flip = im.Scale("images/Soldiers/Soldier/flipped/sol-br1-sw-scare.png", 1956, 1250)
image sol br1-sw-shout-flip = im.Scale("images/Soldiers/Soldier/flipped/sol-br1-sw-shout.png", 1956, 1250)
image sol br1-sw-wince-flip = im.Scale("images/Soldiers/Soldier/flipped/sol-br1-sw-wince.png", 1956, 1250)
image sol br2-neu-flip = im.Scale("images/Soldiers/Soldier/flipped/sol-br2-neu.png", 1956, 1250)
image sol br2-scare-flip = im.Scale("images/Soldiers/Soldier/flipped/sol-br2-scare.png", 1956, 1250)
image sol br2-shout-flip = im.Scale("images/Soldiers/Soldier/flipped/sol-br2-shout.png", 1956, 1250)
image sol br2-wince-flip = im.Scale("images/Soldiers/Soldier/flipped/sol-br2-wince.png", 1956, 1250)

#Engineer
image en neu-neu = im.Scale("images/Soldiers/Engineer/en-neu-neu.png", 1956, 1250)
image en neu-confuse = im.Scale("images/Soldiers/Engineer/en-neu-confuse.png", 1956, 1250)
image en neu-shout = im.Scale("images/Soldiers/Engineer/en-neu-shout.png", 1956, 1250)

#Flipped Engineer
image en neu-neu-flip = im.Scale("images/Soldiers/Engineer/flipped/en-neu-neu.png", 1956, 1250)
image en neu-confuse-flip = im.Scale("images/Soldiers/Engineer/flipped/en-neu-confuse.png", 1956, 1250)
image en neu-shout-flip = im.Scale("images/Soldiers/Engineer/flipped/en-neu-shout.png", 1956, 1250)

#Immortal

#Flipped Immortal

#Other Images

image sunbright = Solid("#fffffe")
image red = Solid("c8373d")
image gold = Solid("ffda8f")
image green = Solid("50c878")
image rose = "images/Other/RoseTransition.png"
image spears = im.Scale("images/Other/SpearsTransition.png", 500, 723)

#audiochannels

init python:
    renpy.music.register_channel("sfx1", "sfx")
    renpy.music.register_channel("sfx2", "sfx")
    renpy.music.register_channel("sfx3", "sfx")
    config.default_fullscreen = True
    
#Music
define audio.romance = "audio/music/ultimateromance.wav"
define audio.priestly = "audio/music/priestlyboys.wav"
define audio.opening = "audio/music/siegeopeningscene.wav"
define audio.sandman = "audio/music/sandman.wav"
define audio.whale = "audio/music/siegebadending1.wav"
define audio.planning = "audio/music/siegemenu1.wav"
define audio.fluff = "audio/music/fluff.wav"
define audio.menacing = "audio/music/menacing.wav"
define audio.downtime = "audio/music/downtime.wav"
define audio.nostalgia = "audio/music/nostalgia.wav"
define audio.timekiller = "audio/music/timekiller.wav"
define audio.dyingsoldiers = "audio/music/dyingsoldiers.wav"
define audio.whataday = "audio/music/whataday.wav"
define audio.droney = "audio/music/droney.wav"
define audio.love = "audio/music/love.wav"
define audio.aftershock = "audio/music/aftershock.wav"
define audio.massacre = "audio/music/massacre.wav"
define audio.spooktacular = "audio/music/spooktacular.wav"
define audio.reprieve = "audio/music/reprieve.wav"

#SFX
define audio.march = "audio/sfx/marching.wav"
define audio.ballistacrank = "audio/sfx/ballistacrank.wav"
define audio.ballistafire = "audio/sfx/ballistafire.wav"
define audio.ballistawhoosh = "audio/sfx/ballistawhoosh.wav"
define audio.ballistaflesh = "audio/sfx/ballistahitflesh.wav"
define audio.ballistawood = "audio/sfx/ballistahitwood.wav"
define audio.javhail = "audio/sfx/javhail.wav"
define audio.javhailshields = "audio/sfx/javhailshields.wav"
define audio.angrycrowd = "audio/sfx/angrycrowd.wav"
define audio.applause = "audio/sfx/applause.wav"
define audio.crowdmurmur = "audio/sfx/crowdmurmur.wav"
define audio.knockdoor = "audio/sfx/knockdoor.wav"
define audio.horsemove = "audio/sfx/horsemovement.wav"
define audio.angrycrowd1 = "audio/sfx/angrycrowd1.wav"
define audio.angrycrowd3 = "audio/sfx/angrycrowd3.wav"
define audio.angrycrowdloop = "audio/sfx/angrycrowdloop.wav"
define audio.applausealt = "audio/sfx/applausealtered.wav"
define audio.ballistaassault = "audio/sfx/ballistaassault.wav"
define audio.marchingalt = "audio/sfx/marchingaltered.wav"
define audio.marchinghorse = "audio/sfx/marchinghorses.wav"
define audio.stonecollapse = "audio/sfx/stonecollapse.wav"
define audio.cheer1 = "audio/sfx/cheer1.wav"
define audio.cheer2 = "audio/sfx/cheer2.wav"
define audio.quill = "audio/sfx/quill.wav"
define audio.singleballista = "audio/sfx/singleballista.wav"
define audio.puke = "audio/sfx/puke.wav"
define audio.hornadvance = "audio/sfx/hornadvance.wav"
define audio.horntestudo = "audio/sfx/horntestudo.wav"
define audio.hornballista = "audio/sfx/hornballista.wav"
define audio.dooropen = "audio/sfx/dooropen.wav"
define audio.doorclose = "audio/sfx/doorclose.wav"


define a = Character('Avalaran', color="9d4fd6")
define c = Character('Clodia', color="c63f47")
define ce = Character('Centurion', color="ffffff")
define g = Character('Guard', color="ffffff")
define g2 = Character('Guard 2', color="ffffff")
define p = Character('Portia', color="ff9d00")
define n = Character('Nasrin', color="45cc5e")
define v = Character('Valerius', color="f7e764")
define s = Character('Sergius', color="1cd1ff")
define sc = Character('Scout', color="ffffff")
define so = Character('Soldier', color="ffffff")
define sos = Character('Soldiers', color="ffffff")
define sj = Character('Sejanus', color="ff6100")
define so2 = Character('Soldier 2', color="ffffff")
define so3 = Character('Soldier 3', color="ffffff")
define il = Character('Immortal', color="ffffff")
define tr = Character('Tribune', color="ffffff")
define tr2 = Character('Tribune 2', color="ffffff")
define tr3 = Character('Tribune 3', color="ffffff")
define trh = Character('Horatius', color="ffffff")
define tra = Character('Antonius', color="ffffff")
define cr = Character('Crowd', color="ffffff")
define pre = Character('Priestess', color="ffffff")
define en = Character('Engineer', color="ffffff")
define m = Character('Messenger', color="ffffff")
define no = Character('Noble', color="ffffff")
define nos = Character('Nobles', color="ffffff")
define po = Character('Pontifex', color="ffffff")
define wm = Character('Woman', color="ffffff")
define nar = Character(None, kind=nvl)

define splash = Fade(0.5, 0.0, 0.5, color="#fff")

image intromovie = Movie(channel = "intromovie", play="images/Video/intro.mkv")
image mainmenupic = "images/Other/main_menu.png"

# New transformations etc.

transform fright:
    xcenter 0.75
    yalign 1.0
    
transform fleft:
    xcenter 0.25
    yalign 1.0
    
transform ffleft:
    xcenter 0.21
    yalign 1.0

transform ffright:
    xcenter 0.79
    yalign 1.0

transform sleft:
    xcenter 0.25
    yalign 0.5
    
transform scenter:
    xcenter 0.5
    yalign 0.5

transform sright:
    xcenter 0.75
    yalign 0.5

transform sergright:
    xcenter 0.7
    yalign 1.0

transform sergleft:
    xcenter 0.3
    yalign 1.0
    
transform mleft:
    xcenter 0.35
    yalign 1.0
    
transform mright:
    xcenter 0.65
    yalign 1.0

transform farright:
    xalign 1.0
    yalign 1.0
    
transform farleft:
    xalign 0
    yalign 1.0

transform sejright:
    xcenter 0.8
    yalign 1.0
    
transform centerleft:
    xcenter 0.4
    yalign 1.0

transform centerright:
    xcenter 0.6
    yalign 1.0

# The game starts here.

label splashscreen:
    show black
    pause (3.0)
    show intromovie
    pause (3.5)
    play music romance loop
    pause (9.5)
    stop intromovie
    show mainmenupic
    call screen splash()
    pause (0.25)
    show red with dissolve
    return
    
label start:

scene black
with fade

stop music fadeout 2.0

pause(2.0)

centered "The most famous epochs of Raskyan history brim with arrogance, greed and bloodshed. {p}Thus, the tale I'm about to tell has been overlooked by dramatists and historians alike. {p}{p=0}Those who write of the Dagaran War always focus on the pious general Sergius and the final clash with his rival, Sejanus. {p}They have ignored an equally compelling sidestory to that affair which I aim to now unveil. {p}{p=0} A tale of the heart, a story shunned until this time for not reflecting Raskyan values of old. {p}In this new age of ours, as poets and players at last open themselves to the sentimental and the erotic, it can finally take its place in our cultural mythology.{p}{p=0}As a people who have always been caged by our devotion to religious stoicism and military brutality, we Raskyans would do well to treasure dearly this, our one true love story.{p}{p=0}- Alexes, from his \"Tales of the Republican Kingdom\""

pause (2.0)

play movie("images/Video/Vtrans0.mkv")
$ renpy.pause(5.5,hard=True)
stop movie

play music opening
pause (3.0)

centered "The bodies are fresh."
pause (1.0)

centered "Their blood is still bright, and they don't smell yet."
pause (1.0)

centered "Well, not as bad as they will."
pause (1.0)

show CG1C:
    xalign 1.0 yalign 0.0 zoom 1.7
with fade

pause 1.0

"They seem to melt into one another, piled carelessly."

show CG1C:
    linear 2.0 yalign 1.0 xalign 0.5
    linear 2.0 xalign 0.0

pause 4.0

"I don’t recognize them. Minor nobles. Their slaves, too. No surprise there."

"Good."

hide black
show black
with fade

show Wall with fade:
    zoom 1.5 xalign 0.5 yalign 1.0
    linear 1.0 yalign 1.0
    linear 3.0 yalign 0.0
    linear 0.5 yalign 0.0
    linear 10.0 xalign 1.0
    
pause 4.0

centered "Before me is the wall of Dagara. Seven traitorous soldiers stand atop it, holding blades to the throats of more noble victims."

centered "One of the rebels shouts down at me."

centered "\"Raskyan dirt doesn’t need any more noble blood! Don’t make us kill these people too!\""

centered "Raskyan dirt, he says? This was Dagaran dirt last year, and for time eternal before that."

hide Wall
with fade

centered "I do agree, however, that it needs no more blood."

show CG1D:
    yalign 1.0
with dissolve

"With every step I take in the mud, red water oozes forth."

show Wall with fade:
    zoom 1.5 xalign 1.0 yalign 0.0

centered "The man shouts down at me again."

centered "\"Well, do you have anything more to say? Or are we done here?\""

centered "Done?"

centered "I wish."

hide Wall
with dissolve

show CG1D:
    linear 6.0 yalign 0.1

"I'm not sure what I was thinking, coming out here like this."

"It's a matter of honor, of course, to offer the enemy surrender."

"But I didn't have to volunteer. I just wanted to see who these bodies were."

"And while I can't quite make out the faces of the living hostages from down here, I can tell who they're not."

"Not who I came for."

"Still, those people matter to someone."

show CG1A:
    yalign 0.0
with dissolve

"The chill Dagaran breeze blusters about as usual. But all seems still in the warm evening sun."

"The whole world feels very empty."

"Mostly."

show CG1B:
    yalign 1.0
with dissolve

hide CG1D

"In the distance behind me, the camp produces the kind of sounds one can't really ignore."

"Construction."

"Everyone with their role to play, and none to spare in guarding me out here from missiles save a handful of bored recruits."

"Standing a rather safe distance behind me, it seems. I know it's the traditional way to negotiate, but..."

show CG1D:
    yalign 0.1
with dissolve

"I pray they can get their big shields over to me quickly if all those javelins I'm looking up at start to fall."

hide black
show black
show val rel-emotional:
    xalign 0.8 yalign 0.5 zoom 1.25
with dissolve

"Very well then. Let’s see what happens."

show val rel-serious-flip:
    xalign 0.4 yalign 0.5 zoom 1.25
with dissolve

pause(0.5)

show Wall
hide black
hide val
show val rel-serious-flip
with dissolve

"Just how loyal are these men, Sejanus?"

hide val
with dissolve

menu:
    "What should I do?"
    "Grant the enemy forces peaceful terms.":
      jump wall_a
    "Show strength. Remind them that they are dead no matter
what they do.":
      jump wall_b

label wall_a:

$ portia = "alive"

show val rel-serious-flip
with dissolve

"I don’t care how loyal these men are. Men like to live."

show val rel-serious-flip at fright
with ease

show val cmd-urgent-flip
with dissolve

v "Sejanus has you convinced he has some way out of this mess he's gotten you into."

v "But it's a shameful man who leads loyal Raskyans into treason."

v "As far as I’m concerned, the fault here only lies with your legatus."

show val cmd-ordering-flip
with dissolve

v "Give us Sejanus, and only Sejanus will be punished."

hide val
with dissolve

"Maybe I’m imagining it, but it seems like they’re looking at one another."

show val cmd-urgent-flip at fright
with dissolve

v "Think about it. Talk it over."

v "And remember that my way is the only way any of you survive."

hide val
with dissolve

pause(0.5)

nar "I turn and begin the trek back to camp, my guard falling in with me."

nar "At least a hail of javelins doesn’t come and strike us in the back. Not that it would do much against drilled veterans like these."

nar "They could turn and lift their shields before the missiles get even halfway to us."

nvl clear

nar "Looking back over my shoulder, I see them taking the hostages down from the wall."

nar "Good."

nar "Not that it really matters. The hostages are not likely to survive when we take the city."

nar "But for the time being, the illusion of civility between our two sides is comforting."

nvl clear

nar "And besides, I don’t want them to get comfortable killing the people in their power."

nar "Not that I fear for them. Honestly, I care little for most of the people trapped in there."

nar "It's all nobles who have nothing to do with me."

nvl clear

scene black
with fade

centered "But there’s one...{p}{p=0.0}One who must live.{p}{p=0.0}Or else I might as well die."

stop music fadeout 1.5
pause(2.0)

nvl clear

jump camp

label wall_b:

$ portia = "dead"

show val rel-serious-flip
with dissolve

"I doubt there’s any deal I can offer them that they’ll take seriously."

"This is a formality. One way or the other, we will attack. They know it."

show val at fright
with ease

show val cmd-ordering-flip
with dissolve

v "Very well. It’s clear you are serious." 

v "But you have absolutely nothing valuable to bargain with."

v "You can drop all of those little lordlets, and you’ll still be dead men in a few days."

hide val
with dissolve

"They don’t seem to be dropping any more lordlets."

"Good for them, I suppose."

show val ac-annoyed-flip at fright
with dissolve

v "Enjoy the rest of the life you’ve got left."

v "And be sure to thank Sejanus for leading you all to your graves."

hide val
with dissolve

"They say nothing."

"The seven falling souls scream, though."

"..."

"Damn."

"Well..."

"They probably would not have survived the coming attack anyway."

show val rel-normal-flip at fright
with dissolve

stop music
play sound javhail loop

v "What-"

"I hear them before I see them."

show CGX with dissolve:
    zoom 2.0 xalign 0.1 yalign 1.0
    linear 0.25 yalign 0.5
    xalign 0.8 yalign 0.5
    linear 0.25 yalign 0.0
    xalign 0.5 yalign 0.9
    linear 0.25 yalign 0.4
    xalign 0.8 yalign 0.5
    linear 0.25 yalign 0.0
    xalign 0.3 yalign 0.6
    linear 0.25 yalign 0.1
    xalign 0.6 yalign 0.7
    linear 0.25 yalign 0.2
    xalign 0.2 yalign 1.0
    linear 0.25 yalign 0.5
    xalign 0.4 yalign 0.5
    linear 0.25 yalign 0.0
    repeat

"The missiles come down."

hide val

"One of them strikes the mud next to me."

"I don't fully understand what's going on before my body runs away. Back to my men, safely away."

"They realize what's happening and start running to me, but-"

"Another javelin lands somewhere near me."

"Another."

"Ghosts, help!"

"Instinctively my hand reaches out to the nearest legionary, grasping for help."

"He takes my hand-"

show CGT with dissolve:
    yalign 0.05
    choice:
        linear 0.1 yalign 0.0
        linear 0.1 yalign 0.1
    choice:
        linear 0.1 yalign 0.07 
        linear 0.1 yalign 0.15
    choice:
        linear 0.1 yalign 0.05
        linear 0.1 yalign 0.13
    choice:
        linear 0.1 yalign 0.25
        linear 0.1 yalign 0.12
    repeat

play sound javhailshields loop

"-and yanks me behind him to the ground."

"Mud seeps into my cloak."

"All I see in front of me is a wall of steel and crimson."

"My guard, their shields pressed together like a wall, deflect the missiles as they rain down on us." 

"Everywhere the soft mud sucks up projectiles until we're in a field of wood and iron flowers."

"There were far more than seven men up on that wall. The rest were waiting just out of sight."
 
"Perhaps even Sejanus."

stop sound fadeout (1.0)
hide CGX
hide CGT

show Wall
with dissolve

play music opening fadein 1.5

"..."

"Seems like its stopped."

show sol neu-neu-flip at sright
with dissolve

"One of my guards offers his hand to help me up." 

show val rel-serious at left
with easeinbottom
 
"He pulls me to my feet, limbs sucking free from the mud."

"My men are stone faced, but it’s impossible to ignore the laughter behind their eyes."

"Just another time I’ve confirmed their suspicions." 

"That I’m just some dirty field boy in tribune’s armor."

show val rel-emotional-flip
with dissolve

"Ghosts damn all of you."

"And damn all you bastards on the wall for loosing missiles at a man treating for peace!"

hide sol
with dissolve

show val rel-serious-flip at center
with ease

"I take one of their shields, just in case, as I turn back to the traitors up on that wall."

show val br2-shouting-flip
with dissolve

v "Thank you for the javelins! We'll return them to you in good time!"

show val rel-serious at fright
with dissolve

"Turning back for camp, I lead my guard away."

hide val
with dissolve

pause(0.5)

nar "They keep their shields half-raised behind them for the short march until we’re out of missile range."

nar "The enemy doesn’t attack again. Maybe they’re actually thinking about my words."
 
nar "Little good it will do them all in the end."

nvl clear

nar "My legatus will order the assault soon."

nar "I don’t have long to figure something out."

nar "Soon, we’ll slaughter everyone inside."

nvl clear

scene black
with fade

centered "I cannot allow that.{p}{p=0.0}Absolutely cannot."

stop music fadeout 1.5
pause(2.0)

nvl clear

jump camp

label camp:

play music planning

scene CampDay
with fade

pause (1.0)

nar "The camp buzzes like a hive."

nar "Every man and woman, soldier and slave here knows their role. Or at least knows to look it."

nar "They’re all aware of what an easy campaign this is."

nar "How could they not be?"

nvl clear

stop music fadeout 1.0

s "Lend me your ears, fellows! Did you ever think our final battle would be so easy?"

play audio angrycrowd1

"Another one of these."

play music menacing

show CG2B
with dissolve

s "Did you ever think at the end, after all the years of blood and dirt and shit, all the iron and fire, that it would come down to this?"

s "Why, we’re not at war, we’re sightseeing!"

s "So thank your former comrades for the opportunity. For if they had not decided to be gutless, filthy traitors, we wouldn’t have the capital of Dagara at the tips of our swords once more."

s "Hell, thank their leader! If Sejanus, that supreme cunt, hadn’t decided to betray all of us, we wouldn’t have that second chance."

show CG2A:
    xalign 0.0 yalign 1.0 zoom 1.5
    linear 6.0 xalign 1.0
with dissolve

play audio angrycrowd1

sos "Fuck Sejanus!"

sos "Let's kill him!"

sos "Traitor bastard!"

s "Aye, that's the thanks he deserves! We'll be sure to give it to him as we take what is ours."

hide CG2A
show CG2B
with dissolve

s "I know how disheartened you all were when last we hammered these walls and took these people."

s "I could not let you run free then, as political matters had my hands tied."

s "But no longer. These are not prospective citizens."

s "These are traitors!"

s "The soldiers!"

s "The slaves!"

s "The women!"

s "And traitors have no rights."

s "I promise you all, this city is my reward to you for your long years of hard service to me."

hide CG2B
show CG2A:
    yalign 0.4
with dissolve

stop audio
play sfx2 angrycrowdloop fadein 3.0

s "For all you’ve done over the years, you brave veterans, I will give you Dagara!"

s "Its gold! Its girls! Its glory!"

s "When we take the walls with the blessing of the Basileus, they will all be yours!"

show CG2C:
    xalign 0.5 yalign 0.7
    linear 1.0 xalign 1.0
    xalign 0.5 yalign 0.8
    linear 1.0 xalign 0.0
    xalign 0.85 yalign 1.0
    linear 1.5 yalign 0.0
with dissolve

centered "\"SERGIUS! {w=0.5} SERGIUS! {w=0.5} SERGIUS!\"{w=1.0}{nw}"

show sunbright
with dissolve

stop sfx2 fadeout 1.0
pause 1.0

centered "{color=#000000}Great Graves, they do love him. {p} More than ever, I expect.{/color}"

hide CG2A
show CG2B
with dissolve

"Gaius Argus Sergius is everything a good soldier wants in a legatus."

"Further proof I’m a poor soldier, I suppose."

scene CampDay
with fade

show val rel-normal with dissolve

stop music fadeout 2.0

"After much vigorous hand shaking and hailing and manly hugging, Sergius disappears inside the great red War Tent."

"The crowd begins to disperse as men find their way back to their tasks." 

"As for my job, I have to report on my weak attempt at diplomacy."

show val rel-emotional with dissolve

"I’m not looking forward to this."

scene TentDay
with fade

play music planning

pause (1.0)

nar "The War Tent is one of the symbols of Raskya’s military might." 

nar "A fortified camp with the tent’s red canvas towers peeking over the top is the sort of omen that many have learned to fear and respect."

nar "Occasionally, armies have dissolved at the sight."

nvl clear

nar "So precious few have seen the inside of one of these tents that I should perhaps feel privileged."

nar "But it’s exactly what you would guess: a place of wealth and war, slaves milling about maintaining incense and fire, tribunes like me groveling at the commander’s feet."

nar "This temporary structure is nothing special, save that it’s a room where men plan spectacular feats of mass death."

nvl clear

show ser neutral-serious-flip
with dissolve

"I can sense Sergius wants to get to planning."

s "Ah, you. Tribune, uh..."

show ser neutral-serious-flip at sergright
with dissolve

show val rel-normal at fleft
with dissolve

v "Valerius Aureus."

s "Tribune Aureus. How did your negotiations go?"

show val ac-fact
with dissolve

v "They refused to surrender."

show ser neutral-annoyed-flip
with dissolve

s "Yes, of course they did."

s "Now that you’ve accomplished absolutely nothing, what say we storm the city tomorrow?"

show val rel-serious
with dissolve

v "I say that’s ill advised."

show ser ac-insulting-flip
with dissolve

s "And that’s why you’re a poor soldier. Among other things."

show val rel-normal
with dissolve

v "I do not mean to suggest that this is a siege that will require cunning and brilliance to end."

v "But there is an optimal way to achieve victory."

show ser neutral-serious-flip
with dissolve

s "Optimal? When you outnumber a trapped enemy with no bargaining power, that’s optimal."

v "All the same-"

show ser neutral-annoyed-flip
with dissolve

s "You know I can send you back to your farm?"

show val rel-emotional
with dissolve

v "Yes. I know."

"You’ve threatened it here and there for the last year, ever since I became one of your tribunes."

show val rel-normal
with dissolve

"Amazing how you've managed it without learning my name."

show ser neutral-serious-flip
with dissolve

s "Then stop with this unmanliness. We’ll take the city, kill all the traitors, and you’ll probably get a nice big bag of gold and a discharge for your help in doing nothing."

show ser neutral-grin-flip
with dissolve

s "Every red blooded Raskyan wants a retirement so easy. You may be the luckiest snake who ever lived."

show val ac-fact
with dissolve

v "It’s not my retirement I have concerns about, legatus."

show ser neutral-serious-flip
with dissolve

s "Then what is the source of this fear? You think I can’t see fear in you, boy?"

show val rel-emotional
with dissolve

"Fear..."

"You’re right. I am afraid. Not that you would understand." 

"From the tales I hear of the endless stream of slaves that pass between your sheets, I'm sure you have little idea of the kind of exquisite terror I face."

show val ac-annoyed
with dissolve

v "We must not turn Dagara into a sacrificial altar. There are more than just traitors inside."

v "When we conquered this city last year, we made its people Raskyan."

v "What curses will you bring down upon us for spilling our own citizens' blood so casually?"

show ser ac-annoyed-flip
with dissolve

s "Curses that are easy to avoid with the blessing of the Basileus."

show val ac-fact
with dissolve

v "And where is this blessing? I'm sure the men would like to hear it read aloud to them."

show ser neutral-annoyed-flip
with dissolve

s "..."

s "... I will request it after our victory."

show val ac-grin
with dissolve

"Oh? The men certainly don’t know {i}this.{/i}"

show val ac-fact
with dissolve

v "Afterward? You mention in every speech that we go with the blessing of the Basileus."

show ser com1-barking-flip
with dissolve

s "We {i}will{/i} have his blessing. He’ll have no choice but to grant it to us."

show val ac-grin
with dissolve

"Oh please, legatus. This is delicious."

"I knew you had issues with the Basileus, but I didn't think it would lead to you ignoring him entirely!"

show val ac-fact
with dissolve

show ser neutral-serious-flip
with dissolve

s "We are the most powerful army in Raskya, and beyond. The Basileus would bring curses upon his own head were he not to bless us."

v "More powerful than his Immortal Legion?"

s "The Immortals do not go into the field, they would leave the capital undefended against Peladon's armada."

"Peladon. Richest city in the world, they want you to think. So why do they always want more, including what we've got?"

show val rel-emotional
with dissolve

v "So you expect to anger the Ghosts of War and beg forgiveness later."

show ser com1-barking-flip
with dissolve

s "No, I will grant them a great gift!"

s "That whole city out there is a waiting sacrifice to their glory! Enough blood to sate them until the blessing is granted."

show val ac-annoyed
with dissolve

v "The men believe we are blessed {i}now{/i}."

show ser ac-annoyed-flip
with dissolve

s "Watch yourself."

s "You’re a tactician so perhaps you don’t understand the bigger picture, but grand and manful risk is the nature of war!"

show ser com1-barking-flip
with dissolve

s "And when they read the history of our era, they will see my risks and weep great tears that they will never bask in glory such as mine!"

"A true Raskyan, through and through."

show ser ac-annoyed-flip
with dissolve

s "..."

v "..."

show ser neutral-annoyed-flip
with dissolve

s "Very well, you bastard. I’m sending for a rider."

v "Legatus?"

s "He will go to the Basileus, explain the situation, and have the army blessed."

s "As soon as word returns that we are protected, everyone in that citadel dies. And I will hear no more of this from you."

show val ac-fact
with dissolve

v "That sounds like a good idea."

s "Hmph. Unnecessary pretense."

"But I can see he feels otherwise."

"Pious men, no matter how brave, feel their nerves when things are not quite right with the Ghosts."
                                                                                                    
show ser neutral-serious-flip
with dissolve

s "Well, get gone then. I have a letter to draft."

"He snaps his fingers at his attendant. The slave immediately takes a seat at a desk with papyrus and ink."

"Seems we both have letters to write."

show ser neutral-serious
with dissolve

s "Blessings of War upon you."

show val rel-normal-flip
with dissolve

v "And upon you, legatus."

hide val
with dissolve

"I leave him, gladly."

stop music fadeout 2.0

scene ValeriusRoom
with fade

play music downtime

pause (1.0)

nar "The War Tent is a mobile palace, a place for the heads of an army to live and work on the campaign trail."

nar "The legatus and each of his six tribunes have their own lavish private quarters, rollicking nightly with debauched parties and orgies."

nar "My room, I suspect, is the dullest of them all."

nar "But parties and orgies require friends, and all my friends are miles and miles south of here, enjoying their spring soaked farms and simple civilian lives."

nvl clear

nar "My attendant waits quietly by the writing desk, as if he hasn’t moved in my absence."

nar "I wave my hand to shoo him from the room. He’s a skilled transcriber, but he’s useless for the message I need to write now."

nar "I must compose this myself."

nvl clear

nar "It will take two days at most for a rider to reach the capital, collect the Blessing, and return."

nar "Whether he actually returns with a Blessing or not, I know Sergius will attack then."
 
nar "Two days is all the time I have."

nar "Clearing my head, I begin to scratch out the words of treason."

nvl clear

stop music fadeout 2.0

show black
with fade

$ renpy.pause(1.0,hard=True)

play movie("images/Video/Ctrans0.mkv")
$ renpy.pause(5.5,hard=True)
stop movie

label HighHallFeast:
scene black
pause (2.0)

play music priestly

centered "The high hall smells rotten tonight."

centered "The feast that decorates the tables is magnificent, that isn’t in doubt. Our cooks always perform well."

play sfx1 crowdmurmur fadein 3.0 loop

centered "Doubly so, it seems, when their lives are in great danger."

centered "I’ve had to eat some of it just to keep my strength up, but no more than that."

show CG3A:
    xalign 0.5 yalign 1.0 zoom 2.5
with dissolve

pause (1.0)

"Hard to turn down the wine, though."

"Even though this feast was fashioned to turn us all to fools."

"And what a view of the foolishness I’ve been granted."

show CG3A:
    linear 5.0 zoom 1.8 xalign 0.0 yalign 0.15

"I am not used to sitting here. My place is a few seats down, near the end, with the other noble ladies." 

"It’s almost a blasphemy to place me here in the Praetrix’s chair."

"I suppose the one who did the placing expects that sin to be forgiven too."

"Just make me the Praetrix, he thinks, and all is well."

"The placing of me in this seat."

"The capture of the city."

"The murder of the woman who should be sitting here."

"And the murder of her Praetor husband."

show CG3A:
    xalign 0.0 yalign 0.0 zoom 1.5
    linear 5.0 xalign 0.8
with dissolve

"All to be forgiven, thinks the killer himself, as he sits next to me."

"Oh Sejanus, you will die in such agonies."

"Why do you smile even now? The grave is stretched open before you, and you act as if you cannot see it."

show CG3C:
    xalign 0.5 yalign 0.1
with dissolve

sj "My lady Clodia, do you know how many women would smother their own babies to sit where you are now?"

show CG3A:
    xalign 0.5 yalign 0.0 zoom 1.0
with dissolve

c "I do not. And if I did, I would have them all strung up for being wretched mothers."

sj "Ha ha, you have such a legalistic mind, my dear. You’ll make a fine Praetrix."

"The easy smile and charming eyes hide the malice of his words and deeds."

"But then, my opinion of him is irrelevant to his schemes."

hide CG3C
with dissolve

"The only votes that count are those of the noblemen devouring this two-faced feast, pinching the bottoms of the slave girls Sejanus brought with him to keep the wine and passions flowing."

"I’ve already seen many nobles taking girls by the wrist and leading them away to some other hall."

"I suppose the rest of them are waiting to finish their meals first." 

"Or perhaps they actually have the decency to control themselves while their lady wives sit next to them."

"A few more rounds of wine will fix that, I’m sure."

show HighHall
with dissolve

show clo reg-stoic at left
show sej neutral-normal
with dissolve

"Sejanus commands the attention of the room."

stop sfx1 fadeout 2.0

"Surprisingly, they are able to take their minds off the serving girls long enough to look."

show sej neutral-smile
with dissolve

sj "My dear friends, I trust you’ve found the evening to be a merry one so far?"

play sound cheer1

"A roar of agreement crashes through the room like a demonic wave."

"I down the rest of my cup. Being a little drunk will make this mass disgrace somewhat easier to ignore."

show sej neutral-normal
with dissolve

sj "Of course you have. And there are more nights like this to come, if I become your Praetor."

sj "I have powerful friends in the Golden Port of Peladon. There will be no end to the finest drinks, and the girls, well, they’re yours."

show sej rel-laugh
with dissolve

sj "There’s plenty of empty rooms we can put them up in now, eh?"

play sound cheer2

show clo reg-emotional
with dissolve

"The laughter that bubbles up is sickening."

"Those rooms are only empty because those amongst you who were not cowards or not capable of voting were dragged out of their beds and tossed over the wall!"

show sej neutral-serious
with dissolve

sj "Sadly, if I am ousted by that brute out there, you can expect him to get rid of all that."

show sej diplo-per
with dissolve

sj "Beautiful girls are what he would call unmanly, I suppose. You can all go back to buggering the stable boys, if you’d like."

play sound cheer1
play sfx1 crowdmurmur fadein 3.0 loop

"Laughter and shouts of \"No!\" fill the room."

show clo reg-stoic
with dissolve
 
"This farce is nearly complete, it’s just a matter of waiting for the vote."

show sej neutral-serious
with dissolve

sj "On a less pleasant note, friends, you all know the reputation of that manful brute."

sj "I doubt he would approve much of any of this pleasure. Big men with limp pricks tend to cloister themselves in a manly temperance."

show sej diplo-order
with dissolve

sj "Furthermore, he is a slavish disciple to the Ghosts of War. If he takes these walls I expect we will all die horribly."

stop sfx1 fadeout 0.5

"That shut them all up. Sejanus, you conjurer."

"If only you had shamed yourself as an actor or prostitute instead of as a soldier."

show sej diplo-per
with dissolve

sj "Has there ever been an easier choice in all of man's time?"

sj "In my hands are life, love and all the pleasures of power."

show sej diplo-threat
with dissolve

sj "In his hands are the spade that digs the Grave for each and every one of you."

hide sej
with dissolve

"And so he sits back down."

play sfx1 crowdmurmur fadein 5.0 loop

"It takes a minute or so for the silence to climb back to the din of furious debate."

"Though I know is it no debate at all."

show clo reg-emotional at center
with dissolve

nar "Tomorrow, they will vote. And I know what their decision will be."

nar "They will vote to make this chair mine in the eyes of the Basileus and the Ghosts."

nar "And then Sejanus will take my hand to make that Praetor's chair his."

nvl clear

nar "I need Portia right now. My dearest friend and companion would help me stay sane through all this."

nar "But she’s been missing since this morning."

nar "She always rouses me from sleep, yet at dawn it was a slave who woke me."

nar "No one will tell me anything about where Portia’s gone, and my worry grows by the minute."

nar "Everything in the whole world is worrisome right now."

nvl clear

show clo reg-stoic
with dissolve

pause (0.4)

nar "I need more to drink."

nar "A serving girl in green seems to read my mind and brings her tray over to me."

nar "She’s rather beautiful. Poor thing. It’s likely why she’s in this mess."
 
nar "She places a new cup before me and then leaves to go serve her many new masters."

nvl clear

nar "A small piece of papyrus sits under the cup, the corner peeking out just so."

nar "Sejanus does not seem to be paying attention. I lift the cup to drink and swipe the note in the same motion."

show clo reg-stoic at right
with ease

nar "Then, rising to my feet, I start for the nearest door."

nvl clear

show sej neutral-normal at center
with dissolve

sj "Something wrong, my lady?"

show clo con-angered-flip
with dissolve

c "I’m feeling a little ill."

sj "You drank too much."

show clo reg-stoic-flip
with dissolve

c "Perhaps."

show sej neutral-smile
with dissolve

sj "Do be careful in the halls. There is lusty fun being had in some of them."

sj "I wouldn’t want your virgin soul to be scarred quite yet."

show clo reg-declaring
with dissolve

c "I know to avert my eyes, Sejanus."

show clo reg-stoic
with dissolve

"I know he wants me to say \"My Lord\", but he lets me go."

hide clo reg-delcaring
with dissolve

stop music fadeout 0.5
stop sfx1 fadeout 0.5

scene HallNight
with fade

"Free of the cloying stink of traitors, I find the nearest lit torch."

show clo reg-stoic
with dissolve

"Double-checking to be sure none of Sejanus’s men are nearby, I unfold the note."

play music whale

show clo snk-worry
with dissolve

c "!"

"It’s... he..."

show clo rel-surprised
with dissolve

"How?"

nar "The note is a jumble of nonsensical phrases and words."

nar "Maybe someone skilled in these sorts of things would know better on first glance, but to almost anyone it would look like little more than babble."

nar "But I know what this is. And when I hold the note to my nose and breathe in..."

nvl clear

show gold
hide clo
show clo defen-crying
with dissolve

pause (1.0)

"{i}Him.{/i}"

"Ghosts of Love, I knew it. I knew that if Sergius was here, {i}he{/i} was here too."

hide gold
with dissolve

"I need to go to my room to read this."

hide clo
with dissolve

nar "With a quick glance around, I start in that direction."

nar "In a nearby alcove, a lord ruts with a serving girl, completely oblivious to me."

nar "I glance at them for a moment, then pass on."

nar "There is no virgin soul here for you to concern yourself with, Sejanus."

nvl clear

show black
with fade

pause (0.4)

nar "Every step of the way, through the halls and up the winding staircase to the noble quarters, the note burns in my hand, begging to be understood."

nar "A final mercy, there’s no one guarding my room."

nar "I suppose there’s no need. It's not as if there’s anything I can do to foul Sejanus’s plans in there."

nar "At least, that must be what he thinks."

nvl clear

scene ClodiaRoomNight with fade:
    yalign 0.1 xalign 0.45

pause(1.0)

show clo reg-stoic
with dissolve
pause (0.4)

nar "My attendants immediately start reaching for me as I enter, trained to dress and undress and fix me up at a moment’s notice."

nar "I wave them all away and dismiss them. They file out obediently, and I make sure to lock the door behind them."

nar "Most wouldn’t think twice of having their slaves around as they read sensitive information, but those people have forgotten that slaves have eyes and ears and tongues."

nvl clear

nar "I sit in the small writing desk by my window and light the candle."

nar "Outside, I can see the great Sergius’s fortified camp in the distance, a dark monolith of sharpened logs and the smoke of campfires."

nar "The red peaks of a Raskyan War Tent are visible even at this late hour."

nvl clear

nar "It takes me several pained minutes to untangle the words in the note, and I don’t help by pausing to inhale the scent again here and there, but eventually I have the thing deciphered."

nar "The code he invented for us, that I haven’t had the divine pleasure of unlocking for months now, gives way to his words."

nvl clear

show black
with fade

hide clo

play sfx1 quill

centered "My Clodia. I am here. Just outside the city walls I wait for you.{p}{p=0.0}In a matter of days, Sergius will have taken the city. Then he will slaughter everyone in the citadel, noble and slave and soldier alike.{p}{p=0.0}I will save you before then. I swear it. You can trust the girl who gave you this note, she is my freedwoman. Give her your response. When I read it and know you are alive and well, I will tell you my plan.{p}{p=0.0}We are so close. Soon enough I will take you in my arms, again and again and a thousand times more. {p}{p=0.0} Help me find my way to you. Be vigilant. I love you, Clodia.{p}{p=0.0} - Your Valerius"

stop sfx1

hide black
with fade

show clo defen-crying
with dissolve

pause (0.4)

"My eyes burn with tears most of the way through."

"I should probably destroy this, but I can’t. It’s unreadable to anyone else anyway."

"And if we should all die, and I never see him again, I want to have this until the end."

"This scent, the Durjan papyrus he always uses."

"I can see his face again when I smell it, remember his body sliding against mine."

"Everything from that rapturous winter floods my memory."

"Setting his note aside, I wipe my eyes and reach for a quill, planning out my response."

show clo reg-stoic
with dissolve

"It’s hard, though. I keep getting lost in memories."

"Memories from over a year ago, what most Dagarans would have considered the beginning of a dark time."

hide clo
with dissolve

show ClodiaRoomNight:
    linear 2.5 zoom 2.0

pause(3.0)

nar "When Sergius’s War Tent loomed in the distance in almost the exact same spot as it does now."

nar "When my Uncle Fausus, then-King of all Dagara, gave in to the daily hammering away at the walls and the legions of murderous Raskyans out for our blood, and surrendered the city and the war to Sergius."

nar "When we lost our independence, became nothing more than a Raskyan province, and King Fausus became Praetor Fausus."

nvl clear

$ renpy.pause(delay=1.0,hard=True)

centered "What a happy time that was for me."

stop music fadeout 2.0

show ClodiaRoomDay with dissolve:
    zoom 2.0 yalign 0.07 xalign 0.45

pause(0.5)

show text "{size=+100}{cps=0}ONE YEAR AGO{/size}{/cps}" at truecenter
with dissolve
$ renpy.pause(delay=1.5,hard=True)
hide text
with dissolve

label flashback_1:
    
play music fluff
    
show ClodiaRoomDay:
    linear 2.5 zoom 1.0

$ renpy.pause(delay=3.0,hard=True)

show ClodiaRoomDay with ease:
    zoom 1.0

show por neu-neu-flip at right
with dissolve

p "Well, at least they don’t seem to be killing everyone."

show clo reg-stoic at left
with dissolve

c "At the moment."
 
"From my window, we could see the famed legions marching through the dirty streets."

"Dirty, but not bloody. No fires, no murders, no rapes."

"That we could see."

"All the same, thousands of the most dangerous men in the world were walking straight towards us."

"And all we had was their word and their honor that we would not die beneath their heels."

show clo con-angered
with dissolve

c "They may be waiting to begin their pillaging."

c "If they don’t kill Uncle when they get here, then I’ll start to relax."

show por neu-distaste-flip
with dissolve

p "He surrendered. Going back on that would be like shitting on the Ghosts."

show clo rel-neutral
with dissolve

"I smiled, despite everything. Portia's tongue has always reflected her propensity for spending time with stable boys."

show clo reg-stoic
with dissolve

c "This is the end of Dagara. One way or the other."

show por neu-smile-flip
with dissolve

p "Now don’t get so gloomy, Clodia. Living under a Raskyan boot is still living."

show clo cha1-sexy
with dissolve

c "I suppose you have experience with Raskyan boots?"

show por con-gossip-flip
with dissolve

p "Aye, taking them off Raskyan feet."

show por con-sigh-flip
with dissolve

p "Though I suppose not all of those boys down there would be satisfied with just a foot rub."

show clo reg-stoic
with dissolve

c "No. These are the same men who’ve sacked twenty villages since I was a girl."

play audio knockdoor

pause(0.5)

show clo reg-stoic-flip
show por neu-neu-flip
with dissolve

c "What is it?"

"A slave entered, holding out a note. Portia took it and dismissed him."

show clo reg-stoic
show por neu-neu-flip
with dissolve

p "Let’s see:"

show por plan-explain-flip
with dissolve

p "\"Lady Clodia, please bathe and dress for a banquet this evening, we will dine with Sergius and his tribunes.\""

p "\"Fear not, we are getting the best deal we could possibly get. All will be well. Fausus.\""

show por neu-neu-flip
with dissolve

p "Hmm. Not King Fausus?"

c "He’s already had to renounce his crown, I’m sure."

show por neu-distaste-flip
with dissolve

p "So that means you’re no longer a princess?"

show clo rel-neutral
with dissolve

c "I was barely one to begin with."

nar "Uncle Fausus was not my blood uncle, but my aunt’s second husband."

nar "He had three sons and five daughters across his various marriages, and after them another four nephews, all blood related."

nar "For an orphaned niece like me to have been granted the title of Princess in the first place had always been a kind show of pity more than anything else."

nar "I had the slaves and the dresses and the luxuries, but in truth, I was almost as far away from the heart of power in Dagara as any common girl."

nar "I often thanked the Ghosts for that."

nar "Now if I was to become yet more distant from the responsibilities of royalty, I could hardly mind."

nar "I just wanted to be sure I would live."

nvl clear

show por neu-neu-flip
with dissolve

p "Well... I guess I’ll have a bath drawn for you."

"She nodded her head to set the attendants to work."

hide por
with dissolve

show clo reg-stoic at center
with dissolve

"I was to dine with the men who had killed thousands upon thousands of my countrymen."

nvl clear

stop music fadeout(3.0)

scene black
with dissolve

centered "My stomach was twisted with nausea, my mind with nerves."

pause(1.0)

scene HighHall
with fade

play sfx1 crowdmurmur fadein 3.0 loop
play music timekiller

pause(1.0)

nar "Eating did little to settle the nausea."

nar "Drinking {i}was{/i} helping the nerves, though."

nvl clear

nar "How exactly could we trust Raskyan wolves? Surely my uncle knew who he was dealing with."

nar "A people who had gone from a ramshackle community of criminals, deportees and exiles, to the masters of all the land from here down to the Central Sea."

nar "How many Raskyan cities were once their own peoples? Until the legions swept through and made everyone Raskyan with the sword?"

show clo reg-stoic
with dissolve

nar "In a hundred years, would anyone remember Dagara had once been free?"

nvl clear

show por neu-neu-flip at right
with dissolve

p "You should eat if you're going to drink so much."

show clo rel-neutral
with dissolve

c "{i}You{/i} should drink at all. I don't know how you're dealing with this sober. You look so frustrated."

show por neu-distaste-flip
with dissolve

p "That is because I have to spend time with my husband."

show clo cha1-sass
with dissolve

c "Careful, he’ll hear you."

show por neu-distaste
with dissolve

"He was unlikely to care though. He leaned pointedly away from his wife and into the bosom of a serving girl, wine stains on his toga."

show por con-sigh-flip
with dissolve

p "Ghosts of Lust, I need a boy to fuck. That would make this all less bitter."

show por con-gossip-flip
with dissolve

p "Hmm. Maybe I can get one of our conquerors to conquer me?"

show clo cha2-joy
with dissolve

c "If you feel like trying."

show por con-sigh-flip
with dissolve

p "Ugh, so many of them are ugly. Where are the dashing soldiers?"

show clo rel-neutral
with dissolve

"All told, Portia was dealing with the situation far better than I."

"I didn’t have a husband to compete with for affairs, another blessing from being so unimportant to the royal workings."

show clo cha1-sexy
with dissolve

c "Hmm. You don’t like the Great Sergius?"

show por neu-worried-flip
with dissolve
   
p "He’s terrifying, please. Looks like he would break me in half at the first thrust."

show clo cha2-joy
with dissolve

c "I would have thought he was exactly your type."

show por neu-neu-flip
with dissolve

p "I don’t like beards."

show clo cha3-sass
with dissolve

c "At least you’re being honest now."

show por con-sigh-flip
with dissolve

p "Ugh. I guess I’ll go visit the jailer again tonight, his silly ass is always ready for a tumble. Bless you, Clodia, for having higher standards than me."

show clo con-charming
with dissolve
   
c "That’s nice of you to say."

hide por
with dissolve
                                
"Higher standards. That was a kind way of saying that our situations were completely different."

show clo reg-stoic
with dissolve

"Mutual adultery was one thing, all well and good as far as nobles were concerned so long as it was kept private-ish." 

"But I was not married. I had never been important enough to be married, and with no father to insist on getting rid of me, I had no one pushing me towards it."

"If I gave away my maidenhead before marriage, and was found out, I would be lucky to get away with shaming and nasty looks the rest of my life."
                                                                                                                                                 
"No thank you."

"And perhaps..."

"I simply didn’t like the handful of men I’d met in my life."

show por con-sigh-flip at right
with dissolve

p "Oh you poor thing. Maybe this is my fault."

show clo con-charming
with dissolve

c "How so?"

show por con-gossip-flip
with dissolve

p "If I hadn’t brought you all those dirty Durjan poems in the first place, maybe you wouldn’t be holding out for an erotic poet."

show clo con-surprised
with dissolve

c "Never apologize for that, please."

c "Without you and those poems I wouldn’t know one thing about what goes on between my legs."

show por con-laugh-flip
with dissolve

p "Haha. Careful, you’ll draw attention."

show clo rel-neutral
with dissolve

"The wine cup in my hand was less full than I remembered."

"I guess I wasn't being very responsible."

stop sfx1 fadeout 1.0
stop music fadeout 1.0

pause 0.6

show por neu-neu-flip
with dissolve

p "What's going on?"

show clo reg-stoic-flip
with dissolve

c "Hmm?"

"The hall had gone silent."

hide clo
hide por
with dissolve

play music nostalgia

show ser neutral-serious at fleft
with dissolve

"Everyone’s attention was on Sergius, who had risen to his feet. My uncle sat next to him, silent and stone-faced as usual."

show ser com1-barking
with dissolve

s "Nobles of Dagara. The Basileus and Senate of Raskya wish to thank and bless each and every one of you for your cooperation."

s "Your former King, in particular, saved all your lives with his surrender."

"He certainly sounded like a Raskyan legatus."

show ser neutral-serious
with dissolve

s "As Fausus clearly has the well-being of you and your citizens foremost in his mind, the Basileus has seen fit to nominate him to be your first Raskyan Praetor."

"The room rippled with applause. My uncle was well loved by Dagara, keeping him in charge was a good idea for everyone all around."

"But what was this nominating business?"

show ser com1-barking
with dissolve

s "Dagara has been a kingdom since time immemorial. But as a province of Raskya, you have been invited to join in a superior form of government."

show ser com1-boasting
with dissolve

s "A Republican Kingdom is something greater, higher, nobler than one where the King is nothing more than a tyrant."

s "In a Republic, men vote."

show ser com2-boasting
with dissolve

s "And so, the Raskyan Senate has voted unanimously to elect Fausus as your Praetor."

s "All of you, Hail Praetor Fausus!"

"More applause, and one by one, cries of \"Hail!\" Uncle rose to his feet, hailing his people back."

"I’m sure there were plenty of nobles who hated this, but what could they do? What could anyone do against men with swords?"

stop music fadeout (2.0)

hide ser
with dissolve

play music timekiller fadein (2.0)

nar "Soon enough the room went back to the low rumble of general chatter, and just like that, we were Republicans."

nar "How much more noble and just. Rather than having one ruler who knew us and our problems, we bowed to a group of strangers a hundred miles away."

nar "Ghosts, I felt awful."

nvl clear

show clo snk-serious
with dissolve

c "Do you think anyone will notice or care if I leave?"

show por con-sigh-flip at right
with dissolve

p "Likely not. I’m sadly stuck here with the husband."

show clo snk-relief
with dissolve

c "I’m sorry, dear friend. Good luck."

show por neu-neu-flip
with dissolve

p "Rest well, love."

hide por
with dissolve

show clo snk-serious
with dissolve

"Rising to my feet, I made my way for the door, moving around servants and soldiers and a whole interesting series of colors and shapes."

"When I reached the door, I found it wobbling."

stop sfx1 fadeout (1.5)
stop music fadeout (1.5)

scene HallNight
with fade

show clo snk-serious at right
with dissolve

"I just barely made it out into the hallway, when..."

show black
with dissolve

play sound puke

"All the wine and what little food I’d put in with it came spilling out of me."

"I made it to a window at least, upchucking my noble guts into the dark courtyard below."

"Portia warned me to eat. Stupid me."

hide black
with dissolve

show clo snk-serious
with dissolve

show sol neu-neu at sleft
with dissolve

"I think I heard snickering behind me."

"Slaves would never dare, so I knew it was a soldier."

so "Poor Dagaran girl had too much to drink."

so "Let it out, darling, let it all out. I'm here for ya."

show clo reg-emotional
with dissolve

"Impudent."

"As I wiped my mouth and straightened up, I tried to not look at him."

"To not let him see my tears."

"I wouldn’t have cried over something so stupid, except that everything was so awful already."

show val rel-normal-flip
with dissolve

play music whataday

v "Hail, legionary."

show clo snk-worry-flip
with dissolve

show sol neu-confuse
with dissolve

so "Shit, uh, Hail Tribune!"

v "Exactly why are you bothering this woman?"

so "I wasn’t bothering her sir, just a bit of flirting."

show val ac-fact-flip
with dissolve

v "You know that she is a Raskyan noblewoman now?"

show clo snk-relief-flip
with dissolve

v "I suggest you go back to annoying girls after you pay them, you’ll find much more success that way."

show sol neu-neu
with dissolve

so "..."

show val ac-annoyed-flip
with dissolve

v "Get."

hide sol
with dissolve

"With a great show of reluctance he shuffles off."

show clo rel-neutral-flip
with dissolve

show val ac-grin at fleft
with dissolve

v "Are you feeling all right, my lady?"

show clo con-charming-flip
with dissolve

c "Uhh... ah, yes. Better. Thank you."

"Much better."

show val rel-grin
with dissolve

v "Good. I apologize for him. The rank and file can be brutish."

show val rel-serious
with dissolve

v "Actually, I take that back, you’ll find that in all ranks of men."

show clo rel-neutral-flip
with dissolve

c "I’ve learned that lesson many times before."

v "A sad reality. I can escort you back to your husband, if you’d like."

show clo con-charming-flip
with dissolve

c "Oh. I’m... I don’t have one."

show val rel-normal
with dissolve

v "Ah. I’m sorry."

c "Don’t apologize, it’s not really a bad thing. Less headaches."

show val rel-grin
with dissolve

v "Ha, I suppose. Are you sure you’re all right? You look shaken."

show clo con-surprised-flip
with dissolve

c "I... ah..."

"He was right. I was."

"And it was all his fault."

"What was I to do?"

show clo snk-serious-flip
with dissolve

"Where was Portia when I needed her? Somehow years of dirty poems had not prepared me for being faced by a man like this."

show val rel-normal
with dissolve

show clo reg-stoic-flip
with dissolve

"Wait..."

"I needed to calm myself. This man was some kind of leader in the Raskyan legion."

"I forgot what they were called, but the symbol engraved on his armor was proof of it."

show clo con-angered-flip
with dissolve

c "After what your men have done to this land, I should be glad I'm only shaken."

show val ac-annoyed
with dissolve

v "That's quite understandable. We destroyed your country."

show clo con-surprised-flip
with dissolve

c "You admit it so easily?"

show val rel-normal
with dissolve

v "Of course. That's what the Raskyan armies do. No sense in pretending otherwise."

show clo reg-stoic-flip
with dissolve

c "You have a say in what Raskyan armies do, you're one of their leaders."

show val rel-serious
with dissolve

v "Oh, I'm a Tribune, true."

v "But first I was a scared young man forced out of his home at spearpoint."

v "And the best way to stay alive since then has been to get promoted."

show val rel-emotional
with dissolve

v "After six years of this foolishness, I can't wait to go home."

show clo rel-surprised-flip
with dissolve

"He was strange for a Raskyan. Or lying."

show val rel-normal
with dissolve

v "Why are you looking at me like that?"

show clo reg-stoic-flip
with dissolve

c "Raskyans love battle. I've heard the stories of what you've done."

show val rel-grin
with dissolve

v "What is it Dagarans love, then?"

show clo rel-angered-flip
with dissolve

c "What... that's not the point."

show val rel-normal
with dissolve

v "Listen... I don't mean to frustrate you."

v "Your people have lost a tremendous amount to mine. There's nothing I can say that will change that."

show clo rel-neutral-flip
with dissolve

c "Yes. You're right."

show val ac-fact
with dissolve

v "But now that the violence is ended, we're all Raskyans."

show clo reg-stoic-flip
with dissolve

c "Somehow I doubt my people are full citizens."

v "Well... that does take a few years. The process is archaic and stupid."

show val rel-normal
with dissolve

v "My people are cruel to the conquered. And I don't know how to stop it. I'm sorry."

"He wasn't lying."

"I wanted to believe he wasn't lying."

show clo rel-neutral-flip
with dissolve

c "Well... I suppose that Sergius is the real architect of this war."

v "And Avalaran."

show clo reg-stoic-flip
with dissolve

c "Who?"

v "Oh, the Basileus. The current one is a whelp that goes by Avalaran."

c "Right, your King."

v "Something like that."

show val rel-grin
with dissolve

v "Anyway, I'll escort you somewhere you'll be safer from my countrymen."

show clo rel-neutral-flip
with dissolve

c "That would be kind of you. I would like to go to my chambers."

show val rel-grin
with dissolve

v "As you wish. Lead the way."

"That smile."

show black
with dissolve

centered "It came from a man who helped conquer my people."

centered "It made me feel like everything was going to be okay."

scene HallNight
with fade

show clo reg-stoic-flip at left
with dissolve

show val rel-normal-flip at fright
with dissolve

nar "I led him back to my room in silence."

nar "The walk was painful. I kept wanting to look back at him."

nar "Our footsteps counted the time of our journey like a torture."

nar "At last, we came to my room."

nvl clear

scene HallNight
with fade

show clo reg-stoic at mleft
with dissolve

show val rel-normal-flip at fright
with dissolve

c "Ah. Here it is."

show val rel-grin-flip
with dissolve

v "You must be close with Fausus’s family to live in the citadel."

show clo rel-neutral
with dissolve

c "Ah, sort of. I’m the King’s... uh, the Praetor’s, niece."

show val rel-normal-flip
with dissolve

v "Hmm. Well, I assure you that your life should continue to be quite comfortable under Raskyan governance."

show clo rel-charming
with dissolve

c "That’s nice to hear... um..."

show val rel-grin-flip
with dissolve

v "Yes?"

c "I’d like to know your name."

v "Valerius."

c "Clodia."

"And in my mind, I could hear Portia’s voice, barely containing her excitement:"

show por con-gossip at ffleft with dissolve:
    alpha 0.6

p "Invite him in."

hide por
show clo con-surprised
with dissolve

c "Ah..."

show val rel-normal-flip
with dissolve

v "... Well, if there’s nothing else you need, I should get back to the hall."

v "Standing around like this is how false rumors start."

show clo con-charming
with dissolve

c "Uh, yes. Yes, we wouldn’t want that."

c "Thank you for your virtue, Valerius."

show val rel-grin-flip
with dissolve

v "Thank you for yours, Clodia."

hide val
with dissolve

show clo rel-neutral
with dissolve

"As I watched him walk away, I felt as if my thoughts were not very virtuous at all."

scene ClodiaRoomNight
with fade

play music whale

nar "So..."

nar "The land had been taken."

nar "Our royalty had been taken."

nar "Our honor was gone."

nar "And with no honor... there was none more to lose."

nvl clear

nar "Ghosts."

nar "I wanted something good and comforting amidst all the uncertainty. What was wrong with that?"

nar "Why shouldn’t one of those sensual Durjan songs come to life?"

nar "It was worth a prayer, at least."

nvl clear

nar "The altar by my bed had various baubles connected to the Ghosts."

nar "Lit incense and a simple offering of grain or fruit was enough in Dagara, we had no need of the ostentatious sacrifices of the Raskyans."

nar "So I lit the incense and crushed a handful of grapes into a wooden bowl and said my prayer clasping a tiny, pink, heart shaped stone."

nar "A stone that, as a maiden, I had no right to own. Another secret gift from Portia."

nar "A stone that strengthens prayer to the Ghosts of Love."

nvl clear

nar "As I laid in my bed and shivered and shook, burning under the covers, the lines of one poem came to the front of my mind, clear and sweet."

nar "It was about Raskyans. I had never understood it before. It never seemed to make sense."

nar "But what if it was true?"

nvl clear

show black with fade

centered "Scarlet as the sunset the Raskyan marches on{p}’Liberating’ son from his mother{p}Yet tame and teach him in your bedroom once the war is won{p}And he loves and fucks you like no other"

pause(1.0)

show text "{size=+100}{cps=0}NOW{/size}{/cps}" at truecenter
with dissolve
$ renpy.pause(delay=1.0, hard=True)
play sound knockdoor
$ renpy.pause(delay=1.0, hard=True)
hide text
with dissolve

scene ClodiaRoomNight
with dissolve

play music downtime

show clo snk-worry-flip
with dissolve

c "Uh, what is it?"

so "Sejanus wants to know if you're feeling well, my lady."

show clo snk-relief-flip
with dissolve

c "Ah, I am. Tell him I'll be down soon."

so "Very well."

show clo snk-serious-flip
with dissolve

nar "I listen until the soldier's footsteps are gone."

nar "Enough of the past. I need to focus on the now."

nar "If I write a response quickly, maybe I can slip it to that serving girl down below. Valerius says she can be trusted."

show black
with fade

centered "Please Valerius. I want to see you again."

centered "Please hurry."

nvl clear

stop music fadeout (1.0)

$ renpy.pause(1.0,hard=True)

play movie("images/Video/Vtrans0.mkv")
$ renpy.pause(2.0,hard=True)
pause(3.5)

stop movie

label strategy_meeting:

scene TentDay
with fade

play music planning

pause(0.5)

nar "The morning sunlight is cold like a knife."

nar "Bless the Ghosts of Fire for the braziers being stoked in the war tent."

show ser neutral-serious-flip
with dissolve

nar "Sergius and his other tribunes are crowded around the table, the best map of the city we’ve been able to acquire laying tattered before us."

nvl clear

show ser neutral-serious-flip at sergright
with dissolve

show val ac-fact at fleft
with dissolve

nar "Meanwhile I sit in the corner. By choice, at least."

nar "I’ve studied that map to death already, and there’s nothing they’ll see on there that won’t ultimately translate to a desire to smash the walls down and destroy everyone."

nar "If I listen close, I can hear the distant sawing and hammering of yet more siege engines being birthed."

nvl clear

show ser com1-barking-flip
with dissolve

s "The south wall is definitely the weakest, we hammered the Grave out of it last year and they never got around to fixing it."

s "We’ll pound that as soon as the ballista battery is ready."

show ser neutral-grin-flip
with dissolve

s "It will be the first of many poundings."

show val ac-annoyed
with dissolve

"All the tribunes chuckle along. Sometimes I wonder if I would be regarded more highly if I simply pretended to laugh more."

tr "We could hurl poxy corpses over the walls, soften them all up. Never a shortage of poxy corpses."

show ser neutral-serious-flip
with dissolve

s "Ah, nonsense. Then when we take it, half the men will come down with the rot just slaying what’s left of the traitors."

show ser neutral-grin-flip
with dissolve

s "This is to be a gift to War, not Plague. No glory in victory if we’re all shitting blood in our armor."

show val rel-normal
with dissolve

"More laughter."

"I am deliciously invisible to all of them. But I should make my presence known."

"Particularly with the tribunes all assembled like this."

show val ac-fact
with dissolve

v "So everyone inside is to die? Raskyan nobles, men, women and children all?"

show ser neutral-annoyed-flip
with dissolve

"Now all their hard eyes are on me. What does this glorified farm boy have to say to them?"

s "If they had been true Raskyans, they would have died to the last defending their home from Sejanus."

s "But the cowards simply let him in the gates with barely a struggle."

show ser ac-annoyed-flip
with dissolve

s "It is nothing on my mind. We already gave them a chance to change from Dagaran to Raskyan."

show val rel-emotional
with dissolve

"Nods from the other tribunes. And nothing but disdain for me."

show ser neutral-serious-flip
with dissolve

s "The ancient law of Raskya is clear about conquered peoples: give them a chance to govern themselves, and if they can’t handle the responsibility, take it from them."

s "That is simply how it has always been."

show val rel-normal
with dissolve

"Funny how Raskya has never once conquered a people it deemed worthy of self-governance in the end."

v "Always? I believe you are thinking of those days centuries gone where the Basileus was merely a humble warrior king, keeping us safe while leaving the laws to the Senate."

v "Now, the law of Raskya is whatever the Basileus's whims deem it to be."

v "You all know we don't have his blessing yet. This whole siege of ours is grossly illegal. You just all want the plunder of Dagara for yourselves, regardless of Raskyan honor."

show ser neutral-annoyed-flip at right
with dissolve

show val ac-fact
with dissolve

"Shit, have I gone too far?"

s "I’ve honored your nonsense this morning so far out of respect for Republican ideals. But you listen to me well, Tribune."

s "Plunder is for the men. Plunder is for the Tribunes. {i}Glory{/i} is for a legatus."

s "Glory is all I am here for."

s "Men who care most about money end up like Sejanus. They chase it straight into the Grave."

s "You think I don’t know how you got where you are, pleb?"

show val ac-annoyed
with dissolve

"The look in his eyes is vicious."

"I can feel him staring right through me."

"Right through the armor and rank and straight into the shit covered farm boy he always sees when he looks at me."

s "Enough. Spreading doubt is not helpful."

s "In fact, it’s borderline treason. You leave this room right now or I'll put you in the Grave myself."

show val rel-normal
with dissolve

"No thanks. Besides, I’ve already said what I needed to say."

v "Blessings of War upon you all."

hide val
with dissolve

"Leaving their villainous faces behind, I head for my room."

label flashback_2:

stop music fadeout 2.0

scene ValeriusRoom
with fade

play music downtime

"Amidst the usual mess of servants I have to banish from my room when I return to it is the one I’ve been waiting for."

show nas2 rel-defaul-flip
with dissolve

"The girl reclining on my bed as if it were hers has a note rolled up in her hand."

show nas2 rel-defaul-flip at right
with dissolve

show val rel-normal at left
with dissolve

v "You found her?"

show nas2 rel-grin-flip at right
with dissolve

"The girl looks up at me and smiles, the sort of mask I’ve come to expect from her."

"She sits up, yawns, and unfurls the note, dangling it from her fingers like a lure for a beast."

n "I did. Her reply smells good. I can see what you like about her."

show val rel-serious at left
with dissolve

v "Give me that."

show nas2 rel-blank-flip at right
with dissolve

"She offers no resistance as I snatch the note from her."

"And, well, she did say it smelled good, so I take a deep whiff."

show red
hide val
show val rel-emotional at left
with dissolve

pause (1.0)

"Red."

hide nas2

"The red, sweet scent of roses hits me like a javelin through the heart."

"My mind dips helplessly into the pools of the past."

show nas2 rel-grin-flip at right
with dissolve

n "Woah, relax yourself, Sir. I think I hear your heart pounding."

show val rel-serious at left
hide red
with dissolve

v "Not right now, Nasrin."

show nas2 arm-blank-flip at right
with dissolve

"I no longer really have the legal right to silence her, but thankfully she’s feeling obedient."

show nas2 rel-blank at right
with dissolve

"With a shrug, she lays back on my bed, stretches like a cat, and closes her eyes."

hide nas2
with dissolve

show val rel-serious-flip
with dissolve

"I rush to my writing desk and begin to decipher Clodia's response."

"Memories rush through me as I read, vivid and firey and red."

show val rel-emotional-flip
with dissolve

"{i}Clodia...{/i}"

stop music fadeout 2.0

scene black
with fade

pause(0.5)

show text "{size=+100}{cps=0}ONE YEAR AGO{/size}{/cps}" at truecenter
with dissolve
$ renpy.pause(delay=1.5,hard=True)
hide text
with dissolve

pause(0.5)
 
centered "Six years ago... Raskya declared war on Dagara."

nar "It was a long time coming, little things building up over the years."

nar "Everyone knew it was inevitable, and when the Blessings of War echoed across the land, Gaius Argus Sergius went on campaign against the Dagaran ’barbarians’ at the northwest border."

nar "I was a heaving, scared, pissed off conscript, surrounded by men who all seemed bigger and stronger and braver than me."

nar "Not to mention frightening killers who had been doing this sort of thing for a decade."

nar "The daily drilling and marching did not suit me, I longed to return to my farm, and every day I looked forward to the implicit reward of being a soldier which never came:"

nvl clear

centered "Girls."

nar "Even when they pluck you from your fields and put a sword in your hand, they still feed you all the poison honey they can to keep you from deserting."

nar "In the dreams they sell, the camp is a riotous nightly orgy."

nar "Beautiful virginal camp followers throw themselves into the arms of every man and rub his back and feet every night and fuck him like the nymphs straight from the myths."

nar "Well, none of that shit is true."

nvl clear

nar "The legatus and his tribunes get all the prettiest and poxfree whores and slaves."

nar "What wanders the camps at night is not what men have wished since they were boys to hold in their arms."

nar "As for the village and farm girls, they can be doe-eyed and fawning, but for some reason their fathers tend to be men of influence who don’t enjoy when their property is fucked."

nar "I learned quickly that the grunts like me only have one real opportunity to fuck pretty girls without consequence:"

nvl clear

centered "A good sacking."

nar "But whatever part of me found such an adventure appealing died when we took our first town."

nar "Perhaps I am a weak man, but everyone running around, stabbing, killing, pulling girls and boys this way and that, setting things on fire..." 

nvl clear

centered "It frightened me."
                   
nar "Only carrying off gold and loot that didn’t fight back suited me."

nar "It was easy and bountiful, especially while all the strongest, blood-drunk men were busy fighting over the girls."

nvl clear

nar "For five years I ran off with the gold while towns burned behind me."

nar "For five year I bribed myself higher and higher up the ladder."

nar "And by the time the war was practically over I had became a tribune. Too late to help much, I'm sure Sergius noticed."

nar "But I didn't care. I wasn't there to help."

nar "I didn’t want tribune for the glory of leading battle, or the honors of high office when the war was won."

nar "None of that suited me at all."

nvl clear

centered "But a tribune could have those pretty and poxfree girls every night."

nar "Tribunes were known for their rooms in the War Tent being more like brothels, and I planned to host the greatest orgies of the most beautiful girls I could, to fulfill my wildest farm boy dreams at long last."

nar "I suppose then that the Ghosts of Luck and Love were laughing at me when we garrisoned for the winter in our newly conquered city of Dagara, and I met that beautiful girl in red."

nvl clear

pause(1.0)

show StreetsNight
with fade

pause(1.0)

nar "The camp was set up in the city square, the tents spilling into the alleys and creating the usual soldier town, a canvas landscape of prostitutes, peddlers and other such businessmen eager for opportunity."

nar "Opportunities that included fever, the red shits, and general plague."

hide StreetsNight
show black
with fade

nar "Lucky me, as a tribune I got to stay in the War Tent, segregated from that hell."

scene HighHall 
with fade

play music timekiller
play sfx1 crowdmurmur loop

pause(1.0)

nar "And every night, I got to enter the citadel and feast at the high table with Sergius and the Praetor of Dagara."

show val ac-fact at mleft
with dissolve

nar "At the {i}end{/i} of the high table, anyway. But that turned out to be just fine."

nar "One night, I found myself next to a familiar woman."

nvl clear

show clo con-charming-flip at mright
with dissolve

c "Good evening, Tribune Valerius."

show val rel-pleased
with dissolve

"It was a pleasant shock to see her lovely face again."

"And so {i}close{/i}."

show val rel-grin
with dissolve

v "Ah, evening, lady Clodia. You look well."

show clo cha2-joy-flip
with dissolve

c "I feel much better too."

show val ac-pleased
with dissolve

v "I can see that."

show clo cha2-sass-flip
with dissolve

c "Well, no ruffians will bother me tonight, so I feel just fine."

show clo cha3-sass-flip
with dissolve

c "As long as you protect me."

show val rel-grin
with dissolve

"Oh Lust. Look what you've sent me."

v "Make sure to keep near, then."

show clo cha1-joy-flip
with dissolve

c "Gladly."

show clo cha2-sexy-flip
with dissolve

c "Are you enjoying my uncle’s food, Valerius?"

show val rel-pleased
with dissolve

v "Oh, yeah, it’s good. Are you enjoying his wine?"

show clo cha3-sexy-flip
with dissolve

c "Why not? The war’s over, it’s time to relax."

show val ac-pleased
with dissolve

v "Ha. I agree. Suppose I should have more."

"It was endearing, in a way, to see this noblewoman handling her alcohol as well as any tavern girl."

"I supposed that old joke about blueblood girls having little else to do rang true."

show val rel-pleased
with dissolve

v "Your name, by the way. Clodia."

show clo cha1-sass-flip
with dissolve

c "Mmhm?"

v "We have a similar name in Raskya. Claudia."

show clo cha1-sexy-flip
with dissolve

c "It sounds like the same name said in a different accent." 

c "I guess many things between Dagara and Raskya are only different to such small degrees."

show val rel-grin
with dissolve

v "To be sure. Your dress is lovely, it wouldn’t look out of place on the Basileus’s wife."

"Well, our young Basileus had no wife yet, but my comment still stood true."

show clo cha1-joy-flip
with dissolve

c "Mmm, thank you, Tribune Valerius. You have such nice things to say."

show val rel-pleased
with dissolve

"The half-empty wine glass swirled in her hand. Her face was a blush that complimented her dress."

"Her beautiful face."

show clo cha3-sexy-flip
with dissolve

c "You must get a lot of girls as a tribune?"

show val rel-normal
with dissolve

v "Haha, ah, perhaps that’s not something a lady should concern herself with."

show val rel-normal-flip
with dissolve

"I cast my eyes briefly over at the Praetor, busy telling some jest to Sergius at the head of the table."

"How quickly the ones who make war can forget it."

show clo rel-surprised-flip
with dissolve

c "Oh, don’t worry about my uncle. He and my aunt don’t give a single shit about me, really."

show val rel-pleased
with dissolve

v "You have an interesting mouth for a lady."

show clo cha3-sexy-flip
with dissolve

c "Just what kind of mouths do you expect ladies to have?"

show val ac-grin
with dissolve

v "Well, most of them aren't exactly taught to be great conversationalists."

show clo cha1-sass-flip
with dissolve

c "Ugh, tell me about it. My friend Portia's wonderful, but otherwise a good noblewoman is the dullest person alive."

show val ac-pleased
with dissolve

v "So how did you end up being such a bad noblewoman?"

show clo cha2-sass-flip
with dissolve

c "Have you ever read Durjan love poetry?"

show val rel-grin
with dissolve

v "Oh. You like those kinds of poems, huh?"

show clo cha3-sexy-flip at centerright
with dissolve

c "Oh, I {i}love{/i} them."

"Something touched my boot under the table."

"Her eyes batted at me over her glass."

"By Lust. What was she playing at? This was remarkably... forward, for a noblewoman. Wined up or otherwise."

"It was refreshing."

show val ac-pleased
with dissolve

v "How many poor boys have you gotten exiled over the years?"

show clo cha2-joy-flip
with dissolve

c "None. How would I do that? I would have to like a boy first to get him exiled."

v "I can't believe you've never liked a boy."

show clo cha2-sass-flip
with dissolve

c "It hasn't been a common occurrence."

show val rel-normal
with dissolve

v "I suppose the real thing can never live up to the heroes from the poems."

show clo con-charming-flip
with dissolve

c "Usually."

show val rel-grin
with dissolve

v "So no sneaking off with a handsome plebeian boy?"

show clo con-surprised-flip
with dissolve

c "Plebeian?"

show val rel-pleased
with dissolve

v "Ah, commoners, you would say."

show clo cha2-sass-flip
with dissolve

c "Well no thank you. He would have to bathe more than once a year, which is cruelly uncommon for commoners."

show val rel-normal
with dissolve

v "Here, maybe. Most Raskyans bathe daily. Even the plebs."

show clo con-charming-flip
with dissolve

c "I've heard that. Well, if we get public baths built here, it will be at least two good things that came of this invasion."

v "The first being?"

show clo cha3-sexy-flip
with dissolve

c "Meeting a handsome plebeian boy who bathes."

show val ac-grin
with dissolve 

v "I..."

show val ac-fact
with dissolve

v "Wait, how could you tell?"

show clo con-charming-flip
with dissolve

c "Well you smell very good."

show val ac-pleased
with dissolve

v "Thank you, but I mean-"

show clo cha2-joy-flip
with dissolve

c "Ha, I know, I know. I just guessed. I've never heard a nobleman defend the lower classes. Especially concerning hygiene."

show val ac-fact
with dissolve

v "Now that I believe. My fellow tribunes are all nobles and they never let me forget it."

show clo cha2-sass-flip
with dissolve

c "I'm sure it's quite a story to explain how you became a tribune at all."

show val rel-emotional
with dissolve

v "Well, not really. I paid the right people off."

show clo con-charming-flip
with dissolve

c "Oh, so you just did exactly what nobles do. How clever, ha."

show val rel-normal
with dissolve

v "I suppose few things are more nakedly plebeian than desperately copying the habits of your social betters."

show clo cha2-sass-flip
with dissolve

c "Well, I have a funny thought about that."

show val rel-pleased
with dissolve

v "What do you mean?"

show clo cha3-sass-flip
with dissolve

c "Did you know whores all spend a great deal of money to have the hair plucked from their legs and underarms?"

show val ac-fact
with dissolve

v "Yes, I've... I knew that."

show clo cha2-sass-flip

c "Do you know why they do it?"

show val rel-normal
with dissolve

v "Sure, they're copying noblewomen."

stop music fadeout (1.0)
stop sfx1 fadeout (1.0)

show clo cha3-sexy-flip
with dissolve

c "Well, here's a secret..."

show clo cha1-sexy-flip
with dissolve

"Her hand touched mine."

show val rel-pleased
with dissolve

"Guided it down to soft fabric."

show val rel-pleased at centerleft
with dissolve

"And then smooth, bare skin."

show clo cha3-sexy-flip
with dissolve

c "It's actually us who're copying them."

"By Lust, what kind of nymph was she?"

show clo rel-neutral-flip at mright
with dissolve

show val rel-normal at mleft
with dissolve

play music timekiller
play sfx1 crowdmurmur loop

"Her leg drew back from my touch, and just as quickly I returned my hand to the table."

show clo reg-stoic-flip
with dissolve

c "Ghosts, I'm... probably embarassing myself."

show val rel-grin
with dissolve

v "Not at all. I'm having a great time."

show clo rel-charming-flip
with dissolve

c "Me too."

show val rel-pleased
with dissolve

v "Would you, ah, like to go somewhere more quiet?"

show clo con-charming-flip
with dissolve

c "I think we absolutely {i}must{/i} if we don't want to get ourselves in trouble."

show val rel-grin
with dissolve

v "Of course, as one of the citadel's noble inhabitants, it would be prudent of you to give me a tour."

show clo cha2-sass-flip
with dissolve

c "Well then, right this way, Tribune Valerius."

show black
with dissolve

stop music fadeout (1.0)
stop sfx1 fadeout (1.0)

scene HallNight
with fade

play music whataday

pause(1.0)

show clo rel-neutral at mright
with dissolve

show val rel-normal at fleft
with dissolve

v "So, where to?"

show clo rel-charming
with dissolve

c "Hmm... not sure. The citadel is pretty big, there's a lot of sights to see."

show val rel-grin
with dissolve

v "I know what I'd like to see."

show clo rel-neutral-flip
with dissolve

c "Oh?"

show val rel-pleased
with dissolve

v "Dagaran glass. I've heard so much about it for the last six years and haven't seen so much as a shard of it. I'm starting to believe it's just a myth."

show clo cha2-sass-flip
with dissolve

c "Oh, it's very real! There's just not much of it. But I can show you some... later."

show val ac-grin
with dissolve

v "Later?"

c "Yeah, just trust me."

show val rel-pleased
with dissolve

v "Ha, very well. You've built it up now, I hope it's as impressive as they say."

show clo con-charming-flip
with dissolve

c "Hmm hmm. We'll see, won't we? In the meantime..."

show clo at right
with ease

"She sat down on a nearby couch, and gestured for me to join her."

show val ac-pleased
with dissolve

v "The tour starts at this couch, huh?"

show clo rel-neutral-flip
with dissolve

c "I may have made a mistake."

show val rel-normal
with dissolve

v "Drank too much?"

show clo reg-stoic-flip
with dissolve

c "It's a problem I've had ever since my home was invaded by violent men."

show val ac-fact
with dissolve

v "Well, fair. Do you need to go to sleep?"

hide clo
show clo rel-neutral-flip at right
with dissolve

c "No, not yet. I just need to rest for a moment."

show val at center
with ease

show val rel-normal-flip
with dissolve

"I sat down with her."

pause (0.5)

show clo at mright
with ease

show val rel-pleased-flip
with dissolve

"It took moments for her to rest her head against me."

show clo con-charming-flip
with dissolve

c "That's better."

v "I don't think there's really a tour, is there?"

show clo cha2-sass-flip
with dissolve

c "I may have lied. Oops."

show val rel-grin-flip
with dissolve

v "I'll find it in my heart to forgive you."

show clo cha3-sass-flip
with dissolve

c "Thank you, Valerius. I already feel better. You're a good antidote for wine sickness."

show val rel-pleased-flip
with dissolve

v "I suppose. I admit, I'll be sad if your affection is just the result of drink."

show clo con-charming-flip
with dissolve

c "Don't they say \"In wine lies truth\" down in Raskya? They say it here too."

show val rel-grin-flip
with dissolve

v "So you're extra honest now."

show clo rel-charming-flip
with dissolve

c "Mmm hmm. And the truth is that you make you me feel better."

"She squeezed my arm, and I allowed myself to put it around her. She snuggled into my side instantly."

show val rel-pleased-flip
with dissolve

v "My armor can't feel very comfortable."

show clo cha3-sexy-flip
with dissolve

c "No, but it won't always be there. I hope."

stop music fadeout(1.0)

"I felt at that moment I could try to kiss her."

show clo rel-neutral-flip
with dissolve

show val rel-normal-flip
with dissolve

"But..."

play music downtime

show clo reg-stoic-flip
with dissolve

c "... I'm scared."

show val rel-normal-flip
with dissolve

v "You have nothing to fear. If any soldier bothers you again I'll have him beaten to death."

show clo rel-neutral-flip
with dissolve

c "Thank you but that's not what I'm afraid of. Well, not much. I'm just afraid of my life being like this forever."

show val ac-fact-flip
with dissolve

v "What do you mean?"

show clo reg-emotional-flip
with dissolve

c "I want to go other places. Meet other people. Do... something else. I have no power outside of my tiny little life. I feel like I'm already dead sometimes."

show val rel-pleased-flip
with dissolve

v "You seem very lively to me."

show clo rel-charming-flip
with dissolve

c "I'm a good faker."

show val rel-grin-flip
with dissolve

v "Very bold to say that straight to a man you're cuddling up to."

show clo rel-neutral-flip
with dissolve

c "I'm trying to be bold. I would need to be bold to... uh..."

show val rel-pleased-flip
with dissolve

v "What is it?"

show clo rel-charming-flip
with dissolve

c "Nothing. To leave Dagara, I guess. Forever."

show val rel-normal-flip
with dissolve

v "No one here you'd miss?"

c "Oh just one person, but I'd bring her with me if I could."

show val rel-emotional-flip
with dissolve

v "I see... I wish I could help."

show clo rel-neutral-flip
with dissolve

c "I wish you could too."

nar "I could. Maybe."

nar "But her uncle was now a Raskyan praetor, so he essentially outranked me. Any attempt to abscond with a woman of his household would be, at best, risky."

nar "At worst, fatal."

nar "Anyway, I hardly knew this girl. My cock's desires were making me very sympathetic to her plight, but there was just not much to be done about it."

nvl clear

show clo at left
with ease

"After a long silence, she disentagled herself from me and rose to her feet."

show clo con-charming
with dissolve

c "Well, I'm feeling better now. Thank you, Valerius."

show val rel-normal-flip
with dissolve

v "Of course. Would you like an escort back to your room?"

show clo reg-stoic
with dissolve

c "No, I'll be fine. Thank you. Just..."

v "What?"

show clo rel-charming
with dissolve

c "Are you going to bed soon?"

show val rel-pleased-flip
with dissolve

v "Maybe in a bit. I'm going back for another drink first."

show clo rel-neutral
with dissolve

c "I see. Well, enjoy it for me, Valerius. Don't make yourself too honest, like I did."

show val rel-grin-flip
with dissolve

v "Nonsense, I appreciate the honesty. Have courage, Clodia."

show clo con-charming
with dissolve

c "I guess we'll see if I do, won't we?"

show clo rel-charming-flip
with dissolve

hide clo
with easeoutleft

show val rel-normal-flip
with dissolve

"She turned and left me with those mysterious words to ponder."

show val rel-normal
with dissolve

"I figured it was time to go get that drink."

show black
with fade

centered "And then maybe a trip to whatever passed for the best brothel in Dagara. I needed it."

centered "Praise the Ghosts that I chose to dawdle with my drink a little while."

scene HighHall
with fade 

play music timekiller
play sfx1 crowdmurmur loop

pause (1.0)

show por neu-neu-flip at right
with dissolve

p "Ahem."

show val rel-normal
with dissolve

v "Hm?"

"The tap on my shoulder was sharp. Some noblewoman."

show por plan-explain-flip
with dissolve

p "Are you Tribune Valerius?"

v "Yes."

show por con-sigh-flip
with dissolve

p "Right. Well, thank the Ghosts she was right about you being handsome."

show val ac-fact
with dissolve

v "Uh, what's going on?"

show por neu-neu
with dissolve

p "Come with me. You've been summoned by Lady Clodia."

stop music fadeout (1.0)
stop sfx1 fadeout (1.0)

scene HallNight
with fade

play music whataday

show por neu-neu-flip at left
with dissolve

show val rel-normal-flip at right
with dissolve

pause (1.0)

nar "The lady led me from the hall, through labyrinthine corridors and up twisting stairs that I vaguely recognized."

nar "I had some idea of where I was going, but..."

nar "It felt a little too much like one of the bawdy tales that gets shared amongst the lowest and highest of men."

nvl clear

v "Who are you, my lady?"

show por plan-explain-flip
with dissolve

p "My name's Portia. You should remember that because I'll be the one who has you punished if you misbehave."

show val rel-pleased-flip
with dissolve

v "Ha, okay. What's misbehaving mean to you?"

show por neu-neu-flip
with dissolve

p "If you're not just a pretty face, you'll understand."

"I wasn't even certain what was going on. Oh, I had an {i}idea{/i}, but what were the chances?"

show black
with fade

scene HallNight
with dissolve

show por neu-neu-flip at left
with dissolve

p "Here we are."

show val rel-normal-flip at right
with dissolve

"We had stopped at a door. I did recognize it, yes."

play sound dooropen

show por at fleft
with ease

"Portia opened it without knocking."

"The room within was dark, though there was a brazier lit in the corner."

hide val
show red
show val rel-emotional-flip at right
with dissolve

"A beautiful, lush, red scent washed over me. A spell, some would say."

show por neu-neu
hide red
show val rel-normal-flip
with dissolve

v "Is it... safe?"

"I couldn't think of a simpler way to word the complex fear of what could go horribly wrong for me from this point onward."

show por plan-explain
with dissolve

p "That depends. Don't hurt her."

show val ac-fact-flip
with dissolve

v "I would never. But be honest with me. Please. How many men have you led to this room like this?"

show por neu-neu
with dissolve

"She stared for a good moment. I was about ready to enter the room without an answer when one finally came."

show por con-sigh
with dissolve

p "You are the first. Believe that or not, it won’t matter."

show por plan-explain
with dissolve

p "Conduct yourself like a virtuous lover. Or I’ll tell the Praetor about this myself."

show black
with fade

play sound doorclose
stop music fadeout (1.0)

centered "Then, almost ushering me inside, she closed the door behind me."

centered "When my eyes adjusted to the low light, I saw what smelled so sweet."

show ClodiaRoomNight
with fade



"Roses wreathed around the window. A strange window, I thought."

show ClodiaRoomNight with dissolve:
    zoom 1.5 xalign 0.5 yalign 0.0

"Ah, so {i}there's{/i} the legendary Dagaran glass. Clear as air. A work of magic, they say."

"But I couldn't care less at that moment."

play music love

show red
with dissolve

centered "It was nothing compared to the girl on the bed."

show CG4:
    xalign 0.45 yalign 1.0 zoom 2.0
    linear 5.0 xalign 0.75
with dissolve

pause 4.0

show CG4:
    xalign 0.75 yalign 0.1
    linear 6.0 xalign 0.5 yalign 0.8
with dissolve

pause 5.0

show CG4:
    xalign 0.1 yalign 0.9
    linear 5.0 xalign 0.25 yalign 0.32
with dissolve

pause 4.0

v "Ghosts take me."

show CG4D:
    xalign 0.25 yalign 0.32 zoom 2.0
with dissolve

c "Hello again, Valerius."

hide CG4

show CG4D:
    linear 2.0 yalign 0.0

c "Uh..."

c "Do I... Do I look ridiculous to you?"

"How could she even ask that?"

v "No. You're beautiful."

show CG4E:
    xalign 0.25 yalign 0.0 zoom 2.0
with dissolve

c "Good. I... I was worried this whole time I would look foolish."

hide CG4D

v "I'm afraid I may be the foolish one."

c "Ha, what do you mean?"

v "This could be dangerous for me. For both of us. If your uncle-"

show CG4:
    xalign 0.25 yalign 0.0 zoom 1.0
with dissolve

c "No one other than Portia will ever know. I swear."

"What could I do but trust her?"

v "I, uh, I thought you were a maiden?"

show CG4B:
    xalign 0.25 yalign 0.0
with dissolve

c "Only my body. My mind went rotten a long time ago."

hide CG4

v "You're much bolder than you give yourself credit for."

show CG4:
    yalign 0.0
with dissolve

c "Please. Take your clothes off and come sit with me."

"Her voice sounded eager, her smile was inviting, but..."

show CG4D:
    xalign 0.25 yalign 0.45 zoom 2.0
with dissolve

"...she was scared. Even in that low light I could see her hands wringing the sheet."

hide CG4

"But I undressed all the same. Whatever part of me saw it as risky was near dead with drink and fire."

c "Ha. I guess it takes a long time to take off armor."

v "Yeah. Apologies if I'm ruining the moment."

c "No, not at all. It's fun to watch."

"I bet."

"At last, I was bare before her."

show CG4D:
    xalign 0.25 yalign 0.0 zoom 1.0
with dissolve

c "Wow."

"Always a good thing to hear. Even if it doesn't mean much coming from a maiden."

show CG4:
    yalign 0.0
with dissolve

"I moved to to join her on the bed, but I couldn't quite climb in yet."

hide CG4B

"Something was... not wrong, but unsolved."

"How... how was the proper way to be with a noblewoman?"

show CG4B:
    xalign 0.25 yalign 0.0
with dissolve

c "What's wrong?"

hide CG4

v "Nothing. I just... I don't want to be some lowborn brute to you."

show CG4:
with dissolve

c "You won't be. I wouldn't have chosen you otherwise."

show red
hide red

show red
with dissolve

show black
hide black

show black
with dissolve

centered "Her fingers grazed my arm."

show CG4:
    zoom 1.2
    
hide black
hide red
with dissolve

c "I've met brutes. Lowborn and highborn, all over the place."

show red
with dissolve

hide red
show black
with dissolve

centered "Traveled up to caress my chest."

show CG4f:
    xalign 0.0 zoom 1.0
with dissolve

hide black
with dissolve

c "I've met cruel men and stupid men and all the men in between until it made me sick."

show red
with dissolve

hide red
show black
with dissolve

centered "Traveled down to feel my stomach."

show CG4f:
    zoom 1.2 yalign 0.0
with dissolve

hide black
with dissolve

c "So over the years I've gotten a rather good idea of what I like."

show red
with dissolve

hide red
show black
with dissolve

centered "And with a smile, she leaned up to kiss me."

show CG4f:
    zoom 2.8 yalign 0.22 xalign 0.33
with dissolve

hide black
with dissolve

c "And I like you."

show red
with dissolve

pause 2.0

scene black
with fade

nar "That night started it all."

nar "Was there a single night for the rest of that occupation I did not spend hidden in her room?"

nar "Only those where we dressed her in a disguise and smuggled her into the War Tent."

nar "So much for my riotous orgies, but I couldn’t complain. The fun we had was beyond what any gaggle of whores could have given me."

stop music fadeout (2.0)

nar "This memory is so sweet. I don't want to leave it."

nvl clear

show text "{size=+100}{cps=0}NOW{/size}{/cps}" at truecenter
with dissolve
$ renpy.pause(delay=1.5, hard=True)
hide text
with dissolve

scene ValeriusRoom
with fade

play music downtime

"It takes me a long time to return to the present, to even begin unlocking our coded language and read my love’s words for the first time in months."

"It’s a long reply, but I can’t get enough of every single word."

play sfx1 quill

centered "My sweet one... I will do my best to explain what is happening. {p}{p=0}Sejanus is bribing the nobles with feasts and slaves. The Praetor and his wife are dead. All of their sons and daughters and nephews and nieces are dead. Except me. {p}{p=0}Tomorrow, Sejanus will call for a vote at dinner. A vote to make me my uncle's daughter, and so his heir. Then, Sejanus will marry me to become Praetor. {p}{p=0} My love, we must hurry. I can delay the wedding for one day after I am adopted, but after that Sejanus will not waste any time. Already he looks at me as his wife, has seated me next to him at dinner. There is no time. {p}{p=0}Presumably this girl who is working for you knows a way in and out of the citadel. Should I attempt to escape with her? They allow me free movement, but a single failed escape will likely see me locked away in my room until the wedding. {p}{p=0}I do have another idea: provide me with a poison. I can kill Sejanus myself, although I cannot guess what chaos will result from such an event. I will continue to watch and listen and think, my love. {p}{p=0}My heart cries out for you. I never thought I would ever read your words again. I love you, Valerius. Please. Please help me find my way to you."

stop sfx1

show val rel-emotional at left
with dissolve

"Tears. I need a minute to wipe away these tears."

show nas2 arm-blank-flip at right
with dissolve

n "Did someone die?"

show val rel-normal at left
with dissolve

v "Why are you still here?"

show nas2 arm-frustrated-flip at right
with dissolve

n "Because out there I have to deal with being pinched and prodded by every man in the camp. I practically trip over cocks whenever I leave and come back."

n "If you would just let me fuck some of them, I could make a decent earning on the side here."

show nas2 arm-blank-flip at right
with dissolve

n "Then maybe I wouldn’t charge so much from your purse."

show val rel-emotional at left
with dissolve

v "No. If you become well known in camp, it breaks the illusion that you’re my bedslave."

n "Maybe Tribune Valerius doesn’t mind sharing his pretty Peladonian whore with the men? It would make you more popular."

v "I don’t care for their love. Or your chatter."

show nas2 rel-defaul-flip at right
with dissolve

n "Pay me for silence, then."

show val rel-serious at left
with dissolve

v "You nasty little viper. I should never have freed you."

show nas2 rel-grin-flip at right
with dissolve

n "Heh. Too late to regret that one."

show val rel-normal at left
with dissolve

v "I need to write my response. Give me a moment here."

n "Very well."

show nas2 sed-flirtatious-flip at right
with dissolve

n "I guess I'll have to figure out how to pass the time on my own."

hide nas2 with dissolve

hide val with dissolve

nar "Laying down on my bed, she begins to reach between her legs."

nar "She does this sometimes. And she likes when I get angry about it."

nar "Turning away, I sit at my desk and begin to think about my response to Clodia."

nar "My love has already hit on two tactics, and now I must evaluate them."

nvl clear

menu:
    "What should I do?"
    "A. Endorse Clodia's escape attempt.":
      jump clodia_escape
    "B. Provide Clodia with poison.":
      jump poison_plot

label clodia_escape:
    
    $ plan = "escape"

nar "Escape it will have to be. I cannot wait, and everything else is uncertain."

nar "Once she is out of the city and in my arms, they can burn it all down for all I care."

nar "All I want is Clodia. All I need is Clodia."

nvl clear

n "Mmmmmmm... mmmmmmm..."

show val rel-emotional-flip at left
with dissolve

"I don’t care to look behind me to see just what she’s up to, but it’s hard to form coherent thoughts with her nonsense filling my ears."

show val rel-serious-flip
with dissolve

v "Silence, please."

n "Oooohhh...."

show val ac-annoyed-flip
with dissolve

"My letter takes several minutes longer to write than it should."

n "Mmmmm, heh heh, ooooohhhh..."

show val rel-emotional-flip at left
with dissolve

v "It’s done."

show nas2 rel-defaul-flip at right
with dissolve

n "Finally. What took you so long?"

show val rel-normal
with dissolve

v "Are your hands clean?"

show nas2 rel-grin-flip
with dissolve

n "Why of course, you know me, I was only fucking with you. Or not, as it were."

show val ac-annoyed
with dissolve

v "Be serious. I won't be sending a letter to my love that smells like your-"

show nas2 rel-defaul-flip
with dissolve

n "Then you have courtesy beyond what can be expected of most Raskyans. My hands are clean, sir."

show val rel-normal
with dissolve

v "Good. Bring this message to Clodia. And then you are to escort her here."

v "I don’t care how you do it, just bring her back with you, safe and unharmed."

show nas2 arm-blank-flip
with dissolve

"Nasrin holds out her hands. I place the note in one, and her fingers close gently around it."

show nas2 sed-sly-flip
with dissolve

"Her other palm stays open, inviting."

show val rel-serious
with dissolve

v "For Greed's sake, Nasrin. I’m still paying for your food and giving you a place to sleep."

v "Most freedwomen would cry for joy at such privileges."

show nas2 arm-disappointed
with dissolve

n "Damn. Why is it that being a freedwoman isn't the same as being a free woman?"

hide nas2
with easeoutright

"But she takes the note and leaves me all the same. Thankfully."

hide val
with dissolve

nar "I'm probably too harsh with her. I just wish she wouldn't insist on the difficult attitude."

nar "And on draining my resources. Peladonian whores are famously the greediest in the world."

nar "So it’s just my luck it was a Peladonian whore I captured from one of the many towns we destroyed over the last six years."

nar "The only captive I ever took in the whole war."

nar "And it wasn't very long until she had... charmed me into freeing her."

nar "A shame."

nvl clear

scene black
with fade

centered "If I had really thought about how much her particular talents would end up costing me once I had to pay for them, I wouldn't have done that."

stop music fadeout(1.0)

if portia == "alive":
    jump portia

if portia == "dead":
    jump bad_news

label poison_plot:
    
    $ plan = "poison"
    
nar "Escape is too risky. If she’s caught, that could ruin any other chance we have."

nar "But poison... damn, that’s risky as well. She may never have an opportune moment to use it."

nar "Still... Clodia needs weapons to protect herself. I must give her whatever I can."

nar "And if she can kill Sejanus, well, all the better. That will certainly prevent him from getting his foul cock anywhere near her."

nvl clear

n "Mmmmmmm... mmmmmmm..."

show val rel-emotional-flip at left
with dissolve

"I don’t care to look behind me to see just what she’s up to, but it’s hard to form coherent thoughts with her nonsense filling my ears."

show val rel-serious-flip
with dissolve

v "Silence, please."

n "Oooohhh...."

show val ac-annoyed-flip
with dissolve

"My letter takes several minutes longer to write than it should."

n "Mmmmm, heh heh, ooooohhhh..."

show val rel-emotional-flip at left
with dissolve

v "It’s done."

show nas2 rel-defaul-flip at right
with dissolve

n "Finally. What took you so long?"

show val rel-normal
with dissolve

v "Are your hands clean?"

show nas2 rel-grin-flip
with dissolve

n "Why of course, you know me, I was only fucking with you. Or not, as it were."

show val ac-annoyed
with dissolve

v "Be serious. I won't be sending a letter to my love that smells like your-"

show nas2 rel-defaul-flip
with dissolve

n "Then you have courtesy beyond what can be expected of most Raskyans. My hands are clean, sir."

show val rel-normal
with dissolve

v "Good. Take this message to Clodia. And include this with it."

"I hold up to the light a small vial of what looks like dark smoke."

"It will turn to liquid the moment the stopper is pulled, however. And a drop of that liquid in anything is enough to summon all the Ghosts of the Grave."

show nas2 arm-blank-flip
with dissolve

n "Mmm. This is quite useful, isn't it?"

show val rel-serious
with dissolve

v "Don’t sound so familiar with this stuff, please."

show nas2 rel-grin-flip
with dissolve

n "No worries. If I ever grow to hate you, I'll just leave."

"The same deal we've always had. Even for the few weeks I owned her."

show nas2 rel-defaul-flip
with dissolve

n "You’re lucky you freed me. Poison like this is the skeleton key to a slave’s freedom."

show val rel-normal
with dissolve

v "Well you never killed me, so what does it matter?"

show nas2 sed-sly-flip
with dissolve

n "I have a better question: if you want someone killed, why not have me do it? I would actually know what I’m doing."

show val ac-annoyed
with dissolve

"She’s not wrong. Shit."

"Clodia says she’s right next to Sejanus at dinner. No one could get closer to his food except perhaps whatever slave serves it."

show val rel-serious
with dissolve

"But my dear Clodia lacks Nasrin’s... history. My freedwoman has done this before. Likely more than once."

hide nas2
with dissolve

show val rel-emotional
with dissolve

"Luck be with me."

hide val
with dissolve

menu:
    "Who should poison Sejanus?"
    "A. Have Clodia poison Sejanus":
      jump clodia_poison
    "B. Have Nasrin poison Sejanus":
      jump nasrin_poison
      
label clodia_poison:
    
    $ plan = "clodia_poison"

nar "Clodia can do this. It should be a simple affair, and... well..."

nar "I only have so much money I’ve brought with me, and I know not to go too far into debt with a Peladonian. Especially over a murder contract."

nar "The wealthiest city in the world naturally breeds the greediest of people."

nvl clear

show val rel-normal at left
show nas2 rel-defaul-flip at right
with dissolve

v "You are to take the vial to Clodia with the note, and then you wait."

v "If she succeeds in killing Sejanus, keep her safe, and escape with her in the chaos."

show nas2 arm-blank-flip at right
with dissolve

n "I’d make a poor bodyguard. Do I look like the strong type to you?"

show val rel-serious
with dissolve

v "Just do it. I’ll toss in a litte extra."

show nas2 rel-defaul-flip
with dissolve

n "As you wish. And what if she fails? I think there’s a reasonable chance of that happening."

v "That depends. If she doesn’t find a good chance to kill him one day from now, return to me and we’ll come up with a new plan."

show val rel-emotional
with dissolve

v "But if she's caught..."

nar "... I don’t quite know what to do then. How would Sejanus react? He needs her if he is to become the Praetor, so her life isn’t in danger."

nar "But there are so many ways he could hurt her if he felt like it."

nar "Damn the Ghosts who ever brought that man power."

nvl clear

show val rel-normal
with dissolve

v "If she is caught, then escape with her all the same."

show nas2 arm-blank-flip
with dissolve

n "Well, I’ll try. No promises. I don’t do suicide missions either."

show val ac-fact
with dissolve

v "No Peladonian does."

show nas2 rel-grin-flip
with dissolve

n "That’s why we’re the smartest people in the world. But don’t let a Durjan hear you say that."

show val rel-normal
with dissolve

v "I’m sure no one else would dare be offended. Now go."

hide nas2
with dissolve

"Her cloak glides behind her as she vanishes through my door. She’ll do what I said, money makes her obedient."

show val rel-emotional at center
with dissolve

"Now I just have to trust in Clodia."

hide val
with dissolve

show black
with dissolve

centered "Blessings of Luck upon you, my Clodia. And Blessings of Love."

stop music fadeout(1.0)

if portia == "alive":
    jump portia

if portia == "dead":
    jump bad_news

label nasrin_poison:
    
    $ plan = "nasrin_poison"

nar "I can’t take any chances whatsoever. Not with this."

nar "Nasrin is a professional, she’ll get this done."

nar "So long as I pay up. The Peladonians always know their worth when it comes to a murder price."

nvl clear

show val rel-normal at left
with dissolve

show nas2 rel-defaul-flip at right
with dissolve

v "As you say. Let me make a quick change to the letter so Clodia will understand."

n "I'll just tell her."

show val rel-normal-flip at left
with dissolve

v "She has no reason to trust you."

"That’s not true, I explicitly told Clodia to trust her in the last letter."

"But Nasrin won’t know that even if she’s a snoop, thanks to my code. You can never, ever be too careful about things that can kill you."

show nas2 arm-blank-flip
with dissolve

n "Well, I guess I’ll just wait here for you to finish scratching that out then?"

v "It will only take a minute."

show nas2 rel-grin-flip
with dissolve

n "Think of the fun we could have in one minute."

show val rel-emotional-flip
with dissolve

v "You should cease that sort of behavior. It would be flattering if your charms were not intimately wrapped up in your purse."

show nas2 rel-defaul-flip
with dissolve

n "I can have plenty of fun without there being any money involved. I just have to want to."

show val rel-serious
with dissolve

v "Regardless, Clodia is my woman."

show nas2 rel-grin-flip
with dissolve

n "What's that old saying? \"Raskyan men have a woman, a girl, and a boy.\""

show nas2 sed-flirtatious-flip
with dissolve
   
n "Can't help you with the boy, but I can always be your girl."

show val rel-normal
with dissolve

v "... I only want Clodia. Now go bring her to me."

show nas2 arm-blank-flip
with dissolve

n "Of all the handsome Raskyans I get stuck with, it's the one who's faithful to his love."

n "Damn Luck."

hide nas2
with dissolve

"As I watch her leave, I really have to wonder what's going on in her mind."

"She's not stuck with me at all. She acts like she is but there's nothing I could, or would, do to stop her if she just disappeared."

show black
with fade

centered "Such a difficult girl."

stop music fadeout(1.0)

if portia == "alive":
    jump portia

if portia == "dead":
    jump bad_news

label portia:

$ renpy.pause(1.0,hard=True)

play movie("images/Video/Ctrans0.mkv")
$ renpy.pause(5.5,hard=True)
stop movie

scene black
with fade

pause(1.0)
    
centered "A soft, gentle hand rouses me from the dark."

p "It’s time to rise, my lady."

nar "That hand squeezes mine softly. Of course I’m not waking from this nightmare."

nar "At least Portia’s here to make things feel somewhat normal."

nar "Wait..."

nar "Portia?"

nvl clear

play music reprieve

scene ClodiaRoomDay
show por neu-neu
with fade

show clo snk-relief-flip at right
with dissolve

c "Portia! Where have you been?"

show clo defen-crying-flip at mright
show por con-sigh
with dissolve

show ClodiaRoomDay:
    zoom 1.5 xalign 0.5 yalign 0.5
show clo defen-crying-flip at mright:
    zoom 1.6 yalign 0.0
show por con-sigh at mleft:
    zoom 1.6 yalign 0.45
with dissolve

"She hugs me tight, and that’s how I can tell it was something awful."

"She's shaking."

p "I’m fine. I’m fine, dear."

show clo reg-emotional-flip
with dissolve

c "What happened to you?"

show por neu-neu
with dissolve

p "Some soldiers grabbed me last night. Without a word. They just dragged me off."

show por neu-worried
with dissolve

p "They took me down from the citadel and put me up on the wall. And... there were others. Lucia and Sella, and my husband."

p "One of Sergius's tribunes was there, down below. He was trying to negotiate for our lives, I think."

show por neu-neu
with dissolve

show clo rel-surprised-flip
with dissolve

p "And... then they threw them. Lucia and Sella, and their husbands and servants, all of them fell."

show clo reg-emotional-flip
with dissolve

"I don’t understand how she can speak so calmly. Lucia and Sella were her friends."

"I want to cry for her. This is horrible."

show clo defen-crying-flip
with dissolve

c "They didn’t throw you, though. You’re here. Praise Ghosts of Luck and Life, you’re here."

show por neu-worried
with dissolve

p "I don’t remember what they were saying because my mind... everything is fuzzy after I saw them push Lucia over the edge."

p "But whatever was said... they were going to push me and my husband and several others next, I could almost feel the air beneath my feet."

show por neu-neu
with dissolve

p "But then Sergius's tribune left. And they didn’t push us."

"I hug her tighter. To hear her voice again this morning is such a relief."

"I can’t imagine losing Portia, please, no."

show por con-sigh
with dissolve

p "Now then... Sejanus has ordered me to give you a bath before you come down to eat."
 
show ClodiaRoomDay:
    zoom 1.0
show clo rel-surprised-flip at mright:
    zoom 1.0 yalign 1.0
show por con-sigh at center:
    zoom 1.0 yalign 1.0
with dissolve

show clo rel-surprised-flip at right
with ease

c "How can we even think about that right now? After what happened to you?"

show por plan-explain
with dissolve

p "Mulling about in the fear isn’t going to help. We have to focus on staying alive now."

p "And the best way to stay alive, I’ve found, is to obey the people in power."

show por plan-concern
with dissolve

p "As little as you can get away with, at least."

show clo reg-stoic-flip
with dissolve

"That last little bit sounds more like the Portia I’m used to."

"My heart stops aching, and my hands at last begin to still themselves."

show por neu-neu
with dissolve

p "Not that Sejanus is the Praetor yet, but I suppose disobeying him would be bad for you."

c "Obeying him will be bad for me as well. It’s all just bad."

show por con-sigh
with dissolve

p "Well. Forgive me for saying so, but the women who are lucky enough to marry men they love are just that. Blessed by Luck."

p "Most of us have to make due with what we get."

show clo reg-declaring-flip
with dissolve

c "It’s a little different from your husband, Portia. His worst crime is dullness."

c "Sejanus is a murderer, an invader. A traitor to his people."

show por neu-neu
with dissolve

p "Well. He’s fairly handsome."

show clo rel-angered-flip
with dissolve

c "He doesn’t care about me. He just wants to be Praetor."

show por con-sigh
with dissolve

p "You should see that as a blessing. A marriage between two people who don’t care for each other is easier than one where only one person cares."

show clo reg-stoic-flip
with dissolve

c "I suppose you have experience with that."

show por plan-explain
with dissolve

p "Certainly. Give Sejanus a son or two, and you’ll be able to play around with all the pretty boy paramours you’d like."

show clo rel-angered-flip
with dissolve

c "I don’t want to give Sejanus a son. I don’t want him anywhere near me."

nar "I want Valerius."

nar "But if I even bring him up, I can sense the row that might start. I can almost hear her admonitions for me putting my hopes in him."

nar "Portia, dear Portia, you are too practical sometimes."

nvl clear

show por neu-neu
with dissolve

p "Besides love, when Sergius gets in, Sejanus will die. A few days of misery, and everything will right itself."

show clo reg-stoic-flip
with dissolve

"That's assuming Sergius's men won't rape and kill us. A big assumption."

show clo reg-declaring-flip
with dissolve

c "It will only take Sejanus a few minutes as soon as it pleases him to give me a baby."

show por plan-insist
with dissolve

p "Then do what we all do and leave it in the woods for the Ghosts."

show por neu-distaste-flip
with dissolve

p "Now get your things off, I’ll have to go find a slave to fetch the water. Can’t believe one isn’t here right now, lazy things."

hide por
with easeoutleft

show clo reg-stoic-flip
with dissolve

"We can't even enjoy her being alive in this situation."

"What a hell this is."

"Even undressing for a bath feels frightful right now."

scene black
with fade

stop music fadeout(1.0)

centered "I’d rather stay shielded in these clothes forever."

jump bathtime
    
label bad_news:

scene black
with fade

pause (2.0)

play movie("images/Video/Ctrans0.mkv")
$ renpy.pause(5.5,hard=True)
stop movie

play sound knockdoor

pause (1.0)

centered "I groggily pull myself out of the dreamless sleep, searching for the sound."

scene ClodiaRoomDay
with fade

play music fluff

show clo reg-stoic-flip at right
with dissolve

"Ah, damn it all. What do you bastards want?"

show clo con-angered-flip
with dissolve

c "What is it?"

play sound dooropen

show sol neu-neu at sleft
with easeinleft

show clo reg-stoic-flip
with dissolve

"I didn’t say come in."

so "Begging your pardon my lady, but I’ve been told to inform you of some unfortunate news."

show clo con-angered-flip
with dissolve

c "Sejanus still breathes?"

show sol neu-confuse
with dissolve

stop music fadeout(0.5)

so "Er... yes, he does, but that’s not the news. Your companion Lady Portia is dead."

show clo con-surprised-flip
with dissolve

c "..."

show sol neu-neu
with dissolve

so "Lady Clodia?"

play music aftershock

nar "I knew this had happened."

show clo reg-emotional-flip
with dissolve

nar "She’s been missing for a whole day. Under these circumstances, it was enough for me to assume the worst."

nar "And I was right."

nvl clear

show clo reg-declaring-flip
with dissolve

c "Explain."

show sol neu-confuse
show clo reg-stoic-flip
with dissolve

so "I don’t know the details. I was told to inform you that she fell off the city wall late last night."

show sol neu-neu
with dissolve

so "... Oh. Also, you’re to bathe and ready yourself to break fast with Sejanus."

so "That’s all, my lady."

"That placid look on his hateful face. This man really thinks nothing of the news he’s brought me."

show clo reg-declaring-flip
with dissolve

c "Go away."

show sol neu-neu-flip
with dissolve

so "Aye, my lady."

play sound doorclose

hide sol
with easeoutleft

show clo reg-emotional-flip
with dissolve

nar "What kind of hollow lie is that, Sejanus?"

nar "Fell off the city wall? How? What was she even doing out there?"

nar "There’s absolutely no reason whatsoever she would have left the citadel, gone down into the city, and climbed up to the walls!"

nvl clear

show clo defen-crying-flip at center
with dissolve

nar "They killed her. Killed the woman who has taken care of me every day and night since I was six."

nar "Killed my dearest friend and companion."

nar "Surprising. Only a few tears."

nar "I’m having trouble... feeling."

nar "Funny. It reminds me of a few lines."

nvl clear

show clo reg-emotional-flip
with dissolve

nar "\"When the world goes mad, the heart grows sane.\""

nar "No more Portia to rouse me from sleep. To laugh and jest with me. To smuggle me copies of poems and sweet tales of distant, better lands than this."

nar "She’s just gone. Portia’s gone."

nar "Other than my love for Valerius..."

nvl clear

show black
with fade

stop music fadeout(1.0)

centered "My heart is growing too sane to feel anything but hate."

jump bathtime

label bathtime:
    
scene CG5:
    zoom 2.5 xalign 1.0 yalign 1.0
    linear 7.0 yalign 0.55
with dissolve

"The bathwater steams with perfumed vapor as I relax back in the tub."

"The heat burns and caresses me, purifies the skin and the muscles. When I close my eyes, I can almost pretend everything is just fine."

c "Ahhh. Ghosts be good."

"Not that they have been, recently."

"I have to enjoy every small mercy I can, I suppose."

n "The Ghosts of Love have been very good to you."

c "Who-"

show CG5:
    linear 0.5 xalign 0.6 yalign 0.27

pause 0.2

show CG5:
    zoom 2.0
    linear 0.5 xalign 0.2 yalign 0.0
with dissolve

"The girl."

"The serving girl who brought me the note sits on the edge of my bath."

"How in the Grave did she get in here without me hearing?"

c "You... ah... don’t sneak up on me like that. I was relaxing!"

n "Sneaking is one of the things I'm really good at."

c "Well, you're... uh..."

show CG5:
    linear 1.0 zoom 1.9 yalign 0.33
    
pause 1.5

"What is she doing?"

n "By the way, you would be very easy to murder. I would work on being more aware if I were you."

c "Cover yourself back up!"

n "No, I think I'd like to relax too. A shame there isn't room for two in there. I guess sitting here will have to do."

show CG5:
    zoom 1.3 xalign 0.5 yalign 0.5
    linear 1.0 xalign 0.8
with dissolve

"I don’t like how close she is."

"Or what she's staring at."

c "Excuse me."

show CG5:
    zoom 1.0 yalign 1.0
    linear 2.0 yalign 0.2
with dissolve

n "Yeah?"

c "Stop staring."

n "Hmm. Damn."

c "What?"

n "I knew you seemed pretty, but I kind of hoped your clothes were doing most of the work. Apparently not."

c "Is that supposed to be a compliment?"

n "Heh, yeah. Why do noble girls hide all their lustiness behind that mask of offense?"

c "Just... argh. What are you here for? Did you bring me something from Valerius or not?"

n "I have."

n "Ink doesn’t like water very much, though. Guess I’ll just have to wait for you to finish."

c "I won’t make you wait long."

"I was so comfortable, too. But Valerius’s words are too important."

c "Look away."

n "Why?"

c "Because you’re a slave and I command you to."

n "Oh, now you'll have to excuse me. I'm no slave. Not anymore. So I don’t have to do anything."

c "Must you insist on being difficult?" 

n "Must you insist on treating a freedwoman like a slave? Ask politely."

c "... Please. Look away so I can dry off."

n "Yeah, sure, no problem."

scene ClodiaRoomDay
with dissolve

show nas2 rel-defaul-flip at left
with dissolve

"She turns around, and immediately I catch her reflection staring at me in the mirror."

"The impudence of this girl! But I can’t let her get to me."

"My servants towel me off, and I have them dress me quickly in something simple."

show clo2 reg-stoic-flip at right
with dissolve

"After that, I dismiss them."

"The whole time, the girl doesn’t stop looking at me in the mirror, all with that same smile."

"She’s worse than a man! What is her problem?"

if plan == "escape":
    jump escape_letter

if plan == "clodia_poison":
    jump poison_letter
    
if plan == "nasrin_poison":
    jump nasrin_letter
    
label escape_letter:

c "All right, now give me Valerius’s letter."

show clo2 at center
with dissolve

show nas2 rel-defaul
with dissolve

"She does, and I take it to the window to read by the morning light."

show nas2 at right
with dissolve

hide nas2
with dissolve

"While I begin to decipher the code, she falls onto my bed like a bored child."

"Finally, the words are untangled and bare before me."

hide clo2
with dissolve

centered "Forgive me for the brief letter. Your idea to escape is splendid. I’ve paid Nasrin here to help you escape and to obey your orders. I would suggest you wait until nightfall. This night you will sleep in my bed, my Clodia."

show clo2 reg-emotional-flip
with dissolve

"What a beautiful thought. Valerius..."

show clo2 reg-stoic
with dissolve

c "Do you know how to get me out?"

show nas2 rel-grin-flip at right
with dissolve

n "Of course, how else do you think I keep getting myself in and out?"

show clo2 con-charming
with dissolve

c "Uh, I'd assume you've been seducing the guards."

show nas2 sed-laughing-flip
with dissolve

n "Huh. Maybe you're smarter than you look."

show clo2 reg-stoic
with dissolve

"She didn’t take that as an insult. Damn her."

c "Then we’ll wait until after dinner. That’s when it would be safest, I’d think."

scene black
with fade

centered "Just one more insufferable dinner. I’ll get to see the election that determines my fate."

centered "And then I’ll dodge that fate."

hide clo2
with dissolve

hide nas2
with dissolve

jump walking_the_lines
    
label poison_letter:
    
c "All right, now give me Valerius’s letter."

show clo2 at center
with dissolve

show nas2 rel-defaul
with dissolve

"She does, and I take it to the window to read by the morning light."

show nas2 at right
with dissolve

hide nas2
with dissolve

"While I begin to decipher the code, she falls onto my bed like a bored child."

"Finally, the words are untangled and bare before me."

hide clo2
with dissolve

centered "Forgive me for this brief letter. Nasrin here has a poison for you. I leave it up to your discretion as how best to use it. Be careful; it is one of the most potent concoctions of its kind. No matter what happens, Nasrin will help you escape afterward. Be brave, be strong, be clever, my Clodia. I’ll be with you soon."

show clo2 reg-stoic
with dissolve

show nas2 rel-blank-flip at right
with dissolve

"I look up to ask, but the girl already holds her hand out, a small glass vial in her palm."

"It’s barely the size of a bean. A cloud dark as night seems to swirl inside."

show nas2 arm-blank-flip
with dissolve

n "Seriously, be careful with that."

show clo2 snk-serious
with dissolve

"Gingerly, I take the vial from her."

"It feels so delicate. Like all my hopes for survival are inside."

show nas2 arm-disappointed-flip
with dissolve

n "I don’t understand why he’d rather have you do it than me, but what do I know about killing people?"

show clo2 con-angered
with dissolve

c "Well, what exactly would a bedslave know about deadly posions?"

show nas2 arm-blank-flip
with dissolve

n "Never mind. And how can you tell I used to be a bedslave?"

show clo2 rel-surprised
with dissolve

c "Are you serious?"

show nas2 rel-defaul-flip
with dissolve

n "Heh. Not really."

show clo2 reg-stoic
with dissolve

"I’m not going to push this. I’m morbidly curious, but..."

"I’d rather not know whose bedslave she was..."
 
"Or at least I'd rather not hear her say it aloud."

show clo2 reg-emotional-flip
with dissolve

"Ghosts damn her."

show clo2 at left
with dissolve

"I need to find a place to hide this poison in my dress."

"And at dinner, I’ll be seated next to him."

scene black
with fade

centered "Time to finish getting ready. I’ll be sure to look lovely for Sejanus."

centered "I’ll dazzle him, make him in lean in close."

centered "Make him so enthralled he doesn’t notice me calling down the Ghosts of the Grave."

jump walking_the_lines

label nasrin_letter:
    
c "All right, now give me Valerius’s letter."

show clo2 at center
with dissolve

show nas2 rel-defaul
with dissolve

"She does, and I take it to the window to read by the morning light."

show nas2 at right
with dissolve

hide nas2
with dissolve

"While I begin to decipher the code, she falls onto my bed like a bored child."

"Finally, the words are untangled and bare before me."

hide clo2
with dissolve

centered "Forgive me for this brief letter. I have given Nasrin here a poison, and she will use it on Sejanus tonight. Don’t worry, she’s good at this. Once she succeeds, she is to help you escape the city. Be brave, be strong, be clever, my Clodia. I’ll be with you soon."

show clo2 reg-stoic-flip
with dissolve

"Seems simple and straightforward enough."

"If Valerius really trusts this Nasrin girl, then I suppose I can bring myself to trust her too."

show nas2 rel-blank-flip at right
with dissolve

"Although..."

show clo2 reg-stoic
with dissolve

c "Poison?"

n "It was your idea."

show clo2 rel-surprised
with dissolve

c "He says you’re ’good’ at this. I thought you were a bedslave?"

show nas2 rel-grin-flip
with dissolve

n "Bedslave and poisoner are slightly related vocations."

show clo2 reg-stoic
with dissolve

c "I suppose."

show nas2 rel-defaul-flip
with dissolve

n "Also, hey, how did you know I used to be a bedslave?"

show clo2 con-charming
with dissolve

c "Well... just look at you."

show nas2 sed-sly-flip
with dissolve

n "Aww, thank you. You happened to be right in this case, but by that logic, I could assume you’re a Durjan hetaira."

show clo2 con-surprised
with dissolve

c "Excuse me?"

show nas2 sed-flirtatious-flip
with dissolve

n "Well, just look at you."

show clo2 reg-stoic-flip
with dissolve

"Ghosts help me. Let her be as skilled as she is annoying, so that I’m free of needing her soon."

scene black
with fade

centered "Every second I’m in her presence..."
 
centered "All I can think about is whose bedslave she used to be."

jump walking_the_lines

label walking_the_lines:

pause (2.0)

play movie("images/Video/Vtrans0.mkv")
$ renpy.pause(5.5,hard=True)
stop movie

pause 1.0

play music planning

centered "\"Give me a forest, and I’ll destroy a city.\""

show CampDay:
    zoom 3.0 yalign 0.0 xalign 0.5
with dissolve

pause 1.0

nar "Albius of Maltria wrote that at the beginning of his excellent treatise on siege warfare."

nar "His words are about a hundred years old, and little has changed since then."

nar "Dagara's mountains are thick with enough trees to destroy several cities. As they go down, the ballistae and onagers go up."

nar "There is something beautiful about them. Were I not a tribune, I would be a siege engineer, I think. The power of these wall destroyers is intoxicating."

nar "Too intoxicating. Soon they’ll begin beating away at the city."

nvl clear

show CampDay:
    zoom 1.0
with dissolve

pause 0.2

nar "Sergius knows that it technically doesn’t count as a military action until the first of his men have breached the walls."

nar "This assault from a distance requires no blessing, even in the superstitious eyes of the legion. I wonder, what is more powerful: a soldier’s fear, or his lust?"

nar "A group of them are chatting nearby. Looks like they’re carving taunts into their sling bullets, something I used to find hilarious."

nvl clear

show en neu-neu at scenter
with dissolve

so "\"Right up ole’ Sejanus’s ass.\""

show sol neu-neu at sleft
with dissolve

so2 "Yeah, that sounds about like you."

show en neu-confuse-flip at scenter
with dissolve

so "What’s that mean?"

show sol neu-neu at sleft
with dissolve

so2 "Nothin at all. I mean, I only wrote on mine \"This is for a noble cunt.\""

show sol neu-shout-flip at scenter
with dissolve

so "Oh, so no one in particular?"

show en neu-neu at sleft
with dissolve

so2 "Cause I don’t know the names of any of those barbarian women."

show en neu-neu at scenter
with dissolve

so "What does yours say?"

show sol neu-neu-flip at sright
with dissolve

so3 "Uh. It says \"Ouch!\""

show en neu-neu at scenter
with dissolve

so "Ha. That's just great."

show sol neu-neu at sleft
with dissolve

so2 "It’s good to have a mixture. I switch between stuff like \"Die treasonous fucks\" and \"We’re coming for you quivering maidens.\""

show sol neu-confuse-flip at sright
with dissolve

so3 "I think he’s just written nothing but threats aimed at their boys."

show en neu-neu at scenter
with dissolve

so "They say a lack of variety in appetite is a sign of a simple man."

show sol neu-shout-flip at sright
with dissolve

so3 "Fuck off."

show sol neu-shout at sleft
with dissolve

so2 "Both of you calm down. No one cares what you’re fucking once we’re in there."

show en neu-neu-flip at scenter
with dissolve

so "Praise Lust for that. This job wouldn’t be worth it without all the fucking."

show sol neu-neu at sleft
with dissolve

so2 "Praise Lust."

show sol neu-neu-flip at sright
with dissolve

so3 "Aye, praise Lust."

show en neu-shout at scenter
with dissolve

so "Hell are we waiting for anyway?"

"I could listen to these brutes jabber their nonsense all day, but I have actual work to do."

show val ac-fact at fleft
with dissolve

v "You’re waiting on orders."

show en neu-confuse-flip at scenter
with dissolve

show sol neu-confuse-flip at sright
with ease

so "Fuck, uh, hail Tribune!"

"It seems to take them all a second to notice the emblem on my armor identifying my rank."

"I do love the power these four little spears grant me."

show val cmd-ordering
with dissolve

v "So keep your minds sharp for when the words come. As for you, engineer..."

show en neu-neu-flip
with dissolve

so "Ah, yes Tribune?"

show val rel-normal-flip
with dissolve

v "I have orders for you now."

show sol neu-neu-flip
with dissolve

so2 "Tribune, if I might ask, we’ve all heard tell this is to be treated like any other sack."

show val rel-serious
with dissolve

v "That is my understanding of Sergius’s intentions."

show sol neu-confuse-flip
with dissolve

so2 "And all that implies?"

show val rel-emotional
with dissolve

"Sometimes I wonder what cruel Ghosts made men like you."

show val ac-annoyed
with dissolve

v "Apparently. Just take the damn city, I doubt Sergius would care if you fucked his wife so long as we win."

show sol neu-neu-flip
with dissolve

so3 "Haha, aye."

hide sol
with dissolve

show en neu-confuse-flip at sright
with dissolve

en "Uh, you said you had orders for me, Tribune?"

show val ac-fact
with dissolve

v "In a moment, I’d like a report first."

show en neu-neu-flip
with dissolve

en "Ah. Well, all goes smoothly. We should be ready to begin loosing our stones within the hour."

show val rel-pleased
with dissolve

v "Excellent. What section of the wall are you targeting?"

en "The southeast wall. It’s the weakest, they never properly fixed it from the hammering we gave it last year, should be easy to break through."

show val ac-fact
with dissolve

"Yes, exceptionally easy. Likely will only take a day at the most to make a gap big enough to send men through."

"Dagara's architecture may be renowned, but a weakened wall is only a formality to the legions."

show val rel-normal
with dissolve

"Hmm... interesting that Sergius is willing to skip step one in any siege of this nature."

show val rel-serious
with dissolve

v "Why are you not targeting the defensive towers?"

show en neu-confuse-flip
with dissolve

en "It’s unnecessary in a siege of this scale."

show val ac-annoyed
with dissolve

v "How so? If we smash the walls and send the army in, the towers will rain fire on them."

show en neu-neu-flip
with dissolve

en "We won’t lose many. We outnumber them, and they only have two towers within range of the southeast corner."

en "Sergius says speed is our greatest ally."

show val rel-serious
with dissolve

v "I’m sure the men who die under tower fire will appreciate that sentiment."

en "With all due respect tribune, these are legionaries. It’s their honor to die for the Basileus."

"Easy for a man who never leaves the siege line to say."

show val rel-normal
with dissolve

nar "Regardless of his orders, I do outrank him and the rest of the engineers, even the prefect. I can have my way."

nar "If I change the artillery's orders I can buy more time for Clodia and cloak it in concern for the lives of the men."

nar "Yet Sergius may resent and reverse my actions. He could make things very bad for me."

nar "One of the supply carts rattles by. From the sign I can tell the barrels stacked inside are full of wine."

nar "That will go to be rationed out so that the men don’t get too-"

nvl clear

show val ac-fact
with dissolve

nar "Hmm."

nar "I'm reminded of a specific tale I've always been fond of. The story of the Siege of Bola."

nar "Perhaps it's time to repeat history."

nar "I must decide, now."

nvl clear

menu:
    "What should I do?"
    "A. Order the siege engines to target the defensive towers before working on the wall. And then...":
      jump siege_a
    "B. Leave the engines to attack the wall, it’s not worth risking Sergius’s ire. There must be something else.":
      jump siege_b
      
label siege_a:
$ siege = "changed"

show val rel-normal
with dissolve

v "Regardless of honor and glory, the defensive towers should be our first priority."

v "Destroy both of them, and only then target the southeast wall."

show en neu-confuse-flip
with dissolve

en "Do these orders come from the legatus?"

show val ac-annoyed
with dissolve

v "Are you refusing to obey me?"

show en neu-neu-flip
with dissolve

en "Not at all, Tribune. Just don’t be surprised if the legatus is displeased."

show val cmd-ordering
with dissolve

v "Don’t concern yourself with that. Order your prefect to change his aim."

en "Yes tribune. Blessings of War upon you."

show val rel-normal
with dissolve

v "Upon you as well."

hide val
hide en
with dissolve

nar "Once we begin loosing stones, it will take some time for Sergius to realize what they are aiming for, I think. They may even destroy the towers."

nar "By then I will have already bought precious time for Clodia."

nar "Now then, where did that wine wagon drive off to?"

nvl clear

stop music fadeout(1.0)

scene black
with fade

jump election

label siege_b:
$ siege = "normal"

show val ac-pleased
with dissolve

v "As you say, prefect. Your orders are unchanged."

v "Thank you for your input. Blessings of War upon you."

hide val
with dissolve

nar "I leave him before he can give the reply. There’s no safe advantage to be gained here."

nar "Well, it will still take them a day and some hours to pound through that wall."

nar "I’ll simply have to find some other way to hamstring us oh-so-slightly."

nvl clear

scene black
with fade

centered "I must."

nar "When we breach the walls and finish sacking the city, the citadel is next, and that will fall in a day."

nar "Any one of these scoundrels called soldiers could find my Clodia and-"

nar "Ghosts. Don’t think about it."

nar "I will prevent that."

nvl clear

centered "I will save her."

stop music fadeout(1.0)

jump election

label election:

$ renpy.pause(1.0,hard=True)

play movie("images/Video/Ctrans0.mkv")
$ renpy.pause(5.5,hard=True)
stop movie

pause 1.0

scene HighHall
with fade

play music timekiller
play sfx1 crowdmurmur

pause 1.0

nar "Another night of treacherous revelry is in full swing. The voters enjoy their bribes while the votes are collected."

nar "Girls with baskets go around to collect the pieces of wine-stained papyrus with the votes scratched drunkenly upon them."

nar "Tonight the nobles openly ignore their wives, slave girls sitting on their laps like pampered concubines."

nvl clear

show sej neutral-normal
with dissolve

"Sejanus seems to be the most sane man in the room, staring soberly at his collection of dupes from on high."

show sej rel-glance
with dissolve

sj "Are you excited to be invested with holy power, my lady?"

show clo reg-stoic at left
with dissolve

c "No."

show sej rel-bored
with dissolve

sj "Hmm... that’s a shame. Power is a delight."

c "If you say so."

show sej neutral-normal
with dissolve

sj "I understand you do not like me, but successful marriages have begun from less."

sj "How many great Queens were kidnapped by their husbands? Captured into power and luxury, that sounds like little horror to me."

show clo reg-emotional-flip at left
with dissolve

c "I don’t feel like talking to you."

show sej neutral-serious
with dissolve

sj "A shame. A great shame."

sj "I think you will like me once you give me a chance."

show clo reg-declaring at left
with dissolve

c "You had my entire family killed."

show sej neutral-serious-flip
with dissolve

sj "What, your cousins and uncle and aunt? I doubt they gave an orphan girl with no useful place in court much reason to love them."

show clo reg-emotional-flip
with dissolve

"You monstrous thing."

show sej neutral-normal-flip
with dissolve

sj "Listen. I wish I only had to kill Fausus, but of course his male heirs couldn’t be left alive, for obvious reasons."

show sej diplo-per-flip
with dissolve

show clo snk-worry-flip
with dissolve

sj "And his female relatives could pop out a son from anyone as low as a stableboy and have it claim to be the true heir of Dagara."

show clo snk-serious-flip
with dissolve

sj "For the sake of the stability of this province, I really had no choice."

show clo con-angered
with dissolve

show sej rel-bored-flip
with dissolve

c "Don’t act like some great Raskyan patriot. We had just gotten back to peace before you came."

show sej rel-laugh-flip
with dissolve

sj "Haha. A very short peace it was to be."

show sej rel-bored-flip
with dissolve

sj "Let me teach you something, dear: if war is inevitable, then the sooner it comes, the better."

show clo rel-angered
with dissolve

c "You’re delusional. The war was over. We were content to become Raskyans, we did everything you wanted us to!"

show sej neutral-normal-flip
with dissolve

sj "Well... I know something you don’t."

show sej neutral-normal
with dissolve

"He shrugs and turns from me, watching the spectacle of whores collecting votes for a Holy Election."

show clo reg-stoic
with dissolve

"Just as I think I'm rid of him for now, he continues to speak."

show sej rel-glance
with dissolve

sj "You know, I could have married any of your cousins."

sj "That one girl, what was it, Aelia? She was the Praetor’s own daughter."

show sej rel-bored
with dissolve

sj "I could have spent less money and time on bribes and avoided this whole election if I married his trueborn daughter."

sj "I would be Praetor and this whole affair would be done with before anyone else had to die."

show sej rel-glance
with dissolve

"He leaves that to hang in the air."

"Bastard. Of course I have to ask."

show clo con-angered at left
with dissolve

c "So why didn’t you?"

show sej rel-laugh
with dissolve

sj "I wanted the most beautiful woman here. It's quite below me at this point to have an ugly wife."

show clo con-surprised
with dissolve

show sej rel-bored
with dissolve

"He leaves me alone after that, goes back to staring at the feast."

hide sej
with dissolve

show clo reg-emotional at center
with dissolve

nar "I did not love any of my cousins, especially not arrogant Aelia, but none of them deserved what this beast did to them."

nar "They were thrown naked and crying to his men like slabs of meat to a pack of wolves."

show clo defen-crying
with dissolve

nar "By the Ghosts, I’m thankful I was not present to witness that horror."

nvl clear

hide clo
with dissolve

"One by one, Sejanus’s girls return the baskets of votes to the high table, to the seat of the Pontifex, a man of about thirty."

"Our local high priest, he has one slave girl massaging his back and another below the table. He giggles drunkenly as he counts the votes."

"At last, he knocks back the rest of his wine, and then gestures to me."

stop sfx1 fadeout(0.5)

show clo snk-worry
with dissolve

stop music fadeout(1.0)

"All the eyes upon me make my stomach feel all the wine I’ve had, and I fight not to throw up."

po "Behold, your new Praetrix Clodia!"

show clo snk-serious
with dissolve

play sound applause

"Now they applaud for me. Or at least they think they do."

no "To Praetrix Clodia! May her sons be long-lived!"

play sound applausealt

"In truth, they toast Sejanus."

show sej neutral-smile at left
with dissolve

sj "Congratulations, my dear."

hide clo
with dissolve

show sej neutral-serious at center
with dissolve

show clo reg-stoic at left
with dissolve

play music priestly

"Sejanus rises from his seat, and the room falls silent. His audience is captive."

show sej rel-laugh
with dissolve

sj "What a wonderful choice you’ve all made."

sj "It is nights like this where our grand Republican Kingdom of Raskya proves once again that its way of government is the surest and most just in all Mundus."

show sej rel-glance
with dissolve

sj "Now I have one further announcement to make, and then you can all fuck and drink yourselves to bed!"

show sej diplo-per
with dissolve

sj "Let it be known that Praetrix Clodia and I are engaged to be married!"

show clo snk-serious
with dissolve

"He barely finishes his statement before the cups are raised again."

play sound cheer1

nos "Hail Praetor Sejanus! Hail Praetor Sejanus! Hail Praetor Sejanus!"

show sej rel-laugh
with dissolve

sj "Thank you my friends. A beautiful new future begins for me, the Praetrix, you, and our City of Dagara!"

show sej neutral-smile
with dissolve

sj "The wedding will take place tomorrow morning, so be sure you’re all at the Temple when-"

hide sej
with dissolve

show clo reg-declaring at center
with dissolve

stop music fadeout(0.5)

c "I must have the Maiden’s Day!"

show sej neutral-angry at left
with dissolve

nos "......."

show clo reg-stoic
with dissolve

"That shut them all up. Though now they all look at me with mixtures of confusion, lust, and surprise."

show sej neutral-serious
with dissolve

sj "Ah... my dear, are you certain? There is really no need, I trust that-"

show clo reg-declaring-flip
with dissolve

play music nostalgia

c "I insist! I have not been a pure woman, Sejanus."

show clo reg-emotional-flip
with dissolve

c "Last year I... I gave myself to a passing vagabond, and to give myself to you now, unclean as I am, would invite disaster from the Ghosts."

c "I refuse to jeopardize our wedding because of my sins."

show sej neutral-angry
with dissolve

sj "I don’t think-"

show clo reg-declaring-flip
with dissolve

c "Your desire to have me as your wife as soon as possible is touching, Sejanus, but I must insist for the good of Dagara, our friend nobles, and the Basileus."

show clo reg-declaring
with dissolve

c "I must spend tomorrow cleansing myself of my wickedness."

hide clo
with dissolve

"And so I take my seat again."

hide sej
with dissolve

"My face is burning, what I just did was ridiculous. But necessary."

"Now I have given myself another day. Hopefully I won’t even need it, but any advantage I see, I must take."

show sej neutral-serious at center
with dissolve

sj "Ahem. Well... very well. Praetrix Clodia will take her Maiden’s Day tomorrow."

show sej diplo-order
with dissolve

sj "But the following morning, we will be wed in the Temple."

show sej neutral-serious
with dissolve

sj "Well then... enjoy yourselves."

hide sej
with dissolve

play sfx1 crowdmurmur

"It takes little time for the conversation to start back up. I know what it’s all about."

show clo snk-serious at center
with dissolve

nar "Maiden’s Days are not particularly rare, but they are almost universally carried out in private."

nar "Announcing to the entire noble collective that one is not a virgin is rather... unheard of."

nar "I suspect I will be remembered as Fellatrix Clodia for this."

nvl clear

scene black
with dissolve
 
centered "If any of them live to remember."

stop music fadeout(1.0)
stop sfx1 fadeout(1.0)

jump manliness

label manliness:

scene black
with dissolve

pause 2.0

play movie("images/Video/Vtrans0.mkv")
$ renpy.pause(5.5,hard=True)
stop movie

scene ValeriusRoomNight
with fade

play music aftershock

pause (1.0)

nar "I can’t focus."

nar "Usually, the sound of my peers carousing about in their rooms doesn’t get to me. At first it was even funny to know all the things the other tribunes shout at their slaves when they fuck them."

nar "Tribune Horatius desperately wants to fuck his mother, from the sounds of it. And Tribune Antonius calls all of his boys \"brother\"."

nar "I suppose the jests they tell in other lands about a natural Raskyan penchant for incest aren't spun out of air."

nar "Tonight, though, it’s too much to bear. My mind is so heavy with plans and worries."

nar "With my Clodia so near and yet not, the sounds of those patrician princes taking their bottled frustrations out is more insulting than ever."

nvl clear

nar "For a year now I’ve been faithful to my Clodia. Night after night I suffer alone while surrounded by the sounds of pleasure."

nar "And Nasrin, the little dog, she’s... her presence is torturous some nights. Often I have to banish her from my room."

nar "Damn. I must stop thinking about these things."

nar "I should leave this place, go on a walk. Perhaps that will tire me out."

nvl clear

scene TentNight
with fade

pause (0.5)

show ser neutral-serious at fleft
with dissolve

pause 0.5

"Ghosts fuck me. I didn’t want to run into {i}him{/i} either."

s "Hail Tribune."

show val rel-normal-flip at fright
with dissolve

v "Hail Legatus."

s "What brings you out of your tomb?"

show val rel-emotional-flip
with dissolve

v "My fellow tribunes making riot in theirs."

show ser neutral-grin
with dissolve

s "What, can’t stand the sound? So drown it out with your own debauchery."

show val rel-normal-flip
with dissolve

v "You aren’t much of a proper Raskyan moralist, are you?"

show ser ac-grin
with dissolve

s "Of course I am. Fucking is as Raskyan as it gets."

show val ac-fact-flip
with dissolve

v "I hear tell they fuck in other nations too."

show ser ac-insulting
with dissolve

s "Ha, if you can call it that. The Peladonians have sex. The Durjans make love. It’s we Raskyans who {i}fuck{/i}."

v "Regulius’s poetry."

show ser ac-grin
with dissolve

s "The only poetry worth a damn as far as a military man should be concerned."

show ser neutral-serious
with dissolve

"Shit, what does he want?"

s "Where’s your little Peladon girl?"

show val rel-normal-flip
with dissolve

v "Out and about."

show ser neutral-grin
with dissolve

s "You share her with the men. Risky."

show val rel-emotional-flip
with dissolve

"Sure, let’s go with that."

show val rel-normal-flip
with dissolve

v "I let her go about her business when I don’t need her. If someone wants to borrow her, what of it?"

show ser ac-insulting
with dissolve

s "You’ll end up borrowing the burning piss from them in return. But I suppose it’s one of your manlier qualities."

s "At least you haven’t brought a wife with you, then you might as well just be a woman."

show val ac-fact-flip
with dissolve

v "There are great men and heroes and, I believe, even a Basileus or two who brought their wives on campaign."

show ser neutral-annoyed
with dissolve

s "Aye, and every history of them you’ll read makes sure to pause and criticize them for it."

show ser com1-barking
with dissolve

s "To love a wife is to be her slave, and it is below the dignity of any Raskyan citizen to be a slave."

s "Keep your love for whores and slaves. A wife is only good for children."

show val rel-serious-flip
with dissolve

"If you say so."

show ser ac-insulting
with dissolve

s "Sharing your whore must make you popular with the men, no?"

show val rel-emotional-flip
with dissolve

v "You would think so."

show ser ac-annoyed
with dissolve

s "You really don’t get why no one likes you, Tribune Aureus. It’s astounding."

show val rel-normal-flip
with dissolve

v "I have a few ideas."

show ser neutral-annoyed
with dissolve

s "Why is it that we never hear you fucking your whore?"

show val ac-annoyed-flip
with dissolve

v "We’re quiet."

show ser com1-barking
with dissolve

s "Why? Conquest is a loud affair. For fuck’s sake, boy!"

show ser com1-boasting
with dissolve

s "When men get their blood high they laugh, the get aroused, their spirits raise. They get ready to go out and fuck. Going out and fucking is the heart of the legion."

show ser neutral-grin
with dissolve

s "Well you won’t hear most writers put it that way, but its the truth. We fucked Dagara until it submitted, and now we will fuck it again for Sejanus’s treachery."

show ser neutral-annoyed
with dissolve

s "The men don’t like you because you’re a bore. You lock yourself away like a sexless corpse while the other tribunes drink, party, fight and fuck with the men."

show ser ac-annoyed
with dissolve

s "I don’t know what’s wrong with you, quite frankly."

show val rel-emotional-flip
with dissolve

"Ghosts of War... please, let a stray javelin catch him in the throat tomorrow."

"I will sacrifice ten bulls for you if you do this, I swear."

show val rel-normal-flip
with dissolve

v "If I might be so bold as to inquire why this is relevant to military matters, Sergius?"

show ser ac-insulting
with dissolve

s "Ghosts be good, do you suckle your mother’s tits with that mouth? You read too much pretentious poetry."

show val ac-fact-flip
with dissolve

v "Reading is the only reason I’m any good at my job."

show ser neutral-annoyed
with dissolve

s "That’s the problem. You have no natural instinct for it. No genius."

s "You’re a farmer pretending to be a soldier."

show val ac-annoyed-flip
with dissolve

v "With all due respect sir, I was conscripted from my plow."

show ser neutral-serious
with dissolve

s "Aye, and you served your years. Why did you come back when I called the legions?"

s "You didn’t have to. You probably shouldn’t have."

show val rel-emotional-flip
with dissolve

v "..."

s "I thought that was very strange."

show val rel-normal-flip
with dissolve

v "Frankly, its the money, sir."

show ser ac-insulting
with dissolve

s "Hahaha! Fuck, now that I believe. Whores love money above all else."

show ser ac-grin
with dissolve

s "Well, you’ll have plenty of money to retire to your womanly ways once this is all over. We’re going to strip everything of value from that city, like we should have last time."

show ser neutral-serious
with dissolve

s "But, I can’t really complain that things went this way. After all, the Ghosts have provided me a..."

show ser neutral-grin
with dissolve

s "Heh. Why should I even tell you?"

show val ac-annoyed-flip
with dissolve

"Oh come now. You obviously want to tell me, don’t put on a show."

stop music fadeout(1.0)

v "Tell me what?"

show ser neutral-serious
with dissolve

play music nostalgia

s "Hmm... you’ve seen Raskylus’s Grand Spoils in the Temple of War, yes?"

show val rel-normal-flip
with dissolve

nar "In the capital. The Temple of War, where great relics and artifacts of heroes and battles gone by are kept."

nar "The centerpiece is a rather mundane looking breastplate, helmet, and greaves, with a spear and shield propped up against them. Every boy who ever even visits the capital is sure to see the display before they leave."

nar "It’s the armor and arms of Golgas, last king of the Bareti."

nar "Raskylus, the first king and founder of all Raskya, slew Golgas in single combat, and stripped his armor and weapons from the corpse in the middle of a battle."

nar "It’s legendary."

nvl clear

v "I have seen it. Many years ago."

s "And Drianus’s Grand Spoils. You saw those too?"

show val ac-fact-flip
with dissolve

v "They’re right next to Raskylus’s. Hard to miss."

nar "Drianus’s is a more complete, more expensive looking suit of armor with no weapons. It sits just to the side of Raskylus’s in the Temple."

nar "Drianus stripped that armor from Kyrosho, a great general of the Peladonians. Again, after killing him in a duel on the battlefield."

nar "That’s the key, after all."

nvl clear

nar "When Raskylus did it, the Ghosts of War came to him and told him that his action would forever be remembered as the greatest act a Raskyan could perform for the state."

nar "Any Raskyan general who killed an enemy leader personally and stripped his equipment would transform that armor and weaponry into the Grand Spoils. An eternal symbol of heroism and faithfulness to Raskya."

nar "Only Drianus has replicated his feat. And Drianus lived a hundred years ago, Raskylus almost two hundred years before that."

nar "It’s incredibly, incredibly rare for two generals to actually meet in the middle of a battle, much less for one of them to successfully deliver the killing blow to the other."

nar "With thousands of soldiers on a battlefield, the odds that two specific men will meet and fight are near zero."

nar "What the hell are you planning, Sergius?"

nvl clear

show ser com1-boasting
with dissolve

s "Well, this time next year, little boys who want to grow into heroes will step into the Temple of War, and they will see a third suit of armor."

show ser com2-boasting
with dissolve

s "And if they want, they will be able to meet the man who stripped the Grand Spoils from Sejanus’s cold corpse."

show val rel-normal-flip
with dissolve

v "You seem to think you’ll meet him in battle. You know that’s unlikely."

v "And anyway, the Grand Spoils have always been taken from barbarian generals. A Raskyan legatus is another thing."

show ser com1-rage
with dissolve

s "A traitor. No Raskyan any longer. An enemy commander is an enemy commander."

show val ac-fact-flip
with dissolve

v "Still. How do you expect to have single combat with him?"

show ser neutral-serious
with dissolve

s "You think I’m mad."

show val ac-annoyed-flip
with dissolve

v "No, I just don’t understand your certainty."

"You are quite mad, though."

show ser ac-annoyed
with dissolve

s "I don’t need to explain myself to you. The Ghosts will give me what I want."

show val rel-serious-flip
with dissolve

"Ah. Well, I shall not argue against that logic."

show ser neutral-serious
with dissolve

s "Anyway, why am I telling you? It doesn’t concern a brownblood with no taste for war like yourself."

show val rel-emotional-flip
with dissolve

"Again. When will this abuse end? Have you truly nothing better to do than insult me for not having the same kind of hatred in my heart as you?"

show val rel-normal-flip
with dissolve

v "Will that be all then, Legatus?"

show ser neutral-serious-flip
with dissolve

s "Dismissed."

hide ser
with dissolve

hide val
with easeoutleft

"I can’t leave the tent fast enough."

scene black
with fade

centered "The air outside is cool on my burning face."

pause 0.2

centered "I want Clodia."

pause 0.2

centered "I want Clodia and I want to be away from this mess of brutes forever."

stop music fadeout(1.0)

pause 0.5

if plan == "escape":
    jump escape_start

if plan == "clodia_poison":
    jump clodia_poison_start
    
if plan == "nasrin_poison":
    jump nasrin_poison_start

label clodia_poison_start:
    
scene HighHall
with fade

play music timekiller

"It seems as if the evening is winding down."

"Half the nobles have left with their slaves. Some of them are passed out in their chairs."

show sej rel-bored
with dissolve

"Sejanus sips occasionally from his cup. Now would be the perfect time, I think. There’s no attention being paid to us anymore."

"He even looks a little tired himself, he won’t notice."

show clo reg-stoic at left
with dissolve

"The vial is clasped in my palm. Earlier in private I practiced slipping the stopper out and seeing how fast I could move it over to my left and back."

"It really doesn’t look like there’s much in it, so I can just dump all of it inside as fast as possible."

"I just have to find the right moment..."

show sej rel-glance
with dissolve

sj "Did you want something, my lady?"

show clo snk-worry
with dissolve

c "Uh, no. No, just... uh, you look tired. Will you be going to bed soon?" 

show sej neutral-normal-flip
with dissolve

sj "I have been drinking to excess this evening. Perhaps I don’t look it, but I am very inebriated."

show clo reg-stoic
with dissolve

c "Ah."

sj "Also, sitting up during eating is strange on my digestion."

sj "In Raskya they recline on couches during meals. I’ll have to have some couches brought up once all the mess outside blows over."

c "If you say so."

show sej neutral-smile-flip
with dissolve

show clo snk-worry
with dissolve

sj "Hmm... so, who was this passing vagabond who took your maidenhead from me? How old were you when you gave yourself to him?"

show clo reg-stoic-flip
with dissolve

c "It doesn’t matter, I’ll cleanse myself tomorrow and it will be as if it never happened."

sj "Only in the eyes of the Ghosts. I’d like to know what kind of scoundrel defiled my wife before I ever got a chance at her."

c "Just some man that was handsome and charming enough to stoke my womanly lust."

show sej diplo-per-flip
with dissolve

show clo snk-worry-flip
with dissolve

sj "Ha. I didn’t know you had any."

show clo rel-angered
with dissolve

show sej neutral-normal-flip
with dissolve

c "Sieges are not the time for lust."

sj "What are you saying, Clodia? Look at what all these fools do with the slaves I’ve given them."

show sej neutral-smile-flip
with dissolve

sj "There’s no time more perfect for lust than when you know you are going to die."

show clo reg-stoic
with dissolve

"I hope you’ve had your fill, then. Now look away for one moment, damn it!"

sj "How old were you? Fourteen? Thirteen?"

show clo con-angered
with dissolve

c "It was only last year. Does that make it better or worse for you? To know you missed my maidenhead by only a year?"

show sej neutral-angry-flip
with dissolve

sj "Ah, curse you then. I wouldn’t have pursued this subject if I knew you would become cruel about it."

show clo reg-stoic
with dissolve

sj "I do hope this attitude disappears once we’re wed."

show sej rel-bored-flip
with dissolve

"Scoffing, he takes his cup, swigs from it, slams it down-"

show sej neutral-angry
with dissolve

stop music fadeout(1.0)

"-and looks away."

show clo snk-serious
with dissolve

"Now. Now!"

"My hand moves, my palms are slick."

"It’s only an instant, but the stopper is pulled."

"The vial hovers over the cup."

"I tilt it."

show sej diplo-order
with dissolve

show clo snk-worry
with dissolve

play music priestly

sj "Ghosts, I need a good hole to fuck!"

"His sudden outburst startles me."
 
"I draw my hand back, my heart racing. The vial remains full."

show clo reg-stoic
with dissolve

"Did he see?"

show sej rel-bored
with dissolve

"He grasps the cup, still unpoisoned, and downs the rest of it."

show sej rel-glance
with dissolve

"Even shakes it out in front of me, almost as if he knows."

show clo snk-worry
with dissolve

"But he doesn’t know. He can’t know!"

show sej neutral-serious
with dissolve

sj "I was looking forward to the pleasure of your cunt tomorrow morning. But now that I have to wait even longer, fuck it."

show sej diplo-threat
with dissolve

"He grabs the arm of the nearest serving girl. The look she gives him is painfully disinterested."

sj "Come with me."

show sej at right
with dissolve

show sej neutral-angry-flip
with dissolve

"Shooting me a final, horrible look, he drags the girl out of the high hall with him."

hide sej
with dissolve

show clo reg-stoic
with dissolve

c "Curse the Ghosts."

show nas rel-blank-flip at right
with dissolve

nar "Across the room, Nasrin looks at me for a moment. She saw."
 
nar "Damn nymph is probably mocking me in her head. Thinking about how she could have done it so much better."

hide nas
with dissolve

show clo at center
with dissolve

nar "But Valerius did say she was to help me escape, one way or the other. Tonight, she’ll come to my room and tell me what to do next."

nar "I just hope it’s possible now, without the confusion of Sejanus’s death."

nvl clear

scene black
with fade

nar "The pill of death in my palm... I’ll hold onto it."

nar "I still may need it. I still may get a chance."

stop music fadeout(1.0)

nvl clear

jump escape_start_a

label escape_start:
    
scene black
with fade

nar "It's a long, painful evening."

nar "I urge the sun to fall faster, yet the sundial in the courtyard taunts me."

nar "At meals I see Valerius’s freedwoman serving the nobles, and not once does she look back at me. As if nothing is amiss."

nar "Sejanus ignores me for most of the evening. My one mercy."

nvl clear

jump escape_start_b

label escape_start_a:
    
scene black
with fade
    
nar "Since I failed to kill my betrothed, I'm stuck relying on that freedwoman to save me."

nar "I suppose I shouldn't be picky about where my salvation comes from."

nvl clear

jump escape_start_b

label escape_start_b:
    
nar "At last, after dinner, night falls."

scene ClodiaRoomNight
with fade

nar "I go to my room, change out of my dress and into the plainest garment I own, and wait."

show clo2 reg-stoic at left
with dissolve

nar "And before long-"

show nas2 rel-blank-flip at right
with dissolve

show clo2 snk-relief with dissolve

nar "-she arrives."

nvl clear

show nas2 rel-defaul-flip
with dissolve

n "Oh good, you’re dressed like a proper nobody. You don’t have anything darker though?"

show clo2 reg-stoic
with dissolve

c "Well no."

show nas2 arm-blank-flip with dissolve

n "Ah. Okay, it will have to do. Come on then."

scene black
with fade

centered "She holds out her hand."

centered "Trusting this woman, if only because of Valerius, is my only hope."

scene Sewer
with fade

nar "I know the tunnels she’s led me to. Sort of."

nar "When I was younger I would explore down here, which was easy to get away with when you were the Praetor’s unimportant half-niece."

nar "I never explored very far, really. Certainly wouldn’t be able to find my way out now."

nar "But Nasrin guides me surely, like a hound on a scent. The torchlights are few and far between, but she navigates even the dark spaces like it was broad daylight."

nar "Naturally, she must be slipping in and out of the citadel through the sewers. I wonder what exitway Sejanus’s men have failed to discover?"

nvl clear

show nas2 rel-blank-flip
with dissolve

show clo2 snk-worry-flip at right
with dissolve

"Suddenly, we stop in our tracks."

show clo2 snk-serious-flip
with dissolve

"In the faint light of the nearest torch, I can see we’ve come to the end of a hallway."

show nas2 rel-blank
with dissolve

"Nasrin puts her finger to her lips, then gestures around the corner. Does she want me to take a peek?"

show nas2 arm-blank
with dissolve

n "The exit is right there. There’s three guards."

"She's so quiet, it takes me a moment to realize she’s spoken. Then another to understand what she said."

"Damn it, did Sejanus find her escape route?"

show nas2 rel-blank
with dissolve

n "I’ll distract them. Go outside and hide in the bushes there. I'll find you."

hide nas2
with dissolve

show clo2 snk-worry-flip
with dissolve

show nas2 rel-defaul-flip at left
with dissolve

"Before I can even nod, she lets go of my hand and steps out into the torchlight."

hide clo2
with dissolve

show nas2 at center
with dissolve

"I can’t help but peek a bit, watching as she approaches the guards. She’s not even trying to hide from them."

g "Oh, thank the Ghosts of Fuckin, luv, I thought you’d never come back."

show nas2 sed-sly-flip
with dissolve

"He places his hand on her shoulder. She doesn't pull away."

show nas2 rel-defaul-flip at left
with dissolve

"Then, violently, as if arresting her, he turns her and marches her into what must be another room."

hide nas2
with dissolve

nar "The two other guards follow, smiling. A few seconds later I hear a door slam shut."

nar "And the tunnel is open out into the night."

nvl clear

scene black
with fade

centered "Letting out my breath, I run."

scene StreetsNight
with fade

pause 1.0

nar "The night air is refreshing and alive. Even crouched awkwardly in this thicket I can’t help but enjoy it."

nar "Now only the city and a wall lies between us, Valerius."

nar "I’m guessing the girl must know another round of crooked, horny men guarding the wall."

nvl clear

show nas2 rel-defaul-flip at right
with dissolve

"After what seems like forever, Nasrin comes out of the tunnel."

show nas2 at center
with dissolve

"Before I can show myself to her, she walks right up to the thicket and simply gestures for me to follow."

show clo2 reg-stoic at left
with dissolve

"The moonlight glistens off the sweat on her body, but she hardly seems winded."

c "Uh... are you okay?"

show nas2 rel-defaul-flip
with dissolve

n "Fantastic. Probably could use a bath now, but whatever."

c "Well, if it’s all the same..."

show clo2 reg-stoic-flip
with dissolve

c "I won’t hold your hand anymore."

show nas2 rel-grin-flip
with dissolve

n "Haha. Okay, milady. Let’s get going."

show clo2 snk-serious-flip
with dissolve

c "And please don’t breathe on me."

hide nas2
with dissolve

hide clo2
with dissolve

nar "As I follow her into the city, I can’t help but wonder."

nar "This bedslave happens to be such an adept spy."

nar "Peladonian whores and bedslaves are famous and expensive and bred from birth. But Peladonian spies rival them in those qualities."

nar "And they are both trained at the same schools, so they say."

nvl clear

scene black
with fade

centered "Just how did my Valerius meet this woman?"

if siege == "changed":
    jump night_assault
    
if siege == "normal":
    jump caught_violently

label nasrin_poison_start:

scene HighHall
with fade

play music timekiller

pause 1.0

nar "It seems as if the evening is winding down."

nar "Half the nobles have left with their slaves. Some of them are passed out in their chairs."

show sej rel-bored
with dissolve

nar "Sejanus sips occasionally from his cup. Now would be the perfect time, I think."

nar "There’s no attention being paid to us anymore. He even looks a little tired himself, he won’t notice."

nar "So what are you waiting for, Nasrin?"

nvl clear

hide sej
with dissolve

show nas rel-defaul-flip at right
with dissolve

"Most of the evening she’s been over on the other side of the hall, playing at being a serving girl like any of the others."

show nas rel-defaul at right
with dissolve

"She must know Sejanus won’t be here forever. His eyes are drooping, he’s sure to head off to bed soon."

hide nas
with dissolve

show sej neutral-normal
with dissolve

sj "Well I’m just about done with all this nonsense. What about you, Clodia?"

show clo snk-worry at left
with dissolve

show sej rel-glance
with dissolve

sj "Damn the rules, let’s go have our first fuck. I’m feeling it."

"No."

"No, don’t do this. You-"

show clo snk-relief
with dissolve

c "You must be jesting. That would be improper. I’m not even cleansed yet."

show sej rel-bored-flip
with dissolve

sj "Exactly! What will it even matter, when it will all be purified tomorrow?"

show sej diplo-per-flip
with dissolve

show clo snk-worry
with dissolve

sj "Come on, I promise you I’m quite good at it."

show clo defen-declaring
with dissolve

show sej rel-bored-flip
with dissolve

c "I don’t think so."

show clo reg-emotional
with dissolve

"Assuming you’re even giving me a choice. Ghosts, don’t let this happen, please."

show nas rel-grin-flip at right
with dissolve

n "Um, master?"

show clo snk-relief
with dissolve

show sej rel-glance-flip
with dissolve

sj "Hm? What is it?"

show nas sed-sly-flip
with dissolve

n "You need me."

show sej neutral-normal
with dissolve

sj "I- beg your pardon?"

show nas sed-flirtatious-flip
with dissolve

n "You’re so hard down there. Let me take care of you."

show clo rel-surprised
with dissolve

"You can’t be serious."

show sej diplo-order
with dissolve

show clo rel-neutral
with dissolve

sj "Reign yourself in, girl. Not at the table. Come along if you want to serve me."

show nas sed-laughing-flip
with dissolve

n "Of course, my lord."

show sej neutral-smile
with dissolve

"He rises from his chair, her hand clasped in his. By all the Blessed, she’s got him."

show sej rel-glance
with dissolve

sj "Clodia. Come along."

show clo snk-worry
with dissolve

show nas sed-sly-flip
with dissolve

c "Excuse me?"

sj "I said come along. Do as I tell you."

show clo snk-serious
with dissolve

c "For what purpose?"

show nas sed-flirtatious-flip
with dissolve

n "Master, I can please you so much better than she can."

show clo snk-relief
with dissolve

c "Listen to her."

show sej neutral-angry-flip
with dissolve

sj "You come along, or I’ll have you dragged behind me."

show nas rel-annoyed-flip
with dissolve

show clo snk-serious
with dissolve

"Nasrin’s staring at me. I... I can’t ruin this chance for her. For me."

show clo reg-stoic
with dissolve

c "Very well."

show nas sed-sly-flip
with dissolve

show sej rel-glance
with dissolve

sj "Good. Mmm, let’s go. You first, my lady."

stop music fadeout(1.0)

scene HallNight
with fade

show clo reg-stoic
with dissolve

"I’m not really sure where I’m going. Where has Sejanus even taken up quarters? Not my Uncle’s room, by the Grave!"

show clo at right
with dissolve

show sej neutral-smile
with dissolve

sj "Upstairs. We’re going upstairs, my dear."

show clo reg-stoic-flip
with dissolve

c "Where exactly?"

show sej rel-laugh
with dissolve

sj "To your room, why not?"

"Valerius, I’m frightened."

show nas rel-blank at left
with dissolve

"Nasrin, please. I don’t care, by the Ghosts, I’ll give you everything in the treasury if you just kill this man before he has his way."

show clo snk-worry-flip
with dissolve

"Please."

show sej neutral-angry
with dissolve

sj "Move."

scene ClodiaRoomNight
with fade

"The servants have kept the braziers stoked and the shutter closed. The room is warm and inviting, and I am shaking."

show sej neutral-smile
with dissolve

sj "Ah, your bed. How lovely. Is that where it happened, then?"

show clo reg-stoic at left
with dissolve

c "I’m sorry?"

show sej rel-glance
with dissolve

sj "Where you lost your virginity. In that bed, I take it?"

c "It doesn’t matter."

show sej diplo-per
with dissolve

sj "Ha ha, it doesn’t matter? Listen to this girl."

show nas rel-defaul-flip at right
with dissolve

n "Oh it matters quite a bit dear, trust me. You’re lucky you’re getting to marry a man of master’s character."

show nas sed-sly-flip
with dissolve

n "I mean, otherwise you’d end up like me!"

show sej rel-laugh
with dissolve

sj "Oh, that’s a delicious thought."

show nas sed-laughing-flip
with dissolve

n "She does look like she'd make someone a pretty little pet. I bet there's a wild slut behind that modest face."

show clo reg-emotional
with dissolve

"You don’t have to act this hard, you nasty little thing."
 
"Just kill him already! Don’t defile my bed with him!"

show sej neutral-smile
with dissolve

sj "Hmm. Join me on the bed, slave."

show nas sed-flirtatious-flip
with dissolve

n "Yes master."

show sej rel-bored
with dissolve

sj "What’s your name, by the way? I don’t remember you right now."

show nas sed-sly-flip
with dissolve

n "Uh, Nasrin, sir."

show sej neutral-smile
with dissolve

sj "I love those firey little Peladonian names."

hide nas
with dissolve

hide sej
with dissolve

"Watching them fumble towards my bed, I back towards the corner and turn."

show clo reg-stoic-flip
with dissolve

"Maybe they’ll just forget me?"

show sej neutral-serious-flip
with dissolve

sj "Clodia. Watch."

show clo rel-angered
with dissolve

c "This is exceptionally improper, my lord."

show sej neutral-angry-flip
with dissolve

show clo reg-stoic
with dissolve

sj "You silence your mouth about all this proper nonsense already. You let a man fuck you before you were married."

show nas rel-blank-flip at right
with dissolve

sj "You’re barely better than this one."

show nas rel-defaul-flip
with dissolve

show sej rel-glance
with dissolve

sj "Now watch me take my pleasure. Watch what I’ll do to you."

show sej diplo-order
with dissolve

show nas rel-blank
with dissolve

"He pushes Nasrin down onto my bed. Turns her over on her stomach."

show sej neutral-smile
with dissolve

show clo rel-surprised
with dissolve

sj "Spread your ass."

show nas arm-blank
with dissolve

n "Um, master, could we have some wine first?"

show sej neutral-serious
with dissolve

show clo reg-stoic
with dissolve

sj "What for?"

show nas rel-defaul
with dissolve

n "It helps me relax myself. For, ah..."

show nas rel-grin-flip
with dissolve

n "{i}That{/i}."

show sej neutral-smile
with dissolve

sj "Ha. Wine sounds good."

show clo rel-neutral
with dissolve

"Yes. It does sound good, doesn’t it?"

show sej rel-bored
with dissolve

show nas rel-defaul-flip
with dissolve

sj "But by lust. I’ll fall asleep if I have any more."

show clo rel-surprised
with dissolve

show sej diplo-per
with dissolve

show nas rel-blank
with dissolve

sj "Just relax, you Peladon girls are supposed to be good at this kind of thing."

show nas arm-frustrated
with dissolve

n "Ah! Master, but I-"

show sej diplo-threat
with dissolve

show sej neutral-angry
with dissolve

show nas act-wincing
with dissolve and hpunch

"He stikes the back of her head."

show nas rel-annoyed
with dissolve

show clo snk-serious
with dissolve

show sej neutral-serious
with dissolve

sj "Just hold still."

show nas rel-blank-flip
with dissolve

show clo reg-stoic
with dissolve

"For one moment, she looks desperate. Her eyes meet mine, and my stomach drops."

"And she moves her hand to her mouth. So quick, so slight, I just barely noticed."

show sej neutral-normal
with dissolve

show nas rel-defaul-flip
with dissolve

n "Wait. Let me kiss you."

show sej neutral-angry
with dissolve

sj "Damn it all, why?"

show nas sed-sly-flip
with dissolve

n "It will help me. I’ll melt around you if I can feel your lips on mine."

show clo rel-surprised
with dissolve

"Unbelievable."

show nas sed-flirtatious-flip
with dissolve

n "Please master. Kiss me. Then fuck me."

show sej diplo-per
with dissolve

sj "Ah..."

show clo snk-relief
with dissolve

"He’s leaning in. Yes!"

show sej rel-bored
with dissolve

sj "Wait."

show clo rel-angered
with dissolve

show nas rel-defaul-flip
with dissolve

n "What’s wrong?"

show sej neutral-normal
with dissolve

sj "Damn lust made me nearly forget. You’ve surely been sucking cock all evening, I don’t want to taste that."

show clo reg-stoic
with dissolve

show sej neutral-angry
with dissolve

sj "Ugh, don’t breathe on me. Turn away and shut up until I’m done."

show sej diplo-threat
with dissolve

show nas rel-annoyed
with dissolve

"Pushing her back down, he turns her over."

show clo snk-worry
with dissolve

"Damn it, what can she do now? He’s stronger than her. I can’t just stand here and watch this."

show clo defen-wincing2
with dissolve

c "Sejanus, please, don’t do this in front of me!"

show clo reg-emotional
with dissolve

show sej neutral-angry-flip
with dissolve

sj "You both need to shut up and understand your place."

show nas sed-sly-flip
with dissolve

n "But master, if I could just-"

show sej diplo-threat
with dissolve

show sej neutral-angry
with dissolve

show nas act-wincing-flip
with dissolve and hpunch

"He strikes her again, this time across the face. Hard. She coughs."

show sej neutral-serious
with dissolve

sj "What the... did I seriously knock out one of your teeth?"

show nas act-stealthy-flip
with dissolve

show sej rel-bored
with dissolve

"And then, he holds something up to the poor light of the braziers."

show clo snk-serious
with dissolve

"I know what it is. I can’t see it but I know."

show sej diplo-threat
with dissolve

sj "You little-"

play music menacing

show nas act-attacking-flip
with dissolve

show sej sin-surprise
with dissolve and hpunch

"She kicks him in the stomach."

show clo rel-surprised
with dissolve

"The surprise stuns him more than anything, and he’s too drunk to react in time."

hide sej
with dissolve

hide clo
with dissolve

show nas act-stealthy-flip at left
with ease

"Like a shadow she glides from the bed, darts away."

show clo snk-worry-flip at center
with dissolve

show nas rel-blank
with dissolve

"She looks back at me for one moment as she throws open the door."

show nas rel-blank-flip
with dissolve

hide nas
with easeoutleft

"Then she’s gone."

show clo snk-serious-flip at left
with ease

"What was she trying to say with that look? I’m sorry?"

show sej sin-snarl-flip at right
with dissolve

sj "Bitch! That traitorous bitch! She tried to kill me!"

show clo snk-worry
with dissolve

"He looks at me. I have to say something. Something before he starts to fill in the gaps himself."

show clo defen-wincing2-flip at left
with dissolve

c "Guards! Guards help! A slave tried to kill Sejanus! Come quickly!"

hide clo
with dissolve

"In seconds there are guards in the room, checking on Sejanus."

show sej neutral-angry at center
with dissolve

"He regains his composure well enough, but even in this poor light, in the glint of the brazier in his eyes, I see he’s shaken."

show sej diplo-order
with dissolve

"He shows his men the little vial that she tried to slip into his mouth. After they take their best look at it, he tosses it onto the flames."

show clo reg-stoic at left
with dissolve

"I’m forgotten in the corner as they rant and rave and debate."

show sej neutral-serious-flip
with dissolve

hide sej
with easeoutleft

stop music fadeout(1.0)

"And at last, they leave. Sejanus doesn’t even acknowledge me on the way out."

show clo rel-neutral at center
with ease

"Wonderful."

show clo reg-stoic
with dissolve

"But everything has been ruined. What do I do now?"

scene black
with fade

centered "What the {i}fuck{/i} do I do?"

if siege == "changed":
    jump night_assault
    
if siege == "normal":
    jump night_before_battle
    
label night_assault:
    
scene CampNight
with fade

pause 1.0

nar "I do not miss partying with the footsoldiers. Especially the youngbloods."

nar "They are rough, stupid men for the most part, with little of interest to say beyond how big their cocks are, how many barbarian girls they fucked at the last village, and how much wine they can drink."

nar "These don’t have to be boring subjects necessarily, but youngbloods are very good at making them seem downright tiring."

show val ac-fact
with dissolve

nar "But tonight, I have to endure their company. After Sergius's... airing of greivances with me, I don't think I can avoid it tonight."

nar "Luckily, few want to speak to me. They're all content with each other just the same as if I weren't around."

nar "Although I can sense a twinge of unease. After all, something strange must be going on if Tribune Aureas is drinking with his men."

nvl clear

show val ac-fact at fleft
with dissolve

show sol neu-shout-flip at sright
with dissolve

"A nearby legionary seems to be having a particularly good night."

hide val
show sol neu-shout-flip at scenter
with dissolve

so "Hey friend, drink up! You look like you have something there to finish!"

show sol neu-shout
with dissolve

so "Hail good man! Are you drunk yet? If not, here you are, drink this!"

show sol neu-shout-flip
with dissolve

so "You big pussy, I thought you were of barbarian stock? Prove it!"

hide sol
with dissolve

nar "What an advocate for the Ghost of Wine. He's already got them all to forget that wine cart isn't ours."

nar "Surely this is the drunkest maniple I've ever dared to mingle with."

nar "Their centurion wobbles where he stands, as stoic as he tries to look."

nar "Yes, I'd say these men are just about ready."

nvl clear

nar "The siege engines destroyed the defensive towers just an hour or so ago. Sergius hardly seemed to notice."

nar "By now he’s been well into his own party with whores and wine and the other tribunes in the war tent."

nar "Funnily, tonight I’ve learned that the common rumor in camp is that the tribunes all compete with each other to spit roast whores with the general."

nar "If that were true, it would only secure my position as least favored tribune."

nvl clear

nar "The sun is setting now. It's time."

nar "Drink alone is the seed, and these men’s lusty hearts are fertile soil. Now they just need a little rain."

nar "The advocate for wine stands up on a stump and is suddenly giving a speech to everyone else."

nar "He's doing very well. It's a relief to know my money was well spent."

nvl clear

show sol neu-shout
with dissolve

so "Great Ghosts, can you imagine what kind of noble girls are dripping for us past those walls?"

show sol neu-shout-flip
with dissolve

so "It’s been forever since I had a girl. What about you? It would be good to take some traitor bitch tonight!"

show sol neu-shout
with dissolve

so "This is going to be better than barbarian villages, boys! We didn’t get to fuck those Dagaran whores last time, but tonight we’ll more than make up for it!"

hide sol
with dissolve

nar "These men are getting quite in touch with their beasts. They scream, they let out war cries, they stare longingly at the city walls."

nar "They stand at the edge."

nvl clear

show sol neu-shout at sleft
with dissolve

so "By Ghosts I’m ready to fuck something!"

show sol neu-shout-flip at sright
with dissolve

so2 "I’m gonna find me a little virgin, nice and sweet. Show her what it’s like to have a husband!"

show sol neu-shout at sleft
with dissolve

so "Praise Lust and War, I’m ready to feel alive again!"

show sol neu-shout-flip at center
with dissolve

so "The defensive towers are gone, just what are we waiting for anyway? Orders? So that Sergius and his cocksucking tribunes can have all the best girls first?"

sos "NO!!!"

show sol neu-shout
with dissolve

so "I’m ready to grab a ladder and run up there myself!"

sos "AYE!!!"

hide sol
with dissolve

nar "And that breaks the bubble."

nar "Chaos around me."

nar "Now they think it’s begun."

nar "What is there to fear? We’re the stronger army, they’re a bunch of traitors, and anyway..."

nar "Fucking a noble girl or boy is beyond the wildest dream of almost all these brownbloods. And no one wants to be last."

nar "Few things could motivate them more."

nvl clear

nar "Ladders are grabbed, torches lit. Some men have already gone careening ahead. There is no order."

nar "The centurion tries to organize his men, but his whistle is ignored. Two men fight over a helmet and one is punched to the ground."

nar "There is fire in every pair of eyes, or is that the torchlight?"

show val cmd-urgent at fleft
with dissolve

v "All of you, stop this instance! Order! Order!"

so "Shove order up your ass!"

show val ac-annoyed at center
with dissolve

v "Who said that?"

hide val
with dissolve

nar "Someone long gone by now."

nar "Soon they're all gone. I make a show of giving chase, but what's about to happen is clear. As soon as they are all ahead of me, I stop."

nar "Time to turn around, be a good tribune, and go stew in the war tent."

nvl clear

scene black
with fade

nar "On my way to crash Sergius's party, I look back."

nar "The torches of the drunken attackers are little red coals in the distant dark."

nar "In the last light of the blood orange sun I see the ladders going up. All around me, the rest of the army wonders what is going on."

nar "They want to attack too, I can see it."

nar "Hell, I can see another maniple already on it's way, ladders in tow."

nar "This one is organized, led by their centurion with dignity, but they are marching quickly all the same."

nvl clear

centered "Just as I thought. When it comes to a sack, no man wants to be last."

if plan == "escape":
    jump caught
    
if plan == "nasrin_poison":
    jump aftermath
    
label caught:

scene StreetsNight
with fade

pause 1.0

nar "A distant roar has vexed us from the moment we began to move through the city."

nar "It’s an ocean roar, powerful and vast. I cannot tell where it comes from. It seems to echo off every building in Dagara’s crisscrossed streets."

nvl clear

show clo2 snk-serious-flip at right
show nas2 rel-blank-flip at center
with easeinright

"Nasrin leads me as surely through the streets as she did in the sewer. I feel propelled towards destiny in her care."

show nas2 act-stealthy-flip
show clo2 snk-worry-flip
with dissolve

show nas2 at right
with ease

"She suddenly pulls me into a narrow side street, for seemingly no reason."

hide clo2
hide nas2
with dissolve

show sol neu-neu at sleft
with dissolve

nar "Until a scant patrol of two soldiers goes by."

nar "There should be more of Sejanus's men out, shouldn’t there? Should it be this easy?"

nar "These two look rather animated about something."

nvl clear

so "War be with us, I think we can actually beat them off. It’s dark and the walls haven’t even been knocked down, what are they playing at?"

show sol neu-neu-flip at scenter
with dissolve

so2 "That’s not like Sergius. He hates night assaults."

show sol neu-confuse at sleft
with dissolve

so "How would you know?"

show sol neu-neu-flip at scenter
with dissolve

so2 "Well I used to fight under his legion, few years back. Most of us did. You’re new, you don’t remember serving under \"Tribune\" Sejanus, do ya?"

show sol neu-neu at sleft
with dissolve

so "No. I’ve heard the other guys talk about it though. None of them ever seem surprised things turned out this way."

show sol neu-neu-flip at scenter
with dissolve

so2 "Ah, that’s a long story. My point was, Sergius gave more than one speech about how cowardly night attacks are."

show sol neu-neu at sleft
with dissolve

so "Well... seems like a good way to make an enemy never expect a night atttack, eh?"

show sol neu-neu-flip at scenter
with dissolve

so2 "Why would he waste that surprise on us? He’s a better commander than that."

show sol neu-neu at sleft
with dissolve

so "Who knows? Anyway, we don’t need to beat them in battle, it’s all-"

hide sol
with easeoutright

show nas2 rel-blank-flip at center
with dissolve

show clo2 snk-serious-flip at right
with dissolve

"Nasrin pulls at me again, and I lose the patrol's conversation as we speed down high, narrows stairs, teetering tenement buildings risings around us."

hide nas2
hide clo2
with easeoutleft

nar "The roar is fading, but not much. We’re moving farther away from it, wherever its coming from."

nar "A night assault would explain why these streets aren't clogged with soldiers. Thank the Ghosts."

nar "I just pray it won't make getting through the wall harder. There's no way we can just sneak through a battle."

nar "Is there?"

nvl clear

scene black
with fade

nar "Suddenly, Nasrin leads me down stairs that sink into the earth, into a pitch black dankness."
     
nar "The smell is horrific, my vision is black as the Grave, but my guide never falters in her movements."

nvl clear

show Sewer
with dissolve

show clo2 snk-serious-flip at right
show nas2 rel-blank-flip at center
with easeinright

"Soon we come upon torchlight, confirming that we're in another sewer. I suppose they must run everywhere beneath the city, how else would it all work?"

show clo2 snk-relief-flip
with dissolve

"This must lead out past the city wall eventually. So close. I’m so close!"

show clo2 snk-serious-flip
with dissolve

"I can feel cool wind streaming from ahead, a fresh air that cuts through the sewer stink."

"Closer, closer."

show nas2 rel-annoyed-flip
show clo2 snk-worry-flip
with dissolve

"Nasrin freezes. I can't tell what's wrong."
 
show nas2 rel-blank
with dissolve

n "Same as last time."

show nas2 rel-blank-flip
with dissolve

"Then she lets go of my hand and moves into the light, before a door. She knocks at it lightly."

"Someone moves on the other side, the sound of armor rattling upon him. I get ready to run."

so "Sorry darling, now’s not a good time. Could get in head-rolling trouble if I let anyone out."

show nas2 rel-defaul-flip
with dissolve

"In response, she moves her hands-"

hide nas2
with dissolve

show nas sed-sly-flip
with dissolve
 
"-and her loose cloak sheds from her body."

show nas sed-flirtatious-flip
with dissolve

"Nearly naked, she presses against the door."

so "Damn you, you know how to make a man smile. Lust be with me, let me just come in there."

"A clink as he opens the door. If he comes in and closes it behind him, this isn’t helpful at all."

show sol neu-neu at sleft
with dissolve

hide nas
show nas sed-flirtatious-flip

show nas at left
with ease

"Nasrin’s a step ahead of me. The second the door is open she throws herself into his arms, kissing him, pushing him back."

so "-damn, you little nymph-"

hide nas
hide sol
with easeoutleft

"She captures his mouth again, wrestles him through the door, gets him to the ground."

show clo2 snk-serious-flip at center
with ease

"I can see the stars in the open door, and I run."

"Nasrin straddles the guard, putting him in another world."

hide clo2
with easeoutleft

scene sunbright
with dissolve

"I burst out into the open air, rushing past them."

scene Wall
with fade

show clo2 snk-relief at center
with easeinleft

"I made it!"

hide clo2
with easeoutright

show sol neu-shout at scenter
with dissolve

so2 "Hey hey hey, hold up!"

hide sol
with easeoutright

"Something grabs my dress, tearing me backwards."

show sol neu-shout at scenter
show clo2 snk-worry at right
with easeinright

"I try to rip free but then he has my hair and then my arms."

"No, no, no!"

so2 "Oh thank Lust you brought a little friend. Come over here love, you ain’t getting out without paying the toll too."

show clo2 snk-worry-flip
hide sol
with dissolve

show nas rel-annoyed at center
with dissolve

"Nasrin looks up at me, confused. She didn't plan for another soldier."

show sol neu-neu at sleft
with dissolve

so "I told you we’d get free cunt guarding this door!"

show nas act-stealthy at center
with dissolve

show sol neu-shout at sleft
with dissolve

show sol neu-shout at sleft
with hpunch

hide nas
show nas act-wincing-flip at center

show nas act-wincing-flip at mleft
with ease

"She pulls away from the man on the ground, but he grabs her."

so "Ah ah, no changing your mind now, deals are sacred and all."

hide nas
hide sol
with easeoutbottom

show clo2 snk-worry-flip at center
with dissolve

show sol neu-neu-flip at sright
with dissolve

show sol neu-neu-flip
with hpunch

"The man who grabs me is already tearing at my clothes, groping for my breasts."

"No, no, no, no!"

show clo2 reg-declaring-flip
with dissolve

c "Stop! I am Praetrix Clodia and you will unhand me!"

show sol neu-shout-flip
with dissolve

so2 "What fucking lie is that, sweetheart?"

show clo2 rel-angered-flip
with dissolve

c "Look at me, beast!"

show clo2 rel-angered
with dissolve

"Somehow, he bothers to turn me around and take a good look."

show sol neu-confuse-flip
with dissolve

so2 "What the... ah shit. Or a whore that looks like her."

show clo2 reg-declaring
with dissolve

c "I am the Praetrix, soon to be wife of Sejanus! And I will be sure to tell him who had me before he could!"

so2 "Ugh. You think she’s telling the truth?"

"The other man has Nasrin pinned down. She is clever and fast and charming, but she is not strong, and he easily restrains her on the dirt."

hide sol
hide clo2
with dissolve

show sol neu-neu at sleft
with easeinbottom

so "Probably would say anything to avoid your tiny cock."

show sol neu-shout-flip at sright
with dissolve

so2 "I’m serious! If she’s telling the truth, it’s the Grave of Nightmares for me. Maybe you too."

show sol neu-shout at sleft
with dissolve

so "Well only if you fuck her. If you’re so worried, take her back to Sejanus and tell him she was trying to escape."

show sol neu-shout-flip at sright
with dissolve

so2 "Well what if she ain’t the Praetrix and I look like a fool?"

show sol neu-shout at sleft
with dissolve

so "Then I’m sure you can fuck her all you want. That will make you feel better. Why am I doing all your thinking for you?"

show sol neu-shout-flip at sright
with dissolve

so2 "Oh great, and you’re just gonna fuck that one while I’m busy working?"

show sol neu-neu at sleft
with dissolve

so "That’s the plan."

show sol neu-neu-flip at sright
with dissolve

so2 "I hate you. Fine, enjoy yourself."

so2 "Come on, \"Praetrix\". Great Ghosts, you are in such trouble when Sejanus gets done laughing at me for bringing him some brothel slave."

show sol neu-neu-flip at sleft
with ease

show clo2 snk-worry-flip
with dissolve

"He pulls me towards the door. I want to fight, kick and scream, but curse it all, the man is strong."

hide sol
show clo2 snk-serious at left
with ease

show sol neu-neu at scenter
show nas rel-blank at mright
with dissolve

"As he drags me back into the darkness, I look back at Nasrin. The man is on top of her. She doesn’t even seem to be fighting back."

hide nas
hide sol
with easeoutbottom

"Damn it all. I can’t allow this."

show clo2 reg-declaring-flip
with dissolve

c "Wait! Wait, you cannot harm my handmaiden!"

show clo2 at center
show sol neu-confuse at sleft
with ease

so2 "Oh for fuck’s sake, shut up!"

show clo2 rel-angered-flip
with dissolve

c "If you spoil her, you can be sure I will do everything in my power to see you wind up in the Grave!"

show sol neu-neu
with dissolve

so2 "Ghosts, come on now."

hide clo2
with dissolve

show sol at scenter
with ease

show sol neu-shout
with dissolve

so2 "Hey! Hey, give it a rest cockhead!"

show sol neu-shout
with vpunch

"He kicks his companion in the side."

show sol neu-shout-flip at sright
with easeinbottom

so "You fucking prick! What’s wrong with you?"

show sol neu-neu at sleft
with dissolve

so2 "That’s her handmaiden."

show sol neu-shout-flip at sright
with dissolve

so "This bitch is no maiden, not by any terms."

show clo2 reg-stoic at left
with dissolve

so "What’s your handmaiden doing sneaking in and out of the city all the time?"

show clo2 snk-worry
with dissolve

"Damn it. Damn it, what do I say?"

hide clo2
with dissolve

show sol neu-neu at sleft
with dissolve

so2 "Planning escape, obviously."

show sol neu-confuse-flip at sright
with dissolve

so "Why would a couple of slaves want to escape?"

show sol neu-confuse at sleft
with dissolve

so2 "Are... are you serious? Ghosts, you are stupid."

show sol neu-confuse-flip at sright
with dissolve

so "You don’t really believe she’s the Praetrix? Come on, you’ve never seen her before!"

show sol neu-neu at sleft
with dissolve

so2 "No. But look at her. She looks and sounds noble at least."

show sol neu-neu-flip at sright
with dissolve

so "Whores make an entire trade out of pretending to be noblewomen."

show sol neu-neu at sleft
with dissolve

so2 "Look, either they’re whores and we can fuck them after we bring them in, or they’re nobles and we’re in shit danger if we don’t bring them in."

show sol neu-shout at sleft
with dissolve

so2 "Get your cock out of your ear and let’s take these girls to Sejanus."

show sol neu-confuse-flip at sright
with dissolve

so "I swear on the Grave, women shouldn't be so complicated."

show nas rel-annoyed-flip at center
with easeinbottom

show clo2 reg-stoic-flip at left
with dissolve

"Hauling Nasrin to her feet, he marches us ahead of him. Back into the darkness, back into the trap."

"Why? I was so close, why?"

show nas rel-blank-flip
with dissolve

n "I’m sorry."

scene black
with fade

centered "It’s all she says to me the entire long, torturous way back."

centered "Valerius. I’m sorry too."

jump aftermath

label caught_violently:

scene StreetsNight
with fade

pause 1.0

show clo2 snk-serious-flip at right
show nas2 rel-blank-flip at center
with easeinright

nar "The city seems quiet."

nar "The girl leads me by the hand, quickly and with certainty."

nar "Every window is dark. How are the citizens faring under this assault? No reports have reached my ears."

nar "There seems to have been no looting and burning, and corpses don’t line the street. As if no invading army at all has taken this place."

nar "But then I see the soldiers of that army. A whole patrol of them marching down the street, right towards us."

nvl clear

hide nas2
hide clo2
with easeoutright

nar "The girl pulls me into an alley, and we rush as quickly from the soldiers as we can. Doesn’t seem like they saw us."

nar "Coming out the other end of the alley, we head up a flight of steps, then down another."

nar "It’s been years since I ever walked in the city, and none of these sights are familiar to me. The smell, however, is recognizeable: chamber pot."

nar "Perhaps that is a good excuse for why I don’t visit often."

show clo2 snk-serious-flip at right
show nas2 rel-blank-flip at center
with easeinright

nar "Rounding another corner and-"

nvl clear

show sol neu-neu at sleft
with dissolve

so "Woah, hold up there, girls."

show clo2 snk-worry-flip
show nas2 act-stealthy-flip
with dissolve

"Shit! No, no!"

hide sol
with easeoutleft

show clo2 at center
show nas2 at left
with ease

show sol neu-neu-flip at sright
with easeinright

show clo2 snk-worry
show nas2 act-stealthy
with dissolve

"They’re all around us."

show clo2
with hpunch

"They seize us by the arms, laughing, pulling."

show sol neu-shout-flip
with dissolve

so2 "What are you two pretty things doing out after curfew? It’s dangerous out."

"I know what comes next. There’s really no choice to be made, between that and being caught."

show clo2 reg-declaring
with dissolve

show nas2 act-attacking
with dissolve
c "I am your Praetrix, Clodia! How dare you handle me this way?"

"They all laugh, but I can hear the nervousness of it."

show sol neu-confuse-flip
with dissolve

so "Aye, you’re Praetrix Clodia. Never fucked a Praetrix before. They teach noble girls how to suck cock, don’t they?"

show clo2 rel-angered
with dissolve

c "You will all be underground this time tomorrow if you do not unhand me right-"

hide sol
with dissolve

show sol neu-shout-flip at sright
with dissolve

so2 "Oh shut up, bitch. Only Sejanus tells us what to do."

show clo2 reg-declaring
with dissolve

c "When you rape his betrothed how do you think he will feel?"

hide sol
with dissolve

show sol neu-confuse-flip at sright
with dissolve

so "Come off it, you ain’t the Praetrix. Just some pretty whore."

show clo2 reg-stoic
with dissolve

c "Will you risk going into the Grave for that?"

hide clo2
hide nas2
hide sol
with dissolve

show sol neu-neu-flip at sright
with dissolve

so2 "Ugh... fine. You two, take her back to Sejanus. The rest of us can have this one."

show sol neu-confuse at sleft
with dissolve

so "And what if she ain’t the Praetrix?"

show sol neu-neu-flip at sright
with dissolve

so2 "Then you can fuck her all you want, probably. Now come on boys, get this one’s clothes off."

show nas2 act-stealthy at left
with dissolve

hide sol
show sol neu-shout-flip at sright
show sol at sleft
with ease

show sol
with hpunch

"They close in around Nasrin, pulling at her."

hide nas2
show nas act-attacking at left
with dissolve

show nas
with vpunch

"Her clothes are torn. She fights, kicks, bites."

"The others drag me away, but I want to say something. What can I say?"

show nas at right
with ease

"Then, somehow, she wriggles free. Darts away from them, slipping between them, nimble as a shadow."

show sol neu-shout at scenter
with dissolve

so "BITCH!"

show sol
with vpunch
show red
with dissolve

pause 2.0

centered "Ghosts."

pause 1.0

centered "She falls without a sound."

pause 1.0

hide nas

"Turns to a dark shape in the dirt."

pause 1.0

show sol neu-neu at scenter
with dissolve

so "Stupid bitch."

hide red
with dissolve

show sol neu-shout at sleft
with dissolve

so2 "Great, now what do we do? Did you have to kill her?"

show sol neu-shout-flip at sright
with dissolve

so "Whore scratched my face, I reacted."

show sol neu-neu at sleft
with dissolve

so2 "Damn it, she was really pretty too. What a waste."

show sol neu-shout-flip at sright
with dissolve

so "What are you, in love? Get over it. And you two, get going with her already!"

scene black
with fade

centered "The two soldiers drag me, and themselves, away from the spectacle."

centered "I try to get one last look at the girl who died in the dirt helping me."

centered "Then she's gone."

jump night_before_battle

label aftermath:
    
scene TentNight
with fade

play music sandman

pause 1.0

show ser com2-rage at center
with dissolve

s "I want their blood!"

tr "Legatus, please calm yourself!"

show ser com1-rage-flip at right
with dissolve

s "You be quiet! All of you, stop looking at me like that and tell me how this happened."

nar "The other tribunes have no answers."

nar "What is there to know? A wine cart was accidentally delivered to the wrong centuria, and then they behaved quite foolishly."

nvl clear

show ser ac-annoyed-flip
with dissolve

s "Well?"

tr2 "Their standard bearer says that by the time he noticed anything was wrong, half of them were already running for the walls."

tr2 "He said he grabbed his standard and ran with them, figuring if they were going to battle they needed him."

show ser neutral-annoyed-flip
with dissolve

s "Why didn’t the centurion tell you this?"

tr2 "Their centurion died on the wall, legatus."

show ser com1-barking-flip
with dissolve

s "Ghosts, there’s at least some justice in the world. Did any men stay behind?"

tr3 "A small handful."

show ser com2-barking-flip
with dissolve

s "Promote one of those men to the new centurion, I don’t care which. Decimate the rest of them."

tr2 "They lost half their numbers in the assault alr-"

show ser com1-rage-flip
with dissolve

s "Decimate them or I will personally decimate you."

nar "Sergius, that great soldier’s legatus, does not usually stoop to this level of terror in his own ranks."

nar "I pissed him off much more than I planned."

nvl clear

show ser ac-annoyed-flip
with dissolve

s "How many other centuriae followed them? How many men did we lose total?"

tr "Ten centuriae in total were involved. We think we’ve lost about a hundred men."

show ser com2-barking-flip
with dissolve

s "Take what corpses you can and burn them in a pyre to War. And throw a bull in there too, that ought to show that we’re proper sorry for this mess."

nar "I have never seen him this distressed before. In the past six years he has always been calm, only letting his blood boil when he harangues the men."

nar "Never have I seen this kind of... fear."

nvl clear

show ser com2-barking-flip at fright
with ease

show val ac-fact at fleft
with dissolve

v "If I may, legatus, this is no great setback. We still outnumber them, and the walls will be battered down by tomorrow morning."

show ser neutral-serious-flip
with dissolve

pause 0.5

"Was saying anything a bad idea?"

show ser com1-barking-flip
with dissolve

s "All of you get out except for Tribune Valerius."

hide ser
hide val
with dissolve

nar "Well, that’s probably the first time that’s ever been asked. The other tribunes make a show of giving me strange looks as they file out."

nar "When they’re gone, Sergius collapses onto his couch. He eyes me like I’m a threat."

nvl clear

show ser neutral-serious-flip at fright
with dissolve

s "This is about much more than just a hundred lost soldiers, I hope you understand."

s "You’re from the gutter, you should know how the common man responds to failure."

show val ac-annoyed at fleft
with dissolve

"The gutter? I suppose everyone less than patrician is from the gutter to you."

v "We have not been beaten in a true battle."

show ser ac-annoyed-flip
with dissolve

s "Aye, but consider that we lack the Basileus’s blessing."

show val rel-normal
with dissolve

"Oh. That's why you dismissed everyone else. Can't even tell your other tribunes, eh?"

show ser com1-barking-flip
with dissolve

s "The men don’t know that, but what if they begin to whisper it now that they have a failure to rally around?"

s "Whole armies have up and abandoned their legatus when they feel the ghosts are against them."

show val ac-annoyed
with dissolve

"You’re only concerned about this now? I suppose blessings never mattered to a man who has won his entire life."

"Look how shaken the great Sergius becomes at his first whiff of defeat."

show ser neutral-serious-flip
with dissolve

s "I must end this quickly. Tomorrow, we attack. Swift, coordinated. In the morning light. That victory will renew us."

show ser com2-barking-flip
with dissolve

s "Once we take the streets it will be no time before I push Sejanus into the Grave myself."

show ser com1-rage-flip
with dissolve

s "Damn him, he’s probably laughing himself to sleep over this."

show val rel-normal
with dissolve

v "Doubtful. I should think he doesn’t sleep, in the knowing terror that he will be dead in days."

show ser ac-annoyed-flip
with dissolve

s "Even so, our honor is tarnished. Clearly, the men who have answered my summons are driven more by the hope of noble holes than glory."

show val ac-fact
with dissolve

v "That is most soldiers, I would think."

show ser com1-barking-flip
with dissolve

s "My men cannot be most soldiers. My honor cannot allow it."

show ser ac-annoyed-flip
with dissolve

s "But if they must be so base, then fine. They’ll have all the city’s people to fuck they want tomorrow. What about you?"

show val rel-normal
with dissolve

v "Hm?"

show ser neutral-serious-flip
with dissolve

s "You have that Peladonian girl, I suppose a man of your low appetite has no need for more."

v "Not particularly."

show ser ac-insulting-flip
with dissolve

s "Just here for the gold, right? Are all those who follow me so self-interested?"

"Yes. You really believe {i}any{/i} of us care about your personal dreams?"

show val ac-fact
with dissolve

v "I can't speak for the others."

show ser neutral-serious-flip
with dissolve

s "Say no more. Serve me well tomorrow, and we’ll see just how much gold you deserve."

show val rel-normal-flip
with dissolve

v "Very well. War be with you."

show ser neutral-serious
with dissolve

s "And you."

hide val
hide ser
with dissolve

nar "As I leave, I cannot help but feel he suspects me. Probably for no reason beyond my brown blood, but still. I should be careful from now on."

scene black
with fade

nar "I did, after all, lie to his face."

nar "I care not if my blood is black as coal. So long as I can save her."

stop music fadeout(1.0)

nvl clear
if plan == "escape":
    jump failed_clodia_escape
    
if plan == "clodia_poison":
    jump failed_clodia_escape
    
if plan =="nasrin_poison":
    jump nasrin_report
    
label night_before_battle:
    
scene TentNight
with fade

play music whataday

pause 1.0

nar "The war tent is serene this evening."

show ser neutral-serious-flip at center
with dissolve

nar "Sergius sits by the table with a glass of wine, staring contently down at the map of the city."

nar "This is his best mood, I’ve found. Any time he’s more excited than this, it’s either because something has gone wrong, or because he’s gone drunk and is partying with his underlings."

nar "The men love him for that. I just see a man who has never been seriously challenged in his life."

nvl clear
 
show ser neutral-grin-flip
with dissolve

s "War be good, I think things are looking up for me."
    
show ser neutral-grin-flip at fright
with dissolve

show val rel-normal at fleft
with dissolve

v "That's good for all of us, then. Where are the others?"

"None of the other tribunes were summoned, and I can’t rightly understand why I’m here."

s "They’re busy."

show val ac-fact
with dissolve

v "Fucking and drinking?"

show ser ac-grin-flip
with dissolve

s "Yes, what else is there to do on the eve of an easy battle?"

v "And why aren’t you joining them?"

show ser neutral-grin-flip
with dissolve

s "I’m not of the mood for it tonight."

show val ac-annoyed-flip
with dissolve

v "Hmph. So you thought you’d speak to the only other man in the army not in the mood for it."

show ser neutral-serious-flip
with dissolve

s "Perhaps. You know, Aureus, I don’t always like you."

show val rel-normal
with dissolve

"What? No! I had no idea. What a blow."

v "I know."

show ser ac-insulting-flip
with dissolve

s "You don’t take it personally. That’s good. But you must admit, you’re a snake. I don’t trust snakes."

show val rel-emotional
with dissolve

v "I’m only trying to do my best for myself and my country."

show ser com1-barking-flip
with dissolve

s "As are we all. No better way to live than to grow rich and glorious in service to the government."

s "That’s what an army is, you know. Take a crowd of men who value their own gain above all else, then add a leader who can direct that selfish energy towards the glory of the state."

show val rel-normal
with dissolve

v "Speaking of leadership and gain... Dagara will need new nobles when this siege is complete."

show ser neutral-annoyed-flip
with dissolve

s "Ah, so that’s what you want? Typical brown blood dream."

"Not really. I'd rather flee here with Clodia and never be heard from again, but it's better if you think I'm just status-hungry like the rest of you."

show ser neutral-serious-flip
with dissolve

s "Don’t grow too ambitious, Valerius. You’ve bought your tribuneship, but there's nowhere higher to go if your only merit is your purse."

s "Content yourself with more gold and girls than any brownblood has a right to."

show val ac-annoyed
with dissolve

"That is what I hate about men like you more than anything else. That you all have the gall to tell a man to his face \"Don’t even bother.\""

v "Is there anything else of importance, legatus? Or may I be dismissed?"

show ser neutral-serious
with dissolve

s "You may go... but don’t forget what I’ve told you."

"So that’s what this was all about. Just to warn me not to get too big for my boots."

show val rel-normal-flip
with dissolve

v "I will see you for the attack tomorrow. War be with you."

s "And you."

stop music fadeout(1.0)

scene black
with fade

centered "Leaving him behind, I repeat my earlier prayer for a stray javelin to catch him between the eyes tomorrow."

if plan == "escape":
    jump failed_clodia_escape
    
if plan == "clodia_poison":
    jump failed_clodia_escape
    
if plan =="nasrin_poison":
    jump nasrin_report
    
label failed_clodia_escape:

scene ValeriusRoomNight
with fade

play music downtime

pause 1.0

nar "My room is empty, cold. There is no one here."

nar "No Clodia."

nar "And there is nothing I can do but wait. Did the escape fail? Were they even able to attempt it? Why hasn’t Nasrin reported back to me?"

nar "Ghosts be damned. I cannot stop the army from taking Dagara tomorrow."

nar "Clodia... Clodia... what can I do? How do I save you?"

nvl clear

scene black
with fade

centered "I think about it all night, lying in bed, eyes open in the dark."

stop music fadeout(1.0)

jump dreams

label nasrin_report:

scene ValeriusRoomNight
with fade

play music downtime

pause 1.0

nar "My room is warm. The brazier has been lit, though I have explicitly ordered the slaves to stay out of my room unless I am present."

show nas rel-defaul-flip at right
with dissolve

nar "There’s no mystery, though. Nasrin sits upon my bed, staring at me with her usual... Nasrin-ness."

nar "The problem is that she is alone."

nvl clear

show val rel-serious at left
with dissolve

v "Where is Clodia?"

show nas rel-blank-flip
with dissolve

n "The plan didn’t work. I had to flee."

show val ac-fact
with dissolve

v "Didn’t work?"

n "Yeah, didn’t work. I tried to poison Sejanus, but he discovered the poison. I was probably about to be killed, so I got out of there."

v "But what of Clodia? She’s the entire reason you were sent in there again!"

n "I couldn’t take her with me, it all happened very quickly. I’m sorry."

show val ac-annoyed at center
with ease

show nas arm-blank-flip
with dissolve

v "Damn you, sorry is worthless to me! She’s still in there, he can still hurt her!"

"I’m not sure what I’m expecting to see in her eyes, but it doesn’t satisfy me."

show val rel-emotional
with dissolve

v "Damn the Ghosts! I suppose now Sejanus would recognize you?"

n "Probably."

show val ac-fact
with dissolve

v "So you’re useless to me now."

show nas rel-blank-flip
with dissolve

n "That’s a rude way of putting it."

show val ac-annoyed
with dissolve

v "If you can’t hide amongst his slaves, you’re useless to me. If you can’t save Clodia, you’re useless to me."

show nas arm-blank
with dissolve

n "... so do you just want me to go?"

show val ac-fact
with dissolve

v "Go? Tch. No. I still may find some use for you one day."

show nas rel-blank
with dissolve

n "Well seeing as I need money to feed myself, you’ll have to keep paying me if you want me to stick around."

show val ac-annoyed
with dissolve

v "You greedy little worm. I’ve paid you better than you deserve."

show nas arm-blank
with dissolve

n "Debatable."

v "Just go fuck some of the soldiers, they’ll pay you gladly. War camps are like gold mines to people like you."

show nas rel-annoyed-flip
with dissolve

n "You’re something else."

v "What do you mean?"

n "I can very clearly recall a time you would have killed any man who dared to lay a finger on me."

show val rel-serious
with dissolve

v "That time was short. You were my property, I don’t like people messing with my property."

show nas arm-frustrated-flip
with dissolve

n "You have a lot of property. Tables, books, candles, the girl who makes your bed and the boy who handles most of your letters."

n "You never say to any of them the sweet things you used to say to me."

show val rel-emotional-flip
with dissolve

v "Come off it."

show nas arm-disappointed-flip
with dissolve

n "Why did you treat me so much better when you owned me?"

"What is this? Where is this coming from?"

show val rel-normal-flip
with dissolve

v "Because I hadn’t met Clodia yet."

stop music fadeout(1.0)

show nas arm-blank-flip
with dissolve

n "..."

show val rel-emotional-flip
with dissolve

v "..."

show nas rel-annoyed
with dissolve

n "Very well, I’m going."

show val rel-serious
with dissolve

v "Where?"

show nas rel-annoyed-flip at left
with ease

show val rel-serious-flip
with dissolve

n "I don’t have to tell you. But I’m going to go find the biggest, dumbest man out there and fuck his brains out. Maybe I won’t even charge him."

show nas rel-annoyed
with dissolve

n "I’ll tell him it was on Tribune Valerius’s coin. He’ll like that."

v "Calm down-"

show nas arm-frustrated
with dissolve

n "Maybe after him I’ll fuck his friend, and the man next to him too. Maybe I’ll fuck every man in the camp and tell them it’s all thanks to you."

n "You’ll become the most popular Tribune in a night."

show val ac-annoyed-flip
with dissolve

v "I don’t appreciate the tone you’re-"

n "Would I be useful to you then?"

show val ac-fact
with dissolve

"Lust damn you, stop this. You shouldn’t be allowed to anger me like this."

show nas rel-annoyed-flip
with dissolve

n "Goodbye, Valerius."

show val rel-normal-flip

v "Wait."

show nas arm-disappointed-flip
with dissolve

n "..."

show val rel-serious-flip
with dissolve

play music whale

v "... what do you want me to say?"

n "... I don’t know. I shouldn’t even care."

v "No. You shouldn’t."

show val rel-emotional-flip
with dissolve

"I shouldn’t."

v "Just... don’t go out there and start fucking people. It’s not safe."

show nas rel-annoyed-flip
with dissolve

n "I’m not your property, my safety is none of your business."

show val rel-normal-flip
with dissolve

v "What do you want?"

show nas rel-blank
with dissolve

n "Money for food would be nice."

show val ac-fact-flip
with dissolve

v "By the Ghost of Life, I’ll feed you. I’ve been feeding you, it’s not really much of an expense."

show nas arm-blank
with dissolve

n "And what am I to do? Sit around eating? Sounds boring."

v "No, I have something for you to do."

show val rel-normal-flip
with dissolve

"I toss her a single coin, the Basileus’s youthful, bored face spinning through the air into her grasping palm."

show val rel-normal-flip at right
with ease

"Then I sit on the edge of the bed."

show nas rel-blank
with dissolve

n "What about Clodia?"

show val rel-serious-flip
with dissolve

v "What are you implying? My shoulders are killing me. Rub my back."

show nas rel-annoyed
with dissolve

"The look she gives is almost funny. What, too mundane after what you’ve been up to?"

show val rel-grin-flip
with dissolve

v "I’m waiting."

hide nas
hide val
with dissolve

show black
with fade

centered "Before long, her fingers are working into my shoulders. It feels sublime."

hide black
with dissolve

show nas rel-blank-flip at right
show val rel-normal-flip at center
with dissolve

v "I’m... sorry I called you useless."

n "Several times."

show val rel-emotional
with dissolve

v "Sorry, sorry, sorry."

n "I just hope there’s going to be more for me to do than rub your back every night."

v "There is, I promise. I have an idea. For tomorrow."

v "But for tonight, this is all I need of you."

n "All?"

show val rel-normal-flip
with dissolve

v "Don’t get too excited. How about some conversation?"

n "About what?"

show val rel-grin-flip
with dissolve

v "Tell me about Peladonia."

n "Tell you about a place I haven't been since I was a child?"

v "That's more familiarity than I have."

n "There’s nothing to say about it. It’s a land of whores, politicians, spies, assassins, dancers, actors... and usually they’re all the same people."

show val rel-normal-flip
with dissolve

v "What a decadent and insane land it must be."

scene black
with fade

stop music fadeout(1.0)

centered "I’d love to go one day with y-{p}{p=0}...with Clodia."

label nasrin_dream:

scene black
with fade

pause 1.5

play music spooktacular

pause 1.0

centered "I was made a tribune the eve before we attacked the town of Garna."

pause 1.0

nar "The last remnants of the Dagaran army were hiding there, so Sergius wanted to destroy them. This would leave the capital undefended."

nar "His orders were to simply burn Garna down."

nar "Naturally, I didn't want to participate."

nar "But tribunes have responsibilities."

nvl clear

nar "When I was a simple legionary, it was easy to hide someplace when horrible things were happening."

nar "Even when I had bribed my way up to prefect, there were ways to ensure I was in charge of cooler parts of the battlefield."

nar "Tribunes, however, had to be in the thick of it."

nvl clear

nar "So at Garna, I watched Sergius light the first house."

nar "Watched him put a torch to the thatched roof."

nar "My job was to lead my maniple on an organized pillage. Break the door, kill the men, take the women and children and valuables, burn the house."

nar "Break the door, kill the men, take the women and children and valuables, burn the house."

nvl clear

centered "Break the door."

centered "Kill the men."

centered "Take the women and children and valuables.{p=1.0}(ignore all the screaming and crying and begging as best you can.)"

centered "Burn the house."

nar "With smoke everywhere, it was hard to see half the time."

nar "Some houses were just set on fire, and when the people came running out they were treated accordingly."

nar "It only took a couple of hours to finish Garna and the rest of Dagara's army."

nar "It felt like days."

nvl clear

stop music fadeout(1.0)

scene ValeriusRoom
with fade

play music aftershock

pause 1.0

nar "When it was all over, I returned to my room in the War Tent."

nar "My new room, fit for a Tribune. More luxury than I'd ever known before."

show val rel-emotional-flip at fright
with dissolve

nar "I sat on my beautiful, comfortable bed, and I wept."

nar "A slave tried to dry my eyes, but I waved him away."

nar "I told them all to leave me."

nar "What a miserable lot, to be the loser in a war."

nvl clear

show sol neu-neu at sleft
with dissolve

so "Tribune Aureus."

show val rel-serious-flip
with dissolve

v "Hm? Ah, what is it?"

show sol neu-confuse
with dissolve

so "Um, is this a bad time? I can-"

v "No, what do you have to say to me?"

show sol neu-neu
with dissolve

so "Your fellow tribunes just cast lots for picking slaves from the captives."

show val rel-normal-flip
with dissolve

v "Oh."

so "They threw your name in though you weren't present."

show val ac-fact-flip
with dissolve

v "How considerate of them."

so "Heh, well, they were pretty angry about it after the lots were drawn. Felt they screwed themselves over."

show val ac-annoyed-flip
with dissolve

v "What are you talking about?"

so "Ah, well, you'll see."

show sol neu-shout-flip
with dissolve

so "Get in here girl!"

stop music fadeout(1.0)

hide sol
with dissolve

show nas rel-blank at left
with easeinleft

show val rel-normal-flip
with dissolve

n "..."

v "..."

play music reprieve

"Beautiful. I see."

show val rel-emotional-flip
with dissolve

v "Well... Tribune Antonius probably wasn't too upset. The rest, I'm sure, hate me now."

show nas:
    xalign 0.45 yalign 1.0
with ease

show sol neu-neu at sleft
with dissolve

so "Haha, maybe."

show val rel-normal-flip
with dissolve

v "So she's mine?"

so "That's my understanding of it... well, that's all, so..."

v "Right. Leave us."

hide sol
with easeoutleft

show nas at left
with ease

n "..."

v "..."

n "..."

show val ac-fact-flip
with dissolve

v "... So... what's your name?"

n "..."

show val rel-normal-flip
with dissolve

v "I'm not going to bite. I don't feel like doing much of anything right now."

n "... why have you been crying?"

show val rel-emotional
with dissolve

v "I... I haven't been."

n "Oh."

show val ac-fact-flip
with dissolve

v "And anyway, you should answer my question before asking any. Especially considering you're mine now."

n "Nasrin."

show val rel-normal-flip
with dissolve

v "Ah. Pretty. But I suppose everything about you is, huh?"

n "..."

show val rel-emotional-flip
with dissolve

v "Ah, why do I even bother?"

show val rel-normal-flip
with dissolve

v "Well, what is it you're good at? I need to know what kind of work to put you to."

show nas rel-defaul
with dissolve

n "I'm good at fucking, my lord."

"Oh."

show val ac-fact-flip
with dissolve

v "I see. You're a whore then."

n "No."

v "Then I'm rather confused. You're either a whore or a tart, and if it's the latter then you're a fool for not charging for it."

n "I'm neither. But my previous master kept me in his bed."

v "Previous master?"

"Oh. I see. I should have found it strange there was a beautiful Peladonian girl this far from her native land."

show nas rel-blank
with dissolve

n "A trader who was stopping by this village when you attacked."

v "What shit luck. At least you survived. Apologies about your master."

show nas rel-annoyed
with dissolve

n "I hated him."

show val rel-normal-flip
with dissolve

v "Oh... will you be inclined to hate me? I have no use for a bedslave that will hate me."

v "Tribune Horatius might, should I send you to him?"

show nas rel-blank
with dissolve

n "No, my lord."

v "So loyal already. How long have you been a slave?"

n "Since I was a little girl."

v "How little?"

n "About eleven."

v "How old are you now?"

n "I don't know my exact birthday, my lord, but I've been eighteen for at least a few months now."

show val rel-emotional-flip
with dissolve

v "Seven years. All with that same master?"

show nas arm-disappointed
with dissolve

n "Yes."

show val rel-normal-flip
with dissolve

"She really was beautiful. If I fucked her, she’d be the most beautiful girl I had ever known."

"I could still hear the wailing of the captives outside though. As long as that wailing kept up, I was as soft as silk."

"And anyway..."

show black
with dissolve
 
centered "She was lying to me."

hide black
with dissolve

v "Do you miss Peladon?"

show nas rel-defaul
with dissolve

n "Not really."

v "Why not?"

n "I haven't seen it in years. Don't remember it so well."

v "I've always wanted to visit, but it's been quite out of the question so far. I've had to make do with reading about it."

v "See these books here? This one talks all about the beautiful buildings and temples in Peladon."

v "This one has information on the plants and animals native to the land."

v "And this one, well, it might be my favorite. Talks all about Peladonian assassins. Always beautiful boys and girls."

show nas rel-blank
with dissolve

v "Curiously, they train until they're eleven years old, and then they're sold into foreign slavery. If they kill their master and free themselves, then their final test has been passed."

n "..."

show val ac-annoyed-flip
with dissolve

v "I don't think it took you seven years to kill your master, that's abysmal. What's really going on with you?"

show nas rel-defaul
with dissolve

n "... You're mistaken, my lord. I'm just a bedslave."

stop music fadeout(1.0)

show val cmd-ordering-flip
with dissolve

v "Oh? But that lovely thing in your hair has a razor in it for slicing throats."

show nas rel-blank
with dissolve

n "..."

show val rel-normal-flip
with dissolve

v "The books talk about those too. There's even a picture, see?"

show nas arm-blank
with dissolve

n "... what kind of Raskyan reads these things?"

show val ac-annoyed-flip
with dissolve

v "This kind. Tell me the truth, or I'll have the guards come in here and haul you off to Horatius."

play music timekiller

n "Ha. Fine."

show nas rel-blank
with dissolve

n "He was my target and I became his slave to kill him."

v "And it took you seven years?"

show nas arm-blank
with dissolve

n "No. About a week. And you lot killed him, not me."

show val rel-normal-flip
with dissolve

v "You're welcome. How many men have you become the bedslave for and then killed?"

show nas rel-defaul
with dissolve

n "I don't have the fingers to count them all."

v "And I'm to be next?"

show nas rel-blank
with dissolve

n "No one has paid me to kill you."

v "But if someone did, you'd do it in a heartbeat?"

show nas rel-defaul
with dissolve

n "What, do you have enemies?"

show val rel-serious
with dissolve

v "Every Dagaran."

show nas rel-grin
with dissolve

n "Well I suppose that makes sense. Lucky for you none of them have any money anymore."

v "You're still too dangerous to keep around."

show nas rel-defaul
with dissolve

n "But you're thinking about it."

show val ac-fact-flip
with dissolve

v "Of course I am. I am a man after all."

n "This is why men are so easy to kill."

v "Kill me and you'll never make it out of the camp alive. They'll throw you straight into the Grave for killing your master, there's no greater crime in Raskya."

show nas arm-blank
with dissolve

n "Again, there's no reason for me to kill you. I don't just murder people for fun."

show val ac-annoyed-flip
with dissolve

v "You also have lied to me once. Lying to one's master is enough to get most slaves beaten."

show nas rel-defaul
with dissolve

n "Oh, well go on and beat me then."

show val ac-fact-flip
with dissolve

v "..."

n "..."

show val rel-normal
with dissolve

v "To what purpose? I'd rather not become another hated master who ends up with his throat opened."

show nas rel-grin
with dissolve

n "Smart man."

show nas rel-defaul
with dissolve

n "So do you want to fuck?"

show val rel-normal-flip
with dissolve

v "Excuse me?"

show nas sed-sly
with dissolve

n "I'm your slave, I know what you want me for. Come on, let's do it."

show val ac-fact
with dissolve

v "Absolutely not."

show nas arm-blank
with dissolve

n "For Lust's sake, I won't kill you. How can I prove that to you?"

show val rel-emotional
with dissolve

v "Even if you could... I can't right now."

show nas rel-defaul
with dissolve

n "Ah, having trouble with your cock? You'd be surprised how common that is."

show val rel-normal
with dissolve

v "How could anyone be in the mood for lust with what's just transpired?"

show nas arm-blank
with dissolve

n "Uh... I thought Raskyans got off on that kind of thing."

v "Whatever book you read that in lied."

n "No books. Just personal experience with Raskyans. I've been fucked by Raskyans fresh after they've killed someone, always seems to get their blood going."

show val rel-serious
with dissolve

v "Hmph. Well I don't know what to tell you, but I won't be fucking you under these circumstances."

show nas rel-defaul
with dissolve

n "Fine by me, it was just an offer."

show val rel-emotional
with dissolve

v "Ghosts of Peace, where are you now?"

show nas rel-blank
with dissolve

n "... you're serious, aren't you? You really don't like what's going on out there?"

show val rel-serious
with dissolve

v "I hate it."

n "How did you even become a tribune?"

show val rel-serious
with dissolve

v "A long story. Doesn't matter."

show nas rel-defaul
with dissolve

n "Huh. What's your name, master?"

show val rel-normal-flip
with dissolve

v "Valerius."

show nas rel-grin
with dissolve

n "Well Valerius, I like you."

v "As you say to all your masters."

show nas rel-defaul
with dissolve

n "No. I'm serious. I can tell, you're different from the rest of these men."

show val rel-emotional-flip
with dissolve

v "They can tell it too. It must be obvious to the world I'm not fit for this."

show nas rel-grin
with dissolve

n "You're also one of the more handsome Raskyans I've ever seen."

show val rel-normal-flip
with dissolve

v "Uh... well, thank you."

show nas rel-defaul
with dissolve

n "You're welcome. Now then, I'm tired. May I go to sleep?"

v "Uh, sure."

hide nas
with easeoutright

show val rel-normal at center
with dissolve

"Then she climbed into my bed and scurried under the sheets before I could say much about it."

n "What? I'm your bedslave, I belong in your bed."

show val ac-annoyed
with dissolve

v "I told you, I don't-"

n "Relax. I just want to sleep. Anyway, I think I'm going to like being your slave."

show val ac-fact
with dissolve

v "That's an odd thing to say."

n "Well, with my job out here complete, I have nowhere else to go and nothing to do. There are far worse fates in this world than being a Raskyan officer's concubine."

n "Especially one so..."

show val rel-normal
with dissolve

v "So what?"

n "Nothing. Good night, my lord Valerius."

stop music fadeout(1.0)

scene black
with fade

nar "I did eventually lay down next to her when I couldn't hold back my tiredness any longer."

nar "Didn't sleep much, but I woke alive the next morning."

nar "And the morning after that."

nar "And the next."

nvl clear

pause 0.5

play music love

show green
with dissolve

centered "And eventually, I could no longer fight myself."

pause 0.5

show CG6:
    zoom 1.8 xalign 0.0 yalign 0.0
    linear 4.0 xalign 0.3 yalign 0.5
with dissolve

pause 3.4

show CG6:
    zoom 1.8 xalign 1.0 yalign 0.8
    linear 4.0 xalign 0.5
with dissolve

pause 3.4

show CG6:
    zoom 1.3 xalign 0.3 yalign 1.0
    linear 4.0 xalign 0.9 yalign 0.1
with dissolve

pause 3.9

n "Mmmm... thank you, my lord."

v "No trouble."

show CG6b:
    zoom 1.3 xalign 0.9 yalign 0.1
with dissolve

n "I told you it would be okay."

v "It's been more than okay."

n "Good. I live to serve."

v "I mean it. You're something else."

show CG6c:
    zoom 1.3 xalign 0.9 yalign 0.1
with dissolve

n "Well you've never been to Peladon, it's understandable I seem amazing."

v "Surely not all Peladonian girls are like you."

scene CG6:
    zoom 1.3 xalign 0.9 yalign 0.1
with dissolve

n "Well no, but more than in Raskya. The women here are so boring."

n "I've seen how they fuck, if you can call it that. Where they just lay there and don't do anything or make any sounds."

v "Most Raskyan men consider that ideal."

n "And you?"

scene CG6:
    zoom 1.0 xalign 0.5 yalign 0.4
with dissolve

v "I don't know how anyone would prefer that to you."

scene CG6b:
    xalign 0.5 yalign 0.4
with dissolve

pause 0.5

n "Aw, thanks Master. But there's courtesans far better than I. The successful ones all manage to get ahold of a copy of \"The Secrets of the Harem\"."

n "But of course those go for several talents a copy, so you have to already be rich to get one."

v "What's that?"

scene CG6:
    xalign 0.5 yalign 0.4
with dissolve

n "An old Eskerian book that used to belong exclusively to the Kings of Kings."

n "Supposedly it was divinely inspired by Lust and Love, and was used to educate girls in the Eskerian harem on how best to please their King."

n "Anyway, someone got ahold of it at some point and made copies, but they're painful rare. Which means painful expensive."

n "So unfair, I'd kill to have a copy."

v "So why haven't you?"

n "That's the rare part. I've never even come across one."

n "Well, supposedly the Great Library at Peladon has one, but as I said, I haven't been there since I was eleven."

v "So that wasn't a lie?"

n "Nope. After I killed my first master, I never went back."

n "Thing is, Peladonian assassins like me are so well known in our homeland that it's hard for us to operate. Many more opportunities in foreign lands."

n "That razor blade of mine you knew about is extremely common knowledge in Peladon, for example, and so pretty useless there."

v "I see. You ever going to go back?"

scene CG6c:
    xalign 0.5 yalign 0.4
with dissolve

n "Maybe. Are you ever going?"

v "I'd like to."

scene CG6:
    xalign 0.5 yalign 0.4
with dissolve

n "Then I suppose I'll go with you then. Otherwise, I'd prefer to never see it again."

v "Hm. What about Durja? It's supposed to be a lovely land."

n "Ha, the Durjans are kind of insufferable."

v "Have you been?"

n "Once. The most arrogant women in the world live there, but so do the most attractive men."

n "Actually, you'd pass for a Durjan man very well. You'd just have to grow your hair out a bit, long hair is very in fashion with Durjans."

v "Maybe we'll go to Durja then. After all, if I have you, I'll have no need for their women, arrogant or not."

scene CG6c:
    xalign 0.5 yalign 0.4
with dissolve

n "..."

v "What is it?"

n "Oh, nothing..."

v "Come on. Tell your master."

scene CG6b:
    xalign 0.5 yalign 0.4
with dissolve

n "Uh, hey, if we're going to go to Durja, we should practice doing it Durjan style."

scene CG6b:
    zoom 2.0 xalign 0.7 yalign 0.3
    linear 3.0 xalign 0.3 yalign 0.6
with dissolve

pause 2.5

centered "By Lust. Durjan style, huh?"

n "Haha. Boys always get excited for Durjan style."

n "Come here, Master."

scene black
with fade

nar "One month."

nar "We lived like that for one month. It was the best month of my life up until that point."

stop music fadeout(1.0)

nar "Then we took Dagara."

nvl clear

centered "Then I met Clodia."

jump dreams

label dreams:

scene black
with fade

play music spooktacular

centered "Dark."

show text "Cold.":
    xalign 0.3 yalign 0.7
with dissolve

pause 0.5

show text "No one cares about me.":
    xalign 0.7 yalign 0.4
with dissolve

pause 0.5

show text "Lonely.":
    xalign 0.6 yalign 0.9
with dissolve

pause 0.5

show text "Forget all of them.":
    xalign 0.2 yalign 0.2
with dissolve

pause 0.4

show text "Myself.":
    xalign 0.8 yalign 0.75
with dissolve

pause 0.4

show text "Only myself.":
    xalign 0.4 yalign 0.6
with dissolve

pause 0.4

show text "Lost.":
    xalign 0.2 yalign 0.9
with dissolve

pause 0.3

show text "Scared.":
    xalign 0.6 yalign 0.3
with dissolve

pause 0.3

show text "Alone.":
    xalign 0.3 yalign 0.1
with dissolve

pause 0.3

show text "Warm.":
    xalign 0.9 yalign 0.4
with dissolve

pause 0.2

show text "Safe.":
    xalign 0.5 yalign 0.8
with dissolve

pause 0.2

show text "Hide.":
    xalign 0.25 yalign 0.4
with dissolve

pause 0.1

show text "Cold.":
    xalign 0.3 yalign 0.7
with dissolve

pause 0.1

show text "No one cares about me.":
    xalign 0.7 yalign 0.4
with dissolve

pause 0.01

show text "Lonely.":
    xalign 0.6 yalign 0.9
with dissolve

pause 0.01

show text "Forget all of them.":
    xalign 0.2 yalign 0.2
with dissolve

pause 0.01

show text "Myself.":
    xalign 0.8 yalign 0.75
with dissolve

pause 0.01

show text "Only myself.":
    xalign 0.4 yalign 0.6
with dissolve

pause 0.01

show text "Lost.":
    xalign 0.2 yalign 0.9
with dissolve

pause 0.01

show text "Scared.":
    xalign 0.6 yalign 0.3
with dissolve

pause 0.01

show text "Alone.":
    xalign 0.3 yalign 0.1
with dissolve

pause 0.01

show text "Warm.":
    xalign 0.9 yalign 0.4
with dissolve

pause 0.01

show text "Safe.":
    xalign 0.5 yalign 0.8
with dissolve

pause 0.01

show text "Hide.":
    xalign 0.25 yalign 0.4
with dissolve

pause 0.01

show gold
with dissolve

show red
with dissolve

hide red
with dissolve

show red
with dissolve

hide red
with dissolve

pause 1.0

centered "Remember?"

stop music fadeout(1.0)

show sunbright
with dissolve

scene ClodiaRoomDay
with dissolve

pause 1.0

nar "My room is cold this morning. The braziers went out in the night with no slaves allowed in to attend them."

nar "That was a strange dream. An old one. Haven't had it in years."

nar "Ghost of Dreams... what did it mean?"

scene black
with fade

pause 0.5

centered "Wait..."

centered "Wasn't it..."

label morning_assault:

pause(1.0)

play music sandman

pause(1.0)

play audio singleballista

centered "Another stone crashes home, smashing into a cloud of dust against the walls of Dagara."

scene WallDamaged with fade:
    zoom 1.02 yalign 0.5

play audio singleballista

pause(0.4)

show WallDamaged with vpunch:
    zoom 1.02

pause(1.0)

play audio singleballista

pause(0.4)

show WallDamaged with vpunch:
    zoom 1.02

pause(1.2)

play audio singleballista

pause(0.4)

show WallDamaged with vpunch:
    zoom 1.02

pause(0.8)

play audio singleballista

pause(0.4)

show WallDamaged with vpunch:
    zoom 1.02 

nar "This process has gone on all night, and in the early morning light the results are plain to see."

nar "The intervals between the explosive cracks are quite short, as every engine is aimed at the same spot."

nar "This sort of furious assault is usually used against stronger walls than this, but since we have all the power, why not use it?"

nar "It will not be much longer now. I have as good as view as one can get of the cracks in the wall, the crumbling brick, the powder snuffed out by the wind."

nvl clear

play audio singleballista

pause(0.4)

show WallDamaged with vpunch:
    zoom 1.02

show val rel-serious-flip
with dissolve

nar "Today, I am to lead the assault from the front."

nar "Sergius is a great son of a bitch. This is not my job, not my specialty, not my training."

nar "The other tribunes are all better warriors than I, seeing as how their noble families trained them all for that purpose. Better leaders."

nar "I'm only here because I'm a crook."

nar "But no, it has to be me. Because I'm probably going to die."

nar "There's no way we'll lose this battle, it's just a matter of who makes it to the other side. And that bastard Sergius knows it."

nvl clear

play audio singleballista

pause(0.4)

show WallDamaged with vpunch:
    zoom 1.02

show val rel-emotional-flip
with dissolve

nar "Clodia... I'm frightened."

nar "I'm no soldier. These bastards are going to make me die for them before I can even glimpse your face again."

nar "I don't know what to do."

show val rel-normal-flip
with dissolve

nar "I'm shaking in my saddle. Can't let the men see it, though. I'm their tribune, I must inspire confidence!"

nar "I'm trying not to throw up."

nvl clear

play audio singleballista

pause(0.4)

show WallDamaged with vpunch:
    zoom 1.02

nar "The order is to go as soon as the wall opens, which could happen at any shot now."

nar "The horse beneath me is barely a warhorse."

nar "The sword at my side is light and simple and feels heavy."

nar "The helmet on my head is painful for what it portends."

nar "The shell of armor around me is gold and red and looks as if it will make me a dashing coffin."

nvl clear

play audio singleballista

pause(0.4)

show WallDamaged with vpunch:
    zoom 1.02

hide val
with dissolve

nar "Where is that fucking rider from the capital? He could be here any hour."

nar "Likely his news will be of no help to me, but by chance there is no blessing... that could stop all of this!"

nar "But that's not going to happen, I think."

nar "Not unles the Ghosts themselves stop it... if the Ghosts even care..."

nar "But... Clodia... If I can get to you first... yes."

nar "I can get to you before any of them."

nvl clear

play audio singleballista

pause(0.4)

show WallDamaged with vpunch:
    zoom 1.02
    
show val rel-emotional-flip
with dissolve

nar "Calm, Valerius. You've read all the great tacticians. You aren't an imbecile."

nar "Calm. Calm. Remember what you have read. Remember what you have learned."

nar "Ghosts of Fear and Courage. Ghosts of War."

nar "Posess me. Guide me."

nar "Please."

nar "Not much time now."

nvl clear

play audio singleballista

pause(0.4)

show WallDamaged with vpunch:
    zoom 1.02

show val rel-serious at center
with dissolve

show CGT with dissolve:
    zoom 1.5 yalign 0.0 xalign 0.0
    linear 20.0 xalign 0.5

nar "The men under my direct command are eager, I see it in their eyes."

nar "They look upon me distantly. I am no one to them, a title and nothing more. Do they see my fear?"

nar "There are six thousand soldiers and then some here. Two legions. Rather small for most cases."

hide val
 
nar "During the war we had ten legions burning their way across Dagara at any given time. The camps would stretch almost to the horizon in every direction."

nar "But for taking this single city, held by barely two thousand enemies, Sergius was sure to not bring more than he needed. Two legions are plenty."

nvl clear

hide CGT
with dissolve

play audio singleballista

pause(0.4)

show WallDamaged with vpunch:
    zoom 1.02

nar "Enough reminiscing, I need to think on my tactics."

nar "When the wall opens, I will send the first maniple of a hundred heavy infantry in. Maybe the first two maniples, depending on how wide a gap we open. I will need to stay close behind them to keep up morale."

nar "Right in missile range, of course. At least I will die before I know what hit me. Maybe."

nar "The cavalry which I command personally is made of eighty horsemen. If we do well, we should not have direct contact with the enemy. Horses are not as useful as usual in the narrow streets of a city."

nar "The infantry are the masters of that, their flexible formations able to flow through city streets like a stream one moment and then freeze solid like a crushing wall of ice the next."

nar "But they will be crushing against other Raskyan shields... this will be awful, I think."

nvl clear

play audio singleballista

pause(0.4)

show WallDamaged with vpunch:
    zoom 1.02

nar "... I must give a speech. Even if it's short. Every leader of battle gives a speech. The other tribunes are surely doing the same further back in the ranks."

show val rel-normal at left
with dissolve

show sol neu-neu-flip at sright
with dissolve

nar "I ride my mount out in front of the first line. Speeches tend to only be heard by the first few ranks, but hell, they're the ones who will need the encouragement the most."

nar "What do I say?"

nvl clear

menu:
    "What do I say?"
    "Raise their spirits with the promise of rape and pillage.":
      jump violent_speech
    "Appeal to their civic and spiritual duty to the Basileus.":
      jump passive_speech

label violent_speech:

$ speech = "successful"

show val ac-annoyed at ffleft
with ease

v "All right you men, I am under no illusions. You do not know me well, and that is fine. Because I know you."

v "The legatus is here for glory and religious satisfaction. The tribunes like myself are here for power. But what are you here for?"

show val cmd-pleasant
with dissolve

v "Gold and girls, I suspect!"

show sol neu-shout-flip
with dissolve

"A rousing cheer goes up from the men."

show val cmd-ordering
show sol neu-neu-flip
with dissolve

v "Well, Dagara may not be the greatest city, but it has more than enough gold and girls for you! You just have to take them!"

v "Do whatever you must to kill the traitorous enemy! Even now, those dogs admire treasure and fuck girls that are yours by right!"

show val cmd-urgent
with dissolve

v "Well if I had a man who had fucked my girl in front of me, I'd run him right through his belly! So what are you going to do?"

hide val
with easeoutleft

show sol br1-sp-shout-flip
with dissolve

"A roar this time, in unison. Their blood is high on fire. They will fight and kill to satisfy their natural beasts. I may have to do very little indeed."

jump battle_begins

label passive_speech:

$ speech = "unsuccessful"

show val rel-normal at ffleft
with ease

show val cmd-ordering
with dissolve

v "Brave soldiers of Raskya, you have fought many battles in these past six years. Already you are hardened souls, and have seen many terrors."

v "It has always been barbarians who fell under your blade, so today we'll be doing something different. How does traitor's blood sound?"

show val rel-serious
with dissolve

show sol neu-shout-flip
with dissolve

"A few cheers go up from the first few ranks. Not exactly the rousing battle cry I wanted."

show val ac-fact
show sol neu-neu-flip
with dissolve

v "Your Basileus awaits the outcome of this siege. Will you take the city back for the glory of our nation? Yes you will!"

show val cmd-urgent
with dissolve

v "For your glory, for Raskya's glory, for the Basileus! We will fight!"

show sol neu-confuse-flip
with dissolve

"A smattering of roars and affirmation. But their fire has not gone up. Many of them seem either bored or stone scared."

show val ac-annoyed
with dissolve

"Damn."

"The great secret that every military man knows deep inside would be betrayed were the Basileus here to see: soldiers do not fight for anyone but themselves."

jump battle_begins

label battle_begins:

stop music fadeout 1.0

hide val
hide sol
with dissolve

play audio stonecollapse

$renpy.pause(1.0, hard=True)

show WallDamaged with vpunch:
    zoom 1.02

show WallDamaged with vpunch:
    zoom 1.02

show WallDamaged with vpunch:
    zoom 1.02
    
show WallDamaged with vpunch:
    zoom 1.02
    
show sunbright
with dissolve

show WallDestroyed with dissolve:
    zoom 1.02

show WallDestroyed with vpunch:
    zoom 1.02

show WallDestroyed with vpunch:
    zoom 1.02

show WallDestroyed with vpunch:
    zoom 1.02
    
show WallDestroyed with vpunch:
    zoom 1.02
    
show WallDestroyed
with dissolve

stop sfx1 fadeout 1.5

"That's it!"

"Smoke billows like the breath of hell from the shattering stone. Rubble cascades to the earth."

"The way is open."

show val cmd-urgent-flip at center
with dissolve

play music menacing

v "GO!"

play sound hornadvance

nar "The trumpeter blows out the command."

hide val
with dissolve

play sfx1 marchingalt loop

show CGT with dissolve:
    yalign 0.93
    linear 0.5 yalign 1.0
    linear 0.2 yalign 0.93
    repeat

nar "The first maniple moves, rattling a hundred weighted javelins. The rumble of two hundred spiked sandals tearing up the earth fills my chest."

nar "They march beautifully, their shields raised, formation unbroken. The light catches like red gold on their armor, gleams like silver on their spears."

nvl clear

hide CGT
with dissolve

nar "The dust is clearing, a soft morning breeze revealing the opening in the stone. The gap cuts through the wall like a jagged bolt from heaven."

nar "It is not wide; the men will have to shorten and deepen their ranks."

nar "Luckily, orders on that scale are not my concern. Each maniple has its centurion to lead them, and they will organize the lines to fit."

nar "This is the best experienced and most drilled army in the entire Kingdom, and my input should be minimal if the machine works."

nar "The first maniple marches steadily. They are now about halfway to the wall. My turn."

nvl clear

show val cmd-urgent-flip at fright
with dissolve

v "Horses, march!"

stop sfx1 fadeout 1.5
play sfx2 marchinghorse fadein 1.5

hide val
with dissolve

"Walking my steed out first, I am followed by quiet thunder. We move directly behind the first maniple. Behind us the rest of the maniples falls in."

show sol neu-neu-flip at scenter
with dissolve

"Up ahead the first men are reaching the gap. There doesn't seem to be any combat yet."

"Surely the enemy wouldn't leave this gap wide open? There has to be at least some of them up ahead."

show sol br1-sp-shout-flip at sright
with ease

so "Above!!!"

hide sol
show WallDestroyed with dissolve:
    zoom 1.5 yalign 1.0
    linear 0.5 yalign 0.0

"I jerk my head upward."

"Men, on top of the wall. Standing at the lip where the stone was shattered. Arrows and javelins at the ready."

show CGX with dissolve:
    zoom 2.0 xalign 0.1 yalign 1.0
    linear 0.25 yalign 0.5
    xalign 0.8 yalign 0.5
    linear 0.25 yalign 0.0
    xalign 0.5 yalign 0.9
    linear 0.25 yalign 0.4
    xalign 0.8 yalign 0.5
    linear 0.25 yalign 0.0
    xalign 0.3 yalign 0.6
    linear 0.25 yalign 0.1
    xalign 0.6 yalign 0.7
    linear 0.25 yalign 0.2
    xalign 0.2 yalign 1.0
    linear 0.25 yalign 0.5
    xalign 0.4 yalign 0.5
    linear 0.25 yalign 0.0
    repeat

play sfx3 javhail loop

"The first missiles begin to fall."

show val cmd-urgent-flip
with dissolve

v "Testudo! Now!"

hide val
with dissolve

play sound horntestudo

nar "My trumpeter blows out the orders."

play sfx3 javhailshields loop

show CGT with dissolve:
    yalign 0.5 xalign 1.0 zoom 1.2
    choice:
        linear 0.1 yalign 0.5
        linear 0.1 yalign 0.6
    choice:
        linear 0.1 yalign 0.65 
        linear 0.1 yalign 0.55
    choice:
        linear 0.1 yalign 0.55
        linear 0.1 yalign 0.47
    choice:
        linear 0.1 yalign 0.52
        linear 0.1 yalign 0.37
    repeat

hide CGX

nar "In a trained second the entire first maniple raise their shields."

nar "The first rank holds their shields before them as normal-"

show CGTTest with dissolve:
    yalign 0.1 xalign 0.2 zoom 1.6
    linear 0.5 yalign 0.3
show CGTTest:
    yalign 0.3 xalign 0.2 zoom 1.6
    choice:
        linear 0.1 yalign 0.3
        linear 0.1 yalign 0.25
    choice:
        linear 0.1 yalign 0.33 
        linear 0.1 yalign 0.28
    choice:
        linear 0.1 yalign 0.28
        linear 0.1 yalign 0.22
    choice:
        linear 0.1 yalign 0.29
        linear 0.1 yalign 0.21
    repeat
    
hide CGT
hide WallDestroyed

pause 1.0

nar "-but every other rank holds their shield above his head, protecting himself and the man in front of him with its length."

nar "The maniple is now a giant tortoise."

nar "And the missiles bounce uselessly off its shell."

hide CGTTest
show WallDestroyed
with dissolve

nvl clear

show val cmd-urgent-flip
with dissolve

v "Keep going! Get inside!"

hide val
with dissolve

nar "The testudo formation can move, but it is slow. As the first maniple moves steadily into the opening, I can't yet follow."

nar "My horses could only safely pass through if we charged, using speed as our defense. But until the maniple has completely cleared the gap that isn't possible."

nar "And we certainly can't form anything like a testudo."

nvl clear

show val cmd-urgent-flip
with dissolve

v "Follow me! And get ballistae on those fucking gnats up there!"

hide val
with dissolve

nar "Leading my horses out of formation, we break out of the massive line marching towards the gap, simply moving aside so that the second maniple can follow the first."

nar "There's just no getting the horses in until that wall is cleared of enemies."

nvl clear

show val cmd-urgent
with dissolve

v "Did you hear me? Ballistae!"

play sound hornballista

"The trumpet signals travel back into the army, heard by one trumpeter and relayed to the next."

stop sfx1 fadeout 0.5
stop sfx2 fadeout 0.5
stop sfx3 fadeout 0.5

scene CampDay
with fade

play sound hornballista

"All the way back to the engines."

show en neu-neu-flip at scenter
with dissolve

en "Looks like they have trouble up on the walls."

show en neu-shout
with dissolve

en "Bolts only! Load!"

play sound ballistacrank

pause (1.0)

hide en
show CampDay:
    zoom 3.0 yalign 0.2
    linear 2.0 yalign 0.0
with dissolve
    
pause (2.0)

en "Loose!"

play sound ballistafire

play sfx1 marchinghorse loop

scene WallDestroyed
with fade

play sound ballistawhoosh

show WallDestroyed:
    zoom 2.0 xalign 1.0 yalign 0.3
    linear 1.0 xalign 0.3 yalign 0.1
with dissolve

play sound ballistawood

pause 0.5

show WallDestroyed:
    zoom 2.0 xalign 0.3 yalign 0.1
with hpunch

play sound ballistawhoosh
play sound ballistawood

pause 0.5

show WallDestroyed:
    zoom 2.0 xalign 0.3 yalign 0.1
with hpunch

play sound ballistawhoosh
play sound ballistawood

pause 0.5

show WallDestroyed:
    zoom 2.0 xalign 0.3 yalign 0.1
with hpunch

pause 0.5

nar "The great black bolts soar over all our heads."

nar "Hard to see how effective they were from down here, but the men up on the wall ease up on their assault."

nar "They don't have long to decide what they will do; the next bolts are coming."

play sound ballistawhoosh
play sound ballistawood

pause 0.5

show WallDestroyed:
    zoom 2.0 xalign 0.3 yalign 0.1
with hpunch

play sound ballistawhoosh
play sound ballistawood

pause 0.5

show WallDestroyed:
    zoom 2.0 xalign 0.3 yalign 0.1
with hpunch

play sound ballistawhoosh
play sound ballistaflesh

pause 0.5

show WallDestroyed:
    zoom 2.0 xalign 0.3 yalign 0.1
with hpunch

pause 0.5

nar "This time a man tumbles from on high, his body hitting the dirt with a final sound that is impossible to hear over the footsteps of thousands of men."

nvl clear

stop music fadeout(3.0)

show WallDestroyed:
    zoom 1.0
with dissolve

pause 1.0

nar "The missles have stopped. The enemy are either fleeing or hiding."

nar "The first maniple seems to have nearly cleared the gap, the second already in testudo formation behind them."

nar "I can't see what is front of the first maniple anymore. Surely there must be some of the enemy there that they've made contact with."

nar "As the minutes pass and rank after rank of men pours through the gap, I figure that there must not be much of a battle going on inside yet if the traffic hasn't halted. It's likely a good time for the horses."

nvl clear

show val cmd-urgent-flip
with dissolve

v "Horses, fall in behind the second maniple!"

hide val
with dissolve

nar "There's enough of a space between the second and third that I can swiftly insert my horses into it. Keeping an eye up on the wall, where I see no more soldiers, we march into the breach."

nar "The shattered bricks around me are still glowing with dust, the air harsh and burnt. The horses have a little trouble with the rubble, but an easy enough path is found."

nar "Now I can see that there is indeed no battle raging on the other side of the wall. Only a city."

nvl clear

show StreetsDay
with fade

play music dyingsoldiers

stop sfx1 fadeout 1.0
play sfx1 march
play sfx2 horsemove

nar "The soldiers are already setting up positions, blocking off alleyways, kicking down doors."

nar "Once this small plaza is secure, sweeping the streets will be a simple matter."

nar "Passing the wall is always the more stressful part of these things, though never have I seen to see it up close!"

nar "The horses are in. Excellent! I turn to my heralds, nine boys on horseback who are my direct connection to the nine centurions underneath me."

nvl clear

show val cmd-ordering-flip
with dissolve

v "All right, old guard maniples hold this plaza. Middle maniples get up on the wall and kill everyone up there. The youngbloods should prepare to begin street sweeping with me."

show val cmd-urgent-flip
with dissolve

v "And by the Ghosts, don't start pillaging until after the city is secure! Go!"

"The boys take off, riding to their respective centurions."

show val rel-serious-flip
with dissolve

"Here is a moment's rest as the men get organized. But we've breached the walls."

show val rel-serious
with dissolve

"I can see the tops of the citadel in the distance. It will likely take us all day to fight our way there."

show val rel-emotional
with dissolve

"I will figure something out before then, Clodia."

show black
with dissolve

centered "I promise."

pause 1.0

stop music fadeout 1.0
stop sfx1 fadeout 1.0
stop sfx2 fadeout 1.0
stop sfx3 fadeout 1.0

if portia == "alive" and plan == "escape":
    jump imprisoned_portia
    
if plan == "clodia_poison" and portia == "alive":
    jump imprisoned_portia

if portia == "dead" and plan == "escape":
    jump imprisoned_no_portia
    
if portia == "dead" and plan == "clodia_poison":
    jump imprisoned_no_portia
    
if plan == "nasrin_poison":
    jump maiden_day

label imprisoned_portia:

scene ClodiaRoomDay
with fade

play music downtime

pause 1.0

nar "The blood blossoms are starting to wilt."

nar "The slaves haven't watered them today. Because they haven't been allowed inside."

nar "I haven't seen a soul since I was thrown in here last night on Sejanus's order. All I have is myself, and my thoughts."

nvl clear

c "Valerius..."

nar "They're assaulting the city now. I have a lovely view from my window. Sejanus must have thought that a convenient punishment when he decided to use my own room as my cell."

nar "The wall appears to have been taken. Smoke and dust obscures the sight from time to time, and all Raskyan legionaries wear red anyway, so I can't tell who is who."

nar "But the little red dots are killing each other below in the hundreds, probably thousands."

nar "Smoke and dust obscures the sight from time to time."

nar "How many citizens are going to die down there? How many children?"

nar "They should all be up here safe in the citadel, but Sejanus has no interest in them. They don't offer him power."

nvl clear

nar "Is Valerius down there? Ghosts of War, if you care, keep him away from the fighting. Keep him safe."

nar "I need to do something. I need to..."

stop music fadeout(0.5)

nvl clear

p "You let me in this instant."

g "Fuck off lady."

p "No, you do the fucking off."

play music reprieve

show clo snk-relief-flip at center
with dissolve

"Someone is speaking outside my door. Someone I know well."

p "Your Praetrix is in that room. I don't care if she's a prisoner, a noble lady needs her attendants! This is shameful, and blasphemous besides."

g "Ghosts of the Grave take you. Get her out of here."

show clo reg-declaring-flip
with dissolve

"Oh no you don't."

c "I order you to let her in."

"A pause."

g "My apologies Praetrix, but the Praetor has more authority than you."

show clo reg-stoic-flip
with dissolve

c "Sejanus is not yet Praetor."
    
g "Even so, milady-"

show clo reg-declaring-flip
with dissolve

c "But once I am freed from this room, I'll be sure to have you hurled from a window unless you allow Lady Portia in here this instant."

g "For fuck's sake."

play sound dooropen

show por neu-neu at left
with easeinleft

p "My lady."

show clo defen-crying-flip
show por con-sigh
with dissolve

show clo defen-crying-flip at mright
show por con-sigh at center
with ease

"She embraces me deeply. For a moment it makes everything seem okay. A moment."

show clo reg-stoic-flip
with dissolve

show clo reg-stoic-flip at right
with ease

c "Portia, we must-"

show por neu-neu-flip
with dissolve

"She cuts me off with a look back at the open door, and the men outside."

show por neu-distaste-flip
with dissolve

p "Privacy."

play sound doorclose

"Grunting, the guard closes the door."

show por neu-neu
with dissolve

show por neu-neu at mleft
show clo reg-stoic-flip at mright
show ClodiaRoomDay:
    linear 1.0 xalign 0.5 yalign 0.1 zoom 1.5
with ease

pause 1.0

"Once I hear the lock click into place I pull Portia over to the window, to show her the spectacle below. A strange sigh escapes her."

show por con-sigh
with dissolve

p "Ah. It's... quite a terrible sight up close."

c "It's going to get closer. We must do something."

show por neu-neu
with dissolve

p "I am not sure what there is to do, except to pray."

show clo rel-angered-flip
with dissolve

c "No Ghosts will listen to us. War is on Sergius's side."

c "Sejanus has no hope. And we have no hope if we're caught with him."

show por plan-concern
with dissolve

p "If I had a way out of the citadel I would have taken you out long ago."

show clo reg-stoic-flip
with dissolve

c "We don't have to leave the citadel."

show por plan-explain
with dissolve

p "What are you thinking?"

show clo rel-neutral-flip
with dissolve

c "There are rooms beneath the citadel. Not just the sewers, I mean."

c "Hidden hallways and passages. Secret rooms."

show por neu-neu
with dissolve

p "What rooms are these? I think I would have heard about them."

show clo reg-stoic-flip
with dissolve

"Should I tell her they're rooms I remembered from a dream? That they may not even be real?"

c "I'm... it's something only Uncle and a few others knew about."

show por plan-explain
with dissolve

p "How did you learn of them?"

show clo reg-stoic
with dissolve

c "I... I found them once. When I was a little girl. By accident, as I was exploring."

show por plan-concern
with dissolve

p "Why haven't you ever told me about this?"

show clo reg-emotional
with dissolve

c "... Uncle found out. He was angry. Horribly angry."

show clo reg-emotional-flip
with dissolve

c "He struck me and told me never to tell anyone or he would have me banished."

show por neu-worried
with dissolve

p "Well... when was this?"

show clo reg-stoic-flip
with dissolve

c "I don't quite remember. Perhaps I was... seven or eight? I had forgotten all about them for years. I think I made myself forget."

c "Until last night. The Ghosts of Dreams came to me and reminded me of them."

show por con-sigh
with dissolve

p "Oh... well..."

"Please. Please believe me, Portia."

show por neu-neu
with dissolve

p "Do you think we can hide there? Won't they find us eventually?"

show clo rel-neutral-flip
with dissolve

c "Maybe not. The attackers don't know this place well. And if we can weather out the assault until passions have cooled, perhaps it would be safe to show ourselves then."

show por con-sigh
with dissolve

p "I suppose if that's the only plan..."

show clo reg-emotional-flip
with dissolve

c "It's either that or I'm leaping from this window when they come for me."

show por neu-worried
with dissolve

"The pain that crosses her face when I say that makes my heart lurch."

show por neu-worried-flip
with dissolve

p "Well then, where are these rooms? I can perhaps go find them."

c "That's the problem. The dream didn't show me."

show por con-sigh-flip
with dissolve

p "Oh. That's just like the Ghosts."

show clo reg-stoic-flip
with dissolve

c "You should go now before the guards grow too suspicious."

show por plan-explain
with dissolve

p "What am I to do?"

c "Ask everyone you can, everyone you trust, about how to find those rooms."

show por neu-neu
with dissolve

p "I don't trust anyone here but you. And if it was something the Praetor kept so secret, I doubt anyone who listens to me knows."

show clo cha1-sass-flip
with dissolve

c "You can make a lot of men listen to you, if it's in their interest."

show por neu-distaste
with dissolve

p "Hmph. You think I could just flirt about with some man like that?"

show clo rel-neutral-flip
with dissolve

c "Yes."

show por plan-insist
with dissolve

p "Hush! Very well. I'll see what I can do."

show clo rel-neutral-flip
with dissolve

c "Be safe."

show por con-sigh-flip
with dissolve

p "You too dear. Oh Ghosts..."

show por neu-neu-flip
with dissolve

show ClodiaRoomDay:
    linear 0.5 zoom 1.0
show por neu-neu-flip at left
with ease

play sound knockdoor
pause 0.3
play sound dooropen

p "Ahem. Thank you very much, sirs."

g "Yeah, whatever. Get going."

show por neu-distaste-flip
with dissolve

p "Ugh."

hide por
with easeoutleft

play audio doorclose

nar "And now, back to waiting helplessly. Or at least, planning for how to get out of my room."

nar "That will be the only piece of the puzzle missing once Portia finds the hidden place from my dream."

nvl clear

show black
with dissolve

centered "Think, Clodia. Think."

stop music fadeout(1.0)

jump street_battle

label imprisoned_no_portia:

scene ClodiaRoomDay
with fade

play music downtime

pause 1.0

nar "The blood blossoms are starting to wilt."

nar "The slaves haven't been allowed in to water them. The usual vibrant crimson is starting to fade. The sweet, seductive scent has become faint."

nar "When I place my nose right inside them and breathe, I only catch the most distant whiff of a passionate memory."

nvl clear

c "Valerius..."

nar "They're assaulting the city now. I have a lovely view from my window. Sejanus must have thought that a convenient punishment when he decided to use my own room as my cell."

nar "The wall appears to have been taken. Smoke and dust obscures the sight from time to time, and all Raskyan legionaries wear red anyway, so I can't tell who is who."

nar "But the little red dots are killing each other below in the hundreds, probably thousands."

nar "Smoke and dust obscures the sight from time to time."

nar "How many citizens are going to die down there? How many children?"

nar "They should all be up here safe in the citadel, but Sejanus has no interest in them. They don't offer him power."

nvl clear

nar "Is Valerius down there? Ghosts of War, if you care, keep him away from the fighting. Keep him safe."

nar "I need to do something. I need to..."

nar "But..."

nvl clear

nar "I have no ideas."

nar "The window is too high. The door is guarded by four men. Portia is dead."

nar "And I've gone and lost Valerius's spy. I have nothing. I can do nothing."

show black
with dissolve

centered "I feel so fucking helpless."

stop music fadeout(1.0)

jump street_battle

label maiden_day:

scene TempleDay
with fade

play music nostalgia

pause 1.0

nar "The incense is hot and cloying and I feel like I'm about to vomit when the priestess hands me a bowl of water."

show clo2 rel-neutral at center
with dissolve

c "Ah. Thank you."

nar "She silently smiles, then goes back to praying and blowing scented smoke about me."

nar "The cool water sends my stomach back down for a bit. If they would have given me more to eat than ceremonial fruit I might feel less faint."

nar "To all sides of me, priestesses silently chant with their eyes turned to the temple's ceiling."

nar "Calling on the Ghosts of Lust to abandon me, the Ghosts of Mercy to forgive me, and the Ghosts of Love to join me to Sejanus."

nar "There are some prayers which you know will never be answered."

show clo2 reg-stoic
with dissolve

nvl clear

nar "This has been going on for hours now. My knees ache from sitting on them, and sweat sticks my ceremonially plain dress to my body."

nar "The windows have been closed, and all the heat from the candles and the bodies has been trapped in here with us. Sin leaves through sweat."

nar "I wish it could just leave through taking a piss, that would be so much simpler."
 
nar "For a second, the priestesses eyes flicker around in fear, before they return their gaze to the Ghosts. I'm sure this is taking so long because they're throwing extra prayers in there for themselves."
 
nar "So I say a prayer. A quick one."

show black
with dissolve

centered "Let them all kill each other out there.{p}{p=0}And let Valerius stand upon all their corpses."

stop music fadeout(1.0)
 
label street_battle:

scene StreetsDay
with fade

play music dyingsoldiers

pause 1.0

nar "The streets before me look empty. Dead."

nar "The low roar of combat elsewhere in the city fills my ears like a bloody ocean, but before me is peaceful."

nar "And the citadel is not so far away. Where is the enemy?"

nvl clear

ce "Orders, tribune?"

show val cmd-ordering at center
with dissolve

v "Advance in formation. Prepare for ambushes. Watch the buildings."

hide val
with dissolve

play sfx1 march
 
nar "This main street should be crawling with foes. There should be a titanic clash of Raskyan shields, a shoving match of attrition, men slipping and dying in the blood."

nar "Instead, the first maniple advances before me against the air. My horses follow closely."

nar "The tall buildings seem bare. Every door and window is sealed."

nar "How many Dagarans are crouched under their beds, uttering prayers, killing themselves because we are here?"
 
nar "Were I in charge they would all have little to fear."

nar "With Sergius though..."

nvl clear

nar "We pass alley after alley on either side, the men peering down them for a moment before moving on."

nar "Our goal is not to kill every rebel, even if Sergius thinks so. If we take the citadel and capture or kill Sejanus, no one of consequence will continue to put up a fight."
 
nar "A figure darts out of an alley ahead of us, running as fast as he can."

nar "Some wayward citizen. He flees from us... but then something causes him to stop, turn, and run back the way he came."
 
nar "Whatever he saw is just around a slight corner up ahead."

nvl clear

show val cmd-ordering at center
with dissolve

v "Halt!"

stop sfx1 fadeout 1.0

"The grinding engine of men ceases for a moment."
 
v "Scouts, check around that corner."

show val rel-normal
with dissolve

nar "Two lightly armed javelinmen detach themselves from the lead maniple and run ahead. After a minute or so, they come running back, allowed between the shields of the men to report to me."

nvl clear

sc "Rebel legionaries around the corner. Blocking the street. Looks like they've decided to defend the main square."
 
v "Numbers?"
 
sc "Four hundred fifty, maybe five hundred?"

hide val
with dissolve
 
nar "A fourth of Sejanus's entire force. Enough to defend Dagara's square against superior numbers."

nar "I probably won't be able to expect much help immediately from Sergius. The rest of Sejanus's men are probably fighting and dying against them as I waste time here."
 
nar "But this is the main street to the citadel. We will have to take this square ahead."

nar "Enveloping them will come naturally if I just wait for the other tribunes to work their ways around, but that's not something I can waste too much time on."
 
nar "I must reach the citadel before Sergius. Depending on how strong or weak its defenses are, he might just keep going inside and kill everyone the moment he gets there."

nvl clear

nar "First thing's first: I need to send more scouts out to be sure about what we're up against."

nar "I can send some of the scouts to check the rooftops, maybe set up an ambush for that maniple around the corner."

nar "Or I can have them search for the best route to flank the enemy."

nar "Doing both would be ideal, but Sergius only spared me so many scouts and I need to keep most of them here in case that maniple comes around the corner. We'll need all the javelins we can get."

nar "Either way, I need to decide right now!"

nvl clear

menu:
    "What orders shall I give the scouts?"
    "A. Send them up on the rooftops to get a better vantage point.":
      jump scout_roof
    "B. Send them down the alleys to find a flanking route.":
      jump scout_street

label scout_roof:

$ scout = "roof"

show val cmd-ordering
with dissolve

v "Scouts, scale these homes and spread out on the rooftops, tell me what you see. First Centurion!"

ce "Tribune?"

v "Prepare your men for attrition. I will be sending you all directly against that square soon."

ce "It shall be done."

hide val
with dissolve

nar "About twenty scouts detach from their rest of their group, carrying ladders with them as they go. They ascend the buildings, with nimbleness only possible in men without any real armor."

nar "Surely from up there they'll be able to see all I need to craft a winning tactic."

nar "As far as numbers go we have the advantage. But the enemy has the defensive position, and depending on how well they use it, this may actually be more of an even battle than we planned."

nar "Battles that are almost equal are the most terrifying."

nvl clear

stop music fadeout(0.5)

so "Enemy javelinmen on the roofs!"

play music menacing

"Shit!"

show val cmd-urgent at fleft
with dissolve

v "TESTUDO! HORSES FALL BACK!"

play sfx2 javhail loop

show CGTTest with dissolve:
    yalign 0.1 xalign 0.2 zoom 1.6
    linear 0.5 yalign 0.3

nar "The infantry's shields all go up above their heads, just as the javelins begins to fall."

show CGX with dissolve:
    zoom 2.0 xalign 0.1 yalign 1.0
    linear 0.25 yalign 0.5
    xalign 0.8 yalign 0.5
    linear 0.25 yalign 0.0
    xalign 0.5 yalign 0.9
    linear 0.25 yalign 0.4
    xalign 0.8 yalign 0.5
    linear 0.25 yalign 0.0
    xalign 0.3 yalign 0.6
    linear 0.25 yalign 0.1
    xalign 0.6 yalign 0.7
    linear 0.25 yalign 0.2
    xalign 0.2 yalign 1.0
    linear 0.25 yalign 0.5
    xalign 0.4 yalign 0.5
    linear 0.25 yalign 0.0
    repeat
    
play sfx3 javhailshields
stop sfx2

hide CGTTest
hide StreetsDay

pause 3.0

show CGTTest:
    yalign 0.3 xalign 0.2 zoom 1.6
    choice:
        linear 0.1 yalign 0.3
        linear 0.1 yalign 0.25
    choice:
        linear 0.1 yalign 0.33 
        linear 0.1 yalign 0.28
    choice:
        linear 0.1 yalign 0.28
        linear 0.1 yalign 0.22
    choice:
        linear 0.1 yalign 0.29
        linear 0.1 yalign 0.21
    repeat
with dissolve

pause 2.0

nar "They whistle down onto the shields of the testudo, bouncing harmlessly off. Thank the Ghosts I had scouts up there to warn us in time."

nar "I see the enemy scouts up on the roofs, hurling their missiles, first down at the testudo, and then when it was clear that wasn't going to work, at our scouts."

show StreetsDay:
    zoom 1.5 xalign 0.0 yalign 0.0
    linear 1.0 xalign 1.0
    linear 0.1 xalign 0.95
    linear 0.1 xalign 1.0
    linear 0.1 xalign 0.95
    linear 0.1 xalign 1.0
    linear 1.0 xalign 0.0
    linear 0.1 xalign 0.05
    linear 0.1 xalign 0.0
    linear 0.1 xalign 0.05
    linear 0.1 xalign 0.0
    repeat
with dissolve

nar "A kind of duel of javelins follows up above, the rival scouts trading missiles across the street."

nar "No reason not to help them. I yell out to the centurion."

show StreetsDay:
    zoom 1.0
with dissolve

hide val

show val cmd-ordering
with dissolve

v "Storm the buildings! Get up on those roofs and kill them!"
 
ce "Take the buildings! Take the buildings!"

hide val
with dissolve

nar "The infantry starts to kick in doors, forcing their way into the nearest buildings."
 
nar "Minutes later, they emerge on the roofs and attack the enemy scouts."

nvl clear

stop sfx3 fadeout 1.0

nar "The javelins stop flying."

show StreetsDay:
    zoom 1.5 xalign 0.8 yalign 0.0
    pause 0.5
    linear 0.3 yalign 1.0
    linear 0.1 yalign 0.95
    linear 0.1 yalign 1.0
    linear 0.1 yalign 0.95
    linear 0.1 yalign 1.0
with dissolve

pause 1.0

nar "It's the javelin{i}men{/i} who fall now."

nar "I see them hurled from windows, from the rooftops, their throats slit. Their heads sometimes fall separately from them."

nar "It's safe now. And I don't think we lost a single man to that ambush!"

nvl clear

show StreetsDay:
    zoom 1.0
with dissolve

show val rel-normal
with dissolve

v "Excellent. Now let's regroup and-"

if speech == "successful":
    jump rapine
    
if speech == "unsuccessful":
    jump order

label scout_street:

$ scout = "street"

show val cmd-ordering
with dissolve

v "Scouts, check these alleyways and search for routes to flank the square. First Centurion, prepare your men for attrition. I will be sending you all directly against that square soon."
 
ce "It shall be done."

hide val
with dissolve

nar "About twenty scouts detach from the rest of their group and spread out, disappearing down various alleys. Finding a flank is going to be key to ending this fight without an hours long struggle."
 
nar "As far as numbers go we have the advantage. But the enemy has the defensive position, and depending on how well they use it, this may actually be more of an even battle than we planned."

nar "Battles that are almost equal are the most terrifying."
 
nar "Hm?"

nvl clear

show sol neu-shout at scenter
with dissolve

so "ABOVE!"

play sound ballistaflesh

show sol neu-confuse
with vpunch

nar "A javelin plants itself in the head of the horseman near me."

hide sol neu-confuse
with easeoutbottom

nar "He slides off his mount, blood running from his lips."

nar "The rooftops are crawling with javelinmen!"

nvl clear

play sfx2 javhail

show val cmd-urgent
with dissolve

v "TESTUDO!"

show CGX with dissolve:
    zoom 2.0 xalign 0.1 yalign 1.0
    linear 0.25 yalign 0.5
    xalign 0.8 yalign 0.5
    linear 0.25 yalign 0.0
    xalign 0.5 yalign 0.9
    linear 0.25 yalign 0.4
    xalign 0.8 yalign 0.5
    linear 0.25 yalign 0.0
    xalign 0.3 yalign 0.6
    linear 0.25 yalign 0.1
    xalign 0.6 yalign 0.7
    linear 0.25 yalign 0.2
    xalign 0.2 yalign 1.0
    linear 0.25 yalign 0.5
    xalign 0.4 yalign 0.5
    linear 0.25 yalign 0.0
    repeat
    
play sfx3 javhailshields
stop sfx2

hide val

nar "That order is all I get out before heavy, whistling missiles rain down upon us."

nar "Bouncing off raised shields. Going through arms, legs."

nar "The horsemen are completely unprotected. I try to order them to retreat."
 
nar "And then, I'm falling."

hide StreetsDay

show StreetsDay:
    zoom 1.3 xalign 0.5 yalign 0.5
    linear 1.0 yalign 1.0
with dissolve

hide CGX

show StreetsDay:
    zoom 1.3 xalign 0.5 yalign 1.0
with vpunch

nar "I think I saw the flash of a javelin sticking out of my horse's neck."
 
nar "Shit."

nar "Shit shit shit."

nar "I'm on the stones. I'm bleeding. What happened?"

show CGX with dissolve:
    zoom 2.0 xalign 0.1 yalign 1.0
    linear 0.25 yalign 0.5
    xalign 0.8 yalign 0.5
    linear 0.25 yalign 0.0
    xalign 0.5 yalign 0.9
    linear 0.25 yalign 0.4
    xalign 0.8 yalign 0.5
    linear 0.25 yalign 0.0
    xalign 0.3 yalign 0.6
    linear 0.25 yalign 0.1
    xalign 0.6 yalign 0.7
    linear 0.25 yalign 0.2
    xalign 0.2 yalign 1.0
    linear 0.25 yalign 0.5
    xalign 0.4 yalign 0.5
    linear 0.25 yalign 0.0
    repeat
    
nar "More missiles. Missing me. Coming close. Striking others."

nar "The men need orders."

nar "The horses are turning, crashing into each other, screaming. Blood is everywhere. I taste it."
 
nar "The men need orders."

nar "Somehow, between all the movement and confusion, I found the centurion."

show val cmd-urgent
with dissolve

v "Storm the buildings! Get up on those roofs and kill them!"
 
ce "Take the buildings! Take the buildings!"

show val br2-scanning
with dissolve

nar "There is a shield dropped to the ground nearby. I grab it and hold it before me, crouching."

show val br2-shouting at right:
    zoom 1.5
    choice:
        linear 0.1 yalign 0.3
        linear 0.1 yalign 0.25
    choice:
        linear 0.1 yalign 0.33 
        linear 0.1 yalign 0.28
    choice:
        linear 0.1 yalign 0.28
        linear 0.1 yalign 0.22
    choice:
        linear 0.1 yalign 0.29
        linear 0.1 yalign 0.21
    repeat
    
pause 1.5

nar "Moments later, I feel it shake the bones in my arms as a javelin glances off it."
 
nar "Just stay here, I tell myself. Stay here and wait."

nar "The hits keep coming, the shield rattling and shaking as though any moment it will explode in my hands."

stop sfx3 fadeout 1.0

hide StreetsDay
show StreetsDay
hide val
hide CGX
with dissolve

stop sfx3 fadeout 2.0

pause 0.5

nar "Eventually, things grow quieter. I don't hear the sounds of battle. The javelins have stopped falling."
 
show StreetsDay:
    zoom 1.5 xalign 0.8 yalign 0.0
    pause 0.5
    linear 0.3 yalign 1.0
    linear 0.1 yalign 0.95
    linear 0.1 yalign 1.0
    linear 0.1 yalign 0.95
    linear 0.1 yalign 1.0
with dissolve

pause 1.0

nar "It's the javelin{i}men{/i} who fall now."

nar "I see them hurled from windows, from the rooftops, their throats slit. Their heads sometimes fall separately from them."

nar "I can see my men on the buildings now, clearing them just as I ordered. It's safe now."

hide StreetsDay
show StreetsDay
with dissolve

nar "My horsemen have returned as well. There are about ten dead or dying horses around, and a few fallen riders with them. But the rest of the horsemen seem fine, if shaken."

nar "I'm such a damn fool. I should have had my scouts up there, warning us. That ambush shouldn't have worked as well as it did, but at least we stopped it rather quickly."

nar "But as for {i}how{/i} I had to stop it-"

nvl clear

if speech == "successful":
    jump rapine
    
if speech == "unsuccessful":
    jump order
    
label rapine:

hide val
with dissolve

wm "Help! Get off me, get-"

hide sol
show sol neu-shout-flip at sright
with dissolve

so "Shut up bitch."
 
nar "When my men emerge from the buildings, they are carrying gold, food, women, drink. Spoils."
 
nar "All things which will slow us down."
 
wm "Let go! Please! Please! Ghosts help me!"
 
nar "And besides... this is still so frightful to me."
 
nar "The men are disordered all around me. Where is their discipline? The centurion calls for it but is ignored."

nar "They think this no different from a barbarian sack! Already they carry things back to camp rather than fight on."

nar "I suppose I encouraged this. Fuck."

nvl clear

show val cmd-ordering at fleft
with dissolve

v "Legionaries, halt!"

show sol neu-confuse-flip
with dissolve

nar "Some of them do, looking dumbly at me."

show sol neu-neu
with dissolve

hide sol
with easeoutright

nar "Others blatantly begin to run away faster."

show val cmd-urgent
with dissolve

v "I said halt!"
 
ce "Obey your tribune!"
 
nar "Perhaps I overestimated us. Perhaps Sejanus does have the better trained army."

nvl clear

show val ac-annoyed-flip
with dissolve

v "First Centurion, after this assault is done, identify all the men who are disobeying. Have them all beaten to death."
 
ce "Y... yes tribune."

nar "If I die here because those looters could have turned the tide..."

show val cmd-urgent
with dissolve

v "The rest of you drop everything and get back in formation. NOW!"
 
nar "The order doesn't sound particularly commanding out of my parched throat, but the men begin to reorganize themselves as the centurion's whistle pierces the air."

nvl clear

if scout == "street":
    jump cavalrytactic
    
if scout == "roof":
    jump choosetactic

label order:

wm "Help! Get off me, get-"

show sol neu-shout-flip at sright
with dissolve

so "Shut up bitch."
 
nar "When my men emerge from the buildings, they are carrying gold, food, women, drink. Spoils."
 
nar "All things which will slow us down."
 
wm "Let go! Please! Please! Ghosts help me!"
 
nar "And besides... this is still so frightful to me."
 
nar "Luckily, only a few soldiers seem to be participating in the early looting."

show val cmd-ordering at fleft
with dissolve

v "Legionaries, halt!"

show sol neu-confuse-flip
with dissolve

nar "Most of them do, looking dumbly at me."
 
v "Cease this rape until after the battle is won! Any man who disobeys will be killed!"

show sol neu-neu-flip
with dissolve

nar "With almost childish looks of frustration between them, the looting soldiers drop their booty and release their captives."

nar "The women run back inside their homes screaming the whole way."

show val cmd-ordering
with dissolve

v "Now get back in formation, there's more traitors to kill!"

show sol neu-neu
with dissolve

nar "They obey, which still surprises me more than anything. I guess well drilled soldiers can make up for me being a novice commander."

nvl clear

if scout == "street":
    jump choosetactic
    
if scout == "roof":
    jump javelintactic

label cavalrytactic:
    
stop music fadeout(1.0)

show sol neu-shout-flip at sright
with dissolve

so "It's an attack! Attack!"

show val ac-annoyed
with dissolve

v "What?"

nar "Not now!"
 
nar "But sure enough-"

play music massacre

show CGTFlip with dissolve:
    yalign 0.93
    linear 0.3 yalign 1.0
    linear 0.1 yalign 0.93
    repeat

play sfx1 marchingalt

pause 0.5

nar "While we were messing around with the ambush and the looting, the enemy maniple rounded the corner."

nar "And they're charging right at us!"
 
v "Formations! Formations!"

nar "I'm still screaming the word as the enemy makes contact."

nvl clear

show red
with dissolve

pause 1.0

show CG7:
    parallel:
        zoom 1.5 xalign 1.0 yalign 0.0
        linear 4.0 xalign 0.0
    parallel:
        choice:
            linear 0.1 yalign 0.03
            linear 0.1 yalign 0.0
        choice:
            linear 0.1 yalign 0.05
            linear 0.1 yalign 0.0
        choice:
            linear 0.1 yalign 0.01
            linear 0.1 yalign 0.0
        repeat
with dissolve

pause 3.5

show CG7:
    parallel:
        xalign 0.0 yalign 1.0
        linear 4.0 xalign 1.0
    parallel:
        choice:
            linear 0.1 yalign 0.97
            linear 0.1 yalign 1.0
        choice:
            linear 0.1 yalign 0.95
            linear 0.1 yalign 1.0
        choice:
            linear 0.1 yalign 0.99
            linear 0.1 yalign 1.0
        repeat
with dissolve

pause 3.2

show CG7:
    zoom 1.0
    choice:
        linear 0.1 yalign 0.03
        linear 0.1 yalign 0.0
    choice:
        linear 0.1 yalign 0.05
        linear 0.1 yalign 0.0
    choice:
        linear 0.1 yalign 0.01
        linear 0.1 yalign 0.0
    repeat
with dissolve

pause 4.0

nar "They smash into our scattered front, their ordered shields knocking men to the ground before swords are driven into the gaps of their armor."

nar "That first hit is so powerful that waves of my men are sent reeling back."

nar "How many did we just lose to them? To this entire trick?"

nvl clear

v "Fight! Fight!"
 
nar "But even as my men get back in line, their shields up, it seems lost."

nar "When near identical forces go up against one another in a level playing field such as this, it almost always comes down to numbers."
 
nar "And they cut my numbers down. If I just wait for this skirmish to end, I will lose."
 
nar "I have to do something. Think, you foolish coward!"

nar "Suddenly, a scout comes running up to me."

nvl clear

show sol neu-shout-flip at sright
with dissolve

so "Tribune, we've found an alley that leads behind the enemy!"

show val cmd-urgent at fleft
with dissolve

v "Praise War. Show me."

hide val
hide sol
with dissolve

jump horse_attack

label choosetactic:
    
stop music fadeout(0.5)

show sol neu-shout-flip at sright
with dissolve

so "It's an attack! Attack!"

show val ac-annoyed at fleft
with dissolve

v "What?"
 
nar "Not now!"
 
nar "But sure enough-"

play music massacre

show CGTFlip:
    yalign 0.93
    linear 0.3 yalign 1.0
    linear 0.1 yalign 0.93
    repeat
with dissolve

pause 0.5

nar "While we were messing around with the ambush and the looting, the enemy maniple rounded the corner."

nar "And they're charging right at us!"

show CGT:
    xalign 0.0
    linear 0.3 xalign 1.0
with dissolve

hide CGTFlip

nar "But my infantry manages to form their line in time, and they meet the clash of the charge head on."
 
nvl clear

show CGTFlip:
    zoom 1.4 xalign 1.0
    parallel:
        linear 0.5 xalign 0.0
    parallel:
        linear 0.3 yalign 0.0
        linear 0.1 yalign 0.07
        repeat
with dissolve

pause 0.2

hide CGT

show CGT:
    zoom 1.4 yalign 0.0
    linear 0.1 yalign 0.1
    linear 0.1 yalign 0.0
    
pause 0.1

show red
with dissolve

pause 1.0

centered "The enemy smashes into our front, shields crashing together like a wave of iron and wood.{p}{p=0}That first charge is the worst, but my men hold firm."

show CG7:
    parallel:
        zoom 1.5 xalign 1.0 yalign 0.0
        linear 4.0 xalign 0.0
    parallel:
        choice:
            linear 0.1 yalign 0.03
            linear 0.1 yalign 0.0
        choice:
            linear 0.1 yalign 0.05
            linear 0.1 yalign 0.0
        choice:
            linear 0.1 yalign 0.01
            linear 0.1 yalign 0.0
        repeat
with dissolve

pause 3.5

show CG7:
    parallel:
        xalign 0.0 yalign 1.0
        linear 4.0 xalign 1.0
    parallel:
        choice:
            linear 0.1 yalign 0.97
            linear 0.1 yalign 1.0
        choice:
            linear 0.1 yalign 0.95
            linear 0.1 yalign 1.0
        choice:
            linear 0.1 yalign 0.99
            linear 0.1 yalign 1.0
        repeat
with dissolve

pause 3.2

show CG7:
    zoom 1.0
    choice:
        linear 0.1 yalign 0.03
        linear 0.1 yalign 0.0
    choice:
        linear 0.1 yalign 0.05
        linear 0.1 yalign 0.0
    choice:
        linear 0.1 yalign 0.01
        linear 0.1 yalign 0.0
    repeat
with dissolve

pause 4.0

nar "And now the swords come out, and the lines of men begin to stab at each other between their shields."
 
v "Fight! Fight!"
 
nar "But even though we took the charge well, this may be lost."

nar "When near identical forces go up against one another in a level playing field such as this, it almost always comes down to numbers."
 
nar "And my numbers have been depleted just a bit. Perhaps just enough to give the enemy the edge."
 
nar "I have to do something. Think, you foolish coward!"

nar "Suddenly, a scout comes running up to me."

nvl clear

show sol neu-shout-flip at sright
with dissolve

so "Tribune, we've spotted an alley that leads behind the enemy!"

hide sol
with dissolve

nar "Praise War. I can lead my horses to attack them from behind!"

nar "And probably get myself killed while I'm at it. I've never been part of a cavalry charge before, let alone led one."

nar "But... what are my other options?"

nar "None, but to wait and hope my side wins out."

nvl clear

menu:
    "What should I do?"
    "A. Lead the horsemen and try to find a flank.":
      jump horse_attack
    "B. Hold and have faith in the men.":
      jump peltasts
      
label javelintactic:
    
stop music fadeout(0.5)

show sol neu-shout-flip at sright
with dissolve

so "It's an attack! Attack!"

show val ac-annoyed at fleft
with dissolve

v "What?"
 
nar "Not now!"
 
nar "But sure enough-"

play music massacre

show CGTFlip:
    yalign 0.93
    linear 0.3 yalign 1.0
    linear 0.1 yalign 0.93
    repeat
with dissolve

pause 0.5

nar "While we were messing around with the ambush and the looting, the enemy maniple rounded the corner."

nar "And they're charging right at us!"

show CGT:
    xalign 0.0
    linear 0.3 xalign 1.0
with dissolve

hide CGTFlip

nar "But my infantry manages to form their line in time, and they meet the clash of the charge head on."
 
show CGTFlip:
    zoom 1.4 xalign 1.0
    parallel:
        linear 0.5 xalign 0.0
    parallel:
        linear 0.3 yalign 0.0
        linear 0.1 yalign 0.07
        repeat
with dissolve

pause 0.2

hide CGT

show CGT:
    zoom 1.4 yalign 0.0
    linear 0.1 yalign 0.1
    linear 0.1 yalign 0.0
    
pause 0.1

show red
with dissolve

pause 1.0

centered "The enemy smashes into our front, shields crashing together like a wave of iron and wood.{p}{p=0}That first charge is the worst, but my men hold firm."

show CG7:
    parallel:
        zoom 1.5 xalign 1.0 yalign 0.0
        linear 4.0 xalign 0.0
    parallel:
        choice:
            linear 0.1 yalign 0.03
            linear 0.1 yalign 0.0
        choice:
            linear 0.1 yalign 0.05
            linear 0.1 yalign 0.0
        choice:
            linear 0.1 yalign 0.01
            linear 0.1 yalign 0.0
        repeat
with dissolve

pause 3.5

show CG7:
    parallel:
        xalign 0.0 yalign 1.0
        linear 4.0 xalign 1.0
    parallel:
        choice:
            linear 0.1 yalign 0.97
            linear 0.1 yalign 1.0
        choice:
            linear 0.1 yalign 0.95
            linear 0.1 yalign 1.0
        choice:
            linear 0.1 yalign 0.99
            linear 0.1 yalign 1.0
        repeat
with dissolve

pause 3.2

show CG7:
    zoom 1.0
    choice:
        linear 0.1 yalign 0.03
        linear 0.1 yalign 0.0
    choice:
        linear 0.1 yalign 0.05
        linear 0.1 yalign 0.0
    choice:
        linear 0.1 yalign 0.01
        linear 0.1 yalign 0.0
    repeat
with dissolve

pause 4.0

nar "And now the swords come out, and the lines of men begin to stab at each other between their shields."
 
v "Fight! Fight!"
 
nar "But even though we took the charge well, this may be lost."

nar "When near identical forces go up against one another in a level playing field such as this, it almost always comes down to numbers."
 
nar "I believe I have the edge there."

nar "And a rooftop covered in javelinmen."

nvl clear

jump peltasts

label horse_attack:

scene StreetsDay
with fade

$ battle = "inside"

"I need a horse."

v "Rider, your horse! And spear!"

"With a little hesitance, the nearest horseman dismounts and hands me the reins."

"I leap onto the mount, and pain shoots through my body. I must have hurt myself quite a bit in that fall."

"Never mind. The horseman offers me his spear as well, and I take it."

v "Horses! With me!"

"I turn and lead them at a canter down the nearest alley. Hopefully the confusion of battle will keep the enemy from noticing that our horses have gone."

"We have to travel in narrow file down these alleys, but so long as we find the flank, I can make this work."

"My heart is pounding so fast I think its humming. The pain inside me adds to the taste of battle. This is like nothing I've ever had to do before."

"I am not made for it."

"If I survive this, I'm deserting this bastard army."

"The alley ends ahead."

so "This is it, Tribune. We'll come out right behind them!"

"Right behind the attacking maniple. The rest of those men in the square will be on our other side."

"If I don't do this quickly, we'll be trapped."

v "Form up!"

"We burst out onto the main street. To my left, I can see hundreds of soldiers crowding the square. If they charged now it would only take them a minute or so to reach us."

"Shit, we have to attack!"

v "Prepare to charge!"

"With fifty horses now amassed and ready, I lead them down the street, back towards the battle."

"Rounding the corner from the other side... we see them up ahead. Red Raskyans killing red Raskyans."

"But the enemy Raskyans have their backs to us."

v "Charge!"

"I break into a gallop, all the bones in me shaking. Every rumbling stride sends shocks of pain up through my being, but I scream it all away."

"Closer."

"Closer!"

"The others are at my side, calling out to the Basileus. Our spears are ready."

"The enemy is turning, noticing. I can see their faces."

"The man who takes my spear in his eye has a red beard."

"He looked frightened."

"The spear tears from my hand painfully, stuck in the man. My horse tramples the foes, pushes them. They stumble, fall beneath our hooves."

"I nearly go flying over my mount's head, but somehow my pained legs hold tight."

"The whole world tastes bloody."

"One of them hacks at my horse's side. Another drives a spear at me, but it goes into my mount. He's crazed, turning, sinking."

"The men and horses are dying around me."

"A blade comes for me. I turn my head to avoid it."

"It rings off my helmet, and my head feels like it's burst."

"I lose my balance, and fall."

scene black
with fade

"It hurts."

"It hurts so much, Clodia."

"My head feels like a bony soup in my helmet."

"Someone steps on my legs, stumbles, keeps going."

"Everyone is killing everyone. They all look the same."

"Some of them are running. Others are chasing. They all look the same."

"They run back to the square. The horses chase them."

"Beyond the square I can see the citadel. The citadel gates."

"They are open."

"Open."

"Everything is confusing."

"What am I doing out here? I don't belong here."

"The enemy is fleeing into the citadel. Abandoning the square. We are chasing them, killing them from behind."

"They all look the same."

"We all look the same."

"I run. I run through them, with them, from my own men. I feel as though my legs will shatter."

"Are the men around me friends? Who are these people? Will they see I'm not one of them?"

"We're all running. Passing through the square. Guts bursting, hearts rending, legs gnashing. Flashes of armor and scarlet. The noon sun heats everyone to a boiling pitch."

"We're flooding through the gates. Hands gesture us in, pulling, urging."

"I'm thrown to the floor."

stop music fadeout(1.0)

scene HallDay
with fade

"A door shuts behind me, heavy. The world goes black, the sunlight taken. The air is cold and musty and all so painful."

"Men are collapsed around me, panting, crying, cursing the Ghosts, checking their wounds, staring at nothing."

"I made it, Clodia. I made it inside."

so "What the fuck? Who are you?"

"The man looking down at me seems more confused than I feel."

v "I... ah..."

"He points at my chest. At the Four Spears of the Tribune engraved into my expensive armor."

so2 "You ain't Tribune Varus. You ain't any of our tribunes!"

so2 "You're one of Sergius's dogs!"

if plan == "escape" and siege =="changed":
    jump she_back
    
if plan == "clodia_poison" and siege == "changed":
    jump she_back

if portia == "alive" and plan == "clodia_poison" and siege == "normal":
    jump portia_plan
    
if portia == "alive" and plan =="escape" and siege == "normal":
    jump portia_plan
    
if portia == "dead" and plan == "escape" and siege == "normal":
    jump sejanus_visit
    
if portia == "dead" and plan == "clodia_poison" and siege == "normal":
    jump sejanus_visit
    
if plan == "nasrin_poison": 
    jump aid_in_disguise

label peltasts:
    
$ battle = "outside"

nar "The shields continue rattling against each other with hate. The air has grown hot with blood."

nar "I see one man slip, go down, and have a javelin hurled into his face. Then he disappears between the others."

nar "A javelin passes over my head, and hits one of my horsemen in the neck. His horse takes off, throwing him to the ground."

nar "His head keeps shaking as the blood runs out of him."

nar "This fight might take a good deal of time and pain, if not for my scouts. They hurl all their missiles from on high."

nvl clear

show CGX with dissolve:
    zoom 2.0 xalign 0.1 yalign 1.0
    linear 0.25 yalign 0.5
    xalign 0.8 yalign 0.5
    linear 0.25 yalign 0.0
    xalign 0.5 yalign 0.9
    linear 0.25 yalign 0.4
    xalign 0.8 yalign 0.5
    linear 0.25 yalign 0.0
    xalign 0.3 yalign 0.6
    linear 0.25 yalign 0.1
    xalign 0.6 yalign 0.7
    linear 0.25 yalign 0.2
    xalign 0.2 yalign 1.0
    linear 0.25 yalign 0.5
    xalign 0.4 yalign 0.5
    linear 0.25 yalign 0.0
    repeat

nar "Some javelins glance off shields, some miss. Some tear into the flesh and bend, the soft lead hooking itself inside them and tearing their insides apart with each movement."

nar "A storm rains upon them, mists of blood fill the air, crimson armor glistens wet. There are guts in the street and brains on their helmets."

nar "Men are running, screaming. I see one crawling away with three javelins in his back. I can smell shit and piss mixing with the blood."

nar "They flee. They flee, back to the square."

nvl clear

show StreetsDay
with dissolve

hide CGX

pause 0.5

nar "My infantry is exhausted, stunned. Some of our men lay slaughtered from friendly missiles, no way to tell them from the enemy. They collapse, dead, resting."

nar "And the enemy is fleeing. NOW is the time for horses."

show val cmd-urgent at center
with dissolve

v "Charge!!!"

nar "Run."

nar "Run, you dead men."

nar "All of you who lay between her and I, die."

nar "Run back into your fortress. Run you sons of dogs."

nar "If you hurt her, then I will flood the Praetor's halls with your blood."

nar "I will make offerings to the Ghosts with your bones and your livers like the victims you are."

nvl clear

nar "Our horses pound through the square, driving the pained men off. They cry and scream."

nar "The door to the citadel is open to them. Men hold it open, entreating."

nar "Then they see us coming."

nar "The doors shut in seconds. Men beat upon it bloody, begging. Their prayers are said."

show red
with dissolve

pause 0.5

centered "My horses reach them and paint the gate with their blood."

pause 0.5

nvl clear

show StreetsDay
with dissolve

nar "The air smells so awful. I am soaked with hot, putrid life. This all has the air of a strange dream."

nar "The last of them are speared and cleaved against the door, then their bodies discarded, piled out of the way."

nar "Did we run any of our own men down?"

nar "Who cares. I am alive."

nar "I want to scream, and I am alive."

nvl clear

stop music fadeout(1.0)

show val cmd-ordering
with dissolve

v "Get the rest of the men up here! Battering rams! Prepare to besiege the citadel!"

show val rel-annoyed
with dissolve

"I only seem to have one wide-eyed, tear-streaked messenger boy with me. The Grave knows what happened to the rest."

show val cmd-urgent
with dissolve

v "Find Sergius! Tell him we've reached the citadel, and to get his arrogant ass over here!"

nar "The boy rides off with a vacant nod. He's apt to report that verbatim in his sorry state of mind. Oh well."

show val rel-annoyed-flip
with dissolve

v "Clear the bodies. Make way for the ram. And then rest."

nvl clear

if plan == "escape" and siege =="changed":
    jump she_back
    
if plan == "clodia_poison" and siege == "changed":
    jump she_back

if portia == "alive" and plan == "clodia_poison" and siege == "normal":
    jump portia_plan
    
if portia == "alive" and plan =="escape" and siege == "normal":
    jump portia_plan
    
if portia == "dead" and plan == "escape" and siege == "normal":
    jump sejanus_visit
    
if portia == "dead" and plan == "clodia_poison" and siege == "normal":
    jump sejanus_visit
    
if plan == "nasrin_poison": 
    jump aid_in_disguise
    
label she_back:
    
scene ClodiaRoomDay
with fade

pause 1.0

nar "They've made it to the citadel."

nar "I think. It's somewhat hard to tell. They all look the same, little red specks."

nar "But the large masses of them are converging around the square, pushing past that towards where the main entrance is. That's Sergius."
 
nar "Valerius too, somewhere. If he hasn't already been-"
                                     
nar "No, no, don't think about that. Don't think about that."

nvl clear

play sound knockdoor

pause 0.3

show clo reg-declaring-flip at center
with dissolve

c "Ugh. What do you want now?"

c "Oh!"

n "Man, they don't have anywhere better to stick you than here, huh?"

c "How did you escape?"

n "Should have sent a eunuch to guard my cell."

c "...and the guards outside my door?"

n "Should have sent a eunuch to guard your room."

c "Did you seriously just-"

n "I doubt those morons will stay relaxed forever. We need to get moving."

c "Wait, how? There's no way we'll be able to get out the same way twice."

n "I know another way. Hurry."

"She grabs my hand and begins to drag me towards the door."

"I'm more than happy to go, but even if they've been fucked silly, how will the guards-"

scene HallDay
with fade

"-they're gone."

"Oh. Naturally she would have taken them elsewhere to seduce. Probably passed out like lovers somewhere nearby."

c "Hang on, I should disguise myself first."

n "It'll be fine, all we need right now is speed. Come on!"

"Where is she leading me? What other exit?"
 
"Not right into the waiting arms of Sergius's army? At the best, we'll only be raped without being killed."
      
"But I have to risk something. I have to trust she knows what she's doing."

if battle == "inside":
    jump brought_sejanus

if battle == "outside":
    jump messenger_arrives

label portia_plan:

scene ClodiaRoomDay
with fade

nar "They've made it to the citadel."

nar "It's somewhat hard to tell. They all look the same, little red specks."

nar "But the large masses of them are converging around the square, pushing past that towards where the main entrance is. That's Sergius."

nar "Valerius too, somewhere. If he hasn't already been-"
 
nar "No, no, don't think about that. Don't think about that."

show clo reg-stoic-flip at mright
with dissolve

pause 0.5

nar "Suddenly, I hear voices from the other side of the door."

nvl clear

g "You again?"

p "Yes, me again. You think a lady needs her attendant only once a day?"

g "What, you gonna give her a bath?"

g2 "Maybe we should insist on watching."

p "Praetrix Clodia is soon to be in a position to punish you. Say and do what you want, the consequences will be your business, not mine."

g "Ugh. Whatever, bitch. Get in there."

play sound dooropen

show por con-sigh at mleft
with easeinleft

play sound doorclose

p "Are gallant soldiers only figments of old histories?"

show clo rel-neutral-flip
with dissolve

c "It seems that way."

show por plan-explain
with dissolve

show por at center
with ease

"She embraces me, whispering in my ear."

p "We're going to change clothes."

show clo reg-stoic-flip
with dissolve

c "Uh... okay."

"She presses something cold and hard into my hand."

show por con-sigh at mleft
with ease

p "That's a key for a jail cell. Got it from the old jailer, thanks to your suggestion."

show por neu-neu
with dissolve

p "It'll open cell 49, and you can lock yourself in there and no one will be able to get to you until you wish to free yourself."

p "You'll be safe until the battle is over and everyone's blood has calmed. You'll have a chance."

show clo rel-angered-flip
with dissolve

c "Why just me? We can both fit in a cell."

show por plan-explain
with dissolve

p "I can only think of how to get one of us down there. I'll dress as you and you dress in something plain."

p "You'll be able to pass for a servant at a distance. Then when they open the door for me to leave, I'll dash off before they get a good look at me."

show clo reg-stoic-flip
with dissolve

c "You're crazy. That won't work."

show por neu-neu
with dissolve

p "Oh won't it? Dressed the same and with my hair down, they won't be able to tell the difference from behind."

p "Then you can slip out the open door and run the other way for the dungeons."

show clo rel-angered-flip
with dissolve

c "And what will happen when they catch you?"

show por con-sigh
with dissolve

p "It doesn't matter, does it?"

show clo con-angered-flip
with dissolve

c "Of course it matters! What if they hurt you?"

show por neu-neu
with dissolve

p "If you escape I don't care what they do to me."

show clo reg-stoic-flip
with dissolve

c "This isn't smart, Portia."

show por plan-insist
with dissolve

p "It's the smartest thing we can do. It's either this, or that army gets in here and hurts both of us."

show clo reg-emotional-flip
with dissolve

"I want to cry. She doesn't deserve to sacrifice so much."

show por neu-neu
with dissolve

p "Do not argue with me, Clodia. Please. This is my duty."

p "Now let's get changed."

show black
with dissolve

pause 0.5

centered "She's already pulling away parts of my dress. This is the plan we're going to have to try, isn't it?"

centered "Thank you, Portia. I'll make this work."

pause 0.5

scene HallDay
with fade

pause 0.5

show sol neu-neu-flip at sright
with dissolve

g "Ghosts, just kill me, I'm getting so sick of standing here."

play sound dooropen

show HallDay:
    linear 1.0 zoom 2.0

show sol neu-neu-flip:
    linear 1.0 zoom 2.0 xalign 0.0 yalign 1.0

pause 1.0

show clo snk-worry at left:
    zoom 2.0
with easeinleft

pause 1.0

g "Uh..."

hide clo
with easeoutright

show sol neu-shout:
    xalign 0.0 yalign 1.0
with dissolve

show HallDay:
    linear 1.0 zoom 1.0

show sol neu-shout:
    linear 1.0 zoom 1.0

g "Shit!"

hide sol
with easeoutright

pause 1.0

show clo2 reg-stoic at left
with easeinleft

"Portia won't be able to run very fast in that dress."

"But both the guards take after her, so at least it seems to have worked."

show clo2 reg-stoic-flip
with dissolve

hide clo2
with easeoutleft

show black
with dissolve

nar "I take off in the opposite direction. But perhaps I shouldn't go so fast. That's suspicious."

nar "Portia... have they got you already? Are they already rushing back to confirm their mistake?"

nar "Just move, don't worry about it. Just move and don't look at anyone."

nar "Now... now I need to find that cell. Cell 49. Down in the dungeons beneath the citadel."

nvl clear

if battle == "inside":
    jump brought_sejanus

if battle == "outside":
    jump messenger_arrives

label sejanus_visit:
    
scene ClodiaRoomDay
with fade

pause 1.0

nar "They've made it to the citadel."

nar "It's somewhat hard to tell. They all look the same, little red specks."

nar "But the large masses of them are converging around the square, pushing past that towards where the main entrance is. That's Sergius."

nar "Valerius too, somewhere. If he hasn't already been-"

nar "No, no, don't think about that. Don't think about that."

nvl clear

play sound knockdoor

show clo reg-stoic-flip at right
with dissolve

"Great. And who is that going to be?"

play sound dooropen

show sej neutral-smile at left
with easeinleft

sj "Hello there."

show clo rel-angered-flip
with dissolve

c "Oh by the Ghosts, what are you doing here? You can't at least wait until our wedding to torture me with your presence?"

show sej rel-bored
show clo reg-emotional-flip
with dissolve

sj "I'm afraid not."

show sej diplo-per
with dissolve

sj "Hmm, these roses you keep are lovely."

c "Are you here to fuck me, then? Running out of time?"

show sej rel-laugh
with dissolve

sj "Ha. No. I have women who can please me far better than you. Were I about to die, I'd rather be in their arms."

show clo reg-stoic-flip
with dissolve

c "Then what possible business do we have right now?"

show sej neutral-smile
with dissolve

sj "Getting to know one another. We are to be husband and wife, and I'll have to suffer your presence too."

show clo reg-stoic
with dissolve

c "Let's agree to divorce then, and save time."

show sej rel-bored
with dissolve

sj "Afraid I can't. Need to be the Praetor and all. Not my fault you're the key to that."

show clo con-angered-flip
with dissolve

c "Yes it is."

show sej rel-glance
with dissolve

sj "Ah, I see what you mean. Very true."

show sej neutral-serious
show clo reg-stoic-flip
with dissolve

sj "Yes, I picked you. So let me clarify."

show sej neutral-normal
with dissolve

sj "It's not my fault I have to marry some girl I don't know or care for to achieve my dreams."

sj "I wish it were less cruel. But things are how they are."

show clo rel-angered-flip
with dissolve

c "What dreams does a man like you even have?"

show sej sin-grin
show clo reg-stoic-flip
with dissolve

sj "Oh, a handul of them. Vengeance against Sergius being one. I want him to know before he dies that he is the cause of all this."

sj "In fact, it's him you should hate."

show clo rel-angered
with dissolve

c "I do hate him. I hate all of you."

show sej diplo-per
show clo reg-stoic
with dissolve

sj "Yet his responsibilty is greatest. Because it was the way he took Dagara last year that sealed your fate."

show sej neutral-serious
with dissolve

sj "Both of our legions converged on your city, but Sergius wanted all the glory of the conquest for himself."

show sej rel-bored
with dissolve

sj "So he sent a messenger to me, warning that his spies had discovered enemy reinforcements approaching from the north."

sj "We were to meet up near Thomun to crush this last army together before returning to Dagara to end things."

show sej neutral-angry
with dissolve

sj "Well, there were no reinforcements near Thomun, it was all a lie."

show sej diplo-threat
with dissolve

sj "And by the time my legion realized this and returned to Dagara, it was already over."

sj "Sergius was the hero of the Dagaran Wars. And I, Sejanus the Coward, had dallied pointlessly north with my legion, as though afraid of the battle..."

show sej sin-snarl
with dissolve

sj "Ghosts take him. I will destroy him for that."

show clo reg-stoic-flip
with dissolve

c "You did all this just to lure him here for revenge. It's true then, that generals are the most petty of men."

show sej rel-glance
with dissolve

sj "Ah, perhaps. But I have other reasons for doing this. Other dreams."

show sej rel-laugh
with dissolve

sj "That's where marrying you comes in. Our marriage will save my life."

show clo rel-angered-flip
with dissolve

c "Save your life? You're dead no matter what. We'll be husband and wife for barely a morning before you're tossed into the Grave."

show clo reg-stoic
with dissolve

c "And Lust knows what will happen to me."

show sej diplo-per
with dissolve

sj "Marriage will save both of our lives. You would really rather die than live as my wife?"

show clo con-angered-flip
with dissolve

c "Yes!"

show sej neutral-serious
with dissolve

sj "Hmm... well, I'd rather we both live. So, sorry you won't get your wish."

show sej neutral-smile
with dissolve

sj "You will live for a frustratingly long time, if I have anything to say about it."

show clo rel-angered-flip
with dissolve

c "You're delusional. They're at the gates. They'll break in before sunrise, and by noon they'll have fought their way up to us."

show clo reg-emotional
with dissolve

c "It's over, fool."

show sej rel-bored
with dissolve

sj "You're so gloomy. I will take genuine pleasure in proving you wrong and saving your life."

"What the hell could he possibly mean? Surely he's just overconfident."

show clo reg-stoic
with dissolve

"And yet..."

show clo rel-angered-flip
with dissolve

c "What are you talking about?"

show sej neutral-smile
with dissolve

sj "Mmm. You'll see."

show clo con-angered-flip
with dissolve

c "Tell me."

"Is it something that could harm Valerius?"

show clo at mright
with ease

c "Tell me!"

show sej neutral-serious
with dissolve

sj "It is already done. All we need to do is wait."

sj "The men below will do their duty and protect us as long as they must. But soon, it will all be over."

show clo reg-emotional-flip
with dissolve

"Stop it. Don't buy into it. He's just trying to confuse you. He's confused himself."

show sej rel-bored-flip
with dissolve

sj "Anyway, I was just here to let you know you have nothing to fear from those men outside. Have a good evening, I guess."

hide sej
with easeoutleft

pause 0.5

nar "With a trifling wave, he departs. Thankfully."

nar "He at least believes he has some kind of brilliant plan. I must not be complacent."

nvl clear

show black
with dissolve

centered "I have to find some way out of here."

if battle == "inside":
    jump brought_sejanus

if battle == "outside":
    jump messenger_arrives

label aid_in_disguise:
                      
scene TempleDay
with fade

pause 0.5

show clo2 reg-stoic at center
with dissolve

pause 0.5

nar "Damn the Ghosts, this is abominably boring."

nar "These priestesses need to all get fucked, find out what they're missing."

nar "I mean, really. Get some Durjan studs in here and remind them what they gave up. Half of them will abandon the service of the Ghosts on the spot, I'm sure."

nar "Hell, that one is gorgeous looking, what is she doing-"

nvl clear

show nas2 rel-defaul-flip at right
with dissolve

show clo2 snk-relief 
with dissolve

pause 0.5

"It's Nasrin."

"She looks at me and holds her finger to her lips."

show nas2 rel-grin-flip
with dissolve

n "Close your eyes and hold your breath."

pre "Sister... your robes are all wrong. Who are-"

show nas2 act-attacking-flip
with dissolve

show sunbright
with dissolve

pre "Ah!"

pause 0.5

nar "I manage to shut my eyes just in time. There's still an awful flash through my eyelids, and then a loud hiss."

nar "The priestesses cough violently, and even with my breath held I can smell the air change."

nar "A hand takes mine and pulls me along."

nar "Nothing to do but follow. The plain dress I'm wearing is easy to run in."

nvl clear

show black
with dissolve

n "You can open your eyes now."

scene HallDay
with fade

show nas2 act-stealthy-flip at center
show clo2 snk-serious-flip at right
with easeinright

pause 0.5

nar "Dark smoke floods the hall behind us. The air smells rotten and stale, clinging to my clothes."

nar "Nasrin keeps leading me on, not stopping. Is she taking me to Valerius?"

nar "Please, please let me see him. Please let this work."

nvl clear

if battle == "inside":
    jump brought_sejanus

if battle == "outside":
    jump messenger_arrives

label brought_sejanus:
    
scene HighHall
with fade

pause 0.5

nar "The last time I was in this room it was full of drunk soldiers sharing perverse tales and musicians trying to drown out the inanity with their little lyres and flutes."

nar "Now the room is silent, and empty."

nar "Well, there's a few soldiers standing around as if on guard. But even the Praetor's chair is empty. I thought-"

nar "Someone's coming up behind me. I'd turn and look if my arms weren't bound and I wasn't held in place by two soldiers."

nar "But soon enough, the man himself walks into my view."

nvl clear

show sej rel-bored at left
with dissolve

sj "Let's hurry this up, I have guests to attend to."

show val ac-annoyed-flip at fright
with dissolve

"{i}So this is Sejanus.{/i}"

sj "You're one of Sergius's tribunes. Is that as insulting to your pride as it was mine?"

v "Like plague in the bowels."

show sej rel-laugh
with dissolve

sj "Ha, exactly. We must be strange men, you and I. Most of his people adore him."

show val rel-normal-flip
with dissolve

v "As long as plebs stay in the ranks and nobles stay on the horses. I overstepped my blood, turns out."

show sej neutral-smile
with dissolve

sj "A brown blooded tribune? Ha, you'd be the first in about fifty years."

show val ac-fact-flip
with dissolve

v "What do you want with me?"

show sej neutral-normal
with dissolve

sj "That's the question I have for you. I'm told you hid yourself in a withdrawal of my men to the citadel. Alone. Right after being victorious in battle." 

show sej neutral-smile
with dissolve

sj "That is very curious. And you don't like Sergius... I'd almost guess you were trying to defect."

"{i}Seriously? Oh, I see. I suppose that is the only thing he could assume."

"{i}Perfect, then."

show val rel-normal-flip
with dissolve

v "I saw the opportunity when it presented itself."

show sej neutral-serious
with dissolve

sj "Or you could be here to kill me. There are much easier ways to do that, though."

show val ac-annoyed-flip
with dissolve

v "As you said, I'm here because I can't stand Sergius. He mistreats and mistrusts me purely for my blood, and there's been one too many outrages at this point."

show sej rel-bored
with dissolve

sj "Even if that is true, curious for you to defect at this late stage, when the Ghosts of Fortune appear to have abandoned me."

show val rel-grin-flip
with dissolve

v "Have they? Perhaps I've been sent by Fortune."

show sej neutral-smile
with dissolve

sj "Ah, I see. You possess some kind of knowledge that will turn this entire situation in my favor."

"{i}Not at all, by the Ghosts. I need to think of a really good lie fast though."

show val rel-normal-flip
with dissolve

v "That's what I'm saying."

show sej neutral-serious
with dissolve

sj "What is it then?"

show val ac-fact-flip
with dissolve

v "I'm not going to tell you unless I get a guarantee you won't just kill me."

show sej rel-bored
with dissolve

sj "If what you say has any merit, then I won't kill you. But you must tell me, quickly. I don't really care about what happens to you one way or the other."

"{i}Shit, not many ways to delay the point further."

show val rel-normal-flip
with dissolve

v "There is a messenger, set to arrive today, perhaps any moment. He was sent to retrieve a blessing from the Basileus for this siege."

v "Now, I have men paid off to ensure that I am the first person in the army of any authority to receive this message."

v "I don't know exactly whether it will carry a war blessing or not, but regardless it can be used to your advantage."

show sej neutral-smile
with dissolve

"{i}A wide smile has broken out onto his face. That must be a good thing, yes?"

v "If it carries a blessing, we destroy the letter, or alter it such that it indicates there is no blessing."

v "Or if there actually is no blessing, we spread the word through the ranks as fast as possible. Either we, we can demoralize-"

show sej rel-bored
with dissolve

sj "Please stop. I've heard enough."

show val ac-fact-flip
with dissolve

v "Well? We need to make sure that letter gets-"

sj "No need."

show val ac-annoyed-flip
with dissolve

"{i}What? Are you crazy?"

v "I'm your only chance to live."

show sej neutral-smile
with dissolve

sj "As it turns out, that is not true. But I believe you really do wish to help."

show sej rel-bored
with dissolve

sj "Sadly, you would make a poor hostage if Sergius really disparages you. So I'm at a loss."

show val cmd-urgent-flip
with dissolve

v "I'm telling you, there's no other way you can even hope to stop that army out there! There's two legions of them and you don't even have a thousand men anymore!"

show sej neutral-smile
with dissolve

sj "Relax yourself, all will be well. The siege outside this citadel will break soon, either tonight or tomorrow morning."

show sej rel-laugh
with dissolve

sj "As it turns out, I've been waiting for your rider too."

show val ac-annoyed-flip
with dissolve

"{i}What?"

"{i}What does that mean? What's going on?"

if plan == "escape" and siege =="changed":
    jump brought_sejanus_a
    
if plan == "clodia_poison" and siege == "changed":
    jump brought_sejanus_a

if portia == "alive" and plan == "clodia_poison" and siege == "normal":
    jump brought_sejanus_b
    
if portia == "alive" and plan =="escape" and siege == "normal":
    jump brought_sejanus_b
    
if portia == "dead" and plan == "escape" and siege == "normal":
    jump brought_sejanus_b
    
if portia == "dead" and plan == "clodia_poison" and siege == "normal":
    jump brought_sejanus_b
    
if plan == "nasrin_poison": 
    jump brought_sejanus_b
    
label brought_sejanus_a:

show sej neutral-smile
with dissolve

sj "Also, forgive me for asking, but you wouldn't happen to be named Valerius?"

show val ac-fact-flip
with dissolve

"{i}... how do you know that?"

sj "A brown blooded tribune. Handsome, like she said."

show val ac-annoyed-flip
with dissolve

v "What are you going on about?"

show sej rel-laugh
with dissolve

sj "I'm only marveling at the Ghosts. They have indeed given you to me, but not so you can help."

show sej diplo-per
with dissolve

sj "Tell me, do you miss your Clodia?"

show val ac-fact-flip
with dissolve

"{i}You-"

v "If you hurt her-"

show sej neutral-angry
with dissolve

sj "Oh fuck off before you even start, little boy. You're beaten and in no position for any of that bravado."

sj "All that will happen is that you will never see your Clodia again."

show sej neutral-smile
with dissolve

sj "But she'll see you one last time."

show val rel-serious-flip
with dissolve

"I'm numb. I'm not quite sure what is going on."

show sej diplo-threat
with dissolve

sj "Take him down below and lock him in a cell. Throw away the key, as they say."

"No!"

show sej diplo-per
with dissolve

sj "And in a fortnight or so, I'll take my bride downstairs for a little tour of the prisons. Show her what's left of my favorite prisoner."

sj "You should still be recognizable at that point."

show val cmd-urgent-flip
with dissolve

v "You are damned. You will die in agony you accursed fucking-"

show sej rel-bored-flip
with dissolve

sj "Enough, take him away. I'll enjoy fucking your lover tomorrow."

show sej rel-glance-flip
with dissolve

sj "In fact, I'll have someone deliver you the news once I've completed the consummation. With details."

hide sej
with easeoutleft

v "SEJANUS!"

hide val
with easeoutright

scene black
with fade

centered "But screaming does nothing as I'm torn away. As I'm lead down into my eternal darkness."

centered "Clodia. Clodia. Clodia."

centered "How did he know? HOW DID HE KNOW?"

jump sejanus_heaven
    
label brought_sejanus_b:
    
show sej diplo-threat
with dissolve

sj "All right, I have no more time for this. Lock him up."

show sej neutral-serious-flip
with dissolve

show sej at fleft
with ease

show val cmd-urgent-flip
with dissolve

v "Wait!"

show sej rel-glance-flip
with dissolve

sj "Show a little bravery. We'll let you out when the battle's over. Perhaps you can even join us."

show sej rel-bored-flip
with dissolve

sj "But it wouldn't do to have you running around until my victory is certain. Just in case."

hide sej
with easeoutleft

nar "The men are already pulling me away. Sejanus turns and walks off quickly."

nar "No. No, you can't lock me up! I need to be free! I need to find Clodia!"

nvl clear

scene black
with fade

pause 0.5

centered "Clodia... where are you?"

if portia == "alive" and plan == "clodia_poison" and siege == "normal":
    jump hiding_jail
    
if portia == "alive" and plan =="escape" and siege == "normal":
    jump hiding_jail
    
if portia == "dead" and plan == "escape" and siege == "normal":
    jump clever_clodia
    
if portia == "dead" and plan == "clodia_poison" and siege == "normal":
    jump clever_clodia
    
if plan == "nasrin_poison": 
    jump splitting_up

label messenger_arrives:
    
scene black
with fade

nar "The men grunt, heave, pull, and release."

nar "The battering ram smashes against the door again and again. The sound is underwhelming. No splintering, just solid wood."

nar "This is going to take all night."

nar "Surely they've thrown everything they can against the other side of that door. And the lowest wall is still beyond the reach of our ladders."

nar "We're just going to have to wait until the door has been bashed in."

nvl clear

nar "Our scouts did find some kind of hidden exit, a small passageway into the sewers. But it had been destroyed by the time they found it. Deliberately collapsed."

nar "I can't help but wonder if that was one of Nasrin's ways inside."

nar "Maybe her only way. She may be trapped inside now."

nar "She may be dead..."

nar "Ghosts, please, let things be going well on her end."

nvl clear

show StreetsDay
with dissolve

show val rel-normal at fleft
with dissolve

show sol neu-neu-flip at sright
with easeinright

"{i}A soldier comes running up to me. He salutes."

so "Tribune Aureus!"

"{i}I recognize this soldier. I've been paying him little bags of gold every day. To let me know of one thing."

show val rel-serious
with dissolve

v "The messenger?"

so "I've managed to detain him outside the wall."

v "Excellent. Take me immediately."

scene black
with fade

nar "I mount the new horse that's been provided for me and we set off for the wall."

nar "At this stage there's no stopping Sergius. Even a religiously demoralized army will take the citadel."

nar "But men who fear the wrath of the Ghosts will perhaps think twice about raping and looting."

nar "Or perhaps not."

nar "But every bit of religion you can get on your side helps."

nvl clear

scene WallDestroyed
with fade

pause 0.5

nar "Most of the army is inside the city walls. Only a small unit of horsemen remain out here, in case Sejanus happens to flee out some secret exit and needs to be chased down."

nar "I can identify the messenger from his dark brown clothing that blends him in with the dead earth."

nar "He salutes as we ride near, his eyes upon the symbol of a tribune on my armor."

nvl clear

show val rel-normal at fleft
with dissolve

v "You have the Basileus's reply?"

m "Yes Tribune."

"{i}He holds out his hand and offers a very tiny scroll of papyrus, gold handled. Not terribly inconspicuous, I've always thought."

"{i}As I take the scroll, I notice something."

show val ac-fact
with dissolve

v "You are not the rider we sent to the capital. Where is he?"

m "I wouldn't know anything about that, Tribune."

"{i}Keeping one eye on him, I break the seal on the scroll."

m "That letter is for the eyes of General Sergius alone!"

nar "I toss a little bag of gold into his lap and go back to opening the scroll, no protests this time. I'll need to prepare more bribes, I'm running low."

nar "The letter is short, by Basileus standards anyway. Some of these things go on for yards and yards."

nar "Luckily His Holiness the Best and Greatest didn't seem to feel a Basileus's usual loquaciousness."

nvl clear

show black
with dissolve

centered "\"His Divine Majesty and Beloved of the Ghosts, Son of the Divine Basileus XVIII Avalarus, Best and Greatest Man in the Holy Republican Kingdom of Raskya, Divine Basileus XIX Avalaran, has received the request of the legions of Gaius Argus Sergius for a Divine War Blessing in their campaign against the rebellious legion of Lucius Rufius Sejanus. {p} The request is denied, by the Will of the Ghosts and the Judgment of the Basileus.\"{p=1.0}"

hide black
with dissolve

nar "Well, that's kind of surprising. I was expecting to have to destroy this letter and spread rumors to the contrary."

nar "Why would the Basileus reject the request?"

nar "It goes on a little more."

nvl clear

show black
with dissolve

centered "\"Furthermore, it is the Will of the Ghosts and the Judgment of the Basileus that the legion of L. Sejanus is to become the Legion of Dagara, and that Sejanus be enthroned henceforth as Praetor, by Lawful Marriage to a maiden of Noble Blood of the Praetors of Dagara, of the city of Dagara and all surrounding lands, to enforce the Will of His Holiness Best and Greatest whenever commanded in that province.\""

hide black
with dissolve

nar "... That changes a lot. I'm not quite sure what I'm reading."

nar "This doesn't make any sense."

nar "But as I read the final part, I realize the why is fairly irrelevant."

nvl clear

show black
with dissolve

centered "\"Furthermore, as the legions of G. Sergius are currently engaged in the siege of the city and people of Dagara, and so endangering the Mandate of the Divine Avalaran and the safety of His Praetor L. Sejanus, it is the Will of the Ghosts and the Judgment of the Basileus that these legions are henceforth declared outlaws, and the traitor Sergius henceforth sentenced to death. {p}The Divine and Greatest Immortal Legion of the Basileus, thus, marches to subdue the traitor Sergius and relieve His ally the Lawful Praetor Sejanus. All from the legions of these damned are now obligated to either flee Sergius or capture or kill him, or else be cast with him into the Grave Below. {p}So is the Will of His Holiness Best and Greatest, Basileus XIX Avalaran.\"{p=1.0}"

hide black
with dissolve

nar "Sergius is a dead man."

nar "{i}I{/i} may be a dead man, depending."

nar "My hand is shaking. Damn it."

nar "The Basileus's Immortal Legion is not like most. It's eight-thousand men strong, all veterans from successful campaigns. The best soldiers in all Dagara. Meant to guard the capital except in extreme emergencies."

nar "Why the fuck would the Basileus consider this an extreme emergency? What the fuck is going on?"

nvl clear

nar "It's coming for us. It's over. It's just over."

nar "And the Basileus wants Sejanus to marry Clodia."

nar "What... what do I even begin to do? How did this happen?"

nar "I stare with what must look like an idiot's gaze at the messenger."

nvl clear

show val rel-serious
with dissolve

v "How... when..."

m "Tribune?"

v "You know what this letter says?"

m "I, uh, I have some idea."

show val ac-fact
with dissolve

v "How is this possible? Peladonia's Armada will invade the capital the second the Immortals leave it! This is the end of Raskya!"

m "Uh... the Armada has returned to Peladon, tribune."

v "What?"

m "The Basileus treated with the Peladonian admiral. Gave him twenty talents of gold and silver to return home."

"{i}No. No no no."

"{i}Sejanus, you planned this. You and the Basileus together!"

show val rel-serious
with dissolve

v "... how far behind you are they?"

s "What are you talking about, Tribune?"

show val cmd-urgent
with dissolve

v "Silence! I ask again, how far behind you are they?"

m "I... I set out just ahead of them."

"{i}So they'll be here tomorrow. Amazing logistics, those men. The Best and Greatest Legion, for the Best and Greatest Son of a Bitch."

"{i}I... I have to do something. I have to do something with this information."

show val rel-emotional
with dissolve

v "Very well... I'll take this to Sergius."

show val rel-normal-flip
with dissolve

v "War be with... fuck. Have a good day, I guess."

s "Tribune, what's going on?"

scene black
with dissolve

nar "I leave them behind, charging back to the city."

nar "The Basileus is coming with the Immortal Legion. I have to make a decision, now."

nar "Do I tell Sergius and give him time to prepare? He will never win, but perhaps if he is distracted with battle in his rear I can convince him to allow me to lead the siege, and I can do more to prevent raping and looting."

nar "On the other hand, if he is destroyed too quickly, I may never get inside, and Clodia will be lost to me forever."

nar "Fuck. By the order of the Basileus she is already lost to me forever. Fuck, fuck, fuck!"

nvl clear

menu:
    "What do I do?"
    "A. Hide the message from Sergius and allow him to be taken by surprise.":
      jump messenger_a
    "B. Warn Sergius and allow him time to prepare.":
      jump messenger_b

label messenger_a:

$ message = "hidden"

nar "I must not warn him. I need his help getting into the citadel. Then I can worry about how to get us out."

nar "And away. Away from all of this."

nar "But I must hurry."

nvl clear

jump sergius_citadel_siege

label messenger_b:
    
$ message = "shown"

nar "I have little choice. If Sergius is away fighting, I can have more direct command over the siege."

nar "I will not have many men, if he's even willing to spare any, but if I'm directly in charge I can prevent rapes when we break in."

nar "Or try to."

nar "But I have to try."

nvl clear

jump sergius_sallies_forth

label sergius_citadel_siege:

scene StreetsDay
with fade

pause 0.5

nar "They've already turned the town into their camp, the war tent now in the middle of the square."

nar "Citizens are being rounded up, chained, pushed into large iron pens. The next round of slaves for Raskya."

nar "Stupid. These people did nothing to us. They should be Raskyan citizens, not slaves."

nar "But the nobles down south always need more slaves. And the citizens always need more land. So Raskya has to find more."

nvl clear

nar "Well, none of it's going to matter now. The Basileus is coming to put the final peace upon this place."

nar "Now to go lie about it."

nvl clear

scene TentDay
with fade

pause 0.5

show ser neutral-grin-flip at center
with dissolve

pause 0.5

nar "There he is. Drinking with the tribunes, sure of success."

nar "Enjoy it, Sergius. This is likely the last evening of your life."

nvl clear

show ser at fleft
show val rel-normal-flip at fright
with easeinright

v "Legatus."

show ser neutral-grin
with dissolve

s "Huh? Ah, evening Tribune Aureus! I heard you commanded your men well in the battle. You look mostly alive."

v "Aye, mostly."

show ser ac-grin
with dissolve

s "At last, you've paid a little blood for your honors instead of just gold. Now you might actually be a soldier."

"{i}The tribunes all laugh with him. I suppose this is meant to be his good nature."

show val ac-fact-flip
with dissolve

v "I did what I had to do to live."

show ser neutral-grin
with dissolve

s "Yes yes, very well, come have a drink with us! Lead more assaults such as that one and your brown blood may grow a little bluer."

nar "I am never invited to drink with Sergius and the tribunes. Perhaps in other circumstances I would actually find a drop of pride to take here."

nar "But the scroll sits heavy in my cloak with the promise of doom. Come tomorrow, there will be no pride left in the hearts of these men."

nar "No hearts left in their chests."

nar "And so..."

nvl clear

show val rel-grin-flip
with dissolve

v "Ha. Very well."

nar "We drink."

nar "Outside the tent I can hear the rhythmic battering of the ram. Likely that door will be burst open early tomorrow."

nar "Or perhaps late tomorrow. And who knows when the Basileus will arrive."

nvl clear

scene black
with fade

centered "But I have made my choice. Now it is only in the hands of the Ghosts."

if plan == "escape" and siege =="changed":
    jump sejanus_heaven
    
if plan == "clodia_poison" and siege == "changed":
    jump sejanus_heaven

if portia == "alive" and plan == "clodia_poison" and siege == "normal":
    jump hiding_jail
    
if portia == "alive" and plan =="escape" and siege == "normal":
    jump hiding_jail
    
if portia == "dead" and plan == "escape" and siege == "normal":
    jump clever_clodia
    
if portia == "dead" and plan == "clodia_poison" and siege == "normal":
    jump clever_clodia
    
if plan == "nasrin_poison": 
    jump splitting_up
    
label sergius_sallies_forth:
    
scene StreetsDay
with fade

pause 0.5

nar "They've already turned the town into their camp, the war tent now in the middle of the square."

nar "Citizens are being rounded up, chained, pushed into large iron pens. The next round of slaves for Raskya."

nar "Stupid. These people did nothing to us. They should be Raskyan citizens, not slaves."

nar "But the nobles down south always need more slaves. And the citizens always need more land. So Raskya has to find more."

nvl clear

nar "Well, none of it's going to matter now. The Basileus is coming to put the final peace upon this place."

nar "Now to go warn these fools about it."

nvl clear

scene TentDay
with fade

pause 0.5

show ser neutral-grin-flip at center
with dissolve

pause 0.5

nar "There he is. Drinking with the tribunes, sure of success. This is likely the last successful moment of his life."

nar "How strange that it would be me who would end it."

nvl clear

show ser at fleft
show val rel-normal-flip at fright
with easeinright

v "Legatus."

show ser neutral-grin
with dissolve

s "Huh? Ah, evening Tribune Aureus! I heard you commanded your men well in the battle. You look mostly alive."

v "Aye, mostly."

show ser ac-grin
with dissolve

s "At last, you've paid a little blood for your honors instead of just gold. Now you might actually be a soldier."

"{i}The tribunes all laugh with him. I suppose this is meant to be his good nature."

show val ac-fact-flip
with dissolve

v "I did what I had to do to live."

show ser neutral-grin
with dissolve

s "Yes yes, very well, come have a drink with us! Lead more assaults such as that one and your brown blood may grow a little bluer."

nar "I am never invited to drink with Sergius and the tribunes. Perhaps in other circumstances I would actually find a drop of pride to take here."

nar "But the scroll sits heavy in my cloak with the promise of doom. Come tomorrow, there will be no pride left in the hearts of these men."

nar "No hearts left in their chests."

nvl clear

show val rel-serious-flip
with dissolve

v "Legatus, I cannot do that in good conscience. The rider from the Basileus returned."

show ser neutral-serious
with dissolve

s "Ah, that bit of business. Well, what is it?"

show ser ac-insulting
with dissolve

s "Do we have a Blessing, or have we won without such formalities?"

"{i}Reaching into my cloak, I pull out the scroll and offer it to him. He looks almost bothered to have to open and read it."

show ser ac-annoyed
with dissolve

s "All right, what does that Holy Shit have to say?"

show ser neutral-annoyed
with dissolve

"{i}The annoyance on his face depletes rapidly with every second his eyes scan the words."

show ser neutral-serious
with dissolve

"{i}By the end the blush of drink is sapped from him, and his face is a pale husk. His tribunes drink and laugh behind him, oblivious."

show ser neutral-serious-flip
with dissolve

s "This is... wrong."

show val rel-emotional-flip
with dissolve

v "I'm sorry, legatus. I don't understand either."

show ser neutral-annoyed-flip
with dissolve

s "He... he tricked me. How did he possibly do this?"

show ser com1-rage-flip
with dissolve

s "How did he get that son of a bitch pompous whoreboy to agree to this?"

"{i}He's shouting by the end of it, and the tribunes have finally fallen silent."

hide val
with dissolve

tra "What's wrong legatus?"

show ser com1-barking
with dissolve

s "... put away the drinks."

trh "What's happening?"

show ser com1-rage
with dissolve

s "I said put away the fucking drinks! We're all dead, boys. The Basileus has declared us an outlaw army."

"{i}I want to enjoy the look on their princely little faces. But it's very hard, as we're all in the same boat."

tra "That doesn't make any sense. Why?"

show ser neutral-annoyed
with dissolve

s "Aye it makes no fucking sense, but read the scroll yourself!"

show ser com1-barking
with dissolve

s "No sleep, no drink, no more fucking. Now it's time to die!"

show ser com2-rage
with dissolve

s "Gather the men, let them all know they'll be dying as well. We're going to sally outside the walls and meet that little prick and his Holiest of Legions and kill as many of them as we can!"

show ser com1-rage
with dissolve

s "And I'll kill the Basileus myself if I have to! I don't care if I'm cut into a million pieces on the way, I'll die with that little fuck's throat in my hands!"

"{i}The Tribunes don't seem to have fully understood, and keep staring into their drinks."

show ser com2-rage at center
with ease

"{i}Sergius strides forwards and knocks the cup from Antonius's hand."

s "Are you deaf? RALLY THE MEN OUTSIDE THE WALL, IT'S TIME TO DIE!"

"{i}The Tribunes manically rush from the room. Only War knows what they're going to say to their men."

show ser neutral-serious at fleft
show val rel-normal-flip at fright
with easeinright

s "That includes you."

"{i}Oh does it?"

show val rel-serious-flip
with dissolve

v "Legatus, if I may. Allow me a few men to continue the siege."

show ser neutral-annoyed
with dissolve

s "Ha. What is the point?"

show val ac-fact-flip
with dissolve

v "We are all dead. But as you just said, you wish to die with the Basileus's throat in your hands. We should give Sejanus the same honor."

show ser neutral-serious
with dissolve

s "... Damn it."

show val rel-serious-flip
with dissolve

v "Why suffer him to live only because of this? If we can't have Dagara, certainly he should not have it."

show ser neutral-serious-flip
with dissolve

s "Yes. He should burn with all of us."

show val cmd-ordering-flip
with dissolve

v "Let me lead the attack against him. Give me one last chance to do something glorious before I die."

"{i}Words that a man like him will understand."

s "Yes... very well. The siege is yours."

s "We've... we've readied Dagara's Pit of Nightmares. If you get that piece of shit, throw him in."

show ser neutral-annoyed-flip
with dissolve

s "I'll die in glorious battle. The thought that Sejanus will rot in the agonies of that disgrace... that will have to be sweet enough."

show val rel-normal-flip
with dissolve

v "I will get him, Sergius. I promise."

"{i}Or not. I don't care what happens to him so long as I can save Clodia."

show ser neutral-serious-flip
with dissolve

s "Yes. Yes. Yes."

nar "He keeps saying it. He's begun to stare off into space."

nar "The Grand Spoils are disappearing before him. His dream is shattering."

nar "I'll give him some time, he likely wants to say his prayers before the end."

nvl clear

show val rel-emotional
with dissolve

v "I'm sorry, Sergius."

hide val
with easeoutright

"{i}Leaving him like that, I go to find the siege engineers."

show black
with fade

if plan == "escape" and siege =="changed":
    jump sejanus_heaven
    
if plan == "clodia_poison" and siege == "changed":
    jump sejanus_heaven

if portia == "alive" and plan == "clodia_poison" and siege == "normal":
    jump hiding_jail
    
if portia == "alive" and plan =="escape" and siege == "normal":
    jump hiding_jail
    
if portia == "dead" and plan == "escape" and siege == "normal":
    jump clever_clodia
    
if portia == "dead" and plan == "clodia_poison" and siege == "normal":
    jump clever_clodia
    
if plan == "nasrin_poison": 
    jump splitting_up

label sejanus_heaven:

scene Sewer
with fade

"Is this really all I can do?"

"Come back down here, trusting my escape to this girl?"

"Will this really work a second time? Certainly they've blocked off all the exits with their enemy right outside the citadel."

"And even if there's somehow another way, won't they think to put men there who don't like girls at all?"

"Yet I have no choice but to follow Nasrin and see what she has planned."

"But as I follow her, I can't help but turn something over in my head. Something has dogged me since I first understood what she was."

c "So, um, how did you meet Valerius?"

n "He captured me when Sergius sacked Garna."

"A little over a year ago. Shortly before he met me."

c "Ah. So... so you were his slave."

n "Yes. Did I give the impression I was anyone else's?"

c "No. I... I just hoped you were."

"Have they..."

"No, don't think about that. Surely he hasn't. He wouldn't."

"It isn't like the dark beauties of Peladon are desired by men so much that they will start wars just to capture more."

"Oh wait. Yes they are. Ugh."

n "You shouldn't be so jealous."

c "Don't tell me how to feel. Did you-"

n "... Go on. Ask it."

c "Did he fuck you?"

n "Yeah."

"Damn her. I know I shouldn't be surprised. I'm really not. But..."

"Ghosts damn her."

n "Don't hate me, it's a waste of time. He hasn't touched me since the day he met you."

c "He hasn't?"

n "No. He freed me the night after he fucked you for the first time. Had no more use for a bedslave, he said. Ha."

n "Some Raskyan he is. They're supposed to be the most rapacious of men, but he's allowed a woman to control his heart."

"He's been faithful to me! Oh Valerius... thank you."

n "Ah, this is a pointless conversation."

c "What do you mean?"

n "It's not worth talking about."

c "But I feel so much better."

n "Good for you, noblewoman. I'm glad your ass is pampered and blue blooded enough for him to stay interested in it."

c "Excuse me?"

n "You heard me. A brown-blooded slave like me really had no shot next to you."

c "You... you lo-"

n "Just shut up. Once I'm done with you here, I'm leaving. Leaving this whole awful country."

n "I'll never have to think about you or him ever again."

"The acid in her voice chills me. I think we'll both be relieved when we part forever."

"I suppose I could pity her, but there's more important things to do right now that waste my thoughts on this."

"All the endless twisting and turning down these dark corridors finally ends in a stone wall. Did we take a wrong turn?"

"But Nasrin raps twice at the stone. The wall suddenly moves, pulling away into darkness. A secret passage."

"But someone opened it from the other side. She has more help?"

n "Inside, quickly."

c "Who is in there?"

n "No time. Go!"

"Her little shout startles me and, well, what choice do I have?"

scene black
with fade

"I pass into the darkness, trying to shake off the bad feeling."

c "What is this place? There's... I can hear breathing."

"And it smells very... strange in here. Kind of sweaty. Gross."

"No!"

"But when I whirl around, the girl is there behind me. Her vague outline is the only thing I can see in the darkness."

c "Oh, by the Ghosts. You frightened me."

"Relax, Clodia. It's fine. Everything will make sense soon, I'm sure."

sj "Hello, Clodia."

c "Huh?"

"Torchlight floods the room, coloring the darkness into shapes."

"Writhing, fleshy shapes wrapped in gold and chains and jewels, skin piled upon skin."

"This is...!"

"By Lust. By Lust what the hell place have I been brought to?"

"My mind has gone dumb. There is something terrifying about the sight. Unreal."

"They're looking at me. I feel unclean."

"The air smells so...ugh. It's sweating bodies piled on top of one other."

n "Now this man behaves like a Raskyan."

"Nasrin."

"She walks past me, towards the mass of flesh, unfazed. What is wrong with her?"

c "Why did you bring me here? Where is Valerius?"

sj "He is in a lot of trouble."

"The voice. A male voice, the same one that spoke a moment ago. Of course."

"Stupid, stupid, stupid. I'm so stupid."

sj "But enough about your doomed lover. What do you think of my garden? Beautiful flowers, aren't they?"

"He... he knows about Valerius."

c "You-! What is the meaning of this?"

"I turn around, but there are men guarding the door. There are guards everywhere, standing at attention. As though there isn't some sort of ritual to Lust to going on in their midst."

sj "I figured as you are my future wife, it's only fair you understand early that I will be taking my pleasure with others. For example..."

"Nasrin steps between the bodies with her usual balance and grace."

"I can only stare as she sits upon Sejanus's lap and looks blankly back at me. He begins to stroke her hair."

sj "This lovely little thing had been infiltrating my slaves up top for a while. Once she was caught helping you escape, I realized what was going on."

sj "Tribune Valerius Aureus must want to fuck you again very badly, Clodia."

"Nasrin seems to have no reaction to any of this. She looks like a doll in his hands."

c "You traitorous bitch!"

sj "Traitorous? She's a broken-hearted whore. All I had to do was offer her more money and... affection than Valerius ever could and she told me everything."

sj "Like how Valerius is the one who took your virginity from me. That was frustrating."

sj "Then to learn that you've been trying to escape so you can go fuck him more. Also very frustrating."

sj "You know, it's not as if I have a terrible affection for you, but you ARE my betrothed, and my pride cannot stand being cuckolded like this."

sj "You're the only girl in this room I expect to not be a little whore."

"His collection of 'flowers' won't stop giggling at every little thing he says. I hate them as much as him."

c "So. This is to be my fate then?"

sj "Of course not. You're the Praetrix! Even I couldn't get away with treating you like a slave."

sj "No, you still need to make public appearances. You still need to have my heir. You need to be my doting, perfectly normal, noble wife."

c "I will kill myself the first chance I get."

sj "Don't go making such threats. Otherwise I'll have no choice but to chain you up from now until you birth a son. Then you can kill yourself or do whatever you want, I don't care."

sj "I will not lose my greatest and only chance at glory, wealth and power just because you don't like me very much."

"At these words, at least, I can find method to insult him."

c "Haha. Glory, wealth and power? You're so stupid."

c "All this just to be the Praetor of the poorest, smallest province of Raskya for a morning? You'll rule a barbarian infested borderland for a few hours and then die in agony."

c "What a meager reward you've sold your life for."

"And yet, the bastard just keeps grinning."

sj "This time next year, Dagara will be the second most important province after the capital. I don't feel the need to explain it to your dumb little mind."

sj "Just know that you'll be very rich and want for nothing if you behave."

sj "Anyway, have a seat. This is the safest place to wait out whatever chaos is going on above."

if battle == "inside":
    jump sejanus_heaven_a

if battle == "outside":
    jump sejanus_heaven_b
    
label sejanus_heaven_a:

sj "And don't worry. You'll see Valerius again one day."

c "What do you mean?"

sj "Why, I mean I have him."

"What?"

sj "He's my little prisoner, locked up in a dark and miserable cell."

"No. But... but if he's that close..."

c "Let me see him. Please."

sj "No."

c "Please. I'll... I'll..."

sj "Do anything? It's tempting, but I have no need to blackmail you when we'll be married soon anyway."

sj "You will see him, I think, in about two weeks time."

"And then he smiles. It's so normal, so slight. It's the most horrifying smile I've ever seen."

sj "Once he is starved away."

jump jailed
    
label sejanus_heaven_b:

sj "Oh, nearly forgot something very important."

"He motions for Nasrin to stand. As she does he slaps her bottom and she steps carefully through the pile of slaves to head for the exit."

"She doesn't look at me once as she goes."

"The guards let her through the door and then seal it shut behind her. Sejanus sighs."

sj "You should forget about your Valerius, my dear. He's soon to die."

if message == "hidden":
    jump grave_nightmares
    
if message == "shown":
    jump murder_attempt
    
label splitting_up:
    
scene HallDay
with fade

"Nasrin leads me on."

"I have no idea what her plan could possibly be though. There's no way out of this place now that it's surrounded."

"But we don't run into anyone as she guides me. Maybe she somehow knows which halls are deserted?"

"Or more likely it's just that all hands are preparing for the attack."

"Still, where are we going?"

scene Sewer
with fade

"We head down several flights of stairs. I vaguely recognize where we are, some corner of the citadel I've had no excuse to visit for years."

"We're going deep into the tunnels, where the jail is. Otherwise I'm not sure what else is down here besides rats and waste."

"I remember being a little girl and often coming down here to explore, but that all stopped when-"

"When I found... something..."

"There's something down here. Something I've forgotten, but..."

"I can't remember. I can't-"

"Nasrin suddenly stops. By torchlight I can see that we're in one of the dungeons. Skeletal cells line the wall on either side of me."

"Nasrin opens one up."

n "Get in."

c "What?"

n "It's safer here than anywhere else. Look."

"She pushes cold metal into my hand. A key."

n "No one will be able to get to you in there. Wait till the fighting's over."

c "Are you coming back?"

n "Maybe. Not for a while."

c "Wait!"

n "What is it?"

c "I... thank you."

"It seems only right to say it. Whatever her reasons, she's helped me tremendously."

n "... no need to thank me. I've been paid to help."

c "Still. You've done a good job. So thank you."

n "... you're welcome."

c "You're going back to Valerius?"

n "If I can even get to him. I may be trapped inside these walls too now. Hard to say."

c "Please. Tell him I love him."

n "... aye. I'll let him know."

"There's something strange about her voice. I can't see her well in the darkness, but I can almost imagine her face."

c "Is something wrong?"

n "Nothing I should open my mouth about."

c "What do you mean?"

n "Ghosts be good, it doesn't matter much when all this serious shit is happening, does it? But if you must know, I'm surprised you love him so much."

c "I... excuse me?"

n "No offense to him at all. I mean, weren't you only together for like a month? And you've exchanged, what, two or three letters since then? You've barely spent any time together."

c "That is completely normal when one's lover is a soldier."

n "Well I've loved a soldier before, and he was around all the time. But eh, never mind."

"She sounds as if she minds it a lot."

"Should I ask? Do I want to hear the answer?"

"But she's already gone. And now I'm... here."

"I stare into the empty cell. Am I really to simply wait here?"

"I've been waiting this whole siege, and all that seems to do is drag me around pointlessly from one place to the next."

"Can't I do anything?"

"I must. I have to find that place."

"That place from when I was a little girl. I... I'm sure it's nearby."

"I have to look for it. If I find nothing I can just come back."

"But I must look."

if battle == "inside":
    jump reunion
    
if battle == "outside":
    jump golden_room

label hiding_jail:
    
scene Dungeons
with fade

pause 0.5

nar "Somehow I made it to the dungeon."

nar "Didn't run into nearly anyone on the way. Of course all the soldiers are defending against the assault, but even the servants and slaves were nowhere to be seen. Perhaps everyone is hiding."

nar "I hope I don't have cellmates waiting on me."

nvl clear

nar "There are few lit torches down here. Grabbing one, I walk the hall, searching for the barred room marked forty-nine."

nar "They're all numbered in ascending order, so it takes me little time at all to find cell XLIX."

nar "The key works. Thank you Portia."

nar "What have they done to you by now? Have they..."

nar "Don't think about it. She didn't want you to think about it. She wanted you to be safe."

nvl clear

nar "The cell is wet, strange moisture glistening by the torchlight on all the walls. There is a stool, but it too is wet, and the wood of it is nearly rotting. It's chilly, except for the torchlight."

nar "But I'm safe. And now I only need to wait."

nar "Wait. That's all I'm good for, apparently. Just waiting around until things happen."

nvl clear

show black 
with dissolve

pause 1.0

centered "And yet, what else is there to do?"

if battle == "inside":
    jump reunion
    
if battle == "outside":
    jump golden_room

label clever_clodia:
    
scene ClodiaRoomDay
with fade

"I have it."

"Sitting at the window, watching my killers pound away at the citadel, it comes to me. What I can do."

"There are two men guarding my room. I'll have to be very careful about how I go about this. And a little lucky. But there's nothing at all to lose now."

"First, I change my outfit. Back into that plain dress I used to escape."

"Then I take my usual crimson dress and stuff my pillows inside of it. Nothing even close to convincing, but something to make it look like it has someone inside it for a split second."

"It only needs to work for a moment."

"There's a sewing bag here that Portia left some days ago. Since my carpet is predominantly red, I grab a spool of red thread and start to unwind it."

"It should be just strong enough, right? And it does blend in with the floor, mostly. They'll be too panicked to notice it."

"I hope."

"With one end tied lightly around the 'waist' of the pillow wearing my dress, I set the dummy on the windowsill. It teeters on the outer edge, the only thing keeping it from falling being the thread I hold tight."

"Lastly, I need just a bit of a delay to make it convincing. Unfortunately Sejanus had the lock on my door broken first thing when he took over."

"But pushing some furniture in front of the door will do just as well. They'll force past it in seconds. But I only need seconds."

"Okay. This ridiculous plan better work."

"Ahem. Ghosts of Lies, I need you now."

"Holding the thread taut, I place myself in the corner of the room."

"And begin to sob."

"It isn't hard to muster up the tears, the blubbering. The whole situation warrants it and I've been keeping it bottled up."

"I go a little overboard, though. Has to be loud. The guards have to hear it and think it's serious."

c "Cruel ghosts! Monsters! What sins did I commit to deserve this? Why has everything been taken from me?"

"My best choking, melodramatic sobs may make an actual stage player roll their eyes."

"But the knock at the door is the only acclaim I need."

g "Praetrix? Are you okay?"

c "No! Stay away from me. It's over. It's over! I won't live to let them ravish me. I can't. I can't!"

"That's it. They're coming in."

g "Praetrix!"

"They force the door open almost instantly, the furniture scattering as they shove it aside."

"And the instant they burst into the room, I release the thread."

"Poor Praetrix Pillow drops out of sight, lost to the Ghosts."

g "FUCK!!!"

"The two men rush to the window. Wildly they thrust their heads out into the air, looking down. They probably see my dress below and know that Sejanus will have their skins."

"But I'm already out the door and running as fast as I can."

"It worked. That stupid plan actually worked!"

"And now..."

"The safe place from my childhood. I need to find it again. It's the only option I have."

"The only safe place I can think of left."

jump golden_room

label golden_room:

scene Sewer
with fade

pause 0.5

nar "What even am I looking for?"

nar "I don't know, I just remember... this hallway. And the way it turns... here."

nar "And here. Yes, I definitely remember this place."

nar "But these hallways were just a strange dream I had long ago. One I'd forgotten, was never going to remember."

nar "Until it unfolded before me again last night."

nar "And something else... the sewers smell terrible, naturally. But the further I go on, the less the smell seems to overpower. The more I can feel some sort of... draft? Yes, that definitely seems to be the case."

nvl clear

scene black
with fade

nar "Up ahead, however, there seems to be no light. No torches. And yet, I know there's something ahead."

nar "I take the last lit torch from its sconce and press on. Thank the Ghosts of Luck I'm in these plain clothes and not one of my dresses, I feel like I would have tripped a hundred times by now over that thing."

nar "Something hovers ahead of me in the darkness, orange in the torchlight."

nar "Wood. A sign. Bright red, commanding words."

nvl clear

centered "DANGER. {p} GO BACK BY ORDER OF THE PRAETOR. {p} ONLY THE GRAVE LIES AHEAD. {p=1.0}"

nar "This sign. I know it. I remember it. A strange joy fills my heart at the sight."

nar "And there's the last confirmation:"

nar "A little human face, scratched into the wood over ten years ago. A circle with two Xs for eyes and an upside down curve of a mouth."

nar "My addition, my childish way of helping emphasize my uncle's message. I thought it was a way of saying sorry after he became angry at me for coming down here."

nvl clear

nar "But that was after he caught me, after I had already pressed on."

nar "I know I went forward and saw... the grave, didn't I?"

nar "And... what the grave protected."

nvl clear

nar "The tunnel becomes narrower as I press on. Uneven. The walls are not carved stone anymore, but jagged cavernous rock. And the air has grown cooler and..."

nar "Ugh. Whatever relief from the sewer smell I had before, it's done away with by this steadily growing odor."

nar "Foul. Poisonous. The smell of a nightmare. A smell that would have made any jailer or guard or would-be explorer turn and run if they had managed to ignore the sign."

nar "But I somehow pressed on as a little girl. Perhaps it was easy to feel invincible then. Protected by the Ghosts, even."

nar "And I survived. So... I have to solve this mystery before I die. I have to know what is here."

nvl clear

nar "Narrower. Narrower. Now only big enough for one person."

nar "A grown man would have quite a bit of trouble here. Thankfully I'm a bit more slender than that. But still, I have to turn sideways as I move."

nar "Just as I push through a gap that is so thin I feel the rock pressing me together, the walls recede, and the hall opens again."

nar "I take a few deep breaths of the foul air, steadying my nerves. But any desire I have to turn back is obliterated by the sight of a wooden door ahead in the torchlight."

nar "The smell is nearly unbearable here. It seems to ooze forth from the door like some sort of curse."

nar "Yet, what curse would be worse than what all those men above will do to me once they get me?"

nvl clear

nar "My heart is racing as I grab the handle. Certainly, the door will be locked. And yet, it wasn't locked when I was a girl, was it?"

nar "Nor is it now. I push it open."

play sound dooropen

nar "And..."

nar "I remember the rest of the dream. The final scene of that long ago memory is bare before me again."

nar "And at last, so much makes sense. I know why Sejanus has done everything."

nvl clear

show gold
with dissolve

c "Ghosts of Greed, what have you placed here to ruin us?"

show PitofNightmares:
    zoom 2.0 xalign 1.0 yalign 0.0
    linear 4.0 xalign 0.0
with dissolve

pause 3.0

show PitofNightmares:
    zoom 2.0 xalign 0.0 yalign 0.0
    linear 3.0 yalign 1.0
with dissolve

pause 2.0

show PitofNightmares:
    zoom 2.0 xalign 0.0 yalign 1.0
    linear 4.0 xalign 1.0 yalign 0.5
with dissolve

pause 3.0

show PitofNightmares:
    zoom 1.0
with dissolve

pause 1.0

nar "Gold."

nar "The walls are gold. Everywhere."

nar "There's enough here to make one almost as rich as the Basileus. The cavern is gigantic, extending back into the darkness, beyond where my torch lets me see. It could go on for miles and miles."

nar "And the ground opens up into a sickly red swamp. Foul mist, blackened bones, horrible worms and bugs with thousands of legs, poisonous liquids."

nar "I know where I am. This is the bottom of Dagara's great Pit of Nightmares where her condemned are hurled. This is where they die, screaming and in pain, if they manage to survive the fall from above."

nar "And all in darkness. They die not even knowing what wonder surrounds them."

nvl clear

nar "Dagara could have been the richest province in all Raskya. Why did we never take advantage of this?"

nar "I'm sure Sejanus has asked himself that question many times. With control of this, he has tremendous leverage. He could easily bargain his way out of this entire civil war, especially knowing the personality of the current Basileus."

play sound dooropen

pause 0.3

nar "What?"

nvl clear

show clo2 snk-worry at left
with dissolve

show sej diplo-per-flip at right
with easeinright

sj "Oh. Oh my. My my my."

sj "How in the name of all Ghosts did you end up in here, little Clodia?"

"{i}I should be surprised to see him. He was actually able to squeeze through that hallway? He's a thin man, true, but I never would have thought it possible."

"{i}And yet... of course he knew about this place. It's so obvious."

show clo2 reg-stoic
with dissolve

c "This is what this siege is all about."

show sej neutral-smile-flip
with dissolve

sj "Yes. And I was going to keep it a surprise until after we were married."

show sej rel-laugh-flip
with dissolve

sj "Rejoice! You are soon to be the richest woman in Raskya. Hey, suddenly marrying me doesn't seem so bad, eh?"

show clo2 reg-declaring
with dissolve

c "Fuck you."

show sej diplo-per-flip
with dissolve

sj "Well, you'll get your chance tomorrow."

show clo2 reg-stoic
with dissolve

c "Did my uncle know what was in here?"

show sej rel-bored-flip
with dissolve

sj "Oh yes. And he was just coming around to the idea that mining the gold would be worth enraging the Ghosts of the Grave over disturbing this most... holy... of its sites."

sj "This pit has been here since before the city was built, after all. Likely, it was founded upon it."

show sej diplo-threat-flip
with dissolve

sj "But you know, your uncle would have split the wealth between himself and all his lords and lordlings and perhaps even shared some with the people."

show sej diplo-per-flip
with dissolve

sj "Luckily, Fausus's trust was easy to earn. I bonded with him over our mutual hatred of Sergius, the man who stole his kingdom and my glory. So he let me know Dagara's great secret."

show sej neutral-smile-flip
with dissolve

sj "Luckier still, the Basileus liked my plan better. No lords, no lordlings. Just a straight fifty/fifty split between me and him."

show clo2 reg-declaring
with dissolve

c "You are the lowest, most traitorous bastard who has ever lived. The Ghosts will tear you apart for a hundred thousand years before letting you become one of them."

show sej neutral-serious-flip
with dissolve

sj "Unlikely. Sorry Clodia, but I just don't believe in the Ghosts. Haven't for years."

show sej diplo-threat-flip
show clo2 reg-stoic
with dissolve

sj "There are no divine punishments or rewards for our actions in life. The only punishments come from failure and bad luck. And the only rewards come from success."

show sej diplo-per-flip
with dissolve

sj "Now then... tomorrow we can both taste success. Do I have to drag you to it?"

show clo2 reg-stoic-flip
with dissolve

pause 0.5

show clo2 reg-stoic
with dissolve

c "... How is the battle going?"

show sej rel-laugh-flip
with dissolve

sj "Ha. Not well at the moment, but the Immortal Legion will arrive tomorrow and relieve us. Our victory is guaranteed."

show clo2 defen-crying
with dissolve

"{i}Valerius... Valerius..."

"{i}I don't know..."

"{i}I don't know what to..."

"{i}............."

"{i}I know one thing I can do."

show clo2 reg-emotional
with dissolve

c "I'll go with you. Peacefully. I'll even marry you tomorrow without a fuss."

show sej rel-bored-flip
with dissolve

sj "As long as I do... what?"

c "One of Sergius's tribunes is a man named Valerius. If he survives the battle and is captured, you must let him live."

show clo2 reg-declaring
with dissolve

c "If you kill him, I'll kill myself."

if battle == "inside":
    jump golden_room_a
    
if battle == "outside":
    jump golden_room_b
    
label golden_room_a:

show sej sin-grin-flip
with dissolve

sj "... hmm. Heh heh. Interesting."

c "What are you laughing about?"

sj "Nothing. I'm just amused."

show clo2 reg-stoic
with dissolve

c "Do we have a deal or not?"

show sej diplo-per-flip
with dissolve

sj "Ha. Very well. If I ever happen to find this Valerius person, I will not kill him."

c "... your word is no good."

show sej neutral-smile-flip
with dissolve

sj "And yet, it is the only word you will get."

"{i}He's right."

"{i}He's planned for all this better than I ever expected. Fooled everyone."

scene black
with dissolve

pause 0.5

centered "It's over. I have to go with him."

jump jailed

label golden_room_b:

show sej rel-glance-flip
with dissolve

sj "... hmm. Your lover?"

show clo2 reg-stoic
with dissolve

c "Do we have a deal or not?"

show sej neutral-smile-flip
with dissolve

sj "Ha. Of course we have a deal."

show sej diplo-threat-flip
with dissolve

sj "Oh, I'll exile him of course. You'll never see him again. But I will not kill him."

c "... your word is no good."

sj "And yet, it is the only word you will get."

"{i}He's right."

"{i}He's planned for all this better than I ever expected. Fooled everyone."

scene black
with dissolve

pause 0.5

centered "It's over. I have to go with him."

if plan == "escape":
    jump nasrin_corpse
    
if plan == "clodia_poison":
    jump nasrin_corpse
    
if plan == "nasrin_poison":
    jump last_letter

label nasrin_corpse:

scene ValeriusRoomNight
with fade

pause 0.5

nar "The legionaries who bring me the body seem innocent enough."

nar "But these men are all killers and liars, so maybe they did this themselves. I can't tell."

nvl clear

show sol neu-confuse at sleft
with dissolve

so "Tribune?"

show val rel-normal-flip at fright
with dissolve

v "Hm?"

so "Uh, I said, shall we toss it into the Pit of Nightmares? Easiest way to dispose of it."

show val rel-serious-flip
with dissolve

v "You will do no such thing. Leave it here with me, I'll have my servants deal with it."

show sol neu-neu-flip
with dissolve

so "As you wish."

hide sol
with easeoutleft

nar "The man and his fellow leave. Now it's just me, some slaves, and Nasrin."

show green
with dissolve

pause 0.5

centered "Nasrin..."

pause 0.5

centered "Her face looks neither peaceful nor pained. Just... like she fell asleep into a bad dream. Her lip is curled, but her eyes are closed.{p}{p=0}I pray it didn't hurt.{p}{p=0}The gaping sword wound in her belly is already beginning to fester. A wound caused by a Raskyan blade.{p}{p=0}But of course, almost every armed man in this city right now is wielding one of those. I'll never know who did this."

nvl clear

hide green
show val rel-emotional-flip
with dissolve

v "Wash her."

nar "The slaves get to work, stripping her, cleaning the wound. Preparing her."

nar "I don't really know how to have her properly buried. There's no good place inside the walls for it and I don't trust anyone, soldier, slave or otherwise, to properly inter her without my presence."

nar "Just as likely they'll toss her in the Pit and then tell me they forgot where they buried her. You know, ground all looks the same and all."

nar "I watch in silence as the body is prepared. Purified, wrapped in a shroud. Then I find my slaves looking at me expectantly. They want to know where to take her."

nar "I don't know."

nvl clear

v "That's enough for now. Leave me alone, all of you."

nar "Left with Nasrin, I sit down next to her and wonder just exactly how I'm supposed to feel. I stare down at her face, her lip now eased by a slave's hand into a peaceful line."

nar "When she was my slave, she was a good slave. When I freed her, she was grateful."

nar "And she's been a mischievous pain in the ass ever since, but she always did good work and rarely let me down. She didn't deserve this."

nar "Few people do. The Ghosts take us all sooner or later, though."

nar "Like as not they'll take me tomorrow."

nvl clear

nar "Tomorrow I storm the citadel. The Ghosts will be there, hovering over the carnage."

nar "Watching. Waiting to claim fresh sacrifices as souls float up out of their freshly carved bodies."

nar "One might be mine. Then I suppose I'll find Nasrin in the Air. I can imagine her joking that I couldn't stand being without her and just had to follow as soon as I could."

nvl clear

nar "There's a few tears in my eyes. Surprising."

nar "I should have been kinder to her. The little trollop wouldn't stop trying to seduce me, had no respect at all for my love for Clodia. But she helped me all the same."

nar "And anyway, I did love her once, for a brief moment in time."

nar "I'm so sorry, Nasrin. There's nothing I can do for you anymore."

nar "Nothing I can do except win tomorrow's fight. You died trying to save Clodia for me. So I have to win."

nvl clear

show black
with fade

centered "I stay up in bed all night. I should be monstrously tired, but sleep is impossible.{p}{p=0}My body is ready to kill."

if message == "hidden":
    jump lightning_siege
    
if message == "shown":
    jump feeble_siege

label last_letter:
    
scene ClodiaRoomNight
with fade

"Sent back to my room by Sejanus. Never mind I've escaped it before."

"But I suppose he knows there's no point in me doing it now, now that we're surrounded. Now that I know that Sejanus will win."

"Sejanus will win..."

"And... I can't trust his word at all about Valerius. Anyway, Valerius still may die in the combat."

"In fact... it's almost certain, isn't it?"

"Oh love... I want to save you. More than anything now, I want to save your life."

n "Hello there."

c "Ghosts!"

n "Shh! Sorry I spooked you, but don't give me away."

c "How did you get in again? Are Sejanus's men truly this stupid?"

n "No, I got back to the room before you did. It wasn't guarded then."

c "Hmph. So, what are you here for? I thought you were going back to Valerius."

n "I was, but my usual way out was blocked. And while searching for another way, I found that you had abandoned the spot I told you to stay put in."

n "Not sure what you did something stupid like that for, but I figured I may as well follow you and make sure you were safe or else Valerius would be pissed at me."

c "Well that's very kind of you, but as you can see, it doesn't matter now. Not unless you want to try to break me out again."

n "No can do. I'm pretty sure there's only one way I can get out of here now, and it's the kind of thing only I can do."

c "Of course..."

n "Also, I overheard you talking to Sejanus down in that cavern. Pretty amazing sight, all that gold. Damn shame it'll all be yours once you've married Sejanus."

c "What is with your tone of voice?"

n "From my perspective, it seems as if you're suddenly giving up on Valerius as soon as you find out Sejanus is about to be the richest man after the Basileus."

c "How dare you! That has absolutely nothing to do with my decision. I am trying to save Valerius's life."

n "Good luck. The fool's going to die tomorrow coming in here to try and save you."

c "No he won't."

n "Ha, listen Praetrix, I don't think you quite get how dumb this boy is. He's one of the smartest men I know, and he's completely stupid about you."

n "Throwing away his life for you is just the big stupid final act to the big stupid play of coming here to save you."

n "Believe me, I've tried to make him forget you for his own sake, but I guess I'm just not good enough next to a Praetrix."

c "Will you stop your insecure ranting and listen to me? He's not going to die here tomorrow because I won't let him. I'm going to write him a letter."

n "Oh?"

c "It will instruct him to flee. Not to come after me. To save his own life."

c "I'll explain about Sejanus and the gold, make him understand that I'll be safe as Sejanus's wife."

n "I doubt his manly jealousy will much like the idea of you being safe with Sejanus."

"If I can overcome my jealousy of you to save his life, surely he can overcome his."

c "I will be insistent. Perhaps I'll even be cruel, if it will make him leave."

n "Don't be cruel. Just say what you need to to convince him. I'll be sure he sees it."

c "You're smiling."

n "I smile a lot."

c "Not like this... you're very happy about this letter, aren't you?"

n "... so what if I am?"

c "Listen. I understand that you love him."

n "Ah Ghosts, don't just say that. He's an asshole, I shouldn't love him."

c "You say that, but you know he's kind. Kinder than most men in Raskya."

n "He can be."

c "He will be sad about me. Maybe for a while. It may consume him."

c "To comfort himself, he may show you affection one day, then guilt will push him to ignore you the next."

c "Don't give up on him. If you really wish to love him, give him time to heal."

n "That sounds like decent advice and all, but how would you know anything about that if he's your first love?"

c "Durjan love poems."

n "Ah."

c "Just stay put, I'll write the letter as quick as I can."

"But it doesn't go quickly. It's so hard to find the right words."

"I'm scared that if even one syllable isn't perfect that he will not understand, or ignore it, or hurt more than he has to."

"Eventually, it's done."

c "Here."

n "All right. Thanks then, Praetrix. I'll be sure to let him read it."

c "I know you will."

n "Now, scream for help."

c "What?"

n "Seriously. Scream for the guards."

c "I don't-"

n "It's part of the plan, okay? Just do it."

"Ghosts damn her. Very well."

c "HELP! GUARDS, HELP!"

n "Thank you. Farewell, Praetrix."

g "What's wrong Praetrix?"

"Thick gray smoke fills the room at a crack of a smokebomb. I cover my face, coughing, my eyes stinging with tears."

g "Block the door!"

"But when the smoke clears, Nasrin is gone, and the guards block the open door against nothing."

g "What did you see Praetrix?"

c "There... there was an assassin! A girl posing as a slave tried to kill me!"

g "Ghosts damn it, I can't wait for this fucking siege to be over."

g2 "Just stay here, my lady. We'll track her down."

"Unlikely."

"The guards leave the room, shutting the door behind them."

"At last, I'm alone."

"Good."

"I lie down in my bed and cry."

"I've never cried so hard in my life."

scene black
with fade

"Valerius..."

"Forgive me."

jump mistress

label jailed:
    
scene black
with fade

nar "I can't see anything. It's pitch black."

nar "And they wrapped a blindfold over my eyes, as if the darkness weren't good enough."

nar "My arms are chained to a wall. All I can do is sit, uncomfortably. I can pull away from the wall just barely enough to stretch, but not enough to lay down and relax."

nar "Fuck you, Sejanus. I will cut your balls off for this when I-"

nar "When I..."

nvl clear

nar "Fuck. I'm not doing anything. I've failed."

nar "I've failed Clodia. Where is she? Is she still alive?"

nar "If she is, she'll be dead soon when Sergius gets to Sejanus and her. And what is the alternative anyway, that she'll become that man's wife?"

nar "Would she try to be happy with him? I doubt it. She would kill herself rather than let him touch her, I know it."

nar "That doesn't make me feel any happier. I just..."

nvl clear

nar "Ghosts, take me now. So I don't have to live any longer with these painful thoughts tearing into me."

nar "Would that I had stayed a farmer soldier with no aspirations beyond lining my pockets and bed."

nar "But..."

nar "I'm so happy I met you Clodia."

nar "Was loving you worth all this?"

nar "Of course it was. Of course it was."

nvl clear

nar "They're breaking in soon. They'll tear everyone in the citadel apart. Rape all the women, some of the boys. Kill the rest."

nar "Sergius will get his bloody offering to the Ghosts of War."

nar "Will I become a Ghost of War?"

nar "Will Clodia?"

nar "Can we be together that way?"

nvl clear

jump ghosts

label ghosts:
    
scene black
with fade

show red
with dissolve

hide red
with dissolve

show red
with dissolve

hide red
with dissolve

show red
with dissolve

hide red
with dissolve

nar "Pain."

nar "When I am awake, I feel pain."

nvl clear

nar "Sleep is the only cure. The world is a dark void full of hurt."

nar "My stomach is full of daggers. My throat is a bleeding desert. My neck is a throbbing agony."

nar "I tried to strangle myself with my chains. I think. Or maybe that was a dream."

nar "My arms are too weak now to lift the chains. Probably why I failed to kill myself."

nar "Breathing hurts."

nvl clear

nar "Ghosts of War, Ghosts of Pain, Ghosts of the Grave. I don't care who, just take me."

nar "Take the pain away. Please."

nar "I can't cry. I have no water left."

nar "I'm filthy and soiled. Every movement feels like sharp bone grinding through muscle. There are bugs on me. Maybe in me."

nar "Throw me in the Pit of Nightmares. I would die faster."

nar "Or maybe I am dead. Is this all there is beneath the Grave? Blind agony?"

nvl clear

nar "What?"

nar "What is that?"

nar "It's coming closer."

nar "Someone who could kill me? Please. Oh please."

nvl clear

sj "There you are, my dear. Don't be shy, take a good look. Sergius lies dead, but his last tribune yet lives."

c "..."

sj "What, you won't say anything?"

c "... Kill him. Please."

"{i}I know that voice. It's... the memory is so far away now. I can't remember..."

sj "What? You begged me to keep him alive."

c "Why do you hate me so? Do you want me to kill myself?"

sj "This never had to happen, dear. If you had only accepted our marriage rather that ignore and hate and belittle me. If you had not insulted me."

sj "Well, I am no man to insult anymore. You must never forget what I can do to those who hurt me."

c "This life is a Hell. Look what you've done to him. Valerius..."

nar "I can't open my mouth to speak. It feels like I will die."

nar "But she's here. Clodia... it's you, isn't it? I almost forgot your voice."

nar "My Ghost of Love."

nvl clear

nar "Everything hurts so much, Clodia. My body. My heart."

nar "I can't move anymore."

nar "I'm dreaming again."

nar "Don't look at me. Please. Leave me. Don't look at what used to be Valerius. Don't hurt yourself like that."

nvl clear

c "Please. Please, my husband. Let me."

sj "Hmph. That's more like it. Here you go."

"{i}Stay away. Stay away!"

"{i}Don't come near me! Don't-"

c "Shh. It's okay. I'm here Valerius."

"{i}Go away."

"{i}Go away. Go away. Go away. Go away. Go away."

c "Please. Hold still. Drink. You need to drink."

nar "Something is placed to my lips. They crack and howl, but the cool water feels like heaven."

nar "The bowl is tipped, and the water runs down my throat. I can barely swallow. The smallest relief imaginable fills my chest for only a moment."

nar "Then the pain returns."

nar "And her hand strokes my cheek."

nvl clear

nar "I have water now. I can cry again. Cold tears."

nar "She cradles my filthy, living corpse in her arms."

nar "I found you again. I found you again."

nar "Clodia."

nvl clear

c "I love you Valerius. I will love you forever. And when we're both Ghosts, I will find you again."

v "...Clo...di...a..."

nar "I feel cold. But good. It doesn't hurt so much anymore."

nar "That water was miraculous. I can feel it inside me. Making everything cold and painless."

nar "My legs are fading away."

nar "My arms are disappearing."

nar "My body is numbing. I'm shrinking into the darkness."

nvl clear

nar "Soon only my weak head rests in her arms."

nar "You saved me, Clodia. I can finally sleep now."

nvl clear

centered "Thank you. My little Ghost of Love."
    
jump epilogue_ghosts

label epilogue_ghosts:

scene black

centered "The tragedy of Valerius and Clodia is, I think, demonstrative.{p}{p=0}He ignored rank and duty to the state for her, a mindset and attitude which in his day would have been seen as treasonous and effeminate. She forwent the sacred duty of the Raskyan woman to her betrothed, no matter the circumstances, indeed made him as good as a cuckold for the sake of her lover, which was punishable by death in those strict times. {p}{p=0} But to those who loved amidst the Nightmares, death would come regardless. We only know most of the story of Valerius and Clodia because the heroine herself left detailed diary entries in the year before committing suicide. She gave birth to Sejanus's child, a boy, an heir, and then leapt from the roof of the citadel with the babe in her arms. Thus, she lived long enough to give Sejanus joy and then take it from him, a fitting revenge. {p}{p=0} The historian cannot help but wonder at the possibilities: What if Valerius had not charged blindly into the citadel, into the arms of his eternal jailer? Or what if Clodia could have freed him before Sejanus's victory was secured? {p}{p=0} But these sorts of questions only make the heart suffer needlessly. The only true solace for those who weep for Valerius and Clodia are the prayers and offerings one may make in their names: to two of the infinite Ghosts of Love, every bright soul who died for the heart and life of another, still dancing between the starlight. {p}{p=0} - Alexes, from his \"Tales of the Republican Kingdom\" {p=1.0}"

centered "THE END"

return

label reunion:
    
scene black
with fade
    
nar "I can't see anything. It's pitch black."

nar "And they wrapped a blindfold over my eyes, as if the darkness weren't good enough."

nar "My arms are chained to a wall. All I can do is sit, uncomfortably. I can pull away from the wall just barely enough to stretch, but not enough to lay down and relax."

nar "Fuck you, Sejanus. I will cut your balls off for this when I-"

nar "When I..."

nar "Fuck. I'm not doing anything. I've failed."

nvl clear

nar "Hmm. The jailer seems to be coming. Finally."

nar "They've given me no food or water since putting me in here. I wondered if they ever would."

nar "Strange. Moving pretty fast for a jailer."

nvl clear

v "Who is that?"

"{i}Well, they stopped, whoever they are."

v "In here. You're not the jailer, are you?"

c "No."

"{i}A woman. Castle servant, perhaps. And yet what is she doing down here?"

v "Well, I don't suppose you're interested in money, miss?"

c "Not at all, and I really don't have time-"

v "Did you live in this place before Sejanus came and polluted it? He's my enemy too."

c "... Who are you?"

v "A man who just wants to see Sejanus die, and his betrothed live."

c "Why do you care if the Praetrix lives or dies?"

v "Ha. If you must know the truth, I'm in love with her."

c "... you best scratch that dream. She cares not for distant admirers and flatterers."

v "I'm not distant at all. Believe it or not, she loves me back. Ah, but what use is it explaining it to some servant girl like-"

"{i}Huh?"

"{i}Hey, wait a second here."

v "Hold on. What's going on? I can't see, what are you doing?"

"{i}What did I say? Is she coming to kill me? What did I-"

"{i}A rustle of fabric. Soft hands on my face. The smell of roses."

v "Clodia."

show CG8A
with dissolve

"Her lips cover mine. In a second, all my dreams for the past year have come true."

"It's her. She's here."

"I wrap my arms around her and pull her close. I can't even begin to question this right now. Ghosts of Love, what miracles you can do!"

show CG8B
with dissolve

v "How did you know I was here?"

c "I didn't. I didn't. I was just looking for a place to hide. Why are you here?"

v "Blind luck. Or an answered prayer. My Ghosts, hold up that torch, let me get a good look at you."

v "You're more beautiful than I remember."

c "And you're still the most handsome man in the world."

v "You have seen little of the world, but thank you, beloved. I can't believe this."

"Praise the Ghosts. I don't care what that means, I'll do anything to not have this taken away from me now."

"But we must act if we're to save ourselves."

v "What's going on up there?"

c "Sergius is besieging the citadel, but otherwise I know nothing."

v "We should lock ourselves in this cell until they've taken over the place. Then perhaps it will be safe to come out."

c "Very well."

v "Ah, but first, help me with these chains. Does that key unlock them as well?"

c "It may. Let me try."

scene Dungeons
with dissolve

"{i}There goes one. And the other..."

c "Shit!"

v "What's wrong?"

c "The key broke."

v "Damn it, what was I saying about luck? At least the chain came off first, there's that. But what now?"

"This is a horrible hiding place if we can't lock ourselves in. Have we found each other just to be slaughtered together?"

c "Well, can't we just join Sergius once he gets in? Surely if I'm with you I'll be safe?"

v "Most of the army doesn't know me. Many that do don't like me. Hell, Sergius doesn't like me. Truthfully, in all their battle lust they'll probably cut me down."

c "I... I know a place we can go."

v "What? Where?"

c "It's hard to explain. A place from when I was a little girl. I think it's safe."

v "It must be better than here. Take us."

"Together, we leave the cell. Her hand in mind."

"She leads me, torch aloft, towards this last hope of ours."

"And even though thousands of murderers are beating at the doors above us, I can't stop staring at her."

"Her red eyes flickering in the firelight, her shadowy hair trailing behind her, her pale skin showing ghostly beauty."

"The scent of roses, of {i}her{/i}."

"At last. At last I have found you, my Clodia."

jump golden_showdown

label golden_showdown:
    
scene Sewer
with fade

"I keep looking back to make sure he's there."

"Even though his hand is grasped in mine, I have to be sure."

v "What kind of place are we heading to? It will be able to keep soldiers out?"

c "I think so. If... if I can find it."

"If it's real."

v "I'm sure you can. Focus, love."

"What even am I looking for? I don't know, I just remember..."

"...this hallway. And the way it turns... here."

"And here. Yes, I definitely remember this place!"

"But these hallways were just a strange dream I had long ago. One I'd forgotten, was never going to remember."

"It seems that many of my dreams are coming true. But I can't enjoy them yet."

"The sewers smell terrible, naturally. But the further we go on, the less the smell seems to overpower. The more I can feel some sort of..."

"...draft? Yes, good."

scene black
with fade

"Up ahead, however, there seems to be no light. No torches."

"And yet, I know there's something ahead. Thank the Ghosts of Luck I'm in these plain clothes and not one of my dresses, I feel like I would have tripped a hundred times by now over that thing."

"Something hovers ahead of me in the darkness, orange in the torchlight."

"Wood. A sign. Bright red, commanding words."

centered "DANGER. {p} GO BACK BY ORDER OF THE PRAETOR. {p} NIGHTMARES AHEAD. {p=1.0}"

"This sign. I know it. I remember it. A strange joy fills my heart at the sight."

"And there's the last confirmation:"

"A little human face, scratched into the wood over ten years ago. A circle with two Xs for eyes and an upside down curve of a mouth."

"My addition, my childish way of helping emphasize my uncle's message. I thought it was a way of saying sorry after he became angry at me for coming down here."

v "Is... that can't mean the Pit of Nightmares is ahead?"

c "I think it is. I found it once, as a girl. Hid there."

v "Hid in the Pit of Nightmares? Oh sweetness, no wonder you went rotten so young."

c "I shouldn't smile at that. But come on, it's safe."

v "How is that possible?"

c "You just have to not go into the dangerous part. Come on!"

v "Oh, well obviously."

c "Come on! I promise we'll be safe, dearest."

"He's scared. I know. But I'm sure of this."

"There are nightmares ahead, yes. But there are nightmares above and all around us."

"At least the ones ahead are not alive."

v "Lead on, then. I trust you."

"The tunnel becomes narrower as we press on. Uneven. The walls are not carved stone anymore, but jagged cavernous rock."

"The air grows cooler, and..."

"Ugh. Whatever relief from the sewer smell I had before, it's done away with by this steadily growing odor."

"Foul. Poisonous. The smell of a nightmare."

"A smell that would have made any jailer or guard or would-be explorer turn and run if they had managed to ignore the sign."

"But I somehow pressed on as a girl. Perhaps it was easy to feel invincible then. Protected by the Ghosts, even."

"And I survived. So I'll do it again."

"Narrower. Narrower. Now only big enough for one person at a time."

"Valerius is a man, he's going to have quite a bit of trouble here. Thankfully I'm on the slender side."

"But still, we have to turn sideways as we move."

v "My armor can't make it through here."

c "Let me help you take it off."

"It takes a minute. I have no idea what I'm doing, but he directs me well enough."

c "There."

v "I'd be lying if I didn't say I was nervous. This is... what if this place collapses? What if-"

c "It's just on the other side, I promise. We'll be safe."

"And now I'm certain no one but the very thin or small could hope to pass through here. Perhaps many brave men made it this far only to turn back from the impossibility."

v "I don't think I can make it."

c "I'm sure you can. This is the end right here. I'll go through first and help you through."

"Just as I push through a gap that is so thin I feel the rock pressing me together, the walls recede, and the hall becomes open again."

c "Okay, tighten yourself up."

v "Oh Ghosts be good."

"He pops through as soon as I give his arm a slight tug."

v "Ouch!"

c "We made it!"

v "To what? I can't tell where we are."

"I take a few deep breaths of the foul air, steadying my nerves. There's a wooden door ahead in the torchlight."

"The smell is nearly unbearable here. It seems to ooze forth from the door like some sort of curse."

"Yet, what curse would be worse than what all those men above will do to me once they get me?"

"My heart is racing as I grab the handle. Certainly, the door will be locked. And yet, it wasn't locked when I was a girl, was it?"

"Nor is it now. I push it open."

"And..."

v "... well shit."

"I remember the rest of the dream. The final scene of that long ago memory is bare before me again."

"And at last, so much makes sense. I know why Sejanus has done everything."

c "Ghosts of Greed, what have you placed here to ruin us?"

scene PitofNightmares
with fade

"Gold."

"The walls are gold. Everywhere."

"There's enough here to make one almost as rich as the Basileus. The cavern is gigantic, extending back into the darkness, beyond where my torch lets me see. It could go on for miles and miles."

"And the ground opens up into a sickly red swamp. Foul mist, blackened bones, horrible worms and bugs with thousands of legs, poisonous liquids."

"I know where I am. This is the bottom of Dagara's great Pit of Nightmares where her condemned are hurled. This is where they die, screaming and in pain, if they manage to survive the fall from above."

"And all in darkness. They die not even knowing what wonder surrounds them."

"Dagara could have been the richest province in all Raskya. Why did we never take advantage of this?"

"I'm sure Sejanus has asked himself that question many times. With control of this, he has tremendous leverage. He could easily bargain his way out of this entire civil war, especially knowing the personality of the current Basileus."

v "The... there are, um, torches on the walls."

c "Oh. Yes, I'll light them."

"One by one I bring the lights to life. One by one they bring the glitter of gold out of the dark, like the infinite starry cosmos."

"This would instantly be one of the wonders of the world if it were known about."

v "Sejanus must know about this. Perhaps it isn't as safe here as-"

c "No."

sj "Hm? Oh, Ghosts be Good."

"What kind of timing is this?"

sj "So here you are, my betrothed. I've had men looking all over for you."

"Valerius steps between us. I place my arm on his shoulder."

"Don't do anything rash, my love."

sj "And the Tribune? I suppose that's your armor I passed just back there. Clodia, why did you let this man go free? Did you promise to fuck him? Have you already fucked him?"

c "This cavern is the cause of this whole war, isn't it?"

sj "Yes. And I was going to keep it a surprise until after we were married."

sj "Rejoice! You are soon to be the richest woman in Raskya. Hey, suddenly marrying me doesn't seem so bad, eh?"

c "Fuck you."

sj "Well, you'll get your chance tomorrow."

c "Did my uncle know what was in here?"

sj "Oh yes. And he was just coming around to the idea that mining the gold would be worth enraging the Ghosts of the Grave over disturbing this most... holy... of its sites."

sj "This pit has been here since before the city was built, after all. Likely, it was founded upon it."

sj "But you know, your uncle would have split the wealth between himself and all his lords and lordlings and perhaps even shared some with the people."

sj "Luckily, Fausus's trust was easy to earn. I bonded with him over our mutual hatred of Sergius, the man who stole his kingdom and my glory. So he let me know Dagara's great secret."

sj "Luckier still, the Basileus liked my plan better. No lords, no lordlings. Just a straight fifty/fifty split between me and him."

c "You are the lowest, most traitorous bastard who has ever lived. The Ghosts will tear you apart for a hundred thousand years before letting you become one of them."

sj "Unlikely. Sorry Clodia, but I just don't believe in the Ghosts. Haven't for years."

sj "There are no divine punishments or rewards for our actions in life. The only punishments come from failure and bad luck. And the only rewards come from success."

sj "Now, I answered your question. Reciprocate. Why did you let this man go?"

v "So I can make love to her once you're dead."

sj "... ah. So."

sj "By chance, are you somehow the boy who took my rights as a husband away from me?"

v "Aye, that I am."

sj "I wish I had known that when I locked you up. I would have savored it more."

sj "I guess now's my chance."

"He doesn't seem to have any guards with him. Of course not, he had no reason to expect anyone would be in here."

"Not that most armored men would ever fit through that hall anyway. How in the Grave did he?"

sj "Step aside, boy. If you do, I'll have you exiled anywhere you'd like."

sj "Stay where you are and I will exercise my right as a cuckolded man to sever your cock and feed it to you."

sj "That is mercy you have no right to expect from a Raskyan legatus. Let me be known as a merciful man."

v "Ghosts of the Grave, you are annoying. To think that my beloved has been facing a future having to put up with you sweating like an ox over her for the rest of her life."

sj "And this is the trick to mercy: I knew what your answer would be."

sj "Come and die then, boy. I've killed barbarian kings in single combat. You are nothing."

"Valerius has no weapon. Thanks to that accursed passageway, no armor either."

"What are you doing my love?"

"Yet there are weapons all around us. Old and damaged mostly, but there they are."

"Swords and poles, rusted armor full of bones. Whole warriors tossed into the pit, equipment and all."

"But there's something useful: a legionary shield near the pit, just about ten feet to the left."

c "Valerius."

"He looks back at me for a second. Then at where my gaze is directed."

c "Go!"

"We both dart to the left. Sejanus holds still, watching us as we reache the shield. Valerius hefts it up in front of us."

sj "Defend all you like."

"He stalks towards us, lazily swinging his sword through the air."

"Closer. Closer."

"The vibration of the first strike goes through Valerius's arms and into me. Ghosts, just that is frightening!"

"How can anyone withstand that for the hours and days of battle?"

"None of the strikes accomplish much except shake us. Maybe that's all they're meant to do."

sj "That shield is old and damaged. Sooner or later I'll break it down."

v "Then I'll shove the splinters in your eyes."

sj "Come now, you mustn't tell me what you're going to do."

"Three blows in succession."

"I try to push against Valerius and help hold our ground, but we have to take a step back regardless."

"About ten feet behind us is the lip of the great stinking pit. But I don't think we're in any immediate danger from that. As long as I'm behind Valerius, Sejanus won't try to drive him over the edge."

"I'm not sure Valerius has noticed though."

c "I think he'll try and separate us so he can push you into the pit."

v "Makes sense. Shit. Okay, stay with me no matter what."

scene PitofNightmares

"Sejanus holds back and watches us for a moment. What kind of move is he planning?"

"I know nothing of combat really, but I know that he's a better fighter than Valerius. There has to be something we can..."

"I have it. I think I have it."

c "My love. I know what to do."

v "What do you mean?"

c "I'm going to guide us. Walk with me."

"I move sideways, keeping the pit behind us, pulling Valerius along. Sejanus seems more bored than anything else as he watches us move away from him."

sj "Do you want to pick up one of those rusty swords? You might need one to win."

v "So you can use the opening after my first clumsy attack?"

sj "What? No, I would never. But as I said, don't one of those swords look really good right now?"

"Good. Keep his mind distracted."

"We're almost to the wall. I pivot us so that we're now with our back against it."

"Sejanus begins to walk towards us, quickly, sword outstretched. Apparently he's not concerned about crushing me against the wall!"

"He's vicious now. Five strikes, seven, I can't count them."

"How is he so fast suddenly? His blows are beginning to hurt me, I can't imagine what Valerius is putting up with."

v "Fuck you!"

"Then, Valerius jerks forward. He's pushing, driving the shield against Sejanus. Sejanus backs away."

"Valerius-"

v "AHHHH!!!"

"No!"

"Bleeding from the leg, Valerius falls back against me. Sejanus charges us, swinging. The shield is limp against us."

"But Valerius steels it just in time, even as he collapses to his knees."

"I bend down, try to pull him up even as Sejanus strikes and strikes and strikes. I can feel my bones rattling around inside me. Ghosts it hurts!"

"And my love is bleeding all over the cavern floor. But we're so close!"

c "Please Valerius! Get up!"

v "ARGH!!!"

"With what must be titanic effort he rises to his feet, withstanding the blows all the way."

"I start to pull us back, along the wall. As quick as I can. Before Sejanus notices what we're doing."

"What we're backing up towards."

"I can see in his eyes the moment he understands."

sj "No. Come back here!"

"But we've already reached the door. I tear it open as Valerius defends against what sounds like a maelstrom of blows."

"Both of them are screaming at each other now, I can't understand a word of it. The terrifying howls of battle."

"But I pull Valerius back through the door, shield and all. Now the hard part."

c "Come Valerius! Hold it up and let's go!"

"The narrow gap in the stone lets me through without too much effort. Valerius slams into it sideways behind me."

"Stuck."

"I grab at him, pull him. He cries out, pushes. Sejanus is screaming, swinging at the shield with all his might."

"Everything is shaking now. Like a quake sent from the ghosts."

"And then, Valerius pops through the gap."

"And the shield, driven by Sejanus's relentless assault, is wedged into the gap, sticking between the stone. Blocking it shut."

sj "COME BACK HERE! DON'T LEAVE ME HERE! I'LL TEAR YOUR SKIN OFF FOR THIS!"

scene black
with fade

"It's pitch black, but with my hand on the wall I guide us through the widening corridor, away from the screams of the entombed man."

"Well, perhaps not entombed. His men could find him later. I suppose if he attacks that shield long enough he will break through it."

"It doesn't matter to us though. We've made it away."

"But now what do we do?"

if plan == "escape":
    jump in_the_dark
    
if plan == "clodia_poison":
    jump in_the_dark
    
if plan == "nasrin_poison":
    jump you_again
    
label in_the_dark:
    
scene Sewer
with fade

"We run through the darkness, until we come again to the first lit torches."

v "Stop."

"He leans against the wall, breathing heavily. I realize what's wrong and feel like a fool. I forgot all about his leg."

c "What... what can I do?"

"It's bleeding so much, like the color is running out of his tunic down his leg."

v "We have to tie it off. Give me your dress."

v "Help me tie this here, tight as you can."

"It's hard. The blood keeps causing my hands to slip. But eventually it's tightened as best we can get."

v "How bad is it?"

c "I don't really know. It's... it looks like a lot of blood."

v "Perfect. Oh, Ghosts of Life, don't fail me. Don't you dare..."

v "Come on love, let's keep moving."

"He limps forward a few feet at a time, leaning on the wall. I don't know what to do."

v "Let's see... by morning, Sergius will have overswept the citadel. It's possible no one will come down this far. Maybe."

c "We can go back to the cells."

"Anywhere. Sejanus may get free at any moment and come after us. In this state he would cut Valerius down in an instant. We have to keep going."

"But there's nowhere to go. We both know it."

"After hobbling down the hall, me holding him steady, Valerius stops entirely."

v "I have to stop. Moving is... I think it's making me bleed more. I'm tired."

c "But..."

"But what? He's right. We have nowhere to go."

"So he stops. Sits down against the wall."

"I sink with him. Blood glistens like a wet shadow on the stone behind him."

v "Come here."

"He pulls me close, to his chest. He smells like blood and sweat and like Valerius. My eyes burn."

"I have him again. He's right here."

"We can rest. Ghosts, please just let us rest."

"That sounds like..."

v "Soldiers."

"Damn the Ghosts then. What help have they been if it all ends like this?"

"I clutch Valerius as tightly as I can, kissing his chest through the bloody tunic. Let them kill us both. I'll make them kill me if I have to."

"They're coming. I can hear them. And they have a light with them."

"The orange glow of the torch grows around the corner up ahead, framing this underground world we're going to die in."

"And the soldiers appear. Two of them."

il "Identify yourself."

"No chance. You'd never kill the Praetrix."

v "...Immortal... Legionaries..."

"His words are so weak I can't understand them at first, but now I see what he means."

"Their tunics are purple. Everyone knows that the only soldiers in all Raskya permitted to wear purple are those in the Immortal Legion."

"And the Immortal Legion means the Basileus is here."

il "I will not ask again, identify yourself or we will kill you."

"If these aren't Sejanus's or Sergius's, then maybe there's a chance."

c "I am Praetrix Clodia Atella Fausina. Help us."

il "Praetrix? You're dressed like a common girl."

c "Never mind that, just please take us into the Basileus's protection."

il "... who is he?"

c "A soldier who has been wounded protecting me. We need a doctor for him, he's dying."

il "Where is Sejanus?"

c "Excuse me? I order you to-"

il "Tell us where Sejanus is and then we will move you somewhere more comfortable."

c "He's back that way. He's trapped back there."

il "Trapped? Great Graves."

"He turns to his compatriot."

il "Take her to the Basileus, I'll find Sejanus."

"Then he rushes off back in the direction of the golden room."

"And the other one is grabbing me. Lifting me."

"Taking me. Not Valerius."

c "Stop, wait, you have to help him first, I order-"

il "Shut up."

c "No. Valerius! Valerius!"

"I kick. I scream."

"In the torchlight I see Valerius's eyes. They're wet. But he's smiling."

"No, why?"

"You can live! Valerius you can live!"

"Why are you smiling?"

"Why?"

"I keep fighting, even though it's no use. The soldier is so strong. And he tears me away from Valerius."

"My Valerius."

"I see him behind me. One last time."

"A dark pool glimmering by torchlight around his body. His eyes black and shining. His mouth smiling at me."

scene black
with fade

"I know we'll never see each other again."

if plan == "clodia_poison":
    jump ending_praetrix_poison
    
if plan == "escape":
    jump ending_sorrow
    
label ending_praetrix_poison:

scene TempleDay
with fade

pause 0.5 

nar "The citadel is peaceful today. As it once was."

nar "Walking the halls you'd never know they were painted with corpses and lifeblood just a few days ago. What a cleanup job the Immortals do."

nvl clear

show av neu-neu-flip at right
with dissolve

a "Excuse me."

show clo reg-stoic at left
with dissolve

c "Oh. I'm sorry."

a "How exactly are you daydreaming when I am right here?"

c "Apologies, Basileus. It has been a trying time. My mind has many images I'm having trouble erasing."

a "Silly woman. I don't just leave the Imperial Palace for anything."

c "I'm deeply sorry."

a "Very well. Hmm. A shame about your husband, it would have been so much easier dealing with him."

c "We're all mourning his death, your Holiness."

a "I've had the reports from all concerned, but I'm curious. I want to hear in your own words what happened."

c "What is there to say?"

a "My Legion and I took the city, killed the traitor Sergius, and everything was perfect. We found you and Sejanus alive and well. You were wed."

a "And yet... well. Clearly I have an enemy out there. I want to know what you saw."

c "I see. There's not much to tell. Sejanus and I were married, and then we took to bed."

centered "I let him finish his quick, nasty business on me and soon he was asleep."

c "In the middle of the night, I awoke to a light breeze. The window pane had been opened."

centered "I never slept at all. I waited hours and hours until I was certain he was asleep."

c "I saw a dark figure crouched over him by the bed. I didn't know what I was seeing at first, and I was too frightened to speak up."

centered "I unstopped the vial of darkness my sweet Valerius had given me. I had failed to use it before, but now I could redeem myself."

c "Finally, I found my voice and screamed. The shadow darted off and was gone. I tried to rouse my husband, but he wouldn't wake."

c "And... there was blood oozing from his ear... oh, by the Ghosts."

centered "I poured the vial into his ear and watched. It was fun to watch him squirm, as though he were only having a bad dream."

centered "Then to watch his muscles quiver, the blood to ooze forth from his ear, then his nose."

centered "He opened his eyes briefly, looked up at me. That was the best part, that he got to see me smiling down at him."

c "I don't want to think about it anymore, it haunts me. I wish I knew who did it. I want their head."

a "Your love for your husband is admirable, Praetrix. You're a model Raskyan wife. The harlots and whores of Dagara need an example like you to look to. As many of them have lost their husbands recently as well."

c "Yes."

a "Hmm, although... you're very beautiful. How would you like to come live with me in Raskya? You can be one of my concubines, it would be a much easier life than this."

c "It's a lovely offer your Holiness, but as you said. The women of Dagara are low and nasty. I need to stay here to keep them moral."

c "And besides, you will never have to worry about access to your gold so long as I am Praetrix. I am eternally grateful to you for saving me and my husband from that betrayer Sergius."

a "Hmm... the gold is mine either way, and I really don't care if Dagara's women are low and slatternly, honestly. I think I would much like to have you around to fuck."

c "... surely you would prefer a virgin, your Holiness? I may corrupt you."

a "Ah, true. Wait a moment, how old are you again?"

c "Twenty."

a "Oh, well nevermind. You're beautiful, but I don't much prefer older women. They try to act like a mother you can fuck, and there's no manliness in it."

c "As you say, your Holiness."

a "Well, if there's nothing else, I need to get back to the capital."

c "There is one request, if I may make it?"

a "You may. Hurry it though, would you?"

c "I'm thankful to you for Transforming my husband. He'll be a fine Ghost of War."

c "But there is a... a soldier. A Tribune, actually. He died protecting me in the siege."

a "You wish him Transformed."

c "Yes, your Holiness. Nothing would make me happier."

a "Hm. What's his name?"

c "Tribune Valerius Aureus. The thing is, he was one of Sergius's Tribunes."

a "Oh. Now I can't go Transforming traitorous soldiers."

c "Perhaps you will make an exception for those who betray traitors? He deserted Sergius and, as I said, protected me. Died protecting me."

a "Ah Hell, very well, very well."

"He nods to one of his slaves."

a "Go see that it's done."

a "Wait, one second. Which kind of Ghost shall he become? I suppose Honor? Virtue? Manliness? Any of those seem to fit."

c "Love."

a "... Ah. I'm not certain my Lady Praetrix fully understands the purpose of those Ghosts."

c "He loved me, you Holiness. That is why he protected me. A doomed love that could never be, that he died for. What other Ghost could he truly be?"

a "Hm. You women and your flowery minds. But very well. I suppose I can do whatever I want."

"The tears try to come up as he says it. But I keep them down."

a "By order of Basileus XIX Avalaran, Tribune Valerius Aureus will be Transformed into a Ghost of Love."

jump epilogue_praetrix_poison

label epilogue_praetrix_poison:
    
scene black
with fade

centered "So ends one of the stranger little tales wrapped up in the drama of the Dagaran War. {p}Though most Raskyans have heard of Sejanus and Sergius, even if they could tell you no details about them, the names of Valerius and Clodia are only now coming into the popular imagination. Indeed, until recently Clodia has been known only as \"Sejanus's treacherous wife\", with the motives for her murder of him (discovered only when she confessed on her deathbed decades later) lost upon the players and the singers. {p}Now that the story is getting the recognition it perhaps deserves, one wonders if there is a Ghost of Love somewhere, dimly remembering its life, that watches over every performance of this drama sweeping the land, and blesses those that live and die in the name of Love. {p}- Alexes, from his \"Tales of the Republican Kingdom\""

centered "THE END"

return

label ending_sorrow:
    
scene TempleDay
with fade

"The citadel is peaceful today. As it once was."

"Walking the halls you'd never know they were painted with corpses and lifeblood just a few days ago. What a cleanup job the Immortals do."

sj "Of course, my scribe will convene with the royal historians to make sure all the details are clear."

a "Yeah, sounds great to me."

sj "Is there anything else I can do for you, your Holiness?"

a "You got a daughter?"

sj "Haha, I'm working on it."

a "Damn. I need two new concubines. One of mine killed another, so I had to have her killed. And anyway, I like how milky pale Dagaran girls are."

sj "You're too kind to our lovely beauties, your Holiness."

a "Hmm, your wife's beautiful. I suppose I could demand you let me fuck her. Would that bother you?"

sj "Well, I'd like to be certain that any baby that grows inside her is mine. Otherwise I don't care."

a "What do you think, Praetrix? Would you like to fuck a Basileus?"

c "... I'm sure it would be a great honor."

a "By Ghosts, what a gloomy woman. You just got married, you should be happier than you'll ever be again."

a "Nevermind. I'm sure there's some comely noble daughters around. Argh, I hate the air up north, it dries out my skin."

sj "I'm sure your Holiness will want to return south to the capital as soon as possible."

a "Tomorrow. Once I've picked my new girls."

a "I'll say Sejanus, this couldn't have come at a better time. I have a splendid little war I'm cooking up, and I'll need that gold to get things rolling."

sj "A war? With whom?"

a "I'm not sure yet, I have to figure out someone that we can easily beat and take all their stuff."

sj "You have a very refreshingly straightforward view of war, your Holiness."

a "I'm glad you think so, since I'm putting you in charge."

sj "Haha. I'd be honored."

a "So prepare yourself to levy legions again. This time next year I'll be sending you off to kill... somebody."

a "Ugh, if only Peladon weren't so powerful. We don't have nearly enough Peladonian slaves, they're such good laborers."

"They go on and on and on like this for some time. I keep fading in and out. I should listen, I know."

"I should try to keep on living. I should try to do something to keep my life worthwhile. Surely Valerius would want that."

"But..."

sj "You heard him."

c "Huh? I'm sorry?"

sj "Wipe that sad look off your face. If you're going to daydream like a twit at least look pretty while you do it."

c "Yes, my lord."

"But it's hard."

"It's so hard."

"Valerius."

"Valerius."

"Please, let me die too."

jump epilogue_sorrow

label epilogue_sorrow:
    
scene black
with fade

centered "The great love story of Raskya indeed. {p}A little sideshow in the midst of the rise to world domination of the Raskyan colossus. {p}A brief love snuffed out by war and politics. {p}A loving heart trapped in a heartless marriage. {p}And the Raskyans call this their golden age! {p}Is it any wonder that the tender hearted all go to Durja? {p}- Eumenion, from his \"The Three Great Civilizations\""

centered "THE END"

return

label you_again:
    
scene Sewer
with fade

"We run through the darkness, until we come again to the first lit torches."

v "Stop."

"He leans against the wall, breathing heavily. I realize what's wrong and feel like a fool. I forgot all about his leg."

c "What... what can I do?"

"It's bleeding so much, like the color is running out of his tunic down his leg."

v "We have to tie it off. Give me your dress."

v "Help me tie this here, tight as you can."

"It's hard. The blood keeps causing my hands to slip. But eventually it's tightened as best we can get."

v "How bad is it?"

c "I don't really know. It's... it looks like a lot of blood."

v "Perfect. Oh, Ghosts of Life, don't fail me. Don't you dare..."

v "Come on love, let's keep moving."

"He limps forward a few feet at a time, leaning on the wall. I don't know what to do."

v "Let's see... by morning, Sergius will have overswept the citadel. It's possible no one will come down this far. Maybe."

c "We can go back to the cells."

"Anywhere. Sejanus may get free at any moment and come after us. In this state he would cut Valerius down in an instant. We have to keep going."

"But there's nowhere to go. We both know it."

"After hobbling down the hall, me holding him steady, Valerius stops entirely."

v "I have to stop. Moving is... I think it's making me bleed more. I'm tired."

c "But..."

"But what? He's right. We have nowhere to go."

"So he stops. Sits down against the wall."

"I sink with him. Blood glistens like a wet shadow on the stone behind him."

v "Come here."

"He pulls me close, to his chest. He smells like blood and sweat and like Valerius. My eyes burn."

"I have him again. He's right here."

"We can rest. Ghosts, please just let us rest."

n "There you are."

c "You!"

n "Is he dead?"

v "No I'm not dead, you greedy vixen."

n "Well that's a surprise. Where are you hurt?"

v "Leg. Here."

"She's suddenly down on her knees, pulling something from within her cloak and then touching Valerius's wound. He winces, but otherwise seems to be relieved."

c "What is that?"

n "Should make it stop hurting and bleeding. I can't believe you don't carry some of this around with you at all times Valerius."

v "Where have you been?"

n "Looking for you."

c "Thank you."

v "Don't thank her, she would leave me to die if I didn't have a bunch of gold squirreled away."

n "You're so stupid. I don't know why I'm even bothering. Let's just go so we can get this over with."

v "You have a way out? The city is completely occupied by Sergius. Those men are just as like to hurt us as help us, considering."

n "The city is quite empty, actually."

v "How is that possible?"

n "Sergius had to sally out."

v "To what?"

n "The Immortal Legion."

c "The Basileus is here?"

n "Yes, and he's taken Sejanus's side. So I suggest we hurry and leave before the battle they're having outside the city is over and the Basileus rides in here to lay down the law."

n "He's already got an advance guard past the walls looking for Sejanus and you, noble girl."

"Sejanus was telling it true then, when he said the Basileus favored him. This was all planned from the beginning."

v "Lead us, then."

"She seems more than happy to."

"I would pay her anything she wants right now. We're getting out, Valerius."

"We're free, my love."

jump ending_farewell

label ending_farewell:
    
scene black
with fade

"We escaped."

"Nasrin led us out of the tunnels, out of the city."

"Out in the fields beyond the city walls we could see the distant mass of men slaughtering each other in Sergius' final battle."

"We ran."

"Valerius's wound pained him all day and night, but the bleeding had stopped. By some miracle, he didn't wake the next morning in the cold sweats of a fever."

"We made a fire in the woods every night, and I would cradle his head in my lap as he slept. Stroke his hair. Look at him."

"Nasrin stayed away from us, her form just barely traced by the firelight. That was fine with me. I was grateful to her, but..."

"I didn't like how she kept looking at Valerius. She looked sad."

"The three of us traveled south. Crossed into Raskya. We passed through the city of Marasima, just long enough for Valerius to collect all the money he had deposited with the goldsmith there. Then we were off."

"To the coast."

scene Forest
with fade

v "There. That should more than cover all you've done for us."

n "Hmm... yeah. Seems about right."

v "Don't act so flippant, it's a lot. Would have been nice to hang onto it."

n "What's it matter, you already paid for your spots on the ship, right? This here should have me set for a while."

c "Well. We thank you for your service."

n "Uh-huh. But I think you'll be more grateful once I'm out of sight."

"She's not wrong, but..."

"Valerius."

v "I don't see why you'd want to stay in Raskya."

n "Hm? What's wrong with Raskya?"

v "Well, it's... it's not as cultured as Durja."

n "Maybe not. But the men are hornier and more desperate with their coin. I'll get by."

v "I'm serious, Nasrin. Come with us."

"Love, what are you doing? We're nearly free of her!"

n "Ha ha, oh my. Why would I do that? Go to Durja with you? Shall I live with you two as your hetaira?"

c "Ghosts no."

n "Ha. Sorry Valerius."

v "That's not what I'm asking, and you both know it. But Raskya is no land for you, Nasrin. At least return to Peladon."

n "I can't ever go back there. You know, there's a reason I left in the first place."

v "So go anywhere else. But this ship can take you there. Or if you insist on staying, here, at least take some more money."

c "Valerius."

n "Listen to your lady, you paid me enough. What do you care?"

v "..."

n "... Don't care for me. I'm not your responsibility. Worry about your own happiness. Besides, I..."

c "Please Valerius, we should get on the ship."

v "What, Nasrin?"

n "... I can't bear being around you two for a second longer."

v "... ah."

n "... so... yeah. Farewell, Valerius. Clodia."

c "Mm."

v "Goodbye Nasrin. Be safe."

n "Ha. Safe. Nowhere's safe."

"She turns and walks away. Disappears around a corner. At last, we've lost her."

"At last."

c "Dear?"

v "... I'm sorry about that. Let's go."

"He leads me arm in arm to the ship. I'm not looking forward to the journey, but the destination will be paradise."

scene black
with fade

"He looks back over his shoulder a few times. When we board the ship, he stays on the deck for a bit. Keeps looking back at the dock."

"Finally, the ship departs. Valerius watches the docks as the ship pulls away."

"He keeps watching. Until we've gone so far out that, I imagine, no person could swim the distance safely."

"Only then does he, smiling at me through distant eyes, lead us down below deck."

jump epilogue_farewell

label epilogue_farewell:

scene black
with fade

centered "The historical trail of Valerius and Clodia goes cold here. {p}Although many tragedians delight in making more of this than is sound, such as inventing a shipwreck that befalls the lovers on their journey, or having them enslaved by Durjans upon arrival, it is known that the ship survived the two week journey, and they were likely not turned away from their destination as the famous Durjan isolationism was not implemented until about twenty years after this time. {p}The fate of the freedwoman Nasrin is also of enduring interest, as references to her in the sources are minimal (she is named only twice, with one other fleeting reference in Alexes to the \"slave that Valerius loved\") and her fate after this point is also unknown. {p}These are the realities the historian must accept- that the destinies of these persons and stories are lost to the aether.{p} - Ben Harrington, from his \"Conquerors: A History of the Ancient Raskyans\""

centered "THE END"

return

label murder_attempt:
    
scene ValeriusRoomNight
with fade

"Ghosts. Tomorrow morning we take the citadel."

if message == "hidden":
    jump murder_attempt_a

if message == "shown":
    jump murder_attempt_b
    
label murder_attempt_a:
    
"It should go like lightning, with nearly the whole army on the assault."

"I have to make sure I'm in the first wave. I have to get to Clodia before anyone else. Then I can give her my protection as tribune."

"The men will sneer and berate and hate me for not letting them fuck her, but they'll ultimately respect authority."

"Probably."

"But only if I get there first."

jump murder_attempt_c
    
label murder_attempt_b:
    
"I hope Sergius spares enough men for the siege to go well."

"We reckon there's about three hundred men holed up in there, so I want at least four hundred of my own. Surely Sergius will grant that much, if he thinks he's dead anyway. If he thinks he can at least ensure Sejanus's death."

"At four hundred men will be much easier to control than a larger force. With me officially in charge, maybe I can even order some of them to find Clodia and keep her safe until I get there..."

"Yeah, that will go over very well..."

"I'll just have to figure out something."

jump murder_attempt_c

label murder_attempt_c:
    
"Tomorrow... I'll see you at last tomorrow, my love."

n "..."

v "Oh. Nasrin. Where's Clodia?"

n "... She's still locked up. Our escape attempt failed."

v "Obviously. Well, glad you're still alive."

"I thought I had lost this asset."

v "Where have you been?"

n "Surviving."

v "No doubt. And otherwise, nothing has changed?"

n "Not really."

v "Very well. Tomorrow we break down that door and finish this. Then the Basileus will arrive and finish it all again, but I plan to be long gone with Clodia by then... ah, why am I telling you?"

v "I need to think of something useful for you to do."

n "I know something."

v "Hm?"

"Her cloak falls like water from her body."

"She crosses the room with the grace of a specter, grabs my tunic."

"Reaches up to kiss me."

"What?"

v "What are you doing?"

"But then her hand is grabbing between my legs, and she pushes her small body against me. Hard."

v "Stop. I'm not paying you for this."

n "Relax. You should. I'm good at it. Remember?"

v "I'm sure. But I will not be unfaithful to Clodia."

n "You aren't married to her. Mine may be the last cunt you ever feel as a free man."

v "Stop this right now, I said get-"

"What-"

"Why-"

"Stop."

"STOP!"

"I push her away and I'm on my knees."

"It hurts. The bitch stabbed me. I can feel razor sharp warmth trickling down my chest, my stomach."

"She's coming at me again, her Peladonian knife in her hands, red. Her eyes are blank."

"I don't understand."

"She's fast. Already upon me again. I try to rise to my feet and strike at her, but she dodges it like a gust of wind. Goes under the blow and-"

v "FUCK! Nasrin! Stop!"

"This time one of my crazed, pained blows strikes her in the head and she reels back."

"I need a weapon. There's a sword in here, somewhere, something, FUCK, where?"

"Again she comes at me, silent and stone. I back away from her, defensive. She hesitates, looking for an opening."

"That's got to be her whole tactic, she's so small and fast. If I turn away from her even once I could be dead."

"I spot the sword I need in the corner, laying against my bloody armor. I need to reach it."

"Slowly, I start to move that way. I can feel the blood running down my legs now."

"Three wounds. Three bloody rivers."

"And the traitorous bitch never takes her eyes off me as I try to reach my weapon."

"She knows what I'm doing, of course."

"She charges."

"When she comes close, I kick at her. My foot drives right into her gut, and I push as hard as I can."

"She falls back and tumbles to the floor, but she swings that razor wildly as she falls, and-"

"-it slashes my leg."

"I collapse to the ground as agony surges through my limb. But the sword is right there, in reach."

"I grab at it just as the girl comes at me again."

"She is so fast. I turn to swing the blade and she's on top of me, one hand reaching to intercept my sword arm, the other driving the dagger towards my eye."

"My blade sinks into the flesh of her hand. And somehow, her dagger misses my head. Plunges into the floor right near me."

"She doesn't scream. But I push the sword as hard as I can, cleaving through her hand."

"She moves back, tries to get off me, to get away."

"But I swing the sword again and slash the back of her leg."

"She crashes to the floor, struggling. Silent."

"And yet, she's still in my blade's reach."

"I jab the blade into her fallen body. Once, twice."

"Screaming, I rise to my feet, sword in hand. I bring it down on her back, as hard as my weakened arms can."

"Then again."

"And again."

"Bitch. Traitorous whore."

"You went over to Sejanus, didn't you? Well, what money does he even have when he is soon to be a dead man?"

"You stupid bitch. Fuck you."

"Were you lying about Clodia being fine?"

"Is she dead?"

"The blade finally drops from my aching palms. Nasrin is... more than dead. Butchered."

"Good riddance."

"..."

"..."

"Ah Nasrin, why? Why did you..."

"Ghosts damn you Nasrin."

"I can hear people coming."

"Then I see them in the doorway. I can't tell who, it's suddenly hard to see."

v "She... she tried to kill me. From Sejanus. She..."

"Clodia..."

scene black
with fade

"I'm sorry. I'm so sorry I died like this."

jump ending_sacrifice_war

label ending_sacrifice_war:

"..."

"..."

"... What is going on?"

"I'm sore. Actually, fuck the Ghosts, it hurts."

"I..."
 
scene ValeriusTentDay
with fade

"I'm in my bed. There are slaves around me, dabbing at my body."

v "What happened?"

"They are so good at being invisible that they have forgotten to listen."

v "What happened to me?"

"Nothing. And I'm too sore to yell at them, I can only whisper."

v "How long? How long have I been asleep?"

"Nothing. They are useless."

"I have to go."

v "Get off me."

"At last they obey, and back away from me."

"Ghosts of the Grave, it hurts."

"My stomach. My back. She got my legs too, I can't tell how many places. And my fucking arm."

"The slaves bandaged me up, but the bandages are already spotted with blood."

"I have to go."

scene StreetsDay
with fade

"The camp is rather dead. Only a few slaves are about, and the camp followers. The legionaries are all gone."

"And smoke is rising from the citadel."

"Clodia."

"I have to go."

"It hurts. Every step, every breath feels like it's peeling open a new wound somewhere inside me."

"But I have to go."

"The front doors of the citadel are wide open, their wood torn and splintered. The battering ram is discarded."

scene black
with fade

"Hobbling through the gates, I step into the Grave itself."

scene HallwayBloody
with fade

"Corpses."

"Blood."

"Entrails."

"That smell. The most awful smell I've ever known, that I've spent most of my military career trying to avoid."

"Stinking dead bodies in a small space. They line the floors, paint the walls."

"Every step forward my boots suck at the blood, leaving bubbles in my wake. Fresh blood is good. Means Sergius can't be too far ahead of me."

"Sejanus and Sergius's men are indistinguishable. Red flows out of their eyes, their skin, bleeds from their tunics and their armor and their blades. Everyone is red, piled upon one another."

"This one killed that one, then took a sword in the back. That one lost his arm and seems to have sat down, as if waiting for the battle to be over. These two ran each other through."

"Dead slaves too. Not just the attendants and the servers and cleaners, but the bedslaves too. Some of them are naked, fucked before their throats were slit."

"Why? I've never understood. Never understood any of this."

"But {i}they{/i} all get it. They've always gotten it. This is their great glory."

"I take a cursory glance into some of the rooms as I pass by. Just in case."

"Some of the rooms are empty."

"Some are full of more dead soldiers."

"In one, a naked, bloody girl stands amongst a heap of corpses. She looks dead even as she stands. I leave before she can look at me."

"I open another door. Two soldiers on the other side, laughing, their pants down. A little noble girl is between them. I think they invite me to join in."

"Another door. Three girls hung from the rafters. I can't tell if they did it to themselves or not. Their breasts have been sliced off, but it could have been done afterward. One of them has \"Dagaran Whore\" cut into her belly."

"Another door. A man lies on the floor in pieces. His arms are over here. His legs are over there. He asks me for a drink of water."

"Another door. A soldier sits there, armed, a cup of wine in his hand. He is covered in blood and smiling."

"He raises the cup."

so "Hail, Tribune Aureus. Ghosts, you look like you ran into a spot of trouble."

"Oh. He must be one of mine."

v "Where is Sergius?"

so "Oh, he went straight up top with the rest of his guard. To the Temple up there, I guess that's where Sejanus is holed up."

so "Would you like some wine? This Dagaran stuff's actually pretty good."

v "No. I must go."

so "Oh, I've got one of Sejanus's little brown bitches here if you'd like to give her a go."

"He nods to the corner. There's a girl tied by her wrists to a bed, unmoving. I leave."

"The temple. Up top."

scene black
with fade

"I climb stairs. Agonizing stairs. Eternal stairs."

"But I have to."

"Clodia. Clodia. Clodia."

"It hurts so much and I'm on my way Clodia, I'm almost there."

"Almost to the top and almost there and we can be together soon and get away from all this and I can just wake up and everything smells so bad and I miss you Clodia."

"I'm close enough to hear screaming now, coming from everywhere. Or nowhere. Is it in my head? I can't tell."

"I feel very drunk. I feel very..."

"I throw up."

"Rest against the wall."

"Catch my breath."

"Press on."

"And at last, the temple."

scene TempleDay
with fade

"This is the cleanest room I've seen. The most orderly. The mass of soldiers here stand around chatting, cheering, congratulating themselves. They're hailing."

"Sergius stands in the middle of all of them, beaming, raising his hands to the sky."

s "Oh Ghosts of War, this great sacrifice is for you! Drink the blood of traitors, and the tears of their women! Bathe in their lives, oh Ghosts, take them into you!"

s "I, Gaius Argus Sergius, your greatest servant, offer it all to you! May your bloodlust be sated, may our glory be eternal!"

s "For all the Ghosts of War, let the blood of traitors run down the walls, seep into the barbarian dirt and Transform it into Raskyan soil!"

"The cheers go up. The men embrace each other, hold their shields and spears and swords aloft."

"The Ghosts are here, summoned at last, filling the room with their power. These men have lived for this moment."

"I hobble through them all."

s "What is this? Oh, you're still alive? I hate to inform you that you've missed everything, Tribune."

v "..."

s "Sejanus is dead. See?"

"He points to the wall behind him. There, Sejanus's head sits upon a spike, black blood dripping from his lips. He has a strange grimace on what is left of his face."

v "Where... where is Clodia?"

s "Hmm? Oh, the Praetrix? She's over there. You were a little too late for any fun, but then, we all were."

"Beyond him. Laying there in the corner."

"I go to her."

"She's..."

"Her wrists are open. Everything that kept her alive has drained from her. She was wearing something white but no more."

"Oh, she looks so empty, so shrunken."

"For a year I've fought day and night to keep her face in my mind, to remember her."

"And she's here."

"She's here."

"I'm here, Clodia."

s "After all these years I still don't get why some women would rather die than take a few cocks. A shame. She's so beautiful, it would have been a fun time."

"I want to lay down with her. I want to pick her up in my arms and laugh with her. I want to put all the blood back inside her and sew her up and... and tell her..."

trh "Uh, Legatus?"

s "What is it?"

trh "Look. Out the window."

"No Clodia... Clodia, is this real?"

"Please, speak to me. I want to hear your voice again. I... I can hardly recall it now. It was beautiful and sweet and filled my soul and I can't remember it."

"When I look at you like this I can't... I can't remember your smile."

s "What in the fucking Grave is that?"

tra "They've... they've got the whole city surrounded."

trh "Fuck surrounded, they're already in! They're attacking the camp!"

s "That... that traitorous little boy. Why?"

s "I've done it. I've done everything! This is my thanks?"

"I suppose the Basileus finally arrived. And his six thousand Immortal Legionaries."

"He'll kill most of them. Probably take Sergius captive, execute him later."

"I won't be around for that, one way or the other."

"But... your blade, Clodia. Here it is. The dagger that saved you from them when I couldn't."

"My pain feels... distant, when I hold your blade."

"Your face... you look so sad."

"I'll see you smiling again soon, my love. This is not the end for us."

tra "We have to do something. I'll rally my men. We can hold the citadel."

s "The citadel has no door anymore, you idiot. He's coming in."

"I've joined them at the window. Sergius is beside me, statue still. Fixated on the sight of little purple men outside in their thousands swarming the city, burning our tents. Nearing the citadel."

"Good. Look upon them, Sergius. Please understand that you've lost."

"Please understand that everything you've done is for nothing."

"Please."

v "Legatus."

"He glances at me. He is fully armored. Except for his head of course."

s "NO!"

"The knife darts into his eye. Then the other. A third stab into his neck, driving, pushing. One of his cheeks almost comes away with the fourth stab."

"By the time he raises his hands I've already pierced his head half a dozen times."

"He's falling. His face is a glorious crimson. He looks like a Ghost of War."

"The tribunes scream."

"I can feel them surrounding me. I can hear their swords coming free."

"I look over in the corner. At the body that once was my Clodia."

"I try to remember her face as their swords kill me."

scene black
with fade

"Ghosts of War. Drink up."

jump epilogue_sacrifice_war

label epilogue_sacrifice_war:

scene black
with fade

centered "For years, the death of Gaius Argus Sergius at the climax of the Siege of Dagara was treated as an incident to be observed in a bubble, as though the reason one of his own tribunes cut him down required little to no explanation. {p}Most historians before now have tended to assume that it was a desperate honor killing to prevent Sergius from falling into the Basileus's hands.{p}But now we know better, and the great sacrifice to War that Sergius wished for so dearly has now become overshadowed in the popular imagination by his righteous death at the hands of a vengeful heart. {p}When asked \"Who was Gaius Argus Sergius?\" your average capital citizen is likely to reply \"Why, the man who killed Clodia, and damn him to the Grave for it.\" {p}- Alexes, from his \"Tales of the Republican Kingdom\""

centered "THE END"

return

label grave_nightmares:
    
scene ValeriusRoomNight
with fade

"Ghosts. Tomorrow I can finally end this."

"With Sergius's help we'll storm the citadel, kill Sejanus, save Clodia, and then her and I can slip away on a horse before the Basileus's army arrives."

"We'll cross the sea to Durja. They don't value war for its own sake over there, they value life and pleasure and the finer things."

"They aren't these stern, vicious men who see no point in anything that isn't some kind of competition. Feminine and weak, the Raskyans call the Durjans."

"Well Sergius, I'd rather be feminine and weak and happy than like whatever the Hell you are."

"Two fully armed men enter my room. Curious."

v "You're forgetting to hail your Tribune."

so "Come with us, Tribune. You've been summoned by the Legatus."

"... oh have I?"

v "Hmph. Wants to talk tactics now, does he? Very well. Step aside."

"They do not move."

so "We are to escort you, Tribune."

"And then their hands are on me."

"Pushing me ahead of them."

"No. What did I do wrong? What is going on?"

scene CampNight
with fade

"Outside my tent, there are men at attention everywhere."

"I... I can hear drums."

"I..."

"I..."

so "Move."

v "Wait. Wait there's been-"

so "Move!"

"I'm shoved forward, nearly tripping. Countless eyes are set upon me. Some grim. Many angry. Enraged, even. The few smiles are sadistic."

"No."

"No no no no no no no."

"Somehow, I manage to walk. What else is there to do? There is a path through the soldiers, walking through their hateful gazes."

sos "Traitor."

sos "Turncoat."

sos "Sejanus's little bitch."

sos "And the Basileus's too."

"No no no no no no no no how did they find out what did I do wrong I don't understand how I did everything right I did everything right I did everything right."

s "There you are."

v "... What is the meaning of all this?"

s "The meaning? Hmph. This."

"He holds up a golden scroll. My mouth is dry as a desert."

s "This scroll is an official communication from the Basileus Himself! One warning of an imminent attack from the Immortal Legion upon us, tomorrow!"

s "We have all been betrayed. By our Basileus. And by this man, Tribune Valerius, who intercepted and hid this scroll from me."

"Were Sergius not standing right here I think they would tear me to pieces on the spot. The abuse they hurl upon me is so loud and vicious I can barely comprehend it all."

s "I thought last night's drunken attack was suspicious. So I had my spies do their nosing around."

s "I never imagined that they would have eventually brought this to me from your tent, Tribune! Squirreled away, hidden safely from my eyes!"

s "So what is it, then? You've been a spy for Sejanus this whole time?"

v "No. No, of course not."

"My words are so weak. I can barely find strength in my throat to speak."

s "Then the Basileus."

v "No."

s "Are you to imply there's a third foe seeking the destruction of this legion? Or are you lying still?"

v "I... you..."

s "Speak! Speak boy, speak your confession. Or die without it, I care not."

"Now even the crowd has gone silent again. They want to hear this."

"You callous, selfish fools. Not a one of you would understand. You're all too cold and hard."

"Fuck you."

v "I, Tribune Valerius, acted alone in working to hinder our legions. Not for pay, not for Sejanus or the Basileus or anyone else."

v "I did this because of you, Sergius."

"The confusion on his face is the only small pleasure I can find in this."

s "To spite me?"

v "No. I care not and never have for your opinion. But you made it clear, before this siege even began, that you will kill everyone inside that citadel."

v "You fanatic. All for the favor of monstrous war ghosts."

s "Ooh, a blasphemer as well. This is too good."

v "The woman I love is in that citadel, Legatus."

"Silence."

s "... heh. Heh heh. Ha ha ha ha ha ha ha ha!"

"The entire army seems to be laughing at me."

"Bastards. Heartless bastards, every one."

s "A blasphemer and a woman as well. I knew you were effeminate but I never dreamed you would sell out the lives of your countrymen for the sake of a cunt."

v "Then kill me. All of you will die tomorrow anyway. I'll take pleasure watching from beyond as each and every one of your pathetic, worthless lives are agonizingly snatched away."

v "And I'll laugh at your tears, as you laugh at mine. I'll take pleasure in your suffering, as you do mine."

v "Then I suppose I will finally be a good Raskyan. Fuck all of you."

"Let them do with that what they wish."

s "It's a shame. Just as I was beginning to think I had you wrong. Throw him in."

"They shove me forward again."

"I need a sword. I need to kill myself before-"

"There's a hole in the ground before me. Just the size of a man. Not a grave. But a pit."

"Of course. Of course they wouldn't waste the effort of a grave for me when Dagara has its infamous Pit."

"I- I can't-"

v "NO!"

"I struggle. I kick, I bite, I push."

"My head... someone struck me... please, no. Don't do this to me."

"Don't throw me in."

"I don't want to die like this."

"I want to see Clodia again."

"Clodia."

v "CLODIA!"

scene black
with fade

"I'm falling."

"Things are hitting me. Rocks. Stones."

"I see nothing. It hurts."

"I plunge into water."

"Shallow water."

"The pain."

"My legs."

"My arms."

"I can't move."

"I can't see."

"Please, let me die. Can't I die yet?"

"Insects. Crawling all over me. Biting me. Writhing."

"They're on my skin, in my eyes, my mouth. Biting. Biting, biting. Eating me."

"The water burns... it burns... fire."

"Acid. Poison. Dissolving my skin. Smells like shit."

"I'm falling apart."

"Disappearing."

"Help."

"Help."

"Help help help help help helphelphelphelphelphelphelphelphelphelphelphelphelphelphelphelphelphelphelp"

jump epilogue_grave_nightmares

label epilogue_grave_nightmares:
    
scene black
with fade

centered "Those who read the story of Valerius and Clodia for the first time, whether Raskyan or otherwise, often find themselves, at best, perplexed. {i}This{/i} is the great love story of Raskya? {p}Indeed, it could be said to neglect sentiment for tragedy at most turns. But recall that this is a true story, and the ghosts are stranger playwrights than humans, often working purely within the realm of symbolic, truer meaning than satisfying narrative. {p}The noble Raskyans, for all their admirable qualities, were a traditionally loveless society. From marriages that were social contracts for even the poorest classes, to a culture that saw expressions of romantic tenderness as effeminate at best and non-Raskyan at worst, this was a people that even the passionate Raskyans of today feel apart from. {p}It is only fitting that their great love tale is as morbid as it is. {p}- Alexes, from his \"Tales of the Republican Kingdom\""

centered "THE END"

return

label mistress:
    
scene ValeriusRoomNight
with fade

"Ghosts. Tomorrow morning we take the citadel."

if message == "hidden":
    jump mistress_a
    
if message == "shown":
    jump mistress_b

label mistress_a:

"It should go like lightning, with nearly the whole army on the assault."

"I have to make sure I'm in the first wave. I have to get to Clodia before anyone else. Then I can give her my protection as tribune."

"The men will sneer and berate and hate me for not letting them fuck her, but they'll ultimately respect authority."

"Probably."

"But only if I get there first."

jump mistress_c
    
label mistress_b:
    
"I hope Sergius spares enough men for the siege to go well."

"We reckon there's about three hundred men holed up in there, so I want at least four hundred of my own. Surely Sergius will grant that much, if he thinks he's dead anyway. If he thinks he can at least ensure Sejanus's death."

"At four hundred men will be much easier to control than a larger force. With me officially in charge, maybe I can even order some of them to find Clodia and keep her safe until I get there..."

"Yeah, that will go over very well..."

"I'll just have to figure out something."

jump mistress_c

label mistress_c:
    
"Tomorrow... I'll see you at last tomorrow, my love."

"In the meantime... where is Nasrin? She left this morning to sneak back in, but I haven't heard from her since."

"It's likely she's stuck inside. Or worse."

"Ugh. Don't think about that."

"I shouldn't concern myself with Nasrin so much. There's naught there but lust and temptation."

"Mmm... and the way the little whore parades about around me. Teasing me, enticing me."

"This past year has been nothing but a series of frustrating displays from her."

"I shouldn't remember them..."

"... the time I came back and found her naked on the bed with a jar of oil asking for a body rub..."

"... the times she goes naked under her robe and flashes little glimpses of herself..."

"... that time she joked it was a shame that I hadn't gotten around to branding her with my name before I freed her..."

"Damn. I suppose it's a miracle I haven't given in and fucked her. It's what any Raskyan man would do in my situation."

"Ghosts of Lust, thank you for giving man a left hand. It's been exceedingly useful this past year."

"I need to use it now, I think."

"Oh."

n "Hey... what's wrong with you?"

v "Nothing. You're... I'm glad to see you're alive."

n "Oh are you?"

v "Why wouldn't I be?"

n "Hmm... well, anyways, I have strange news to report."

v "Strange? How is Clodia? Did you get her down into a cell?"

n "I did. And then the little noble went and wandered off for some reason."

n "So I tracked her down again and, uh... well..."

v "Is she okay?"

n "She's fine. But Sejanus caught her."

v "Damn. Are you serious? By the Ghosts, Clodia, why didn't you stay put?"

"What to do now? What to do? Damn, this is such short notice to figure things out."

n "It's not just that. Clodia found something."

v "What do you mean?"

n "Promise to believe me?"

v "What does that mean? Of course."

n "There's gold underneath the citadel. A lot."

v "You mean a treasure horde?"

n "No, I mean like gold rocks, stones, walls. A cavern made of gold, more than I've ever seen in my whole life combined."

n "It looks like enough gold to make another Basileus."

"Ghosts of Greed. I understand now."

v "... I see. That makes sense. And Sejanus knows about it?"

n "He was there checking on it. That's how he caught her."

v "Hm. Well then."

n "What?"

v "That rider I was waiting on came back. But it wasn't the same one we sent out. I think he's probably dead."

v "This one was from the Basileus, bearing an official scroll. Sergius has been declared a traitor and outlaw, and the Basileus has sided with Sejanus."

n "That fits with what Sejanus said."

v "Now the Basileus and the Immortal Legion are on their way. They'll probably get here tomorrow, by noon at the latest."

n "I guess even the Basileus can't ignore all that gold. This was a double-cross from the beginning."

n "Can't say I'm too surprised coming from perfidious Raskya."

v "It certainly complicates things..."

n "So... what are you going to do?"

v "Storm the citadel at first light. Get to Clodia before the other soldiers and declare her my captive, that should keep her safe."

n "But after that, I mean. What good is saving Clodia if the Basileus comes and kills everyone anyway?"

v "... well, I need to figure out that part still. I suppose we could escape through whatever passage you've been using?"

n "That's not going to be possible. All the ways in and out of the city I know have been discovered and put under heavy guard."

v "What, there haven't been guards so far?"

n "There have been. But the methods I used to get past them won't work on the new guards."

v "Meaning?"

n "Sejanus seems to have found every boylover in his legion and made them his sentries."

v "I'm surprised it took him this long. Damn. How did you get out if there's no more ways then?"

n "I slipped past the guards."

"Little fox."

n "I doubt that either you or Clodia can do the same. So, I have no way to get all three of us out."

v "... very well. I just have to think about all this and... I'll come up with something."

n "Well there's one other thing you should know about."

v "What's that?"

n "Clodia went back with Sejanus after making a deal with him."

v "What deal?"

n "In exchange for sparing your life if you survive the battle, Clodia agreed to marry him."

v "He's making her marry him no matter what. Any 'deal' is just her bargaining a concession, it's probably the best she can do in her situation."

"With a great show of a sigh, Nasrin pulls a scrap of papyrus from her cloak."

n "She wrote this for you."

v "..."

"Taking it from her, I sit down at my desk and begin to decode it. It seems to use our cipher, so it's indeed Clodia's."

"..."

"..."

"..."

"I've been trying to avoid reading it as I decode it. I've only caught parts here and there, but... surely they'll make more sense once I read the whole thing."

"Here we go:"

centered "My love. My sweetest. My red Raskyan. I need you to listen to me. The Ghosts have not sided with us. There is a colossal gold vein beneath the citadel, and Sejanus has promised it to the Basileus. Soon everyone associated with Sergius will be dead. So I beg you to flee. We tried, Valerius. But no matter what, I will become Sejanus's wife tomorrow. He will be the law in Dagara. Therefore, you must forget me and save yourself. I want you to live. Live and be happy. Love another. Since the Ghosts have not seen fit to grant us a life together, let us at least seek lives apart, and not your meaningless death. Do not try to save me. I will weep no matter what for some time. But if you live, if you flee, I will weep for joy. If you stay and die, I will despair. I loved you with a fire I'd never known before. Do not let the soul I love more than any other disappear from this world. Good bye, my Valerius."

"..."

"..."

"..."

"I can't be terribly surprised she feels this way. From her point of view it must seem hopeless."

"I have to figure something out quickly."

v "Well..."

n "?"

v "She and I will have a good laugh about this letter once this is all over."

n "What does it say?"

v "Never you mind. Now, leave me, I need quiet to come up with-"

"In a flash she sweeps the decoded message from my desk. I don't suppose I'd even be swift enough to take it back from her if I tried."

n "She wants you to flee."

v "Yes, I read that."

n "But you aren't going to?"

v "Of course not. And leave Clodia in the arms of that villain?"

n "But that's what she wants."

v "What she wants is the best possible outcome. She's simply lost hope that there's a better outcome than this, that's all."

v "It makes perfect sense. I'll just have to give her hope again."

n "I think she would know better than most just how hopeless things are."

v "Well I'm not hopeless. There's always a way, always, I just need to think enough. And you're distracting my thinking."

n "Shut up and listen, will you? You didn't see how she agonized over this letter. I'm surprised you can't see it in the writing, or won't let yourself see it."

n "This is what she wants more than anything right now."

v "She would take a better choice if she knew about it."

n "There is no better choice, that's the whole problem here. She wants you to take the best option, the only way you can both live."

v "I will not be a coward and leave the woman I love!"

n "Then you don't respect her wishes at all."

v "... ridiculous. Listen to yourself."

n "No, you listen to yourself. Hell, read the fucking letter again. She couldn't make it any clearer."

"How dare you argue with me about this?"

v "You're biased."

n "Excuse me?"

v "Of course you think that's a good idea. You want Clodia out of the way, you want things to go back to how they were before I met her."

v "Hell, I wouldn't be surprised if you forged this, you're clever enough."

n "Now you're the one who sounds ridiculous. But I suppose it makes sense you'd have no care for her wishes, you barely know her."

n "You fucked her every day for a month and then left and have written three letters to her. You don't-"

v "Silence. Do not do this to me, it's cruel."

n "Throwing your life away for noble cunt is cruel."

v "Oh, of course you would think this is just about cunt. If all I wanted was cunt I would have kept you under my yoke."

n "Not cunt, you fool, noble cunt. Every Raskyan man wants to fuck ladies and princesses. What better way to feel like a lord or a prince?"

v "So you're bitter you're a brownblood like me?"

n "I'm bitter you think a brownblood isn't good enough for you."

v "You..."

n "But anyway, there's another thing you're wrong about. I don't need you. Once we're out of here, we can part ways no problem."

n "So no, I didn't forge the damn letter."

"She tosses it at my feet and I scramble to pick it up. I can't let it be damaged, it's the last words I'll ever read of hers."

"The last words..."

"..."

"..."

"I read it again."

"..."

"And again. I linger on certain lines, trying to puzzle out hidden meaning. What she must surely mean beyond all this."

"..."

"..."

"There's nothing, isn't there?"

"She just wants me to leave."

"This is it."

"Clodia..."

n "Valerius..."

"I'm crying. I can't stop it."

"Ghosts damn me, she means this. And I have to do it."

"I sink onto my bed and weep. I'd forgotten what it feels like, haven't wept since I was a boy."

"I've been able to stop myself, until now."

"Nasrin comes towards me. Her face looks tender. Ghosts damn her."

v "Not right now, for Sorrow's sake."

n "No, it's all right. I just... I just wanted to hold you."

v "Hold me? What for?"

n "Are you serious?"

"Sitting on the edge of the bed, she places her arms on my back, cradles my head."

"I numbly follow as she guides me to lie down, my tears flowing into her lap."

if message == "hidden":
    jump cant_abandon
    
if message == "shown":
    jump lets_leave
    
label cant_abandon:
    
scene black
with fade

"No dreams. None at all."

scene ValeriusRoom
with fade

"I'm alone in my bed."

"I fell asleep holding her though. Wonder where she's off to?"

"Hmm. I can hear birdsong, first light is coming soon. Surprised that I wasn't woken by the order to assemble for battle."

"Within the hour, a slave will come tell me it's time. Time to take the citadel."

"Damn. Already."

"Sergius will win this battle. With the might of the entire army, with his leadership, our army will destroy Sejanus. It's going to be horrific."

"And they will rape Clodia. Kill her. Sacrifice her honor and pain and blood to War."

"That's the flaw in Clodia's plan she doesn't see. Her life is in danger. If I flee, I'm leaving her to die."

"I... I don't know what to do. I can't figure out how to save her. No matter what I do, she's going to die."

"And... and if she's going to die no matter what... should I throw my life away as well? When I could flee and take Nasrin and..."

"Coward."

"Traitor. You're no better than anyone else in this army, just out for yourself."

"No. No, I can't. I must not."

"But there's nothing I can do to save her! Nothing!"

"Nothing... except..."

"..."

"Clodia..."

"I can't fulfill your last wish. Because there's no way we can both survive this."

"And if it's you or I, then I must die."

"I can't leave with Nasrin and live with myself."

"And I know how to save your life."

"Ghosts of the Grave, take me. I'm scared."

n "Good morning."

v "Mmm. Good morning. Where have you been?"

n "Securing and packing horses for our flight. Get your armor and stuff, we need to go before everyone else starts waking up."

v "Nasrin-"

n "It's a good plan, isn't it? I figure we can reach Bellaventum before they realize you're gone. You'll have to leave your slaves behind of course but-"

v "Nasrin, listen to me."

n "What?"

v "I can't flee."

n "Are you fucking with me?"

v "No."

n "Good fucking Ghosts. Look, if this wasn't life and death I wouldn't care. You could marry her and I'd be perfectly happy as your mistress."

n "But you're going to die if you go in there."

v "Maybe."

n "You will! You told me yourself the Immortal Legion is coming. They'll kill you all!"

v "Shh, keep it down with that!"

n "They will. You're going to die, there's nothing you can do!"

v "Yes there is. Sergius is mad, he wants to slaughter everyone in those walls, every single soul."

v "I'm not worried about Sejanus anymore. It's Sergius who will kill Clodia. I have to stop him."

v "So I have to fight alongside him. And kill him."

n "You can't be serious."

v "Early on in the attack. That will be the end for me, but without him as their leader, their assault will fall into disorganization and fail."

n "Don't be an idiot then. Let me kill him. I'll go right now, I'll go into his tent and offer myself and-"

v "Then you'll be the one to be cut down by his guards. You don't want that."

n "Well, no."

v "And anyway, he would refuse you. I know him. Today he cares about nothing but war."

n "Fuck."

v "So you see, I'm the only one who can do it."

n "But you... you can't..."

v "I have to."

n "No. You don't. You can come with me and we can be free of all of this. We can go somewhere far away and never be found and do whatever we want."

v "And I will never forget that I left the woman I loved to die."

n "..."

v "I want to flee. Truly, I do. I don't want to die."

v "But leaving would be wrong. I would hate myself forever, Nasrin. I can't."

n "..."

v "But you can go. Go see those far away places and do whatever you want."

n "It's not about that. Were you not listening when I said I'd be happy just being your mistress? I don't want to go do any of those things without you."

n "I can't just... leave you."

v "What happened to you not needing me?"

n "I don't need you. I just want you."

"Nasrin..."

"I'm sorry to you too. No one's going to be happy after today, are they?"

v "Then you must understand why I can't leave her."

n "... Why? Why must you still love her when it means your death? I'm right here, damn it."

n "You can live."

v "I know."

v "..."

n "..."

v "You... you should flee."

n "Of course. There's nothing I can do here if you won't listen to me."

v "There is one more thing you can do for me. I'm willing you my entire fortune."

n "Ghosts be good, don't do that. I know you think I'm greedy but-"

v "I have no family, who else would it go to? I want you to have it."

v "I'll give you my receipt so you can claim it from the goldsmith in Marasima."

v "If you prefer, consider it payment for one last assignment."

n "Oh yeah?"

v "Yes. Wait there, I have to write something."

jump traitor_siege

label lets_leave:
    
scene black
with fade

"No dreams. None at all."

scene ValeriusRoom
with fade

"I'm alone in my bed."

"I fell asleep holding her though. Wonder where she's off to?"

"Hmm. I can hear birdsong, first light is coming soon. Surprised that I wasn't woken by the order to assemble for battle."

"Within the hour, a slave will come tell me it's time. Time to take the citadel."

"Damn. Already."

"It will be hard. With Sergius leaving me so few soldiers, I'll be evenly matched with Sejanus's remaining men for numbers, but with them having the advantage of the defense, and of morale."

"They know that they will survive if they only hold us off long enough for the Basileus to crush Sergius. Because then he will come inside the citadel and kill us all."

"If I go into that citadel, I'll die."

"Clodia..."

"..."

"Damn you. You're right."

"The only chance in the Grave that skeleton force has of getting through the citadel and to you in the first place is with my leading them. Alone, they'll be killed handily."

"They'll never get to Sejanus, meaning they'll never get to you."

"Clodia..."

"I did love you. I do. I don't know."

"But I can't save you and take you away with me. It's impossible now, just as you already understood."

"If I stay and fight, I die. We both may die."

"If I flee... we both live."

"If those are our only choices, surely I should flee?"

"What, and leave Clodia in Sejanus's bed? To bear and raise his children? While I run off with my little whore!"

"Ghosts, help me. What kind of predicament is this to put a man through? Every choice is full of pain and guilt somewhere. I've wronged Clodia no matter what."

"But... if this is the best way to save her life..."

"I'll be a coward then, if my cowardice can bring you happiness."

"Ghosts of the Grave, take me. I'm scared."

n "Good morning."

v "Mmm. Good morning. Where have you been?"

n "Securing and packing horses for our flight. Get your armor and stuff, we need to go before everyone else starts waking up."

v "That's... that's good."

n "I figure we can reach Bellaventum before they realize you're gone. You'll have to leave your slaves behind of course but I suppose I can be your slave again for awhile."

v "... was that a joke?"

n "Yes. Sorry, I guess this isn't a time for jokes."

n "What's wrong?"

v "... I feel like a coward."

n "Well don't."

v "It's not that easy."

n "Yes it is."

v "For you, maybe. No one expects women to be brave."

n "No one in Raskya. Drop this manly ridiculousness of theirs. You'll fit in much better in Durja."

v "I hope you're right."

n "Are you ready to go? We must leave."

v "Ah... give me a few minutes. I won't take long."

n "Okay. I'll be outside."

"As she leaves, I sit down at my desk and begin to write."

jump flight

label flight:
    
scene StreetsNight
with fade

"The camp is coming alive."

"Men are going here and there, mostly quietly. Gathering in their units, then sallying out of the city to join in their maniples."

"Grim are all the faces, so unlike two nights ago. Two nights ago they knew they were winners."

"It makes everything easier for us, though. They're all leaving through the front gate, so all the energy and attention is focused that way."

"Meanwhile, the great gap we entered the city through in the first place is almost unguarded. About ten men mill around this space, and they barely look at us as we ride our horses through them and out the gap."

scene WallDestroyed
with fade

"Beyond the wall, we can see what is about to become a battlefield."

n "Fuck me, that's a lot of men."

"The Immortal Legion. Thousands and thousands of little purple men, gathered on the plain where just two nights before our army camped.."

"Just behind them are the wooden walls that we did not pull down in our haste, and the siege engines we did not dismantle. The Basileus surely will begin loosing stones as soon as he is able."

"Praise the Ghosts I will not be part of the slaughter that is about to happen. Sergius's men charging into arrows and javelins and great hurled stones, against superior numbers. It will be awful."

"And there, pouring out of the gate, are Sergius's men. I have to hand it to the bastard, he wants to go out like a true Raskyan."

v "We should go. Quickly."

"Turning our horses, we break out into a trot for the closest treeline."

"If someone looks over and sees us, well, they may not care. From this distance they can't see that I'm a Tribune, and they can't spare a single horse to ride us down anyway."

"Still. My heart pounds every second until we reach the forest."

scene black
with fade

centered "Before we disappear into leaves, I look back at the city one more time."

centered "Clodia. Ghosts forgive me for this. But you will live."

centered "And I... fuck. I pray that you will somehow find happiness with Sejanus. I pray he will be good to you."

centered "I don't know how. But I pray."

centered "Goodbye, Clodia."

centered "I'm sorry I failed you."

jump forest_scene

label forest_scene:
    
scene black
with fade

centered "We ride south all day, staying on the main road."

centered "Signs say we're heading towards Bellaventum, the first town on the Raskyan side of the border (although, it's all the Raskyan side now, isn't it?)"

centered "Marasima is the next town after that. I have the rest of my money with their goldsmith. Once we have the gold, we'll keep riding until we reach Dolthemus on the coast. We can get a ship to Durja there."

centered "All very straightforward and simple. So why are my nerves so agitated?"

scene Forest
with fade

"We stop to make camp as the forest grows dark. A little ways off the road, just in case."

"Soldiers fleeing the battle very well may take this road, and they will be riding and running as fast as the Ghost of Winds will allow."

"Seated by our fire, Nasrin and I sit in silence for a long time. I keep expecting her to say something, but she doesn't."

"It's unusual, to say the least."

"So I say something."

v "How, uh... how are you feeling? After the ride I mean."

n "A little sore. I'm out of practice."

v "Well, we only have two more days of this ahead of us."

n "Then we get to be seasick and praying we don't drown for a few weeks."

v "Ha, yes."

n "We'll just have to comfort each other on the journey."

v "..."

n "..."

v "..."

n "I'm sorry."

v "Hm? For what?"

n "I should... I shouldn't tease you so much."

v "I can take it."

n "But you're still thinking about her. You're not in the mood for this sort of thing."

v "You've never cared if I was in the mood or not before."

n "You also haven't been very nice to me this past year."

v "I have fed and paid you. I only stopped fucking you, which is hardly what I would call cruel."

n "It felt cruel to me... but I should have not cared so much. I was stupid, falling for my master."

v "Slaves fall in love with their masters all the time."

n "I don't think most masters are like you. You never hit me, even when I disobeyed. Not once."

v "People who beat their slaves only reveal that they are poor masters."

n "This world is full of poor masters. I've had many of them."

v "I'm sorry."

n "Ah, doesn't matter. I'm free now. Which is also thanks to you. You've made my life better three times now."

v "Three times?"

n "Becoming my master, freeing me, and then running away with me. With every one of those decisions, my life got better."

v "... I just made that last decision, things can't change so quickly."

n "Oh?"

"She rises, rounds the fire to me and sits down. Her head rests against my shoulder. She places her hand upon mine."

n "I feel much happier."

v "... I'm glad."

n "But I know you won't be happy at first."

v "... I want to be. But... what I've done is-"

n "The best thing you could have done."

v "That's hard to accept. I still feel like a coward. The Ghosts of Love will damn me, if nothing else."

n "No they won't. They're blessing me right now with you. Do I strike you as a damnation?"

v "Hm. Hard to tell. I'll let you know in a few days."

n "Ha! While you're puking your guts out into the sea?"

v "Maybe that's the only punishment I'll have to suffer."

"Maybe. Sitting here, holding her like this, I can believe it."

jump basileus_victorious

label basileus_victorious:
    
scene StreetsDay
with fade

"Corpses."

"Hills of them, here and there. Spilling from doorways, filling the alleys. Their blood turns the streets to streams, their bodies stepping stones."

"Most of them wear red, Sergius's men who fled back into the walls after losing the battle out in the field. They held the city no better, not with a giant hole in the wall for the Basileus to strut through."

"I didn't see any of that, of course. I spent the battle in the temple."

scene TempleDay
with fade

"I walked down the aisle. Sejanus took my hand. The priests and priestesses chanted and prayed, and smoke filled the room."

"I felt hot and sick and it was all very much a blur. A fever dream."

"Sejanus placed his hand upon mine, and kissed my forehead."

"I could not look at him. I was not there."

"Till the herald came rushing in."

"The battle is won, he cried."

"The temple, full of Sejanus's lackeys and the Dagaran nobles who joined them, burst into jubilation."

"The priestesses threw flowers into the air, out the windows for the wind to carry them away."

"People fell to their knees and praised the Ghosts. The Pontifex wept."

"Sejanus just squeezed my hand. The smile on his face said it all."

scene black
with fade

"And my Valerius."

"Just like I told him to, he did not appear. He did not save me."

scene StreetsDay
with fade

"And now, here I am. Walking with the Basileus, and my new husband."

sj "My my. What a delightful mess you've made of all these traitors, Your Holiness."

a "I'm just doing my job."

sj "So humble about it too."

a "The work's not done yet. Several of them have escaped. We don't have Sergius yet."

sj "I'm sure you'll find him soon. He's not a clever man, he can't hide in ignominy."

a "Hm. I have a pack of riders out looking for him. We'll see."

a "I don't much care about Sergius anymore, he's finished. We'll speak of the gold instead."

sj "Ah, why of course."

a "Hm... speaking of treasure, I assume this is your new wife?"

sj "Ah, yes your Holiness, I did introduce her."

a "Yes yes, I have more important things on my mind right now. Well Praetrix, congratulations on your marriage."

c "... thank you, your Holiness."

a "Ah, girls can be so gloomy on their wedding day. Just the opposite of what you would think. I pity you, Sejanus."

sj "If I am in a pitiable situation, then may the Ghosts continue to curse me."

a "Ha."

c "Your Holiness?"

a "Hm? What is it, girl?"

c "Among the dead... are all of Sergius's tribunes accounted for?"

a "What kind of woman cares about military matters?"

sj "Actually, I am curious about the same question."

a "Ah. Well, I believe we've found five of them. The other one must have fled with Sergius."

"Valerius..."

"If you're dead, my love, I don't know what to say. I don't know what prayers to offer you. I don't know when it will be safe for me to weep."

"But if you live..."

"Run. Run far away and don't come back."

"We tried, my dearest. We tried, but it's too late now. I am Sejanus's, and Sejanus is Praetor, and everyone in all Raskya would return me to him if I fled."

"I am chained now. Would that we could have been chained to one another."

"So flee, love. Don't come back. You should live. I don't know where you'll have to go, but go there. Go and leave me and... and..."

sj "Hm? What's wrong, my wife?"

"He reaches out to brush the tear from my eye. I pull away."

c "As his Holiness observed, this is a gloomy day for me. May I return to my room?"

sj "You may return to {i}our{/i} room."

c "... yes."

sj "Yes what?"

c "... yes, my lord."

"Ghosts, if you have any mercy or compassion at all, if Valerius yet lives, let him find a happiness I cannot. Grant him long and pleasant days."

"And if I have any right left to ask for something for myself..."

"Let my life, and my miseries, be short."

jump traitor

label traitor:

scene Forest
with fade

"The next day we break camp and make for Bellaventum."

"We haven't seen any soldiers fleeing along the road yet. We pass by the occasional drover, but otherwise the road has been fairly uneventful. Which means safe."

"But it also means that, most likely, anyone fleeing the battle is still behind us. Unless they passed us in the night, but I don't see how we could have slept through that."

"We just have to keep moving. Keep breaks short."

"Yet we have to water the horses when we can. If the stream that winds like a snake towards and away from the road happens to be close by, we have to take the opportunity for a drink."

"I don't know this area well enough to know when the water will wind back to the road again."

"It's during one of these stops that Nasrin tugs on my arm."

v "What?"

n "Someone's coming. A lot of people."

"Shit, she's right. I hadn't heard them over the stream, but now that I listen, I can distinctly hear a mass of people, probably horses and carts too."

"Fleeing soldiers."

v "Shit, we have to go."

"Nasrin is already back on her horse before the words are out of my lips. I pull myself into the saddle of my mount."

"Just as we turn back down the path-"

s "TRAITOR!"

"I look back over my shoulder once."

"There, mounted at the head of his shattered men, is Sergius."

"The look in his face, his eyes locked onto mine, makes me feel cursed."

v "Ride!"

"I dig my heels into the horse's side, slapping Nasrin's horse at the same time. As both of us gallop off, I can hear his voice behind us, over the rumbling hooves and my own internal screaming."

s "RUN HIM DOWN! KILL HIM! KILL HIM!"

"Another glance back. There's at least five riders after us, Sergius at their head."

"Shit shit shit, how did they catch up to us like this? We spent too much time relaxing this morning, should have been moving."

"Ghosts, I don't care what you have to do, just don't let this be the end of it."

"The road is too open and straight. We need to get off into the woods."

"We need to split up."

"Damn it. We need a miracle."

"But there are no miracles here. None to be found."

"I simply shout the first tactic that comes to mind."

v "Nasrin! Ford the stream!"

n "Okay."

"She tears off to the right and plunges her horse into the water. It's not a fast or deep body of water at all, won't stop them from coming after us. But maybe-"

n "Shit!"

"I've already followed her into the river before I see what she's seen."

"Sergius's riders, waiting for us on the other side. There were far more than five."

"Ghosts damn everything."

s "You are something else, Tribune Aureus!"

"Sergius and his other riders wait behind us on dry land. His skirmishers are already deploying on the banks, with the infantry coming behind them."

"There's barely a hundred of them, maybe all that's left. Fugitives and dead men walking. The great Legion of Sergius, brought low."

"And now we're to die at the hands of the low."

s "When we lost in the field, we withdrew to the city. Imagine my surprise when, instead of finding Sejanus rotting alive in the Grave, as you promised, I found the men you were to command, crying and blubbering about how you abandoned them."

s "Abandoned us!"

"The growls and shouts roll like thunder. The men I've betrayed all stare at me with hateful smiles. They spit at us and gesture and scream their curses."

so "You coward! Fucking traitor!"

so2 "You left your men to die!"

so3 "We'll cut your balls off for this!"

so "I say we fuck his whore in front of him!"

so3 "Hell, I'll fuck {i}him!{/i}"

"The roar of laughter that follows that one chills me more than any of the words."

"This is more than war now. These men will enjoy our deaths."

"I see. This is my punishment then. Ghosts..."

n "Valerius."

"Her eyes break my heart. Ghosts, why must she suffer too? I'm the traitor, I'm the coward."

s "What a great shame it has been to have you in my Legion. I should have known that a weakness like you would lead to my downfall."

"That accusation, among all the others, stabs into my chest and stirs my rage. Because it's not true."

v "You lost to the Basileus on your own, Sergius! Do not put your failures on me. I fled a sinking ship, but I did not sink it."

s "You didn't stay behind to pump the bilge either, you snake!"

s "If you had done as you said you would, Sejanus would be dead. I would at least have left this world knowing I took that accursed cunt with me!"

s "Now... now he is a hero, and I am nothing. I will be slandered throughout history, a traitor, a crushed rebel!"

s "And for the rest of eternity, the annals with scream to the Ghosts that Sergius the Betrayer was bested in the end by Sejanus the Great!"

s "AND THAT IS INDEED YOUR FAULT!"

"His men cheer. Even after everything else, this man gives them power and hope and passion."

"There were probably many more survivors than these, but they have all gone for themselves. These last men are the fanatics, the worshippers. The men who will gladly die with Sergius."

"They have nothing else but him now. What he wants, they want."

"And vice versa."

n "Valerius... I'm frightened."

v "I know."

n "No. You don't. I mean... they're going to rape me. It's plain on their faces. I don't want to die that way."

v "There's... there's got to got be something we can do."

n "Please... I have my dagger. I'm going to kill myself."

v "Absolutely not."

s "Speak up, will you? We'd all very much like to know what you're saying to the little whore."

n "Valerius, I refuse to suffer the indignity."

v "Just shut up. If they come down here to put hands on you, fine, slit your throat then. Don't you dare touch that dagger a second before that moment."

n "..."

"This is ridiculous. There's a correct solution here, I just have to think."

"But there's no fucking time to think! We have minutes to live, if that. What can I do?"

"I can..."

"I..."

"Ah. That's it."

"At the very least, I can be a snake."

v "Sergius! My freedwoman here has done nothing wrong. She has followed my orders, but all for pay. Let her live."

s "Hm. No. She'll kill me in the night if I make her a bedslave, and I have no other use for her."

v "Then let her walk free. What does it matter to you?"

s "To me, little. But seeing as how we're all soon to die, I may as well let my boys have some fun before the end. I've always taken good care of you, haven't I boys?"

sos "Hail Legatus!"

s "Now make sure you don't fight each other too badly over her holes, it's all in good fun, although I understand the rush to get there before things are too loose. Or dead."

n "He's not going to go for anything you say."

v "He doesn't need to. Be patient."

n "... okay."

"Trust."

"Good Ghosts. Seeing it on her face like this... if I live, I don't think I'll ever forget it."

v "If you don't kill me personally Sergius, you're a coward."

"The screams from the soldiers are horrifying. Like all the Ghosts of War came down to torment us."

"But Sergius raises his hand for silence."

s "Now that is the first and only thing you have ever said to me that is the truth."

"The men protest. But he raises his hand once more."

s "Boys, we're done. No matter what happens, we're done. I'd ask you to flee for your own lives if I thought you would."

sos "NEVER!!!"

s "Then let me have this last moment of glory. Let me strike down someone I hate one final time."

s "And if the Ghosts have truly turned against me, and they guide this womanly scum's sword into my heart, then you may avenge me in whatever slow, torturous ways you see fit."

s "Either way, you'll get to fuck the girl."

"That gets them cheering again."

"Alone, Sergius rides his horse into the water. His men close in tighter, lining up on the banks to watch. The stream is a theater now."

s "Do ask your woman to step aside. I'm more than willing to face two opponents, but virtue combat with a woman is beneath even what little honor I have left."

v "Do as he says, Nasrin."

n "Valerius."

v "What is it?"

n "I love you."

"I don't know what to say back. I feel like saying what I should, what I want to, would be just one more betrayal."

"So I say nothing as she rides her horse away from me, going downstream, staying in the middle of the water, as far away from the soldiers on either side of her as she can possibly be."

"The men seem more focused on her than Sergius and I."

s "All of you, pay attention! The girl can wait. Witness my final victory!"

sos "HAIL LEGATUS!!!"

"Sergius stops about ten feet in front of me. His horse's head twitches furiously, like it senses the energy in some way my mount clearly doesn't. I can feel the thing wanting to bolt between my legs."

s "Draw your sword, traitor."

"I have not learned to duel on horseback. I've learned to charge and to thrust with a lance, but using a sword in the saddle is not part of Raskyan training. The sword is meant to partner with a shield, on foot."

"Yet, if I don't draw, I suspect he will simply ride over and cut my head off."

"So then."

"Let's see what he'll do."

s "A helmet? You really want this to be the second easiest fight of my life, don't you?"

"The soldiers laugh. It is kind of funny."

"I became a decent rider in the legion, at least. I can probably dance around him for a bit."

"He closes the gap between us at a trot."

"The swing from his blade is deceptively pathetic. It's just feeling about. I parry it easily and back my horse away."

"It feels more than willing as Sergius's mount lashes out with its monolithic teeth. The bastard has been trained to bite the other horse."

"Backing away, backing away. Sergius seems willing to play with me a bit, as all his blows are nothing more than feints. When I raise my own sword to guard against them, I feel almost nothing where the blades touch."

"That just means that his first real blow will be all the more shocking."

"His men are as loud as any crowd in the Great Circus at Raskya. They sound like they want nothing more than to charge down and kill me themselves."

"I sneak just a tiny glance over my shoulder, at Nasrin. They're not going for her yet, at least."

s "Will you hold still? I know dancing is in a woman's nature, but you're making this boring."

v "If I'm a woman, come chase me like one."

"At that, he squeezes the horses's sides. He and his mount both grit their teeth and fly at me."

"No need for pretense. I turn my horse around and ride upstream. The cries of \"Coward\" and other such things make the trickle sound like a mighty river."

s "Are you serious?"

"I can't get too far away from Nasrin. Thankfully, this stream is rather wide, and I can make rather easy turns. Whether Sergius will let me pass is another matter."

s "That's it, come back to me. Tell you what, the more boring you make this for me, the slower I'll kill you once I get ahold of you."

s "And the more of my men I'll let fuck your whore until I decide she can die."

"I should have known it wouldn't be so easy. Ghosts of Luck, I need you again."

"You've failed me an unusually small number of times in my life so far, truth be told. Grant me this last favor."

"Just let him miss."

"I command my horse to charge. Right at Sergius."

"I raise my sword."

"He's smiling now. He's close. Ghosts it's happening very fast."

"He doesn't swing. Just holds the sword out to his side. Level with my head."

"I tighten my arm-"

"It slides along my blade."

"Strikes my helmet."

scene forest

"Ghosts, that hurts!"

"Fuck you Sergius!"

"I'll fucking kill you!"

"I scream the rage out of me. And then I can think about how I'm not dead."

"But I'm not bleeding. My arm feels like it's exploded on the inside, but my ringing head still sits upon my shoulders."

"And Sergius is behind me, turning around."

s "Damn, I've seen better ways to survive that."

"The waves of laughter that roll down on me are kind of reassuring, actually. Makes the whole thing feel... more relaxed. Even with my arm screaming at me."

"I don't think I can block another blow like that, my arm may break. He's so fucking strong, that's all there is to it."

"Oh, my helmet is hanging off my head."

"How did I only just know notice that? That's not good..."

"Ugh, I need to take it off. Another blow like that and my skull will cave in underneath this ruined metal."

"He's preparing for his own charge."

"Yes. Right, right, get your head together. Get it together, this is it!"

"Charge!"

"Shit... I need to take this fucking helmet off!"

"But we're charging. His sword is held out the same way. I can't block it like that again. I have to do something else."

"And I need to take this damn helmet off."

"Letting go of the reins, I grab my helmet and pull it free. Sergius is closer, closer, he's almost here he's-"

"I throw the helmet at him."

"He moves his sword to bat the helmet aside. Moves it out of my way."

"And I pass by him."

scene forest

"The groans from the crowd feel amazing. I feel amazing. It feels amazing just to live another few seconds."

"Then I remember that my head and arm are killing me."

"Still, I think this might work."

"As long as I'm not wrong."

"I don't think I can buy much more time."

"I look to Nasrin. I'm too far away to see her face, but she hasn't moved. She's still safe."

"I can do this."

s "You don't have any tricks left!"

"He's charging again!"

"Shit. I do have one trick left, actually."

"No time, fuck it, have to use it."

"Charge!"

"Again we surge towards one another, water splashing all about us. I just realized how soaked my legs are."

"It might be the last thing I realize."

"He's right in front of me again. No helmet, I can't block it."

"Sorry horse."

"Throwing my leg over the horse's back, I leap off."

"I have just an instant to see my horse collide with Serigus's. To see his mount rear back, to see him teetering, wheeling his arms and his sword."

"I land, hard, in the stony water."

"More pain. Everywhere now it seems. Ah shit, do you ever get used to it?"

"Splashing. Crazy neighing. The horses are mad. I have to move or they'll stomp my head in."

s "AUREUS!"

"Pushing myself to my feet, I see Sergius in the midst of the two panicked horses, recovered from his own fall. He hesitates for only a moment before driving his sword into my horse's neck."

"The heavy beast cries and sinks into the water, and as Sergius's horse gallops away in fury, I'm left facing my enemy on foot."

"Something I'm actually trained in, thank Luck. Now if only I weren't half-beaten."

s "Come then, this is it boy."

"He rushes through the water at me, faster than I thought."

"I parry his first blow and back away, but the second one comes right after it, and I only just block that."

"Every swing of his sword, I barely get my own blade up in time to deflect it, and every guard sends blood-hot fire up my arms."

"I'm using both my hands on my sword to deflect his single handed strikes. I knew he was stronger, but I hoped it would mean he was a bit slower than me."

"But I'm in good shape. I can still keep distance between us. I just have to not trip over a stone or something."

"His armor has weak points, I just need to find a gap and push the sword inside. The armpit most likely."

"Or I could simply strike his unprotected head, but of course he's expecting that. Likely the first strike I throw at it, he'll counter and take my own skull off."

s "What cowardly trick are you planning now, Aureus? Pick up a stone and throw it at me, why don't you? Or dance away from me as if I'll get tired, yes, do anything but fight me like a man."

s "What a fucking disappointment you are, right up to the end."

"He keeps striking at me, faster, harder than before. I can still keep away from him, but that's all I can do."

"Any attempt to counter-attack, to do anything except back away and guard, will kill me."

"I need a shield. Swords alone are a desperate thing, but with a shield I could block his blows easily and focus on where to time my own attacks."

"But I won't be getting one of those. This sword is all I have."

s "Fine! If you refuse to fight me, maybe I can change your mind."

"I watch the angry smile on his lips as he raises his sword high. He's stopped chasing me though, so I stop moving backward."

"Finally a second to catch my breath. My armor is heavy, I feel myself tiring."

"How much longer?"

"And what is he about to do?"

"The roar of his men is hard to understand. Are they impressed? Or suddenly fearful?"

"How do they feel about the fact that the Great Sergius just threw away his sword?"

s "Will you attack me now, little girl?"

"That seems a very stupid thing. It's the fundamental rule of warfare: never do anything your opponent wants you to do."

v "Feeling like being tricky yourself, Sergius?"

s "No tricks. If you swing that sword, I won't run away like you do. I'll fight you till the end."

"I believe that."

"I shouldn't do this."

"But his head... I just need to strike his head once. Just once."

"Ghosts of Luck, one last time."

"I step forward."

"And halfway through my step, I realize I've already fallen for it."

"He doesn't even know the exact way he's beaten me, but he has. He simply made me forget that I'm not trying to beat him."

"Because if I beat him, his men will all rush over here and kill me. Kill Nasrin."

"Winning this is the last thing I want to do."

"But nonetheless, he made me step forward."

"And I'm already swinging my blade. I have to commit to my attack."

"I have to."

"My arm hurts. Twists."

"My sword goes flying through the air. He grabbed the damn blade with his hands and tore it from mine."

"The sword splashes into the water a good twenty feet away."

"Then Sergius punches me in the face."

"I can barely hear the roar of his men over the thundering blood in my head. My face is numb and I can't see."

"Oh... I've fallen into the water. The gentle current washes over me, filling my nose and mouth. I sputter frothy blood and water from my lips, trying to see what's going on."

s "Well there you have it. Can't even kill an unarmed man with no helmet."

s "You never could have been a tribune without your conniving and bribing. You were barely functional as ballista fodder."

"A hand reaches out for me to grab. I reach out for it, confused."

"A fist slams into my head."

"He's over me."

"I can't breathe."

"He's choking me."

"He says something and I can't hear it."

scene black
with fade

"Nasrin screams and I can't hear it."

"I can't hear anything but water and blood."

"Everything hurts and I can't think and I think I'm dying and I don't know what to do and I'm sorry Nasrin I'm sorry Clodia I deserve this I-"

"..."

"..."

"..."

"..."

"..."

"..."

"..."

"..."

"..."

scene forest
with fade

"Somehow... I was pulled from the Grave."

s "Come at me then, Immortal dogs! I'll show you how your blood still runs red!"

"I can't make sense of my surroundings. I still can't believe I can breathe again."

"But I can hear the screaming of men, the rumble of horses, the clanging of iron."

"And I can see purple. The color of the Basileus."

"I did it. I stalled long enough. The Immortal Legion is here."

"Oh, how they carve through Sergius's men. It's so beautiful. Men falling in the water and mud, lances plunging into skulls and hearts, hooves trampling red soldiers."

"Sergius screams at them, rants, beckons as his men are cut down on both shores."

"I need to get up. But I can barely move."

n "Valerius!"

"Nasrin!"

s "Aggghh!"

"I see Nasrin, her hand grasping the hilt of her dagger driven into Sergius's underarm, in the gap."

"But that dagger can't be long enough to kill him."

"He whirls, his arm striking her away. She lands in the water with a splash, but is already back on her feet before I can worry."

"Howling like a dying wolf, Sergius sinks to his knees. He stares almost in fascination as Nasrin helps me to my feet."

"I can see more of what's going on around us now. The men on the shore are mostly dead. The Immortal Legion is moving into the water. Coming after us."

n "Get on!"

"She has her horse here. Dumbly, I obey, using the last of my strength to pull myself up on the beast. Nasrin climbs on in front of me and takes the reigns."

"And we're off."

"Galloping as fast as we can from the slaughter behind us."

"Will the Legion follow us?"

"I glance back once, to see Sergius, still on his knees, still staring after me with a look I've never seen on any man's face before."

"He's surrounded by the Immortals. They have their prey."

"They don't give chase."

"Then I forget it all, and lean into Nasrin."

scene black
with fade

centered "I hurt. I feel dead."

centered "But I'm not."

jump sergius_betrayer

label sergius_betrayer:

scene StreetsDay
with fade

"The priestesses won't cease their incoherent mumbling."

"It's just babbling nonsense, do you really think the Ghosts understand a thing you're saying? Especially not over the jeering of the crowd."

"But the people of Dagara seem invigorated. They're lively, smiling, joking."

"No wonder. A few days ago, Sergius had them locked up in cages, ready to be sent to Raskya as slaves."

"Now he sits on his knees before them."

"I haven't seen him since I met Valerius. He looked a frightening, yet noble man then. Proud."

"I can hardly believe the figure covered in rotten fruit and pig's blood and shit in front of me is the same man. I suppose that's the idea."

"At last the priestesses stop fighting to be heard over the crowd and shut their mouths."

"I've always hated them, ever since my aunt threatened to force me to become one. I couldn't imagine anything more frightening than celibacy at the time."

"Now I realize there are worse things."

"My husband steps forth."

sj "Sergius."

s "..."

sj "You look smaller than you used to, somehow."

s "..."

sj "I understand, you don't want to give me the satisfaction. That's perfectly fine, I already have all I need."

a "Ah, this is boring, can we get to the part everyone wants to see?"

"With a bow to the Basileus, Sejanus steps back from the condemned."

"Two Immortals haul Sergius to his feet, and lead him to the edge of the pit. Everyone in the crowd cranes their neck for a view."

"How much of a spectacle do they expect of throwing a man into a hole?"

s "SEPTIMUS!"

"The crowd falls silent as though cursed. Perhaps we all will be. The blasphemy of addressing the Basileus by his birth name is next to few."

a "What is it, he who is about to die?"

s "Why? Why did you side with him?"

"Has no one told him?"

"But of course not. Otherwise, Sejanus would not have ordered all the torches in the Pit to be lit today. He wants Sergius to see all that gold as he drowns in poison."

s "Why?"

"For some reason, the Basileus doesn't answer, but neither does he give the order of execution. There's just silence and the murmuring of the crowd."

"And Sejanus is the one who speaks."

sj "You'll find the answer down there, Sergius the Betrayer."

a "Ah, yes, good answer. Very well, throw him in."

"I happens in seconds."

"I don't see Sergius's face before he is simply pushed by both men over the edge."

"He quietly drops out of sight."

"And the crowd roars."

a "Ghosts, that wasn't much, was it?"

sj "The real show is happening beneath us this very moment."

a "I'm surprised you didn't want to watch from down there."

sj "To be honest, I'm quite tired of suffering. I've seen enough."

"Oh have you?"

"He turns to me. As though he heard my thoughts."

"And hands me a letter."

c "What is this?"

sj "It was found in Tribune Aureus's room in the War Tent."

"I snatch it from his hand, sure it's fake. But the cipher seems to be ours."

sj "He's the only Tribune unaccounted for."

c "Don't lie to me just to make me easier to deal with."

sj "There won't be much dealing. I have the feeling that, apart from making a few sons, neither of us will pay much attention to this marriage."

c "He's alive."

sj "Probably. If he's ever caught in Dagara again I'll have him thrown in after his legatus, but I think he's smart enough to stay away."

"I need to get back to my room."

sj "Where are you going?"

"I don't answer. Thankfully he doesn't care enough to follow me."

scene ClodiaRoomDay
with fade

"Once in my room I sit down and decode the words as quickly as I can."

"And when his words are bare before me..."

centered "Clodia, if this has reached you, then you are still alive and, presumably, everything is over. {p}I too am alive, hopefully. I am carrying out your wish soon. I am fleeing this place, somewhere far away. {p}I do not want to, but I understand we have no choice. This is hard for me to admit. {p}I wanted to save you and have everything be perfect. My failure weighs on my heart, and I wonder if even now the Ghosts of Love are cursing me for cowardice. {p}So if I may redeem myself to them in some small way, let me remind you that you are a Praetrix of Raskya. Sejanus has power over you, yes, but your power is stronger than you may think. Never hesitate to use your power to better your life in any way you can. [p}I don't believe I shall ever see you again, which breaks my heart to write. Even now I want to burn this letter and go charging in there tomorrow. To die for one's beloved seems better than to live as a coward. [p}But you want happiness for me as well, so I will try for it. I don't know how long it will be until I can feel that happiness again. I'm not sure yet if I can ever feel it again, after what I've known with you. But I will try. {p}I live, Clodia, but Valerius Aureus will be as dead. I will change my name, I will make no waves, I will live in a foreign land. This is goodbye forever. A strange thing to write. Know that I loved you more than I thought I could love anyone. {p}I will keep my ear to the wind for news of the Raskyan Province of Dagara, and their beautiful and dignified Praetrix. So know that when you are happy, when you better the lives of your citizens, when you relax in the splendor of the wealth that will be extracted from all that gold, when you stare into the eyes of your children, I will hear of it. {p} Whenever news of your happiness reaches me, my dearest Clodia, know that I am happy too."

"I read it again."

"And again."

"And..."

"I weep."

"For joy."

jump to_durja

label to_durja:
    
scene Ship
with fade

"We're three days at sea now. And I've only thrown up once, fairly good."

"Nasrin has fared better in that regard. Her great misery on the trip so far has been boredom. Every night the ship docks at a new port, and every night Nasrin wants us to go out and see what the cities offer."

"For the first two nights I was able to convince her that it was too dangerous. But tonight we docked in Cetasia, the first town not under Raskyan dominion, and she was able to convince me:"

n "I need a bath."

v "Everyone on this ship needs one. No one can smell you. I can't smell you."

n "I NEED a bath."

v "Look, I get that prostitutes bathe more often than normal and all-"

n "Oh shut up. I just feel gross."

v "Ghosts be good. Fine. We'll find a bathhouse, spend an hour there at the most, and then it's back to the ship. Okay?"

n "Yes, thank you \"Master\"."

v "Oh come off with that."

scene black
with fade

"And then, a few hours later..."

scene Ship
with fade

n "Ah, I feel so much better."

v "Any second we could be boarded by soldiers informed by a spy. I don't know how to feel better."

n "You're paranoid. Only word about the siege has come this far, not any of the actual soldiers. No one recognizes you."

n "By the Grave, none of the town criers said a word about keeping an eye out for Tribune Valerius Aureus."

v "Gah. I suppose you're right."

n "Three hours in a bath and you're still tense. You poor man."

v "I have every right to be tense."

n "I suppose so. Still. We've made it."

v "So it seems."

"Sitting on the edge of the bed, I wonder if I paid too much for this room. It's a fine room, it's probably just too fine for our purposes."

"But of course, I was meant to share this room with Clodia."

"Clodia..."

"I have to stop thinking about her. It hurts too much."

"But I just don't know-"

n "Hello."

v "Mmm... what are you doing?"

"The fox has crawled up behind me on the bed and begun to massage my shoulders. She does this from time to time, her attempts at seduction."

"Does she really think it will work now?"

n "You said it yourself, you're having a hard time. I'm helping."

v "Yeah, right. Well thank you."

n "No problem. Just relax. Breathe."

v "Hard to do."

n "I know. Just try."

"She works on my muscles for a while, rubbing me into a trance. I feel like I could almost pass out sitting up after a while."

v "Have you ever been to Durja before?"

n "No."

v "Hmm. So we'll both be confused foreigners."

n "I have a lot of experience in being a confused foreigner though."

v "Says the girl who speaks Raskyan with almost no accent."

n "You'd learn Raskyan quickly too if it was the only language your master understood."

v "Fair point... shit, they don't speak any Raskyan in Durja, do they?"

n "Just Durjan and Peladonian."

v "Well I know some Peladonian, so that's good."

n "Oh? I've never heard you speak it before."

v "It's probably very bad, I only taught myself from books."

n "Let me hear it."

v "Ah Ghosts... um, {i}Hello, my name Valerius.{/i}"

n "<i>My name{/i} is {i}Valerius.{/i}"

v "{i}Is Valerius.{/i} Of course, I always was confused about when you use {i}is{/i} and when you don't."

n "Oh it's easy. For example, {i}Nasrin is in love with you.{/i}"

v "I caught your name, but I don't know the rest."

n "I'll teach you all you need to know, Master."

v "You've been calling me that again recently. What's with that?"

n "Oh, I don't know. It just feels like old times."

"For one of us."

"Mmm... she does smell good though."

"Damn... I suppose even if I lack the soul of a Raskyan, I have the prick of one."

"Ghosts of Lust... would it be wrong?"

"Damn. Damn damn damn."

v "Nasrin."

n "Hm?"

v "Things can't ever be quite like they were before."

n "Oh of course. And I wouldn't really want them to be, I do like being free and all."

v "Yes."

n "But just because I'm free doesn't mean I can't take care of you."

v "You'll drain me of my money very quickly that way."

n "Oh, I don't need your money."

v "What's changed?"

n "In Durja I'll be able to find work that doesn't involve my vagina."

v "Right. They prefer assholes in Durja."

n "Ha. Seriously though, my skills are applicable to a lot of different jobs that have low value in Raskya but that the Durjans prize quite highly. Dancing, acting, that sort of stuff."

v "You're just describing prostitutes."

n "My point is that those are seperate professions in Durja."

v "Weird. So after a performance if a rich man in the audience offers to rent the leading actress for the evening-"

n "She's allowed to say no."

v "I suppose that's what one could expect in a land ruled by women."

n "You'll get along much better in that kind of land, I think."

v "Perhaps. So you making your own money means you suddenly want to care for me for free."

n "It means I can afford to care for you for free."

v "But you want to."

n "... yes."

v "Hm. That's nice. I suppose I'd like to be cared for. And you already know what I like."

n "In many different ways."

"She isn't really massaging me anymore, just leaning against my back. I can feel her breathing."

"Can feel her breasts against my back."

"Her heat."

"Nasrin, you..."

"I turn to face her. She's looking up at me with emerald eyes. She's so beautiful."

"It's been one of the greatest challenges of my life to ignore that beauty this past year. To go from loving her to being cruel."

"Maybe I really was a fool to neglect her for Clodia."

"But, I love Clodia."

"Loved her."

"I don't know."

"But..."

n "Valerius. Please kiss me."

"Fuck it."

"I obey."

"Her lips are soft, full. I remember them instantly."

"It's just a short kiss. Just a test."

"But it feels good. I like it."

"I like her."

"I might do anything she asks right now."

n "Valerius."

v "Hm?"

n "Please fuck me."

scene black
with fade

centered "To be true..."

centered "It's the most amazing sense of relief to finally do just that."

scene Ship
with dissolve

"Afterward I stare at her body like she's a work of art. She's covered in little beads of sweat and her chest won't stop heaving. I must look about the same."

n "I missed you."

v "I'm sorry I was away for so long."

n "This... this was okay, right?"

v "What do you mean? It was very, very good."

n "No, I just mean... I don't want you to hate me tomorrow morning. I don't want you to wake and realize this was something you regret."

v "I won't. You know the restraint I've shown for over a year. If it had been wrong I wouldn't have done it."

n "Okay. I'm glad. I just... I got used to you hating me. I need to adjust again."

v "I never hated you."

n "It felt like it often."

v "I'm sorry. It... it will be different now."

n "Yeah?"

v "Yes. I won't be cruel to you ever again."

n "Okay. And I'll take care of you without charging. Fair deal?"

v "Ha, fair deal."

n "Uh... I understand if you can't say the same thing right now. But... I love you."

"... well I-"

n "Please don't say it unless you mean it. I understand."

v "... okay. Very well. But let me say this: I loved you when we met. I truly did."

v "Then I thought I threw that love away for something else. But really, I just locked it away. It's still here. I can feel it's here."

v "I just... I just need a little time and help to unlock it again."

n "Okay. I like that idea. A strange one, though."

v "What?"

n "To unlock you. Because you're the one with the key and I have all the holes."

v "Ghosts be good, you keep making jokes like that and we'll have me unlocked in no time."

n "No time, hm? I have doors just begging to be opened right now."

"We laugh together, and hold one another, and after a few more of these jokes we fuck once more, harder and sweeter than before, and finally we sleep."

scene black
with fade

centered "I'm sure tomorrow I'll feel strange. I'll mope. I'll wonder."

centered "Perhaps I'll feel guilt."

centered "But then the day after that maybe I'll be happy again."

centered "One day, maybe I'll be able to be happy like this all the time."

centered "It's what I promised, after all."

centered "Ghosts, you are confusing and you rarely give us what we want. But what you give us can still be good."

centered "Thank you for this next life."

centered "Thank you for Nasrin."

centered "This is good."

jump epilogue_durja

label epilogue_durja:
    
scene black
with fade

centered "The tale of Valerius and Clodia is, per usual with such stories that are so poorly documented, confusing. {p}In truth, with all the famous plays and histories which are little more than a series of lies that wear these historical figures as costumes, most are quite shocked to learn that Valerius fled Clodia with his freedwoman. {p}What love story is this? they protest, in particular those who treat the love on the stage and on the page as somehow more thrilling and passionate than any real love. How, they argue, can such a thing be true? {p}Yet more accurate plays are being written every day, and the character of Nasrin has increased in prominence in recent years. Taking into account that for the first century or so of Clodia and Valerius literature that she was referred to as little more than \"Valerius's freedwoman\", the Peladonian spy has come a long way in the popular imagination. Now there are even roaming youth gangs that will fight each other over whether they prefer the girl playing Clodia or the girl playing Nasrin in the latest productions, even adopting rival colors of red and green to match. {p}It seems that, generations later, these figures still hold power over us and our imaginations, whichever truth we are willing to accept. {p}- Entello Revolo, from his \"Literary Traditions of the Valtian Peninsula and Its Associated Peoples\""

centered "THE END"

return

label feeble_siege:

scene black
with fade

"Every strike of the ram against the splintered door sounds like a falling tree."

scene StreetsDay
with fade

"They've been at it all night. The door is well constructed, thick. A testament to Dagaran architectural engineering."

"Our ram is Raskya's testament."

"I have four-hundred infantry with me. Two hundred green hastati, a hundred and fifty principes, and the rest a smattering of triarii. Sergius took the rest of the veterans to go die on the battlefield with him."

"Their experience better prolong the battle for a while, I'll need all the time I can get."

"About time. I can see Sejanus's legionaries through the holes in the door."

"I should say something to my men then. I can see in their eyes what they want."

"The gold. The clothes. The noblewomen and girls and boys. Same thing they wanted in the city."

"Only nobler. Juicier prey."

"I could care less about any of that. I'm here to kill one man and save one woman. That's it."

"I'm going to die doing it. I'm going to fail."

"But I'm going to try."

v "All right men."

"We're all going to die, actually. Have you all figured it out yet? Or is that why you're so crazed for spoils?"

"Time to tell you what you want to hear."

v "All that stands between us and the noble holes of Dagara are five hundred men. If we stick together, fight well and fight strong, and do this as quickly as we can, we can have all those holes to ourselves."

"A few cheers go up, but mostly they seem impatient. The young hastati in particular look eager to pull their cocks out. I should wrap this up before they charge in without me."

v "So obey my commands. Our priority is to kill Sejanus before any raping. Once he's dead, his men will submit, and we'll be free to do what we what to anyone we want."

v "But no pilfering and ravaging until that man is dead. Now fall in!"

"Storming fortresses calls for different organization and tactics. Full units of hundreds would not function in close quarters. Instead, we form into twenty man maniples, all lined up as neatly as possible, one after the other."

"The idea is simple: push our way inside and then spread out accordingly. Direct units down hallways, into side rooms, make sure nothing is left uncovered as we press inside."

scene black
with fade

"At last the door bursts open, swinging loosely on its hinges. The ram is pulled out of the way, giving us in front a view at last of what awaits us inside."

"Can't see how many total. But a row of shields sits patiently ahead of us, blocking the entrance hall. Above the shields are arms holding javelins. Let's get rid of those first."

v "First maniple, shields up and prepare for engagement!"

"The first group of hastasti, led by me in place of a centurion, raises their shields. I advance us through the door, which we can manage about five at a time, shields up the whole way."

"The javelins do not loose yet."

scene HallDay
with fade

"Inside we spread out, our line filling the width of the hallway to match the enemy's. Ten men including myself in the front row, shields up; ten men behind us, ready to step forward and replace any man who dies."

"Still, the enemy does not attack."

v "Advance!"

"We push forward, masked by our shields, closing the distance between us and the foe. Thirty feet. Twenty-five. Twenty-"

"There we are. They hurl their missiles."

"A quick storm, and its over. Our line's shields are stuck with javelins, made heavy and awkward by the extra weight."

"The line stops the advance, holds even without my orders. This is standard procedure."

"Funny thing is, Sejanus's men have been trained in these exact tactics, they must know what our game is."

v "Replace shields!"

"The javelins have lead points, soft enough to bend when their weight pulls them down after being embedded in a shield."

"This renders shields unwieldy and useless, and is an excellent tactic to use on just about all armies since they tend to not carry many extra shields at the ready."

"Raskyans, though-"

"With drilled precision, our fellows behind us pass fresh shields up to the front line. We all exchange for the new defenses, keeping our eyes out for more javelins in the split-second we must uncover ourselves."

"But no more come. They must be out, they were never going to be well supplied in a situation like this anyway."

"With new shields, our advance continues."

"In no time, we close the gap."

"And the battle begins. Raskyan swords against Raskyan shields, just as yesterday."

"Men yell, men push. Shields smash against shields and iron scrapes bronze."

"I throw my weight behind my shield and push into the line, meeting an opponent."

"He holds his shield firm, strong. Too firm."

"He doesn't even notice when I simply reach my sword over his shield and stab down into his neck."

"He falls and I draw back, just as his comrades swing too late at where my arm had been."

"Close, but every second in every battle is close."

"Ghosts be good, let this day be my last battle. One way or the other."

"Some of my men die, but I can't pay attention to them. They're instantly replaced in line by the man behind them as others drag the bodies and wounded back, getting them out of the way."

"If we didn't this hallway would become congested with corpses, which would almost be a great defensive strategem if Sejanus had the numbers to pull it off."

"But they need every man they can get, and after several minutes of this slaughter, they begin to fall back."

"The smart thing to do would be to give a little, here and there, letting us push them back, until we are surprised by units that ambush us from doors at our sides"

"On most armies that would work. But not on a Raskyan one."

v "Advance!"

"We step over the bodies, pushing down the hall. The men ahead of us continue their retreat, falling back much faster now."

"They couldn't make it more obvious they've set a weak trap. There are entry ways on both sides of us ahead, likely with men in each room waiting to strike our sides when we reach them."

v "Shields up on the sides, prepare to be pincered!"

"I feel for the bastards a little. What can they be expected to do against an enemy that knows all their drills?"

"And sure enough, as we pass by the entrances, there are yells and a ripple of pressure whips through us as men on both sides of us brace their shields and are pushed back agaisnt one another."

"An unprepared enemy would be cut in two with Raskyans on each side like this."

"But our shields were up, and our minds were steeled. And we hold like a rock."

v "Keep going! Let second and third maniple deal with them!"

"Our momentum, stalled as we defended our sides, returns and carries us forward. The maniple of enemies ahead, who had been retreating, hold firm again."

"Seeing that their trap failed, what else are they to do but fight?"

v "Attack!"

"Another advance, and the shields crash again."

"The man ahead of me, blood in his eyes already, stabs out at me, striking my shield again and again."

"He's scared, he's not thinking, he's just biting like a dog."

"When he steps forward to attack again, I let the blow glance off my shield, and step to the side."

"I swear I see the surprise in his eyes as he stumbles forward, into the small gap in our line I made just for him."

"My men see this and react instantly: they pull him back into the second and third lines, and hack him to pieces."

"I close the gap as quickly as I opened it. Not for nothing do they say that Raskyans eat their prey."

"The fighting continues. I am not a great warrior by any means, but I am a trained one, and the Raskyans fight as a single flexible body."

"It makes up for a lack of individual skill. So long as I give the right orders and do things by the manual, I can do this."

"At some point, we break them and they flee. They split up, disorganized, fumbling to get away."

"Some are stabbed in the back, hamstrung, as we press after the runners."

"Behind us, the other maniples have begun to pour down other halls, breaking into rooms, searching, killing."

"I assumed my speech would do nothing to prevent this, but it was worth a try. I just have to hope I don't lose too many men before we get to Sejanus."

"Up ahead is the high hall, where it looks like several soldiers have gathered. I can't waste time with them."

v "Halt! Which maniples are present?"

"I shout it back into the mass of men behind me, most of whom have only had to march behind us in an orderly manner. A quick series of reports gives me my answer."

v "Then fifth, six and seventh maniple, take the high hall! The rest of you, continue to follow me!"

"The trumpeter blares my orders, the sound ringing my ears in the hallway."

"Morale seems good, energetic. The better you do in battle, the better you continue to do. This is going much better than I had feared."

"But I need to move faster."

"The temple is at the very top of the citadel. Ghosts, I pray these men feel like climbing stairs today."

if plan == "escape":
    jump wedding
    
if plan == "clodia_poison":
    jump poison_kiss
    
label wedding:
    
scene TempleDay
with fade

"What is going on?"

"I can see the mouths moving around me. The nobles and their families shuffle in place, chattering to one another, in attendance for no reason other than this is the farthest place away from the attackers below."

"The priestesses move their hands. They wave their lanterns of incense."

"The Face of the Ghosts carved upon the altar seems to beckon me."

"Sejanus takes my hand and kisses it, and my forehead. Someone tells me to bow to him."

"I obey."

"This is what I promised, didn't I?"

"Everyone can see the Immortal Legion outside the windows, our saviors. That must explain the energetic mood."

"I can't share it, don't understand it."

"Jars of wine are passed around. I'm sitting next to Sejanus and people are toasting us and congratulating us."

"Little girls and maids want me to kiss their hands so they'll be blessed with good husbands one day."

"I give them what they want, but say nothing."

"I wonder if any of them notice I don't seem happy."

"I don't suppose any of them should care. They're realizing that they will get to live."

sj "Tell me."

c "Hm?"

"He's whispering it. What could he have to hide from these people who are powerless against him?"

sj "Am I really so frightful?"

"How to answer that? I think it's an honest question."

c "Probably no more or less than any other husband that would have been found for me."

sj "Then smile, you have things no worse off than other ladies. You're about to be richer than all of them."

c "I just... don't care."

"I don't know how else to put it."

"His only answer is to turn from me and continue celebrating with the nobles."

"The only strange moment is when a soldier boy comes running into the temple and whispers in Sejanus's ear."

"My new husband's face doesn't change, but the soldier boy looks terrified as he rushes from the temple, his message delivered."

"What is there possibly for them to fear now?"

"..."

"But if I listen closely, over the congratulations and the drunken merriment..."

"I can hear swords clashing."

jump ending_marriage

label ending_marriage:
    
scene black
with fade

"We're almost there, but..."

scene HallwayBloody
with fade

v "Look out!"

"I'm too late to stop the sword from cleaving the head of the soldier next to me."

"Another fills his place in line as blow after blow hammers my shield. We're running out of reserves."

"We've been cut off. Somehow they got behind us, got behind all of us."

"Someone in the back fucked up, let themselves get taken."

"And now they're to our left, our right... except for our formation, I can't tell which of these men are my enemy."

"All the same, all Raskyans legionaries."

"Fuck. Fuck fuck fuck."

"We're so close. The stairs took the wind out of us, but we were doing so well, hadn't lost too many men."

"But Sejanus had most of his soldiers up here. I figured he would want to stop us as far down as he possibly could, but I underestimated the bastard's penchant for risks."

"Putting most of his men up here, in the hallway just outside the temple. So he could ambush men exhausted from climbing hundreds of steps in full armor."

"Clever fuck. I was too stupid to predict it."

"But we can still make it."

"The temple is right there, I can see its door ahead."

v "Push on! We're close! Sejanus is just ahead!"

v "Noblewomen are waiting for you beyond that door! You assholes want to die before feeling a lady's cunt?"

"I shout anything I can to motivate them."

"Some of them still have the fight in them. They're covered in blood and screaming and cutting and the enemies are falling like leaves before them."

"But others in my maniple are leaves themselves, and when they fall, and are replaced, I can see that it's almost over."

"I began this assault with four hundred men. Now, cut off and surrounded, I think I'm left with barely thirty. And about a hundred surround us."

v "Circle formation! Press forward to that fucking temple, nothing else matters!"

"The men try their best to face outward, to have shields raised on all sides, to stand in a circle."

"But some slip over the bodies and are stabbed and hacked."

"The hall is filled with corpses now, you can't move without stepping over limbs and gore, and the room is hot and wet and smells like piss and shit and blood, and I can't tell if the sick coppery taste on my mouth is coming from my own body or someone else's."

"But we are getting closer."

"Bit by bit, we near the door."

"Soon we've even cleared past the worst of the bodies."

"At last, one of us reaches the door, feels the handles."

"Of course its locked, but this temple was not built to be a stronghold."

v "Break this door down now!"

"Only a handful of men can spare themselves for the task; the rest have to guard us against the deluge of soldiers."

"My ears are still ringing. I think."

"The clashing of iron and wood and bronze pounds through my head."

"Already holes are opening in the wood of the door. Splintered gaps almost big enough to stick your first through."

"But the whole thing has to come down, and faster."

"I was a farmboy, fuck it, I'm good at this."

"Taking my sword in both hands, I join my men in chopping away."

"Come on, come on, come on, this is torture, I can see through to the other side!"

"Break the lock, break the hinges, break everything!"

"COME ON!"

"Something cold strikes my back."

"It hurts... it..."

v "Argh!"

"My legs buckle beneath me, I can't stop myself."

"I crash to the floor, feeling warm blood fill my armor, pool out of it onto the floor."

"Men are trying to help me up. Other men are dying."

"What about the door?"

"It's... it's open."

"They break it apart, bash the shattered pieces aside, and drag me through."

scene TempleDay
with fade

v "Block... block the door..."

"It sounds like more than a pained whisper to me."

"But they are trained, they know, and soon four men have guarded the door behind us, their shields raised to cover the entire passageway."

"Swords and shield and fists beat mercilessly against them, but they are locked together and they hold firm. It will take some time before they weaken enough for the enemy to break through."

"We're here, right? This is the temple. Where is..."

"..."

sj "This is it? There's scarcely fifteen of you."

"Sejanus, marked clear as day by his Praetor's toga, smiles at me from the altar."

"About thirty armed, rested men guard him. They're all smiling too."

"I hobble to my feet, eyes darting about."

"No Clodia."

"Where is Clodia?"

sj "Kill all but the tribune. Leave him for me."

"The soldiers push at us like a wave, organized in their line. They're on us in seconds."

"They ignore me."

"Pass right over me, like fog on a ship."

"They kill my men. Cut them down beside me."

"I'm too weak to even lift my sword."

"They stab the men guarding the door in the back, pull them away. A cheer goes up from the men on the other side, the victors."

"And soon, they're all dead."

"Except me. I am merely surrounded."

sj "Where is Sergius?"

v "... he's out there."

sj "Fighting the Basileus? Hm. In the end, he did not want to face me himself. Pathetic."

sj "Now I am left with you as my final enemy. How... underwhelming."

v "Where is Clodia?"

sj "My wife is safe in the back with the other nobles, away from you."

"Wife..."

"The word hurts. Just that one word hurts like another blade in my back."

"I grip my sword."

sj "Very well then. Come at me."

"His sword is drawn, and he waits."

"I shuffle towards him. It hurts to walk, to stand up straight."

"I can't lift my shield, so I drop it."

"I switch my sword to my off hand, the one that feels stronger right now."

"No one tries to stop me. There is no need."

"I'm almost to him. His blade hangs lazily from his hand."

"I have to swing my blade."

"I have to-"

"To..."

"..."

"I'm dizzy."

"I can't see."

"I just want to sleep."

"The cool tile kisses my cheek."

"The rest of me burns."

sj "Good Ghosts, are you even still alive?"

"I don't know. Just kill me."

"I've failed."

"I'm so sorry, my love."

sj "Hm? What are you doing here, go-"

"I can see blood spilling onto the floor. From where?"

"It's hard to see, but I crane my neck upwards."

"Sejanus is standing there."

"His throat is open, his toga painted red with his lifeblood. Only a wet gurgle pours forth from his lips."

"He falls."

"And Clodia, beautiful, real, with the red knife in her hand, smiles down at me."

"I found you."

"I found you, Clodia."

"She sinks to her knees and holds my head in her lap."

"I can't move, can barely see, but I can feel her. Her warmth."

"I can smell her again. The smell of blood blossoms. The sweetest smell in the world."

"Sejanus's men are coming closer. They didn't know what to do when their master died, but they know they must do something."

"I can see them coming closer. Feel it."

"Run Clodia!"

"I try to speak it, but my lips won't move. My warning dies in my throat."

c "Shh... rest, my love. Let's rest at last."

"She kisses my lips. It feels like a Ghost of Love has come down to embrace us."

"Then, smiling at me with all the love of the world in her eyes, she holds the knife to her throat and opens it."

scene black
with fade

"And then everything is warm, and red, and gone."

jump epilogue_marriage

label epilogue_marriage:
    
scene black
with fade

centered "\"The favored tragedy of the youth today is mortifying. Absolutely vicious in its violence, in its shocking obscenity, in its gratuitous impiety. That tale of Valerius and Clodia is just another on a long list of reasons the young people today are not of sound mind. This will be the death of Raskya, these foolish little children who believe the heat of their loins is worth defying their parents and challenging the state. Do they not see that their very model ends in horrible death? Or do they embrace that? All I know is that it is a depressing time to be a proud Raskyan citizen, when the children want to take everything from us and give it all away. Ghosts help us.\" {p}- Jaragus, famous Raskyan moralizer, from his \"The Death of the Citizen\""

centered "THE END"

return

label poison_kiss:
    
scene TempleDay
with fade

"What is going on?"

"I can see the mouths moving around me. The nobles and their families shuffle in place, chattering to one another, in attendance for no reason other than this is the farthest place away from the attackers below."

"I have no choice, don't I?"

"The priestesses move their hands. They wave their lanterns of incense."

"The Face of the Ghosts carved upon the altar seems to beckon me."

"My life of servitude begins now."

"Sejanus takes my hand and kisses it, and my forehead. Someone tells me to bow to him."

"I obey."

"This is what I promised, didn't I?"

"I did. So Valerius can live."

"So Valerius might live."

"I will be a slave now."

"... but I don't have to be."

"Everyone can see the Immortal Legion outside the windows, our saviors. That must explain the energetic mood."

"I can't share it, don't understand it."

"No one is coming to save me."

"I can only help myself."

"And I have only one weapon to do that with. The weapon my Valerius gave me."

"The priest declares our union. Begs the Ghosts to bless us, to hold us up, to guide us as we lead the people of the land."

"Please, Ghosts... ignore that fool. Listen to me."

"I have a sacrifice for you."

c "Sejanus."

sj "Hm? Speak up, I can't hear-"

"I seal his mouth shut with mine."

"The gasp from the crowd is stomach-churning. Or maybe that's the taste of the liquid in my mouth as I bite down on the little vial I hid in my cheek."

"Bitter."

"And I push that bitterness with my tongue into Sejanus's mouth."

"His tongue tries to feel mine for a moment."

"Sure, drink up, Sejanus."

"Drink up."

"Suddenly, I'm shoved away."

sj "Ghosts be good, I think the priestesses called down too many Ghosts of Lust!"

"A round of laughter goes up, dispelling the tension."

"All around me I see approval from the men and hatred from their wives. Smiles, frowns, all of it."

"No matter what faces these people have made at me my whole life, they've all served the same role."

"You're all so predictable, every last one of you. I hate you all."

"Now what will you traitors do, without your savior?"

sj "Well as much as I'd love to get to the marriage bed, we haven't even celebrated the wedding yet."

sj "Bring out the wine and food, let's all sit at the windows and watch the Basileus come and-"

"He stops."

"His hands go to his stomach, his throat. Some nobles ask him what's wrong."

"He opens his mouth."

"And blood streams from his lips."

"The screams fill the room before he's finished collapsing to the ground."

"Silent, choking words bubble through the blood, but in seconds Sejanus lies still."

"And now... now I feel sick..."

"I have to... sit down..."

"Sinking onto my knees, I'm suddenly surrounded by hands, by frightened voices."

"What's wrong? What's wrong, Praetrix?"

"None of you care. You're just scared."

"I can taste blood. Sejanus's soldiers are shouting at one another, surrounding his body. One of them points at me."

"Ha. And what can you do about it now?"

scene black
with dissolve

"Oh... Ghosts... I can't see..."

"Everything sounds so far away..."

"My stomach hurts... I want to throw up, but... my body won't..."

"My lips are wet. I'm bleeding."

"..."

"Am I dead?"

"Is this death?"

"..."

"..."

"..."

"Hands. On me."

"Light. Can't make it out."

"What?"

"Nothing makes sense. What's going on?"

"Touching my cheek."

"Weeping."

v "Clodia... Clodia, Clodia, Clodia..."

"Valerius..."

"You're here..."

"You're-"

jump ending_mercenary

label ending_mercenary:
    
scene black
with fade

centered "When the Basileus stormed the citadel, I was arrested rather than killed."

centered "They put me in the dungeons for a few days. I was told that any day they would drag me out and throw me into the Pit."

centered "Instead, when a soldier finally did come, he let me out."

centered "Want to work for the Basileus?"

centered "They need good men like you. We understand you were only following your commander."

centered "What does the Basileus want with me?"

centered "With Sejanus and Sergius and Clodia all dead, there's no one to run Dagara."

centered "The Dagarans in the country have rallied again, formed a new rebel army. The Basileus is offering mountains of gold to any man he can get with any experience."

centered "Ah, I see, I see."

centered "I never say \"Yes\" but they free me and give me a small purse of gold anyway."

centered "They give me armor and equipment. Meet here in three days, they say, pointing to some town on a map. Very well, I say."

centered "That night, I buy a horse and ride away from Dagara."

centered "The village where I am to meet the army is the opposite direction. But I still have a ship waiting on me."

centered "A few days later... I'm on that ship."

scene Ship
with fade

"To Durja. Alone."

"Nasrin always said I would like Durja..."

"I just don't know."

"What will I even do there except spend all my money fucking girls who remind me of Clodia and then die penniless in an alley?"

"I suppose it will be mercenary work for me then."

"A life of fighting. Fighting, fighting, fighting."

"I hate fighting. I don't want to fight anymore. I just wanted to love."

"I hate all of this."

"Clodia... did you save my life with yours? If Sejanus's men hadn't been so thrown into chaos by his death, I doubt we would have succeeded in taking the temple."

"If Sejanus lived, I never would have been let out of that cell."

"So... my life and my freedom are because of you, Clodia."

scene black
with fade

"My love. I'm sorry. I don't know what I'm supposed to do with this life you've given me. This life without you."

"I don't know anything at all."

"It... it may be too hard to keep living."

"..."

"..."

"Well, I have a whole voyage by sea to think about it."

"Whenever I want, I can go into the water and find your embrace again."

"So, we'll see..."

"We'll see, my love."

jump epilogue_mercenary

label epilogue_mercenary:
    
scene black
with fade

centered "\"There are no mentions in the history books of Valerius Aureus after this point. {p}Those who suggest the Aureus merchant family could be descended from Valerius tend to overlook that the family itself makes no such claims. {p}The truth will never be known, but comfort can be taken in the fact that Valerius likely reached Durja, as it is known that the ship he boarded did arrive weeks later at its destination. {p}What happened to the man that poets today sing lyric odes to by the hundreds? Who girls hold up as the standard when they beg the Ghosts of Love to bring them a caring husband? {p}Only the Ghosts know. Such is history.\" {p}- Koromanus, from his \"Poetic Heroes of Old\""

centered "THE END"

return

label traitor_siege:
    
scene black
with fade

"Every strike of the ram against the splintered door sounds like a falling tree."

scene StreetsDay
with fade

"They've been at it all night. The door is well constructed, thick. A testament to Dagaran architectural engineering."

"Our ram is Raskya's testament."

s "Heave-ho men! We're almost through."

"Sergius holds his sword aloft, swinging it back and forth in time with the ram."

"Behind us are thousands of men, most of whom won't even be needed in this assault, yet all who wish to participate in the plunder."

"There's not going to be nearly enough inside to go around, this is going to be an absolute disaster."

"We've split into much smaller units, still called maniples although we now stand in groupings of twenty. These units are flexible, capable of flooding enemy passageways rapidly."

"The twenty best veterans in the army make up Sergius's maniple, just ahead of me. Very unusual to put triarii out front, but I suppose Sergius insists on being protected by the best."

"I and the other tribunes lead the next six maniples, each made of twenty hastati, and the centurions lead hundreds more after us, in the standard order."

"Due to the massive amount of maniples it's neccesary to temporarily promote many men to being centurions."

"... and none of it matters to me anyway. I'm not here for victory."

"I'm shaking in my armor. Everyone else is too, but I suppose they're quivering with anticipation."

"Thousands of rapacious legionaries storming a citadel of only about five hundred foes guarding a few hundred nobles, elderly, women and children."

"This is about to become a nightmare. Ghosts of War, I hope you're thirsty."

s "All right men of Raskya, heed my words!"

"All ears that can hear him prick up at his call. Sergius's booming command is about the only thing that could break through the fog of hate and greed and lust thickening these men's minds."

s "Today is a beautiful day. The sun is shining. The air is cool. And I can smell the tears of Dagaran women!"

"The cheer from the army thunders around me like an earthquake."

s "We outnumber them by such a degree it would be insulting for me to waste much time telling you what to do."

s "So here is all I have to say to you: get inside and take everything you see."

s "Take the girls, take the boys, take the gold. Hell, take the old people, I don't care."

s "Don't go burning anything yet, we'd like to get ourselves out alive. Otherwise, tear it up."

s "Soldier or slave or little weeping virgin, get them all."

s "And leave Sejanus to me."

"Their response, louder than before, drowns out the cracks of the ram."

"And then the ram is pulled back from the shattered door."

s "Ghosts of War, fight with us! Drink of this slaughter and know we have served you well!"

s "Now kill everyone!"

"Slaves equip Sergius with his helmet and shield. Now transformed for battle, he points his sword ahead and leads his veteran guard into the citadel."

"All I see is darkness beyond the door from back here, but that won't be for long. My maniple will go in second, a 'reward' for my bravery yesterday."

"More likely he hopes I'll be killed that close to the front. Maybe he's right."

"But I don't need to live long. I just need to get close enough to Sergius to run him through the back. That's all. Simple."

"Okay... all of Sergius's men are in. We're next."

v "Second maniple advance!"

"The rhythm of marching feet and rattling iron fills my helmet. Shield held cautiously aloft, I lead my men through the destroyed door and into the jaws."

scene HallDay
with fade

"A fine sight greets me as my eyes adjust: Sergius's maniple clashing with a group of rebels blocking the way. Both sets of men fill the width of the hallway, and so there is nothing my maniple can do yet."

"We stop marching about ten feet behind Sergius's men, raise our shields just in case a projectile is hurled over the lines, and wait."

"Ahead, Sergius's back is exposed, but he's got a whole line of veterans between him and I, and more all around him."

"I will simply have to wait. As we press forward, surely his men will be weakened somewhere. Surely there will be a gap, an opportunity. Then I can strike."

"I don't need to get out, after all."

"I just need enough time to get in."

jump traitor_ceremony

label traitor_ceremony:
    
scene TempleDay
with fade

"The wedding is a smaller affair than I assumed."

"Traditionally the nobles are all in attendance for this sort of thing. But only soldiers fill the temple."

"The nobles are all hiding. In their rooms, their private fortresses down below. There is nowhere else for them to go, with the full might of Sergius's army ready to fall upon us."

"I told Sejanus they would be safest here. Where it would take Sergius longest to get to them."

"He said \"If you want to delay a predator, drop fresh meat as you run.\""

"Those women. Their children..."

"I don't like any of them, never have. Never had a real place among them."

"Now they're going to suffer in sacrifice."

"Ghosts, why do you hate our miserable race so? What did Dagara do? Why do you want this?"

"The sensations around me are a blur in the back of my mind."

"Chanting, singing, priestesses blowing warm, sweet incense. Sejanus kisses my hand."

"The Ghosts are invoked. The soldiers praise their commander. Happy faces."

"All I can think is that the Grave is creeping closer, floor by floor."

"Somewhere just a little below us, is Hell."

jump traitor_hell

label traitor_hell:
    
scene black
with fade

"We're doing well. Very well. The enemy is so easy to overwhelm."

"We flank them down hallways, rolling over them with our numbers, our training. Our leadership."

"Sergius is a beast. A monster wrapped in a man's skin."

"I've seen him fight many times, but I've never seen him so happy. Never heard him laugh so much."

"He sounds drunk. His men all laugh the same way."

"Every one of them seems so joyous. The energy is unreal. It's electric."

"The Ghosts must have been summoned successfully. I can feel them here, flitting around us, blessing the slaughter."

"And with the enemy dead, dying, falling back, dragging their injured to safety..."

"... what they protected is now naked and vulnerable."

s "Take whatever you want, my boys! It's all ours!"

scene HallBloody
with fade

"Our maniples break down in minutes, as men pair off in twos and threes to go roam the halls."

"Doors are burst open, kicked down, hacked apart. And treasures are pulled from the rooms."

"Gold. Jewels. Fine food, cutlery, carpets, fabric. The smell of liberated spices punctuates the rusty blood in every nose for a moment."

"But those aren't really the goal, and they don't need to be taken now. Better to secure it and cart it back to camp once the battle is over."

"None of that is what Sergius really meant anyway. He means to add to the sacrifice."

"The screams of women pierce the cheering of men like an arrow. Noble ladies dragged from rooms and into dark corners, their clothes torn from them every step of the way."

"Further lamentations echo from near every room."

"My maniple has completely dissolved and joined in the carnage."

"I am left standing in the halls, looking around at my comrades. I know I should not look in the rooms, but curiosity is an evil thing sometimes."

"Inside one room, I see two women huddled naked in the corner, as soldiers wave their swords at them, laughing as they threaten to stab their breasts."

"Prayers never sounded so desperate."

"In another, the floor is just a mass of writhing soldiers on top of naked, bloody women. Some move. Some don't."

"In another, a group of men are standing around something, laughing, cheering."

"A quick movement lets me see what excites them: a little girl bent over a table, one man thrusting into her violently."

"When he settles his business and pulls away, wiping his brow with a smile, the next man has his turn. The girl seems to make no sounds."

"In another, two bodies lie in bed. One is female, her breasts cut off and her throat slit. The other is a male, holding her, his penis gone and his chest torn open."

"There are no soldiers here. They did this, and promptly left."

"In another, two naked girls kiss each other like lovers, tears running through their makeup. Men stand around them, laughing, waving swords."

"\"Now stick your fingers in each other's pretty little cunts or else,\" I hear one of them shout. The girls obey, to roars of approval."

"In another is simply a single man fucking a girl's corpse."

"The knife protudes from her back, she doesn't move. He keeps fucking."

"\"Dagaran whore,\" he growls, over and over."

"I've seen this so many times in the last six years. This same sort of thing, every time."

"Before it was always peasants and farmer's children, but it's all the same."

"Some of these men will remember this as the greatest day of their lives."

"All I know is there's only one man whose death might put a stop to this whole thing. And I can't find him anywhere."

"My room by room search turns up nothing. Sergius has vanished in the chaos."

"Fuck. This would be the perfect time to murder him, with the maniples disbanded as such."

"But then, I come across someone who might know."

"Tribune Antonius is fucking a young boy on the floor of one room. No one else is in here."

v "Tribune Antonius."

tra "What is it? You? Find your own ass to bugger, there's plenty to go around."

v "Where is Sergius? I can't find him."

tra "Ghosts be good, leave me alone! He went on ahead to get Sejanus."

tra "You better hurry if you'd rather be involved in that than this, you woman."

v "Very well."

"Damn. How did I lose track of Sergius that long? I suppose I was more captivated by the horrors around me than I thought I would be."

"I need to get going."

"But first, I look around to be absolutely sure the room is empty save myself, Antonius, and the whimpering boy he's fucking."

"All clear."

"Antonius removed his helmet before he attacked this boy. They usually do, easier to have that bothersome thing off during a rape."

"With an almost lazy swing from behind, I bury my blade in the back of Antonius's skull."

"Pulling the twitching corpse off the boy, I sheathe my sword, and leave. I've done all I can do here."

"And if I thought I would survive this day, I probably wouldn't have risked it anyway."

"If I hate this all so much, I need to kill the man responsible. I'm wasting time."

"Drawing on my memories of the citadel's layout, I hurry towards the temple."

jump dancing_ghosts

label dancing_ghosts:
    
scene black
with fade

"Ascending the stairs, I feel my legs burning, my armor weighing me down, but I press on."

scene HallBloody
with fade

"I gave them a huge head start, but they can't have gotten that far."

"And I have the advantage that they've already killed everyone in their way. Dozens and dozens of corpses fill the halls I pass through, and riotous behavior sounds from every room."

"This entire citadel has been transformed by the Ghosts into the Grave."

"The temple is near. I have to be close... I have to be close..."

"There!"

s "Come on you traitors! Face me! Fight me!"

"Sergius's men travel ahead of me through a red mist as they hack at the rebel defenders. Beyond them all, I can see the doors to the temple."

"But now, there is nothing between Sergius and I. His men are spread out, fighting, sloppy on their defense as they drink victory."

"My sword drawn, I walk towards Sergius."

"No need to make a big deal of it. Once I stab him, I can try to get away, but it's unlikely it will go completely unnoticed."

"This is it then."

"Clodia... this is so you may live..."

"I'm right behind him. Readying my sword, I decide on the best gap in his armor."

"There, under his arm. This will be an awkward angle to stab at."

"But I can do it."

"Just do it."

"Go."

"I move my blade-"

s "ARGH!!"

"Sergius falls."

"Falls back, almost on top of me."

"Without thinking, my arms go up to steady him, my sword clanging to the floor."

"He's incredibly heavy, thick with muscle and metal, but I'm able to let him down to the floor as other men notice and rush to their commander's side."

"They scream for doctors, for the medic slaves. The battle keeps raging, it cannot stop for Sergius, but it's nearly over, the enemy fleeing for the temple in terror."

"The victors cheer, then turn and see what has become of their leader, and every face pales."

"And I am ignored."

"Of course I am."

"I feel the rush of adrenaline from my murder, the sense of wrong from having killed my commander. I felt it all in the moment I moved my blade to end him."

"I can't stop feeling it, even though I did not get my chance."

"In that moment, a javelin found a gap, probably the smallest gap on his armor, between the breastplate and the helmet."

"And now, Sergius is bleeding out on the carpet next to the trodden corpses he made minutes before, a lead point hooked into his neck."

"Someone removes the hook, but that just tears more veins and flesh with it, and he bleeds faster. Only faint wet rattles bubble up through the lips, through the hole in the neck."

"His helmet is removed, and the ashen face of Sergius stares blankly beyond his men."

"In this moment, he is supposed to be able to see the ghosts dancing amongst us. But he cannot speak what he sees. It is forbidden."

"Can he see the Ghost that guided that javelin true?"

"The hall is clear of enemies, but none of our soldiers press forward. They crowd around Sergius, weeping, wailing. Never have I seen such tears flow from these men before this moment."

"The way to the temple is clear."

"..."

"I don't know what to do. I shouldn't be alive right now, I didn't plan for this."

"..."

"Clodia... she's probably right there in that room."

"..."

"I'm coming, Clodia."

"What's that?"

"Oh shit."

"The Immortal Legion's arrived."

"Damn, they got up here quick, they must have not been far behind me."

"They're killing the remainder of Sergius's guard."

"They see me."

"Shit."

"Clodia!"

"My shield falls to the ground as I turn and run."

"I don't think anymore."

"I have to see Clodia."

"I'm almost to the door."

"I can hear them behind me."

"Clodia!"

scene TempleDay
with fade

"Clodia!"

c "!"

"There she is."

"There are others in the room, but I don't notice them."

"She may be the only person in the world. I can see her so clearly."

"She's beautiful. She's here."

v "Clodia!"

"She turns."

"Our eyes meet."

"And then-"

"I feel the arms grabbing me. Dragging me."

v "No!"

"Something cold locks into my back. I scream, the world goes dizzy and heavy."

scene black
with fade

"It's cold, and warm. And dark."

"Clodia..."

jump ending_water

label ending_water:

scene Dungeons
with fade

"I don't know how long I've been down here."

"A few days?"

"I never see anyone. The jailer comes by with slop twice a day, that's it. He ignores me if I ask him anything."

"I don't know anything that's going on at all. I just woke up here. And I've been sleeping and waking here ever since."

"I would think there would be other soldiers locked up near me, but when I shout there's no response."

"Why didn't they kill me? I was stabbed in the back, that's pretty clear. It hurts like a bitch."

"But there's also bandages wrapped around me. They bothered to save me from dying. Doesn't make a fig of sense."

"Unless... I'm to be here for the rest of my life, aren't I?"

"Ha."

"Clodia... I'm glad I got to see your face one last time."

"Someone's coming. But the jailer just fed me... I think. It sure seems like it hasn't been long."

n "Ah, there you are."

v "Nasrin? You... why'd you come back?"

n "I heard you were not quite as dead as you swore you would be."

v "Heard from who?"

n "I get that you're confused, but could you add a little gratefulness in there?"

"She jangles a thick ring of keys in the torchlight."

v "Thank you. Sorry, I'm just... I don't understand what's going on. I shouldn't be alive."

n "So come out of there and figure out what you're going to do next."

"She pulls the door open, gesturing me out."

"I can't move very fast, every step causes pain to surge through my back."

"As I stumble out the door, Nasrin gets under my arm and supports me."

v "Thank you, Nasrin."

n "Yeah yeah, don't mention it."

"You ask for gratitude and then react that way."

"Ah well."

"She begins to lead me, at no great rush. I haven't seen many guards down here, and looking at all the other cells, they seem almost all empty."

"They didn't take many prisoners. But they took me."

"Why?"

v "What happened up there?"

n "I wouldn't know the politics of it, but the Basileus is in control. Sejanus is praetor. And the great Sergius got a javelin in the neck, so sad."

v "Sejanus is praetor. With..."

n "Yeah."

v "But she's alive? She's safe?"

n "Oh yeah."

"Isn't this what I wanted, then?"

"Well, yes."

"But..."

"I wished to be dead so I wouldn't have to live through it."

v "I... I don't know what to do with myself anymore."

n "What's that supposed to mean? Cause the army's gone? Just become a mercenary, that's what most soldiers in your position do."

v "I'm sick of fighting. Done with it."

n "You wanna go back to being a farmer? Ha, might be your best shot."

v "My farm has probably been taken by the Basileus by now. It's standard policy to confiscate the land of traitors."

n "Well the fields of Durja are fertile. Sometimes."

v "Durja? Why Durja?"

n "Have you forgotten that was the entire plan?"

v "With Clodia."

n "Well you sure as the Grave shouldn't stay in Raskya. Come on, if we ride day and night we can reach your ship before it leaves."

"She sounds so certain, so sure. I don't understand."

"All I can do is follow her."

scene black
with fade

"Two sleepless days and nights in the saddle later, with a lightning stop on the way to withdraw my gold from the goldsmith at Marasima, we reached the sea, and the ship."

"The captain was angry at us for making him wait, but I slipped a few extra coins into his palm and he got over it."

"And then... we were off. To Durja, I suppose."

scene Ship
with fade

n "Mmm."

v "What?"

n "Nothing."

v "Ah."

n "Mmmmmmmm."

v "What is it?"

n "Nothing."

v "Will you stop that? I know it's not nothing."

n "It's just... we're all alone on this ship for a week. There's nothing to dooooo."

v "Sorry, don't know if you noticed, but I'm a little disgusting after spending a few days in jail."

n "There's a bathhouse at the town we're stopping at. We should both freshen up."

v "I suppose."

n "Come on, it'll be fun."

v "Much isn't fun right now."

n "Damn you."

v "What do you want from me? Have you no sympathy?"

n "Of course I do. And I'm promising you, if you fuck me, you'll get over her."

"I almost strike her for that, but I keep myself composed."

"Rising to my feet, I head for the deck."

n "Where are you going?"

v "Clear my head. It's stuffy down here."

n "Aww, did I make you mad? I'm sorry."

v "It doesn't matter. A whore wouldn't understand."

"I leave before she has any chance to respond."

"I hope that hurt her."

scene black
with fade

"It's cold out on deck. Good."

"I'm too hot."

"I could use a swim."

"Yes..."

"A swim..."

jump epilogue_water

label epilogue_water:

scene black
with fade

centered "\"The Raskyan is red for the blood on his sword, {p}His passion for life and his lust. {p}So any red soldier that ends his own life, {p}Is no true Raskyan, we trust.\" {p}- Urotos, from her epic poem \"Raskyalus\""

centered "THE END"

return

label lightning_siege:

scene black
with fade

"Every strike of the ram against the splintered door sounds like a falling tree."

scene StreetsDay
with fade

"They've been at it all night. The door is well constructed, thick. A testament to Dagaran architectural engineering."

"Our ram is Raskya's testament."

s "Heave-ho men! We're almost through."

"Sergius holds his sword aloft, swinging it back and forth in time with the ram."

"Behind us are thousands of men, most of whom won't even be needed in this assault, yet all who wish to participate in the plunder."

"There's not going to be nearly enough inside to go around, this is going to be an absolute disaster."

"We've split into much smaller units, still called maniples although we now stand in groupings of twenty. These units are flexible, capable of flooding enemy passageways rapidly."

"The twenty best veterans in the army make up Sergius's maniple, just ahead of me. Very unusual to put triarii out front, but I suppose Sergius insists on being protected by the best."

"I and the other tribunes lead the next six maniples, each made of twenty hastati, and the centurions lead hundreds more after us, in the standard order."

"Due to the massive amount of maniples it's neccesary to temporarily promote many men to being centurions."

"... and none of it matters to me anyway. I'm not here for victory. Just Clodia."

"I'm shaking in my armor. Everyone else is too, but I suppose they're quivering with anticipation."

"Thousands of rapacious legionaries storming a citadel of only about five hundred foes guarding a few hundred nobles, elderly, women and children."

"This is about to become a nightmare. Ghosts of War, I hope you're thirsty."

s "All right men of Raskya, heed my words!"

"All ears that can hear him prick up at his call. Sergius's booming command is about the only thing that could break through the fog of hate and greed and lust thickening these men's minds."

s "Today is a beautiful day. The sun is shining. The air is cool. And I can smell the tears of Dagaran women!"

"The cheer from the army thunders around me like an earthquake."

s "We outnumber them by such a degree it would be insulting for me to waste much time telling you what to do."

s "So here is all I have to say to you: get inside and take everything you see."

s "Take the girls, take the boys, take the gold. Hell, take the old people, I don't care."

s "Don't go burning anything yet, we'd like to get ourselves out alive. Otherwise, tear it up."

s "Soldier or slave or little weeping virgin, get them all."

s "And leave Sejanus to me."

"Their response, louder than before, drowns out the cracks of the ram."

"And then the ram is pulled back from the shattered door."

s "Ghosts of War, fight with us! Drink of this slaughter and know we have served you well!"

s "Now kill everyone!"

"Slaves equip Sergius with his helmet and shield. Now transformed for battle, he points his sword ahead and leads his veteran guard into the citadel."

"All I see is darkness beyond the door from back here, but that won't be for long. My maniple will go in second, a 'reward' for my bravery yesterday."

"More likely he hopes I'll be killed that close to the front. Maybe he's right."

"But I can do this. I just have to keep close behind Sergius and do this by the book. We will win."

"We will. I'm coming, Clodia."

"Okay... all of Sergius's men are in. We're next."

v "Second maniple advance!"

"The rhythm of marching feet and rattling iron fills my helmet. Shield held cautiously aloft, I lead my men through the destroyed door and into the jaws."

scene HallDay
with fade

"A fine sight greets me as my eyes adjust: Sergius's maniple clashing with a group of rebels blocking the way. Both sets of men fill the width of the hallway, and so there is nothing my maniple can do yet."

"We stop marching about ten feet behind Sergius's men, raise our shields just in case a projectile is hurled over the lines, and wait."

"A surprising amount of battle is waiting."

"But waiting has never been more painful. My energy is fighting to burst through my skin and out my armor."

"I'm ready."

jump lightning_ceremony

label lightning_ceremony:

scene TempleDay
with fade

"The wedding is a smaller affair than I assumed."

"Traditionally the nobles are all in attendance for this sort of thing. But only soldiers fill the temple."

"The nobles are all hiding. In their rooms, their private fortresses down below. There is nowhere else for them to go, with the full might of Sergius's army ready to fall upon us."

"I told Sejanus they would be safest here. Where it would take Sergius longest to get to them."

"He said \"If you want to delay a predator, drop fresh meat as you run.\""

"Those women. Their children..."

"I don't like any of them, never have. Never had a real place among them."

"Now they're going to suffer in sacrifice."

"Ghosts, why do you hate our miserable race so? What did Dagara do? Why do you want this?"

"The sensations around me are a blur in the back of my mind."

"Chanting, singing, priestesses blowing warm, sweet incense. Sejanus kisses my hand."

"The Ghosts are invoked. The soldiers praise their commander. Happy faces."

"All I can think is that the Grave is creeping closer, floor by floor."

"Somewhere just a little below us, is Hell."

jump lightning_hell

label lightning_hell:
    
scene black
with fade

"We're doing well. Very well. The enemy is so easy to overwhelm."

"We flank them down hallways, rolling over them with our numbers, our training. Our leadership."

"Sergius is a beast. A monster wrapped in a man's skin."

"I've seen him fight many times, but I've never seen him so happy. Never heard him laugh so much."

"He sounds drunk. His men all laugh the same way."

"Every one of them seems so joyous. The energy is unreal. It's electric."

"The Ghosts must have been summoned successfully. I can feel them here, flitting around us, blessing the slaughter."

"And with the enemy dead, dying, falling back, dragging their injured to safety..."

"... what they protected is now naked and vulnerable."

s "Take whatever you want, my boys! It's all ours!"

scene HallBloody
with fade

"Our maniples break down in minutes, as men pair off in twos and threes to go roam the halls."

"Doors are burst open, kicked down, hacked apart. And treasures are pulled from the rooms."

"Gold. Jewels. Fine food, cutlery, carpets, fabric. The smell of liberated spices punctuates the rusty blood in every nose for a moment."

"But those aren't really the goal, and they don't need to be taken now. Better to secure it and cart it back to camp once the battle is over."

"None of that is what Sergius really meant anyway. He means to add to the sacrifice."

"The screams of women pierce the cheering of men like an arrow. Noble ladies dragged from rooms and into dark corners, their clothes torn from them every step of the way."

"Further lamentations echo from near every room."

"My maniple has completely dissolved and joined in the carnage."

"I am left standing in the halls, looking around at my comrades. I know I should not look in the rooms, but curiosity is an evil thing sometimes."

"Inside one room, I see two women huddled naked in the corner, as soldiers wave their swords at them, laughing as they threaten to stab their breasts."

"Prayers never sounded so desperate."

"In another, the floor is just a mass of writhing soldiers on top of naked, bloody women. Some move. Some don't."

"In another, a group of men are standing around something, laughing, cheering."

"A quick movement lets me see what excites them: a little girl bent over a table, one man thrusting into her violently."

"When he settles his business and pulls away, wiping his brow with a smile, the next man has his turn. The girl seems to make no sounds."

"In another, two bodies lie in bed. One is female, her breasts cut off and her throat slit. The other is a male, holding her, his penis gone and his chest torn open."

"There are no soldiers here. They did this, and promptly left."

"In another, two naked girls kiss each other like lovers, tears running through their makeup. Men stand around them, laughing, waving swords."

"\"Now stick your fingers in her cunt!\" I hear one of them shout. The girls obey, to roars of approval."

"In another is simply a single man fucking a girl's corpse."

"The knife protudes from her back, she doesn't move. He keeps fucking."

"\"Dagaran whore,\" he growls, over and over."

"I've seen this so many times in the last six years. This same sort of thing, every time."

"Before it was always peasants and farmer's children, but it's all the same."

"Some of these men will remember this as the greatest day of their lives."

"Suddenly, I realize I've lost track of Sergius. I can't see his blue cloak anywhere in all the red."

"But then, I come across someone who might know."

"Tribune Antonius is fucking a young boy on the floor of one room. No one else is in here."

v "Tribune Antonius."

tra "What is it? You? Find your own ass to bugger, there's plenty to go around."

v "Where is Sergius? I can't find him."

tra "Ghosts be good, leave me alone! He went on ahead to get Sejanus."

tra "You better hurry if you'd rather be involved in that than this, you woman."

v "Very well."

"Damn. How did I lose track of Sergius that long? I suppose I was more captivated by the horrors around me than I thought I would be."

"I need to get going."

"But first, I look around to be absolutely sure the room is empty save myself, Antonius, and the whimpering boy he's fucking."

"All clear."

"Antonius removed his helmet before he attacked this boy. They usually do, easier to have that bothersome thing off during a rape."

"With an almost lazy swing from behind, I bury my blade in the back of Antonius's skull."

"Pulling the twitching corpse off the boy, I sheathe my sword, and leave. I've done all I can do here."

"I'm wasting time."

"Drawing on my memories of the citadel's layout, I hurry towards the temple."

jump duel_between_rivals

label duel_between_rivals:
    
scene black
with fade

"Ascending the stairs, I feel my legs burning, my armor weighing me down, but I press on."

scene HallBloody
with fade

"I gave them a huge head start, but they can't have gotten that far."

"And I have the advantage that they've already killed everyone in their way."

"Dozens and dozens of corpses fill the halls I pass through, and riotous behavior sounds from every room. This entire citadel has been transformed by the Ghosts into the Grave."

"The temple is near. I have to be close... I have to be close..."

"There!"

s "Come on you traitors! Face me! Fight me!"

"Sergius's men travel ahead of me through a red mist as they hack at the foe. Beyond them all, I can see the doors to the temple."

"Heaving from the climb, I draw in my breath and rush to the battle line."

"As I come up behind Sergius, I see a man hurling a javelin towards him. It sails over the heads of the enemy line, straight at the commander."

"Just in time, I get to him and raise my shield, batting the projectile away."

s "Aureus? Where have you been?"

v "Trying to find you, legatus."

s "Ghosts be good, in the end it's you by my side while my other tribunes fuck themselves senseless."

s "Very well, press on! Kill!"

"We make short work of the leftovers."

"Then, the bodies are dragged to one side, and the door to the temple stands before us."

"Of course, it's locked. But it's only wood."

s "Cut this bastard open, lads! And then, hold yourselves back."

s "If any of you touches Sejanus before I wring his scrawny neck, I'll tear your pecker off and sell you as a girlboy!"

"No worries about me, legatus. Have your duel. It'll make a good distraction while I find Clodia."

s "What is it you're doing after this, Tribune?"

v "Hm? Ah..."

s "Retiring at last?"

v "Yes."

s "Hm. Well, I'll see to it you have a decent pension."

v "Ah. Thank you, legatus."

s "Thank me by paying close attention in here. The history books will want to know every detail of what transpires in this room."

s "We're giving the playwrights their grand climax now. Let us not disappoint them."

v "I shall do my utmost, legatus."

"Whatever. Enjoy your immortality on the stage as a scrawny Peladonian boy in a mask."

"As last, they tear the remnants of the door away and breach the room, shields lifted in case of a volley."

"But when no sounds of battle erupt from beyond the door, Sergius and I stride in behind the advance guard."

scene TempleDay
with fade

"The Dagaran Temple would normally be an impressive sight, but my eyes can only focus on the people in it."

"About thirty legionaries, shields up, ready for battle."

"At their head, Sejanus himself."

"and-"

c "!"

"Clodia."

"There she is!"

"At Sejanus's side."

sj "There you are."

s "Great Ghosts of Women, you look as girly as ever."

sj "And every word out of your mouth still fills me with violent urges."

s "Your chance has finally come. Sejanus the Betrayer, I challenge you to single combat."

sj "Oh, so it's the Grand Spoils you're after? Ha, of course you would be so arrogant."

sj "Very well Sergius the Dead, I accept."

"The nearest soldier to Sejanus passes him a shield and sword. Then, the entire line of men backs away, almost to the far wall."

s "Well stand back, men."

"All of the guard that accompanies us steps back as well. Except for me. It is Sejanus who notices first."

sj "Someone seems to have a grievance with our combat."

"I have a grievance with just about every person and thing in these walls."

"Except for-"

"Clodia nods at me. Smiles."

"My love..."

s "{i}Stand back{/i}, Tribune."

"Very well. There's nothing I can do yet. I'll simply watch this battle and react accordingly."

"Obeying, I fall in line."

"Satisfied, Sergius and Sejanus walk towards each other in the center of the temple."

"Before, so many men have been running and clashing and yelling that my mind has forgotten what silence is; now the footsteps of these rivals echoes through the stone walls and between the ears of the solemn men on both sides."

"This is, after all, only the third time this has happened since the birth of Raskya. And since both of the combatants are Raskyan generals, no matter who what happens the Grand Spoils will be claimed today."

"This is momentous, really, something the historians will pore over for centuries."

"The two commanders stop just about ten feet from one another."

"Sergius is almost a head taller than his opponent, and nearly twice as thick. But the confidence on Sejanus's face is as though he were facing a freshly drafted boy."

sj "Do you remember the day you stole everything from me? We were on the same side and you stabbed me in the back for your glory."

s "Pups who rise above their station need a good beating. If you had been more patient, you would have been a great legatus."

sj "Do not stand there and lecture me as if you are my commander. We are equals now."

sj "And then, only for a few minutes more."

s "Hm. Surprising. I agree with you."

"My fellow soldiers are rapt."

"If I'm honest, I am too. Despite my wishes."

"The duelists raise their shields, swords ready."

"Dueling is a very different art than line combat. The extra room to maneuver changes everything. Only having to worry about a single opponent changes everything."

"In my opinion, having never dueled anyone, it seems far more difficult."

"Throw a recruit into a line and as long as he keeps his shield up, his fellows' strengths combines with his and they have a decent chance of victory."

"That same recruit against an even moderate duelist is almost certain to die."

"I've never known Sergius to duel. He seems like he could kill just about anyone, but he's always acted as though dueling barbarians was beneath him."

"Maybe it was."

"Sejanus, though... he won three duels against Dagaran officers during the war. Never anyone of enough importance to warrant the Grand Spoils, but in retrospect, I suppose he was practicing."

"They spend a moment staring at one another."

"Something like this is certain to please the Ghosts of War. They've already filled this citadel, but this is the climax to their sacrifice."

"If Sergius wins, then he's to be blessed by the Ghosts of War eternally, here and up in the Air."

"If Sejanus wins... well, we'll charge and try to kill him and his men."

"I can't see Clodia anymore, they've moved her behind the line of rebels. Damn it. I need to get to her before the Immortal Legion arrives."

"They may be but an hour away. They may be filling the streets."

"Over here, we're too far from the windows to see, but it's just as well. If they're out there, I don't want to see them."

"This is in my commander's hands now."

"It's Sejanus who moves first."

"He steps forward cautiously and stabs straight at Sergius, who takes it easily on the shield."

"He stabs back, and Sejanus blocks it and backs off."

"Now that battle has begun, the momentum goes to Sergius quickly. He keeps moving forward, threatening with his shield and his size, and Sejanus continues to back away."

"He backs almost entirely into his men, and only then does Sergius halt."

"Per the rules of honor as they are commonly understood, if a combatant goes too near his opponent's men, they are allowed to strike at him. And vice versa."

"Some duels have famously been won by men who purposefully attempt to lure their opponent into their waiting allies."

"But Sergius won't have that. He returns to the enter of the room, and Sejanus follows him."

"Shields at the ready again, their weapons make contact, and now they fight more as I would have expected."

"Quick, testing thrusts beyond the shield, searching for an opening, trying to intimidate."

"Sergius is more forceful with his stabs, throws more weight behind his shield, but Sejanus handles it all well."

"He keeps taking Sergius's blows at an angle and shifting his body to the side, until the two have switched sides."

"Now Sergius, drive him towards us!"

"But if that was the commander's plan, he loses the opportunity, as Sejanus dances away and reorients himself."

"Some single combats happen in much smaller rooms than this. Sejanus knows he can take advantage of his speed here."

"Several of Sergius's swings only strike air as the so-called Praetor twists and turns and backs away. Always backing away."

"Of course, he must be trying to tire Sergius out. But how long will that take?"

"And anyway, all it will take is one mistake and Sergius will cleave him in two."

"Just as I think that, Sejanus stumbles."

"Just for a moment, and he recovers well, but it's enough for Sergius."

"His blade comes down right on Sejanus's sword arm, just above his gauntlet."

"A loud, metal shriek stings the ears as Sejanus pulls away, his right arm limp."

"But not severed. No blood running. How?"

"And with a quick shake, his arm is back up and at the ready."

"Ah, I think I see. The clever bastard has armor underneath his sleeves. Has he armored his upper legs as well?"

"Traditionally these weak points aren't covered on Raskyan soldiers because they increase weight, and a good shield will block those areas most of the time."

"I can almost hear Sergius calling him a coward in his mind."

"Sergius keeps attacking, and Sejanus keeps backing away, twisting in and out of the rows of columns."

"Swords and shields scrape and glance off of the smooth stone pillars as the soldiers crane their necks to not lose sight of the action."

"Then, with a massive surge of speed, Sergius places his full weight behind his shield and-"

"-pins Sejanus to a column."

"A wail of sorrow goes up from Sejanus's men, as cheers go up around me."

"Several seconds pass as Sergius holds his opponent in place with crushing pressure from the shield. Only Sejanus's own shield is stopping Sergius from simply pushing until something pops."

"Just stab him Sergius, do it!"

"But my commander, Ghosts damn him, he seems to be more interested in enjoying the moment."

"He lifts the sword slowly, holding it level with Sejanus's eyes. He holds it there as a taunt, as the shields quiver against each other, Sejanus pushing back with all his might just to not be crushed."

"At last, Sergius drives his blade forward."

"And Sejanus moves."

"It's so quick I don't understand until afterward. All I see is Sergius strike the column, metal scraping stone where Sejanus had been but an instant before."

"Sejanus let go of his shield, and so doing, squeezed free. Sergius now pins a masterless shield to the column."

"He whirls around to face his enemy, but Sejanus has already backed well off. He brandishes his sword, to the applause of his remaining men."

"It was an impressive escape from death, but this is good for us. Without a shield, Sejanus is dead the moment Sergius gets near him."

"Sergius stalks towards his foe."

"And stumbles."

"The gasps are as though his head had been severed. Yet it's only a trickle of blood that runs down his leg. He lives."

"But Sejanus drew first blood. He must have pulled his blade across the unguarded flesh of the back of Sergius's thigh."

"But I didn't see it. I don't think anyone did."

"He's that fast?"

"Growling, Sergius starts moving again. What's one more scar on that monstrous body?"

"But when he reaches Sejanus, it's clear that he's slower. His attacks miss Sejanus completely."

"Without his shield Sejanus is even faster, dodging and turning and always backing away, keeping out of range of every stab and slash and push."

s "Bastard!"

"Sergius throws all of his weight into one gigantic swing."

"Sejanus sidesteps it easily."

"The giant general stumbles, and Sejanus slashes him, slow enough for us to all catch it this time, across the back of his other leg."

"Lower this time."

"No."

"The men beside me cry out in anguish."

"The men backing Sejanus cheer."

"Sergius was probably hamstrung by that. He collapses to his knee, crying out in a deep, demonic howl. His shield and sword are steadying him, but only just."

"Sejanus comes up behind him. His blade is raised, Sergius is unprotected-"

"Yet Sejanus holds."

"Our side is screaming, cursing, losing their minds. Yet Sejanus's men seem almost still as statues. Where's their jubilation at their commander's victory?"

sj "All you loyal Sergian dogs listen well. You stay right where you are. Your legatus is my hostage now."

"Ah."

"Obviously he doesn't want to just kill Sergius. The second he does he'll be overrun by our side."

"But if he just delays until the Basileus arrives..."

"What was that sound? Us?"

"Those trumpets. Ghosts."

"He's here. Here's in the citadel."

"Every one of the men around me look at one another in confusion. They all recognize that signal. It always heralds the Basileus."

"Shit. There's no time for this!"

sj "Well Sergius, that was good. I hope you're satisfied. Your arms and armor will rest in the Temple of War for time eternal."

"Sergius looks up with the most confused expression at his rival. I don't know if he can even fully understand this reversal."

"Ghosts, what do I do? Where is Clodia? There is no time!"

"I have to think of something before-"

if plan == "escape":
    jump too_late

if plan == "clodia_poison":
    jump finish_him
    
label too_late:
                                                                        
"Suddenly, a manic soldier comes running into the temple behind us."
    
so "They're inside! They're coming!"
                         
"And looking past them, I can already see the purple clad veterans marching straight towards us."
                    
"The men near me start screaming."

"I turn and look at Sejanus."

"He's smiling at the new arrivals as he drives his sword into Sergius's neck."

"Chaos."

"Pure chaos erupts."

"Clodia."

"I run."

"Men are charging in every direction."

"Javelins are flying, glancing off shields, striking the walls, the floors, shields, men. Nothing is being aimed, everyone simply panics."

"Clodia."

"Some of the men in their fear have turned and begun to fight the Immortal Legion."

"Some charge straight at Sejanus to avenge their commander, though he has already fallen back behind his men."

"Some simply run screaming at the nearest enemy, swinging their weapon, all discipline gone."

"Clodia."

"I run through the mess. Through the riot."

"I run towards the last place I saw Clodia."

"I'm running right at armed men but I don't care. I slam into them, smashing my shield with all the force I can possibly muster into the small gap between two of them."

"I practically leap off my feet to add to the strength of my hit."

"And somehow, the force breaks me through their line."

"I go sailing between them and smash hard to the tile. My armor rattles my whole body, but all I know is that I got behind them."

"And there's-"

v "Clodia!"

c "Valerius!"

"I clamor to my feet."

"Something hits me from behind, but I'm already running."

"I grab her."

"No time to think. There's nowhere to go, no other rooms."

"I push her into the nearest corner."

"I get one look at her face. Our eyes meet."

"Then I turn, raise my shield, and protect her."

"Javelins."

"They start pounding the shield, sticking into it. My bones shake with each missile, one after the other."

c "Valerius!"

"I have to hold firm."

"Someone is attacking us, someone is beating at my shield. I can't look, I just hold on as tight and hard as I can."

"Two men attacking us now. Hacking and stabbing and glancing off the shield, each blow surging through me."

"I cry out, screaming all the energy I can muster."

"I have to keep Clodia safe."

"My leg. Something hit my legs but I can't fall."

"Something hits my arm but I can't let go of the shield."

"My heart feels like it's going to explode but I can't die. Not yet."

v "Clodia!"

"Screaming her name helps me stand."

"Her name gives me the power to do what I must."

"And then-"

"It stops."

c "Valerius."

"I can't let go of the shield. I can't fall. Can't look."

"Have to keep her safe."

a "Great Ghosts! It smells awful in here!"

"Who was that?"

"Why has the fighting stopped?"

sj "My apologies for all the spilled blood and shit. You can blame this dead man here."

a "Ah, the great Sergius. You delivered the final blow?"

sj "Yes. In single combat."

a "Single combat? You mean to tell me we have the Grand Spoils wrapped around this man's corpse?"

sj "Indeed we do, Your Holiness."

a "Shit, well get them off him. This is fascinating."

sj "Oh, and someone get that man away from my wife."

so "We've tried, he's being stubborn."

"Stubborn? That's all you can call this?"

"They're grabbing at me. Pulling."

c "No, stop! Let him be!"

"I can't resist anymore. All the strength left me when the fighting stopped."

"They pull me away from her."

"Strange. I'm bleeding. There are large cuts on my arms, and my legs."

"Shit, I'm bleeding a lot."

"I'm..."

c "Valerius! Get up! Someone help him, please! He protected me, help him!"

sj "Hm, what a strange man. You're a Tribune, aren't you?"

"Even if I wanted to answer you, my throat doesn't want to move."

"I can barely breathe for some reason."

c "Valerius, open your eyes! Look at me!"

"What?"

"I'm... I'm on the ground."

"Clodia is holding me."

"Clodia."

"Please stop crying. I don't want you to be sad."

c "Valerius... you're here."

v "..."

"I don't want..."

jump ending_clodia_mourn

label ending_clodia_mourn:
    
centered "His eyes don't see me."

centered "His breathing is shallow."

centered "Blood runs from his lips."

centered "Tears fill his red eyes."

centered "He's so beautiful. My Valerius."

centered "My Valerius..."

centered "..."

centered "..."
 
centered "My Valerius is dead."

jump clodia_mourn_epilogue

label clodia_mourn_epilogue:
    
centered "\"Death and Love are man and wife. {p} They bring out the best in each other.\" {p}- Laerkes, famous Durjan wit."

centered "THE END"

return

label finish_him:
    
c "Husband."

sj "Hm?"

"Emerging from the enemy line, Clodia does not look at me."

"Her eyes are entirely on Sejanus."

"The word she used for him makes my stomach turn."

"And why did she say it so... sweetly?"

"The cup in her hand is..."

c "Have a drink. You look thirsty."

sj "Ha. Thank you, my dear. You see, you really have married the best soldier in all Raskya."

c "Indeed I have."

"He takes the cup from her hand, and without a second thought, drinks a few large gulps."

"He's already drunk on victory, I suppose he sees no harm in adding to that rush."

"In fact, he simply tosses the empty goblet to the floor when he's done with it."

sj "Thank you, wife. Now go away would you, I have business to finish."

"Some of his men grab her and escort her out of my sight. Damn it, I need to-"

"Suddenly, a manic soldier comes running into the temple behind us."
    
so "They're inside! They're coming!"
                         
"And looking past them, I can already see the purple clad veterans marching straight towards us."
                    
"The men near me start screaming."

"I turn and look at Sejanus."

"He's smiling at the new arrivals as he readies his sword to drive it into Sergius's neck."

"And then he freezes."

"Looks perplexed."

"Coughs red mist."

"His eyes search the floor for a moment, settle on the empty cup on the floor."

sj "Oh."

"And then-"

"With a mighty roar, Sergius moves at last. He twists and grabs the stunned Sejanus's sword arm."

"There's a moment that hangs in time. Sejanus tries to pull his grip free, almost comically."

"With his bare hand, Sergius grabs Sejanus by the throat. He lifts him like a doll, and with a terrifyingly violent surge of his arm, throws him against the nearest wall."

"The crack that echoes through the room is unmistakable."

"Sejanus's head bounces off the wall, his neck snapping this way and back. And with a sort of confused look on his face, he sinks to his knees."

"He stares at the floor like it's some strange, alien thing as blood pours out of his mouth, his nose, his eyes."

"At that moment, the Immortal Legion reaches our rear."

"Chaos."

"Pure chaos erupts."

"Clodia."

"I run."

"Men are charging in every direction."

"Javelins are flying, glancing off shields, striking the walls, the floors, shields, men. Nothing is being aimed, everyone simply panics."

"Clodia."

"Some of the men in their fear have turned and begun to fight the Immortal Legion."

"Most simply run screaming at the nearest enemy, swinging their weapon, all discipline gone."

"Clodia."

"I run through the mess. Through the riot."

"I run towards the last place I saw her."

"I'm running right at armed men but I don't care. I slam into them, smashing my shield with all the force I can possibly muster into the small gap between two of them."

"I practically leap off my feet to add to the strength of my hit."

"And somehow, the force breaks me through their line."

"I go sailing between them and smash hard to the tile. My armor rattles my whole body, but all I know is that I got behind them."

"And there's-"

v "Clodia!"

c "Valerius!"

"I clamor to my feet."

"Something hits me from behind, but I'm already running."

"I grab her."

"No time to think. There's nowhere to go, no other rooms."

"I push her into the nearest corner."

"I get one look at her face. Our eyes meet."

"Then I turn, raise my shield, and protect her."

"Javelins."

"They start pounding the shield, sticking into it. My bones shake with each missile, one after the other."

c "Valerius!"

"I have to hold firm."

"Someone is attacking us, someone is beating at my shield. I can't look, I just hold on as tight and hard as I can."

"Two men attacking us now. Hacking and stabbing and glancing off the shield, each blow surging through me."

"I cry out, screaming all the energy I can muster."

"I have to keep Clodia safe."

"My leg. Something hit my legs but I can't fall."

"Something hits my arm but I can't let go of the shield."

"My heart feels like it's going to explode but I can't die. Not yet."

v "Clodia!"

"Screaming her name helps me stand."

"Her name gives me the power to do what I must."

"And then-"

"It stops."

c "Valerius."

"I can't let go of the shield. I can't fall. Can't look."

"Have to keep her safe."

"Why has the fighting stopped?"

c "Valerius."

"Hearing her voice in the silence finally breaks me out of my trance."

"And all the strength leaves me. The shield falls away, and I sink into my beloved's arms."

"She weeps. We both do."

"I smell her hair. Blood blossoms."

"We found each other again."

"Soldiers fill the room. Purple clad legionaries. Immortals."

"The floor is a mess of red clad corpses. What few of Sergius's men are left are speared one by one by the methodical purple veterans."

"They're making their way towards us."

v "Clodia, I'm sorry."

c "Don't be. You're here."

"Her arms tighten around me."

c "I love you."

v "I love you too."

a "Oh great Ghosts, it smells terrible in here!"

"The speaker is..."

"Oh."

"Here's here."

a "And is that what's left of Sejanus? Is there anyone of authority still alive to explain what the hell happened here?"

a "You, tribune. Identify yourself."

v "Ah... uh, Valerius Aureus. Your Majesty."

c "That's the Basileus?"

"Yeah. And please don't sound so surprised, I'm sure he won't like that."

a "Who's tribune are you?"

v "..."

a "Don't know which is safe to say, huh? Whatever."

a "Tell me what happened here. And be honest, or I'll have you killed on the spot."

"I still have to get over myself. I'm so confused that I'm not dead at the end of an immortal spear right now."

v "There... there was single combat between Sergius and Sejanus. Sergius... well, he won, I think."

a "He sure doesn't look like a winner to me. Neither of them do."

a "Hm? Who's the girl?"

c "Uh... Praetrix Clodia sir."

a "Oh dear. This was your husband then?"

c "For a short time."

a "Well. Sorry for your loss I guess."

"He's nineteen, I think? Strange to say there's been even younger kings of Raskya, but I never met any of them."

"Actually, to even see the Basileus in person is a great honor bestowed among very few. Even on campaign the Basileus tends to stay in his tent."

"Exactly why are we receiving this honor?"

a "Hm..."

"The Basileus stands over the two rivals, fallen next to one another. One of them is still breathing."

"Blood pools around him in a black mirror and his skin has gone white, but Sergius the Great yet lives."

a "Gaius Argus Sergius, is it true that you engaged the legatus Sejanus in single combat?"

"I'm not sure if Sergius can even speak, but after a long moment-"

s "... yes..."

a "And is it true that you defeated him?"

s "Yes."

a "Very well. Then there is only one task that remains to you."

"Sergius turns his neck with pained effort to stare at his enemy, the two side by side like any other pair of corpses."

a "Strip him of the Grand Spoils."

"An awful, grating sound comes from Sergius as he struggles to rise. His neck can barely hold his head up."

"His shallow breathing quickens as his arm pulls him across the floor with the speed of a snail."
 
"There's no energy left in this man, he can't do it. Can't complete the final task of his greatest dream."

a "Ghosts, what a disappointment. So much for this historical moment."

"He speaks as though his favorite chariot just lost a championship race. As though there's always next year."

"And then..."

"Sejanus stirs."

sj "... help..."

a "What's that? You're alive, by the Grave, how unfortunate for you."

sj "I can't see anything... I don't feel my... what's going on?"

a "And neither of you lost anyway. This just grows more and more boring by the second. What a disappointment you both are."

sj "Where... where is Sergius? Is he dead?"

"Sergius is right before you. The expression on his face as he stares at Sejanus is poisonous. A dead man's face that somehow yet lives."

a "Hmm... Valerius, was it?"

v "Yes Your Holiness."

a "You said you're one of Sergius's tribunes. Where are the other five?"

"Before I can even answer, one of the young monarch's guards whispers in his ear."

a "Oh, dead down below, okay. Well, perfect, that means you're the legatus if anything happens to him."

"Before I can understand what he means, the Basileus casually gestures with his thumb towards Sergius."

"A second later, three Immortals draw their swords and plunge them into gaps in Sergius's armor."

"The commander rattles once, and then finally falls still."

"Clodia and I hold each other tighter as the men withdraw from the body of the general."

a "Well there we are. Valerius, you are now the legatus of your Legion."

a "Now do me a favor: kill your enemy commander and claim the Grand Spoils already. My scribe here is growing impatient to record this historic event."

"A slave with a quill and fresh papyrus stands at the ready, staring at me expectantly."

"I'm not sure I'm hearing the Basileus right. This is... none of this is what I thought would be happening today."

"Am I in a dream? Have I not woke yet on the morning of the attack?"

c "Do it, love."

"She squeezes my hand, looks into my eyes."

v "Very well."

"This is no single combat, I don't know what the Basileus is on about."

"But it's no great crime either, not after what this man's hubris has put us all through."

"I stand over him, blade in my hand. It's crimson with all the men I had to tear into pieces just to get here."

"Here where I never thought I would be."

"I hang the point of the sword over his empty eyes."

sj "Avalaran? I know you're there. We had a deal. Save me."

"I look at the Basileus. He only shrugs."

a "I just want the gold. Sorry."

sj "... of course."

scene black

"..."

"..."

"..."

a "Hurry up with it."

v "Yes your Majesty."

"Drawing my sword from his brain and returning it to the scabbard, I squat down and begin to strip Sejanus's body of its armor."

"Of its weapons. Of its Praetor's toga."

"The Ghosts of Victory are swirling around me. I don't feel anything, I don't sense them, but that's what's supposed to be happening right now as I complete this ritual."

"I'm doing something that only two Raskyans have ever done, and one of them was Raskyalus himself."

"I am going to be remembered as a Great Raskyan for this. One of the greatest."

"..."

"..."

"..."

"Why are the Ghosts doing this to me?"

jump ending_together

label ending_together:
    
scene black
with fade

"Down below, from this great glass window, I can see a city that's returning to some kind of normality."

scene ClodiaRoomNight
with fade

"Firelight glows from windows and people mill about in the streets, completing their evening tasks, going home."

"You can't tell those same streets were rivers of blood just a week ago. Capital slaves work fast."

"I suppose they had to, so that the Basileus could leave as soon as possible with them in tow."

"Ah, my wife has returned from her visit to the city. At least I can take some joy in those words, 'my wife'."

c "There's my Praetor."

v "As though this was the last place you would look?"

c "Haha. It was actually the first. It's been a busy day."

v "No trouble among the people?"

c "No, they seem to like me quite well."

v "You're quite likable."

c "Aww, why thank you dear. You're quite likable too, why haven't you been out being liked?"

v "Mmm, I've been too busy figuring out exactly what it is I'm supposed to do. All these slaves trying to teach me how to be a proper noble is exhausting."

v "Apparently I don't walk the \"correct\" way."

c "Haha, well, you don't have to do anything you don't want to do. You're the Praetor, so I say walk how you'd like."

v "Hmm. Praetor. It's bizarre."

v "Praetors are important, capable men. At least they've always seemed that way to me."

c "I promise you, most nobles are morons. There's no actual requirements for these jobs other than being born correctly, after all."

c "Even my uncle, well meaning as he was, could be a tremendous twit like any of them."

c "I would think your brown blood would be an advantage. You actually understand something about the people you'll be ruling."

v "Do I? I was a Raskyan farmer, not a Dagaran city peasant."

c "Those two have more in common with each other than either do with nobility."

v "Perhaps. It's just..."

c "What, love?"

"Go ahead and say it. If you can't say it to your own wife then what is the point?"

v "I... didn't want this. I wanted us to run away together, to Durja. Or Peladon."

v "Hell, I would have preferred the wastes of Zaratia to this. Anywhere but somewhere controlled by Raskya."

v "When I see legionaries patrolling the streets of my home, I don't care that they're my legionaries. I never wanted to see another red Raskyan ever again."

v "And the Ghosts, laughing all the while, have seen fit to make me one of the greatest Raskyans ever. Third ever to win the Grand Spoils!"

v "Who cares that it was set up by our boy Basileus? I'm in a class with Raskyalus himself now."

v "Ghosts damn him, and damn me."

"Now how will she react to that? That our marriage has imprisoned me?"

"A long silence passes. I'm almost afraid to turn and look at her."

"But just as I summon the courage, I feel her hand on my back."

c "I wondered. You haven't worn the Praetor's toga once since claiming it."

v "It's a funeral shroud as far as I care. I killed the last person to wear it, after all."

c "Can I tell you something? I used to feel the way you do now."

v "Exactly when were you a brownblood?"

c "Not that. I mean becoming a ruler. I was never going to be Praetrix, didn't want to be."

c "When Sejanus changed everything and made it my fate, I was horrified. If I didn't have the hope of fleeing with you to cling to, I don't know what I would have done to avoid the responsibility."

v "... and now you're all fine with it? What changed?"

c "Tell me love, what do you hate about Raskya so much? What makes you feel apart from your own people?"

"Ghosts, I may need a minute to make a list."

"But in general..."

v "The people. When I read stories of the other people of this world, they all seem so much better."

v "The Peladonians value fairness and hard work. The Durjans value love and tenderness. Even the Dagarans here are honest and just."

v "Give it a few years though, and I'm sure they'll become just as rotten as any other Raskyan."

v "Raskyans only value violence and bloodshed and conquest. They steal and they kill and they rape and they find it all so very amusing."

v "And now I'm to lead a piece of that wretchedness. Ghosts help me."

c "Well... I can't really disagree with you. But you're overlooking something, darling. And you said it yourself."

v "What?"

c "You're to lead a piece of that wretchedness. You have power over it. And power means you can change things."

v "Change what, exactly? I can change laws, I can't change mens' minds."

c "No. But you can be clever about it. Change a law here and there. Incentivize different behavior. Promote better ideas."

c "Trust me, as long as the people aren't hungry, they'll assume you're the smartest man in the city and obey your wishes."

v "... and if by some miracle I can make things different this way... I only have power over Dagara. The rest of Raskya will ignore us."

v "Worse, if the Basileus doesn't like the changes, he can simply override them all."

c "And so the key is subtlety. And patience."

c "It won't be fast, but if you change the bricks in a wall one at a time, eventually, you have a different wall than what you started with."

v "Maybe..."

"Maybe she's right. Clever Clodia, what would I do without you?"

v "I'm not sure where I would start."

c "We have time and power to figure it out, my love. Together."

v "Together... yes."

"Turning to take her in my arms, I kiss my wife."

c "Mmm... you know... we haven't..."

v "Yes. Ridiculous, isn't it?"

"There hasn't been a moment to breathe since the siege ended. Everything has been work, work, work."

"The clean up, the Basileus's inspections, and mostly, my having to learn how to do any of my Praetor's duties."

"Even our wedding was short, the Basileus quickly handwaving our marriage into existence, then taking me aside for business without leaving me any time with my new wife."

v "Let's fix that now, shall we?"

c "Yes. Come to bed, my love."

scene black
with fade

"I lose myself in her."

"I missed this. This heaven. Her little sounds, her breaths."

"The look in her eyes, hungry. The taste of her mouth, and her breasts, and between her legs."

"Her fingers in my hair, and on my back."

"This is what I did all this for. To be like this with her."

"Afterward, cradled in her arms, I breathe easy for the first time in a year."

c "How do you feel now, my handsome Praetor?"

v "Better. Much better. I missed you."

c "I missed you too. Everything's going to be okay now."

v "Yes. I actually believe it, somehow."

c "There's an old Durjan poem about this, actually."

v "Oh?"

c "\"The king has a heart that is burdened with power, and a mind that in pain does wallow; {p}The queen saves the realm by soothing his body, for the mind and the heart always follow.\""

v "That's a good one. A little antiquated wording, but there's truth in it. I feel it right now."

c "It works the other way around too, I feel like we must have died and gone up into the Air."

v "Thank the Ghosts of Fucking."

c "I should sacrifice a white bull to the Ghosts of Fucking."

v "Ha ha. The priestesses would go into fits I expect."

c "Oh my, you have no idea. No one has less of a concept of fun than a Priestess of the Ghosts."

v "Do we have the power to make it legal for them to marry?"

c "If this were just Dagara, no. Uncle Fausus never dared to step on the Pontifex's toes, since that tends to anger the commoners."

c "But since Raskya is in charge, well, who knows?"

v "I suspect as long as that boy of a Basileus is fine with it, we can do whatever we want."

c "All that gold means that we have his ear, at least for a little bit."

v "We should do something with it then."

c "In time. We can discuss work tomorrow. Tonight... I just want you to fuck me and hold me."

v "Mmm, as you wish my Praetrix."

"I oblige her."

"Our bodies entwine in the heaven we fought for. The world is gone as the Ghosts of Love and Lust dance around us, blessing us."

"Silence rules after the ceremony."

"Silence and the rustling of skin on sheets, the breathing of lungs and the quiet beating of hearts."

"My eyes grow heavy. Sleep is coming. Maybe that's why I can see this vision clearly now."

"It hangs before me as vividly as a dream. A dream I can make come true."

v "Thank you, Clodia. You're right."

c "About..."

v "The power we wield. What we can do with it."

v "These powers won't shackle us to be like other Raskyans. These powers will free us, and others, from the whole awful Raskyan way of thinking."

v "Brick by brick, law by law, we can change things."

c "Together."

v "At last. Good night, sweet Clodia. I love you."

c "And I love you, my Valerius."

scene black
with fade

jump epilogue_together

label epilogue_together:
    
scene black
with fade

centered "\"The reign of Valerius and Clodia, though not without its faults, is usually held up as a kind of golden age for Raskyan Dagara. {p} As it was to be nearly a thousand years before Dagara regained its full independance, there was plenty of time for the province to become \"Raskyanized\", for better and for worse. But in the process, Raskya became somewhat \"Dagaranized\", as innovations introduced by the Praetor and Praetrix became fashionable throughout the entire Republican Kingdom. {p}Elements of culture that today seem utterly Raskyan such as compassion for the poor, architectural ingenuity, and vigorous diplomacy before the last resort of war, can all be traced back to the inital legal reforms of these two rulers to their tiny province. Over the course of their nearly fifty year reign, Valerius and Clodia increased the standard of living for Dagaran peasants, expanded the rights of slaves, and, as one historian put it, \"provided the basis by which the whole nature of the Raskyan beast would change in the coming centuries, from that of a bloody and vengeful wildcat to a patient and noble lion, if one that still delights in showing off its claws.\" {p}It is by these results that Valerius and Clodia are celebrated to this day as culture heroes for Raskya. Their love story, told countless times in plays, novels, books, music, films, and even a computer game, is not only the foundation of the Raskyan concept of sexual and marital love, but love towards one's fellow citizen, and indeed, humanity itself.\" {p}- Antono Barus, from his \"A Modern History of Ancient Raskya\""

centered "THE END"

return